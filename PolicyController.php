<?php

namespace jick\col\controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use JWTAuth;
use Validator;

use jick\col\models\ColEntity;
use jick\col\models\ColPolicy;

class PolicyController extends Controller
{

    function getPolicyList()
    {
        $entity = $this->getEntity();//dd($entity);

        if(is_null($entity)) {
            return [0 => 'not found'];
        }

        $entityId = $entity->entityid;

        $WSDL = 'http://192.168.0.216:8813/Actisure/ASWSCOL?singleWsdl';
        $SACTION = 'http://services.activus.com/actcol/servicecontracts/2010/05/ActCOLServiceContract/Act_GetEntityPolicyList';
        $DEFAULT_WSA_TO = 'http://192.168.0.216:8813/Actisure/ASWSCOL';

        $params = array(
            'wsdl' => $WSDL,
            'action' => $SACTION
        );

        $client = new \Jick\col\SoapManager($params);

        $maxLimit = 10;
        $PolicyId = 17226;

        $response = $client->request('<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://services.activus.com/actcol/messagecontracts/2010/05" xmlns:ns1="http://services.activus.com/common/datacontracts/2010/02" xmlns:ns2="http://services.activus.com/actcol/datacontracts/2010/05">


        <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
            <wsa:Action>'.$SACTION.'</wsa:Action>
            <wsa:To>'.$DEFAULT_WSA_TO.'</wsa:To>
        </soap:Header>
        <soap:Body>
            <ns:Act_GetEntityPolicyListRequestMSG>
                <!--Optional:-->
                <ns:GetEntityDetailsRequest>
                    <!--Optional:-->
                    <ns2:ActisureEntity>'.$entityId.'</ns2:ActisureEntity>
                    <!--Optional:-->
                    <ns2:ReturnNonBeneficiaryPolicyholderPolicies>1</ns2:ReturnNonBeneficiaryPolicyholderPolicies>
                </ns:GetEntityDetailsRequest>
            </ns:Act_GetEntityPolicyListRequestMSG>
        </soap:Body>
        </soap:Envelope>');

        //dd($response);

        $result = $response['Act_GetEntityPolicyListResponseMSG']['GetEntityPolicyListResp']['bActServiceResult']['cSuccess'];//['bActClaimsAssessmentsList']['bAct_ClaimsAssessment'];
        //return $result;
        if($result === 'false')
        {
            $error = $response['Act_GetEntityPolicyListResponseMSG']['GetEntityPolicyListResp']['bActDISBLLErrors']['cActisureDISBLLError'];
            return response()->json(['message' => 'There was an error',
                                        'error' => 'An error -'.$error["cDescription"].' -has occured'] );
        }
        else{

            $policies = $response['Act_GetEntityPolicyListResponseMSG']['GetEntityPolicyListResp']['bActEntityPolicySummaryList']['bAct_AccountSummary'];

            $policies = array_values(array_sort($policies, function ($value) {
                return $value['bPolicyEffectiveDate'];
            }));

            return $policies;//response()->json($policies);
        }

    }

    function getPolicyExclusions()
    {
        $entity = $this->getEntity();

        if(is_null($entity)) {
            return response()->json(['message' => 'Entity not found']);
        }

        $entityId = $entity->entityid;

        $policy_list = $this->getPolicyList();

        //Check if the entity was found from the get policies method
        if($policy_list[0] == 'not found')
        {
            return response()->json(['message' => "The entity for those details is not found"]);
        }

        $latest_policy_list = $policy_list[count($policy_list)-1]; //return $latest_policy_list;

        //$curr_policy_id = $latest_policy_list['bPolicyID'];
        $curr_policy_id = $latest_policy_list['bPolicyDescription'] == 'Corporate' ? $latest_policy_list['bEmployeeCategoryPolicyID'] : $latest_policy_list['bPolicyID'];
        $pol_effective_date = $latest_policy_list['bPolicyEffectiveDate'];

        $WSDL = 'http://192.168.0.216:8813/Actisure/ASWSCOL?singleWsdl';
        $SACTION = 'http://services.activus.com/actcol/servicecontracts/2010/05/ActCOLServiceContract/Act_GetPolicyExclusions';
        $DEFAULT_WSA_TO = 'http://192.168.0.216:8813/Actisure/ASWSCOL';

        $params = array(
            'wsdl' => $WSDL,
            'action' => $SACTION
        );

        $client = new \Jick\col\SoapManager($params);

        $maxLimit = 10;
        $PolicyId = 17226;

        $response = $client->request('<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://services.activus.com/actcol/messagecontracts/2010/05" xmlns:ns1="http://services.activus.com/common/datacontracts/2010/02" xmlns:ns2="http://services.activus.com/actcol/datacontracts/2010/05">


        <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
            <wsa:Action>'.$SACTION.'</wsa:Action>
            <wsa:To>'.$DEFAULT_WSA_TO.'</wsa:To>
        </soap:Header>
        <soap:Body>
            <ns:Act_GetPolicyExclusionsRequestMSG>
                <!--Optional:-->
                <ns:GetPolicyExclusions>
                    <!--Optional:-->
                    <ns2:ActisurePolicy>'.$curr_policy_id.'</ns2:ActisurePolicy>
                    <!--Optional:-->
                    <ns2:ActisurePolicyEffectiveDate>'.$pol_effective_date.'</ns2:ActisurePolicyEffectiveDate>
                </ns:GetPolicyExclusions>
            </ns:Act_GetPolicyExclusionsRequestMSG>
        </soap:Body>
        </soap:Envelope>');

        $exclusions = $response['Act_GetPolicyExclusionsResponseMSG']['GetPolicyExclusionsResp'];

        //check if there is an error and return no exclusions
        if($exclusions['bActDISBLLErrors']['cActisureDISBLLError']['cType'] == 'Error')
        {
            return ['message' => 'This policy has no exclusions'];
        }
        else{
             return $exclusions['bActPolicyPolicyExclusions'];
        }



    }

    private function getEntity()
    {
        $user = JWTAuth::parseToken()->authenticate(); //dd(ColEntity::all());
        $entity = ColEntity::join('MIDBPOLICYBENEFICIARY', 'MIDBPOLICYBENEFICIARY.ENTITYID=MIDBENTITYPHONE.ENTITYID')
                    ->where(['PHONENUMBER' => $user->phone,
                        'RELATIONSHIP' => 'Policy Holder'
                    ])
                    ->orWhere(['PHONENUMBER' => $user->phone,
                        'RELATIONSHIP' => 'Company Employee'
                    ])->first();

        return $entity;
    }

    function getPersonalDetails()
    {
        $entity = $this->getEntity();//dd($entity);

        if(is_null($entity)) {
            return response()->json(['message' => 'Entity not found']);
        }

        $entityId = $entity->entityid;

        $WSDL = 'http://192.168.0.216:8813/Actisure/ASWSCOL?singleWsdl';
        $SACTION = 'http://services.activus.com/actcol/servicecontracts/2010/05/ActCOLServiceContract/Act_GetEntityDetails';
        $DEFAULT_WSA_TO = 'http://192.168.0.216:8813/Actisure/ASWSCOL';

        $params = array(
            'wsdl' => $WSDL,
            'action' => $SACTION
        );

        $client = new \Jick\col\SoapManager($params);

        $response = $client->request('<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://services.activus.com/actcol/messagecontracts/2010/05" xmlns:ns1="http://services.activus.com/common/datacontracts/2010/02" xmlns:ns2="http://services.activus.com/actcol/datacontracts/2010/05">
        <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
            <wsa:Action>'.$SACTION.'</wsa:Action>
            <wsa:To>'.$DEFAULT_WSA_TO.'</wsa:To>
        </soap:Header>
        <soap:Body>
            <ns:Act_GetEntityDetailsRequestMSG>
                <!--Optional:-->
                <ns:GetEntityDetailsRequest>
                    <!--Optional:-->
                    <ns2:ActisureEntity>'.$entityId.'</ns2:ActisureEntity>
                </ns:GetEntityDetailsRequest>
            </ns:Act_GetEntityDetailsRequestMSG>
        </soap:Body>
        </soap:Envelope>');

         $result = $response['Act_GetEntityDetailsResponseMSG']['GetEntityDetailsResp']['bActServiceResult']['cSuccess'];//['bActClaimsAssessmentsList']['bAct_ClaimsAssessment'];
        //return $result;
        if($result === 'false')
        {
            $error = $response['Act_GetEntityDetailsResponseMSG']['GetEntityDetailsResp']['bActDISBLLErrors']['cActisureDISBLLError'];
            return response()->json(['message' => 'There was an error',
                                        'error' => 'An error -'.$error["cDescription"].' -has occured'] );
        }
        else{

            $policy_list = $this->getPolicyList();

            //Check if the entity was found from the get policies method
        if($policy_list[0] == 'not found')
        {
            return response()->json(['message' => "The entity for those details is not found"]);
        }

            $latest_policy_list = $policy_list[count($policy_list)-1];

            $personal_details = $response['Act_GetEntityDetailsResponseMSG']['GetEntityDetailsResp']['bActEntity'];

            $all_personal_details = array(
                'policy_list' => $latest_policy_list,
                'personal_details' => $personal_details
            );

            return $all_personal_details;
        }
    }

    function getClaimAssessment(Request $request)
    {
        $entity = $this->getEntity();

        $policy_list = $this->getPolicyList();

        //Check if the entity was found from the get policies method
        if($policy_list[0] == 'not found')
        {
            return response()->json(['message' => "The entity for those details is not found"]);
        }

        $latest_policy_list = $policy_list[count($policy_list)-1]; //return $latest_policy_list;

        //$curr_policy_id = $latest_policy_list['bPolicyID'];
        $curr_policy_id = $latest_policy_list['bPolicyDescription'] == 'Corporate' ? $latest_policy_list['bEmployeeCategoryPolicyID'] : $latest_policy_list['bPolicyID'];

        if(is_null($entity)) {
            return response()->json(['message' => 'Entity not found']);
        }


        $entityId = $entity->entityid;//dd($entityId);
        $policy_id = $request->policy_id;

        $WSDL = 'http://192.168.0.216:8813/Actisure/ASWSCOL?singleWsdl';
        $SACTION = 'http://services.activus.com/actcol/servicecontracts/2010/05/ActCOLServiceContract/Act_GetClaimsAssessments';
        $DEFAULT_WSA_TO = 'http://192.168.0.216:8813/Actisure/ASWSCOL';

        $params = array(
            'wsdl' => $WSDL,
            'action' => $SACTION
        );

        $client = new \Jick\col\SoapManager($params);

        $response = $client->request('<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://services.activus.com/actcol/messagecontracts/2010/05" xmlns:ns1="http://services.activus.com/common/datacontracts/2010/02" xmlns:ns2="http://services.activus.com/actcol/datacontracts/2010/05">
        <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
            <wsa:Action>'.$SACTION.'</wsa:Action>
            <wsa:To>'.$DEFAULT_WSA_TO.'</wsa:To>
        </soap:Header>
        <soap:Body>
            <ns:Act_GetClaimsAssessmentsRequestMSG>
                <!--Optional:-->
                <ns:GetClaimsAssessments>
                    <!--Optional:-->
                    <ns2:ActisureEntity>'.$entityId.'</ns2:ActisureEntity>
                    <!--Optional:-->
                    <ns2:ActisurePolicy>'.$curr_policy_id.'</ns2:ActisurePolicy>
                </ns:GetClaimsAssessments>
            </ns:Act_GetClaimsAssessmentsRequestMSG>
        </soap:Body>
        </soap:Envelope>');

        $result = $response['Act_GetClaimsAssessmentsResponseMSG']['GetClaimsAssessmentsResp']['bActServiceResult']['cSuccess'];//['bActClaimsAssessmentsList']['bAct_ClaimsAssessment'];
        //return $result;
        if($result === 'false')
        {
            $error = $response['Act_GetClaimsAssessmentsResponseMSG']['GetClaimsAssessmentsResp']['bActDISBLLErrors']['cActisureDISBLLError'];
            return response()->json(['message' => 'There was an error',
                                        'error' => 'An error -'.$error["cDescription"].' -has occured'] );
        }
        else{
            return $response['Act_GetClaimsAssessmentsResponseMSG']['GetClaimsAssessmentsResp']['bActClaimsAssessmentsList']['bAct_ClaimsAssessment'];
        }
    }


    //Function to display all the claims for a user
    function getClaims()
    {
        $entity = $this->getEntity();//dd($entity);

        $policy_list = $this->getPolicyList();

       //Check if the entity was found from the get policies method
        if($policy_list[0] == 'not found')
        {
            return response()->json(['message' => "The entity for those details is not found"]);
        }

        $latest_policy_list = $policy_list[count($policy_list)-1]; //return $latest_policy_list;

        //$curr_policy_id = $latest_policy_list['bPolicyID'];
        $curr_policy_id = $latest_policy_list['bPolicyDescription'] == 'Corporate' ? $latest_policy_list['bEmployeeCategoryPolicyID'] : $latest_policy_list['bPolicyID'];

        if(is_null($entity)) {
            return response()->json(['message' => 'Entity not found']);
        }


        $entityId = $entity->entityid;//dd($entityId);

        $WSDL = 'http://192.168.0.216:8813/Actisure/ASWSCOL?singleWsdl';
        $SACTION = 'http://services.activus.com/actcol/servicecontracts/2010/05/ActCOLServiceContract/Act_GetEntityPolicyAssessments';
        $DEFAULT_WSA_TO = 'http://192.168.0.216:8813/Actisure/ASWSCOL';

        $params = array(
            'wsdl' => $WSDL,
            'action' => $SACTION
        );

        $client = new \Jick\col\SoapManager($params);

        $response = $client->request('<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://services.activus.com/actcol/messagecontracts/2010/05" xmlns:ns1="http://services.activus.com/common/datacontracts/2010/02" xmlns:ns2="http://services.activus.com/actcol/datacontracts/2010/05">
        <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
            <wsa:Action>'.$SACTION.'</wsa:Action>
            <wsa:To>'.$DEFAULT_WSA_TO.'</wsa:To>
        </soap:Header>
        <soap:Body>
            <ns:Act_GetEntityPolicyAssessmentsRequestMsg>
                <!--Optional:-->
                <ns:GetEntityPolicyAssessmentsRequest>
                    <ns2:PolicyId>'.$curr_policy_id.'</ns2:PolicyId>
                    <ns2:EntityId>'.$entityId.'</ns2:EntityId>
                </ns:GetEntityPolicyAssessmentsRequest>
            </ns:Act_GetEntityPolicyAssessmentsRequestMsg>
        </soap:Body>
        </soap:Envelope>');

        $result = $response['Act_GetEntityPolicyAssessmentsResponseMsg']['GetEntityPolicyAssessmentsResponse']['bActServiceResult']['cSuccess'];//['bActClaimsAssessmentsList']['bAct_ClaimsAssessment'];
        //return $result;

        if($result === 'false')
        {
            $error = $response['Act_GetEntityPolicyAssessmentsResponseMsg']['GetEntityPolicyAssessmentsResponse']['bActDISBLLErrors']['cActisureDISBLLError'];
            return response()->json(['message' => 'There was an error',
                                        'error' => 'An error -'.$error["cDescription"].' -has occured'] );
        }
        else{
            return $response['Act_GetEntityPolicyAssessmentsResponseMsg']['GetEntityPolicyAssessmentsResponse']['bPolicyAssessments']['bAct_ClaimsAssessment'];
        }
    }


    //Function to display the reports
    function getClaimReports(Request $request)
    {
        $entity = $this->getEntity();

        $policy_list = $this->getPolicyList();//dd($policy_list);

        //Check if the entity was found from the get policies method
        if($policy_list[0] == 'not found')
        {
            return response()->json(['message' => "The entity for those details is not found"]);
        }

        $latest_policy_list = $policy_list[count($policy_list)-1]; //return $latest_policy_list;

        //$curr_policy_id = $latest_policy_list['bPolicyID'];
        $curr_policy_id = $latest_policy_list['bPolicyDescription'] == 'Corporate' ? $latest_policy_list['bEmployeeCategoryPolicyID'] : $latest_policy_list['bPolicyID'];

        // $curr_policy_id = 15465;////$latest_policy_list['bPolicyDescription'] == 'Corporate' ? $latest_policy_list['bEmployeeCategoryPolicyID'] : $latest_policy_list['bPolicyID'];

        $pol_effective_date = $latest_policy_list['bPolicyEffectiveDate'];
        // $pol_effective_date = '2016-05-01T00:00:00';//$latest_policy_list['bPolicyEffectiveDate'];

        if(is_null($entity)) {
            return response()->json(['message' => 'Entity not found']);
        }


        $entityId = $request->entity_id ? $request->entity_id : $entity->entityid;//dd($pol_effective_date);
        // $entityId = 1178315;//$request->entity_id ? $request->entity_id : $entity->entityid;

        $WSDL = 'http://192.168.0.216:8813/Actisure/ASWSCOL?singleWsdl';
        $SACTION = 'http://services.activus.com/actcol/servicecontracts/2010/05/ActCOLServiceContract/Act_GetBeneficiaryBenefitUsageList';
        $DEFAULT_WSA_TO = 'http://192.168.0.216:8813/Actisure/ASWSCOL';

        $params = array(
            'wsdl' => $WSDL,
            'action' => $SACTION
        );

        $client = new \Jick\col\SoapManager($params);//dd($pol_effective_date);

        $response = $client->request('<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://services.activus.com/actcol/messagecontracts/2010/05" xmlns:ns1="http://services.activus.com/common/datacontracts/2010/02" xmlns:ns2="http://services.activus.com/actcol/datacontracts/2010/05">
        <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
            <wsa:Action>'.$SACTION.'</wsa:Action>
            <wsa:To>'.$DEFAULT_WSA_TO.'</wsa:To>
        </soap:Header>
        <soap:Body>
            <ns:Act_GetBeneficiaryBenefitUsageListRequestMsg>
                    <!--Optional:-->
                    <ns:BeneficiaryBenefitUsageListRequest>
                        <ns2:PolicyId>'.$curr_policy_id.'</ns2:PolicyId>
                        <!--Optional:-->
                        <ns2:PolicyEffectiveDate>'.$pol_effective_date.'</ns2:PolicyEffectiveDate>
                        <ns2:BeneficiaryEntityId>'.$entityId.'</ns2:BeneficiaryEntityId>
                        <!--Optional:-->
                    </ns:BeneficiaryBenefitUsageListRequest>
            </ns:Act_GetBeneficiaryBenefitUsageListRequestMsg>
        </soap:Body>
        </soap:Envelope>');

        //dd($response);
        $result = $response['Act_GetBeneficiaryBenefitUsageListResponseMsg']['GetBeneficiaryBenefitUsageListResponse']['bServiceResult']['cSuccess'];//['bActClaimsAssessmentsList']['bAct_ClaimsAssessment'];
        //return $result;

        if($result === 'false')
        {
            $error = $response['Act_GetBeneficiaryBenefitUsageListResponseMsg']['GetBeneficiaryBenefitUsageListResponse']['bBusinessErrors']['cActisureDISBLLError'];
            return response()->json(['message' => 'There was an error',
                                        'error' => $error["cDescription"] ]);
        }
        else{
            return $response['Act_GetBeneficiaryBenefitUsageListResponseMsg']['GetBeneficiaryBenefitUsageListResponse'];
        }
    }

    //A function to display policy getPersonalDetails
    function getPolicyDetails()
    {
        $policy_list = $this->getPolicyList();//dd($policy_list);

        //Check if the entity was found from the get policies method
        if($policy_list[0] == 'not found')
        {
            return response()->json(['message' => "The entity for those details is not found"]);
        }

        $latest_policy_list = $policy_list[count($policy_list)-1]; //return $latest_policy_list;

        $curr_policy_id = $latest_policy_list['bPolicyDescription'] == 'Corporate' ? $latest_policy_list['bEmployeeCategoryPolicyID'] : $latest_policy_list['bPolicyID'];
        $pol_effective_date = $latest_policy_list['bPolicyEffectiveDate'];

        $entity = $this->getEntity();

        if(is_null($entity)) {
            return response()->json(['message' => 'Entity not found']);
        }


        $entityId = $entity->entityid;
        $curr_year = date('Y');

        $WSDL = 'http://192.168.0.216:8813/Actisure/ASWSCOL?singleWsdl';
        $SACTION = 'http://services.activus.com/actcol/servicecontracts/2010/05/ActCOLServiceContract/Act_GetPolicyDetails';
        $DEFAULT_WSA_TO = 'http://192.168.0.216:8813/Actisure/ASWSCOL';

        $params = array(
            'wsdl' => $WSDL,
            'action' => $SACTION
        );

        $client = new \Jick\col\SoapManager($params);

        $response = $client->request('<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://services.activus.com/actcol/messagecontracts/2010/05" xmlns:ns1="http://services.activus.com/common/datacontracts/2010/02" xmlns:ns2="http://services.activus.com/actcol/datacontracts/2010/05">
        <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
            <wsa:Action>'.$SACTION.'</wsa:Action>
            <wsa:To>'.$DEFAULT_WSA_TO.'</wsa:To>
        </soap:Header>
        <soap:Body>
            <ns:Act_GetPolicyDetailsRequestMSG>
                <!--Optional:-->
                <ns:GetPolicyDetails>
                    <!--Optional:-->
                    <ns2:ActisurePolicy>'.$curr_policy_id.'</ns2:ActisurePolicy>
                    <!--Optional:-->
                    <ns2:ActisurePolicyEffectiveDate>'.$pol_effective_date.'</ns2:ActisurePolicyEffectiveDate>
                    <!--Optional:-->
                    <ns2:ActisureEntity>0</ns2:ActisureEntity>
                    <!--Optional:-->
                    <ns2:IgnoreBenefitUsage>0</ns2:IgnoreBenefitUsage>
                </ns:GetPolicyDetails>
            </ns:Act_GetPolicyDetailsRequestMSG>
        </soap:Body>
        </soap:Envelope>');

        $beneficiaries = $this->PolicyBeneficiaries($curr_policy_id,$pol_effective_date);//dd($beneficiaries);

        $exclusions = $latest_policy_list['bPolicyDescription'] == 'Corporate' ? '' : $this->getPolicyExclusions();

        $result = $response['Act_GetPolicyDetailsResponseMSG']['GetPolicyDetailsResp']['bActServiceResult']['cSuccess'];//['bActClaimsAssessmentsList']['bAct_ClaimsAssessment'];
        
        if($result === 'false')
        {
            $error = $response['Act_GetPolicyDetailsResponseMSG']['GetPolicyDetailsResp']['bActDISBLLErrors']['cActisureDISBLLError'];
            return response()->json(['message' => 'There was an error',
                                        'error' => $error["cDescription"] ]);
        }
        else{
            $policy_details = $response['Act_GetPolicyDetailsResponseMSG']['GetPolicyDetailsResp']['bActPolicyDetails'];//['bAct_ClaimsAssessment'];

            $output = array(
                'beneficiaries' => $beneficiaries,
                'policy_details' => $policy_details,
                'policy_exclusions' => $exclusions
            );

            return $output;
        }

    }

    //List beneficiaries to a given policy number_format
    function getPolicyBeneficiaries()
    {
        $policy_list = $this->getPolicyList();//dd($policy_list);

        //Check if the entity was found from the get policies method
        if($policy_list[0] == 'not found')
        {
            return response()->json(['message' => "The entity for those details is not found"]);
        }

        $latest_policy_list = $policy_list[count($policy_list)-1]; //return $latest_policy_list;

        //$curr_policy_id = $latest_policy_list['bPolicyID'];
        $curr_policy_id = $latest_policy_list['bPolicyDescription'] == 'Corporate' ? $latest_policy_list['bEmployeeCategoryPolicyID'] : $latest_policy_list['bPolicyID'];
        
        $pol_effective_date = $latest_policy_list['bPolicyEffectiveDate'];

        $entity = $this->getEntity();

        if(is_null($entity)) {
            return response()->json(['message' => 'Entity not found']);
        }


        $entityId = $entity->entityid;//dd($entityId);
        $curr_year = date('Y');

        $WSDL = 'http://192.168.0.216:8813/Actisure/ASWSCOL?singleWsdl';
        $SACTION = 'http://services.activus.com/actcol/servicecontracts/2010/05/ActCOLServiceContract/Act_GetPolicyBeneficiaries';
        $DEFAULT_WSA_TO = 'http://192.168.0.216:8813/Actisure/ASWSCOL';

        $params = array(
            'wsdl' => $WSDL,
            'action' => $SACTION
        );

        $client = new \Jick\col\SoapManager($params);

        $response = $client->request('<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://services.activus.com/actcol/messagecontracts/2010/05" xmlns:ns1="http://services.activus.com/common/datacontracts/2010/02" xmlns:ns2="http://services.activus.com/actcol/datacontracts/2010/05">
        <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
            <wsa:Action>'.$SACTION.'</wsa:Action>
            <wsa:To>'.$DEFAULT_WSA_TO.'</wsa:To>
        </soap:Header>
        <soap:Body>
             <ns:Act_GetPolicyBeneficiariesRequestMsg>
                <!--Optional:-->
                <ns:GetPolicyBeneficiariesReq>
                    <ns2:EntityId>'.$this->getEntity()->entityid.'</ns2:EntityId>
                    <ns2:PolicyId>'.$curr_policy_id.'</ns2:PolicyId>
                    <ns2:EffectiveDate>'.$pol_effective_date.'</ns2:EffectiveDate>
                </ns:GetPolicyBeneficiariesReq>
            </ns:Act_GetPolicyBeneficiariesRequestMsg>
        </soap:Body>
        </soap:Envelope>');

        $result = $response['Act_GetPolicyBeneficiariesResponseMsg']['GetPolicyBeneficiariesResp']['bActServiceResult']['cSuccess'];//['bActClaimsAssessmentsList']['bAct_ClaimsAssessment'];

        if($result === 'false')
        {
            $error = $response['Act_GetPolicyBeneficiariesResponseMsg']['GetPolicyBeneficiariesResp']['bActDISBLLErrors']['cActisureDISBLLError'];
            return response()->json(['message' => 'There was an error',
                                        'error' => 'An error -'.$error["cDescription"].' -has occured'] );
        }
        else{
            $beneficiaries = $response['Act_GetPolicyBeneficiariesResponseMsg']['GetPolicyBeneficiariesResp']['bBeneficiaries']['bAct_EntityPolicy'];

            return count($beneficiaries) === count($beneficiaries, COUNT_RECURSIVE) ? array($beneficiaries) : $beneficiaries;
        }
    }

     //List beneficiaries to a given policy number
    private function PolicyBeneficiaries($policy_id,$date)
    {
        $entity = $this->getEntity();

        if(is_null($entity)) {
            return response()->json(['message' => 'Entity not found']);
        }


        $entityId = $entity->entityid;
        $curr_year = date('Y');

        $WSDL = 'http://192.168.0.216:8813/Actisure/ASWSCOL?singleWsdl';
        $SACTION = 'http://services.activus.com/actcol/servicecontracts/2010/05/ActCOLServiceContract/Act_GetPolicyBeneficiaries';
        $DEFAULT_WSA_TO = 'http://192.168.0.216:8813/Actisure/ASWSCOL';

        $params = array(
            'wsdl' => $WSDL,
            'action' => $SACTION
        );

        $client = new \Jick\col\SoapManager($params);

        $response = $client->request('<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://services.activus.com/actcol/messagecontracts/2010/05" xmlns:ns1="http://services.activus.com/common/datacontracts/2010/02" xmlns:ns2="http://services.activus.com/actcol/datacontracts/2010/05">
        <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
            <wsa:Action>'.$SACTION.'</wsa:Action>
            <wsa:To>'.$DEFAULT_WSA_TO.'</wsa:To>
        </soap:Header>
        <soap:Body>
             <ns:Act_GetPolicyBeneficiariesRequestMsg>
                <!--Optional:-->
                <ns:GetPolicyBeneficiariesReq>
                    <ns2:EntityId>'.$this->getEntity()->entityid.'</ns2:EntityId>
                    <ns2:PolicyId>'.$policy_id.'</ns2:PolicyId>
                    <ns2:EffectiveDate>'.$date.'</ns2:EffectiveDate>
                </ns:GetPolicyBeneficiariesReq>
            </ns:Act_GetPolicyBeneficiariesRequestMsg>
        </soap:Body>
        </soap:Envelope>');

       $result = $response['Act_GetPolicyBeneficiariesResponseMsg']['GetPolicyBeneficiariesResp']['bActServiceResult']['cSuccess'];//['bActClaimsAssessmentsList']['bAct_ClaimsAssessment'];

        if($result === 'false')
        {
            $error = $response['Act_GetPolicyBeneficiariesResponseMsg']['GetPolicyBeneficiariesResp']['bActDISBLLErrors']['cActisureDISBLLError'];
            return response()->json(['message' => 'There was an error',
                                        'error' => $error["cDescription"]],422);
        }
        else{
            $beneficiaries = $response['Act_GetPolicyBeneficiariesResponseMsg']['GetPolicyBeneficiariesResp']['bBeneficiaries']['bAct_EntityPolicy'];

            return count($beneficiaries) === count($beneficiaries, COUNT_RECURSIVE) ? array($beneficiaries) : $beneficiaries;
        }
    }

    function renewalNotification() {
        $policy_list = $this->getPolicyList();//dd($policy_list);

        //Check if the entity was found from the get policies method
        if($policy_list[0] == 'not found')
        {
            return response()->json(['message' => "The entity for those details is not found"]);
        }

        $latest_policy_list = $policy_list[count($policy_list)-1]; //return $latest_policy_list;

        $curr_policy_id = $latest_policy_list['bPolicyDescription'] == 'Corporate' ? $latest_policy_list['bEmployeeCategoryPolicyID'] : $latest_policy_list['bPolicyID'];
        $pol_effective_date = $latest_policy_list['bPolicyEffectiveDate'];

        $entity = $this->getEntity();

        if(is_null($entity)) {
            return response()->json(['message' => 'Entity not found']);
        }


        $entityId = $entity->entityid;
        $curr_year = date('Y');

        $WSDL = 'http://192.168.0.216:8813/Actisure/ASWSCOL?singleWsdl';
        $SACTION = 'http://services.activus.com/actcol/servicecontracts/2010/05/ActCOLServiceContract/Act_GetPolicyDetails';
        $DEFAULT_WSA_TO = 'http://192.168.0.216:8813/Actisure/ASWSCOL';

        $params = array(
            'wsdl' => $WSDL,
            'action' => $SACTION
        );

        $client = new \Jick\col\SoapManager($params);

        $response = $client->request('<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://services.activus.com/actcol/messagecontracts/2010/05" xmlns:ns1="http://services.activus.com/common/datacontracts/2010/02" xmlns:ns2="http://services.activus.com/actcol/datacontracts/2010/05">
        <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
            <wsa:Action>'.$SACTION.'</wsa:Action>
            <wsa:To>'.$DEFAULT_WSA_TO.'</wsa:To>
        </soap:Header>
        <soap:Body>
            <ns:Act_GetPolicyDetailsRequestMSG>
                <!--Optional:-->
                <ns:GetPolicyDetails>
                    <!--Optional:-->
                    <ns2:ActisurePolicy>'.$curr_policy_id.'</ns2:ActisurePolicy>
                    <!--Optional:-->
                    <ns2:ActisurePolicyEffectiveDate>'.$pol_effective_date.'</ns2:ActisurePolicyEffectiveDate>
                    <!--Optional:-->
                    <ns2:ActisureEntity>0</ns2:ActisureEntity>
                    <!--Optional:-->
                    <ns2:IgnoreBenefitUsage>0</ns2:IgnoreBenefitUsage>
                </ns:GetPolicyDetails>
            </ns:Act_GetPolicyDetailsRequestMSG>
        </soap:Body>
        </soap:Envelope>');

        $beneficiaries = $this->PolicyBeneficiaries($curr_policy_id,$pol_effective_date);//dd($beneficiaries);

        $exclusions = $latest_policy_list['bPolicyDescription'] == 'Corporate' ? '' : $this->getPolicyExclusions();

        $result = $response['Act_GetPolicyDetailsResponseMSG']['GetPolicyDetailsResp']['bActServiceResult']['cSuccess'];//['bActClaimsAssessmentsList']['bAct_ClaimsAssessment'];

        if($result === 'false')
        {
            $error = $response['Act_GetPolicyDetailsResponseMSG']['GetPolicyDetailsResp']['bActDISBLLErrors']['cActisureDISBLLError'];
            return response()->json(['message' => 'There was an error',
                'error' => $error["cDescription"] ]);
        }
        else{
            $policy_details = $response['Act_GetPolicyDetailsResponseMSG']['GetPolicyDetailsResp']['bActPolicyDetails'];//['bAct_ClaimsAssessment'];

            $output = array(
                'policy_details' => $policy_details
            );

            return $output;
        }
    }
}
