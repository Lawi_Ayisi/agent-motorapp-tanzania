<?php

return [
	'driver'      => env('FCM_PROTOCOL', 'http'),
	'log_enabled' => true,

	'http' => [
		'server_key'       => env('FCM_SERVER_KEY', 'AAAAVz05p40:APA91bFxrZ6A-DGOssNoJyoIxT3oVir7fs5os0JyoZBDtxEr6Z5oGYnzifsdWoU448My1kVgxn8NkvXeo-Qf8kUG9LZZSRgxYs2ldP_Fosp0FsoS6wBDVmVysEXwqEizLohIVPPAhqnA'),
		'sender_id'        => env('FCM_SENDER_ID', '374689343373'),
		'server_send_url'  => 'http://fcm.googleapis.com/fcm/send',
		'server_group_url' => 'http://android.googleapis.com/gcm/notification',
		'timeout'          => 30.0, // in second
	]
];
