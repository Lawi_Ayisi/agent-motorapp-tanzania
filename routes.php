<?php

Route::group(['prefix' => 'api/col/', 'middleware' => 'jwt.auth'], function() {

    Route::get('personal-details', 'jick\col\controllers\PolicyController@getPersonalDetails');
    Route::get('renewal-notification', 'jick\col\controllers\PolicyController@renewalNotification');
    Route::get('policies', 'jick\col\controllers\PolicyController@getPolicyList');
    Route::get('claim-assessment', 'jick\col\controllers\PolicyController@getClaimAssessment');
    Route::get('claims', 'jick\col\controllers\PolicyController@getClaims');
    Route::post('my-reports', 'jick\col\controllers\PolicyController@getClaimReports');
    Route::get('policy-details', 'jick\col\controllers\PolicyController@getPolicyDetails');
    Route::get('policy-ben', 'jick\col\controllers\PolicyController@getPolicyBeneficiaries');
    Route::get('exclusions', 'jick\col\controllers\PolicyController@getPolicyExclusions');
    Route::resource('col-user-details', 'jick\col\controllers\EntityController');
});

