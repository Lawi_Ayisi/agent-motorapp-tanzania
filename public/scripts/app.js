/*global define, require */

define(['angular',
        './namespace',
        './modules/shared/namespace',
        './modules/dashboard/namespace',
        './modules/main/namespace',
        './modules/users/namespace',
        './modules/settings/namespace',
        'angularStrap',
        'loadingBar',
        'ngMessages',
        'ngStorage',
        'config',
        'uiRouter',
        './modules/shared/module.require',
        './modules/dashboard/module.require',
        './modules/main/module.require',
        './modules/users/module.require',
        './modules/settings/module.require',
        ],

    function (
        angular,
        namespace,
        namespaceShared,
        namespaceDashboard,
        namespaceMain,
        namespaceUsers,
        namespaceSettings) {
        'use strict';

        var app = angular.module(namespace, [
            'ui.router',
            namespace + '.config',
            'ngStorage',
            'ngMessages',
            'angular-loading-bar',
            'mgcrea.ngStrap',
            namespaceShared,
            namespaceDashboard,
            namespaceMain,
            namespaceUsers,
            namespaceSettings]);

        app.run(function($rootScope) {

        }).config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
            var opts = {
                lines: 13 // The number of lines to draw
                , length: 28 // The length of each line
                , width: 14 // The line thickness
                , radius: 42 // The radius of the inner circle
                , scale: 1 // Scales overall size of the spinner
                , corners: 1 // Corner roundness (0..1)
                , color: '#eb2329' // #rgb or #rrggbb or array of colors
                , opacity: 0.25 // Opacity of the lines
                , rotate: 0 // The rotation offset
                , direction: 1 // 1: clockwise, -1: counterclockwise
                , speed: 1 // Rounds per second
                , trail: 60 // Afterglow percentage
                , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
                , zIndex: 2e9 // The z-index (defaults to 2000000000)
                , className: 'spinner' // The CSS class to assign to the spinner
                , top: '50%' // Top position relative to parent
                , left: '50%' // Left position relative to parent
                , shadow: false // Whether to render a shadow
                , hwaccel: false // Whether to use hardware acceleration
                , position: 'absolute' // Element positioning
            }

            var target = document.getElementById('ajax_load_spinner')
            var spinner = new Spinner(opts).spin();

            cfpLoadingBarProvider.spinnerTemplate = spinner.el;
        }]);

        return app;

    });