/*global define, require */

define([
  'app',
  './modules/dashboard/namespace',
  './modules/main/namespace',
  './modules/users/namespace',
  './modules/settings/namespace'],
  function (app, namespaceDashboard, namespaceMain, namespaceUsers, namespaceSettings) {

    'use strict';

    app.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

          $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: 'scripts/modules/main/templates/home.html',
                controller : namespaceMain + '.MainCtrl'
            })

            //Auth
            .state('signin', {
                url: '/signin',
                templateUrl: 'scripts/modules/users/templates/sign-in.html',
                controller : namespaceUsers + '.UserCtrl as vm'
            })
            .state('signout', {
                url: '/signout',
                controller : function(Auth) {

                }
            })
            .state('forgot-password', {
                url: '/forgot-password',
                templateUrl: 'scripts/modules/users/templates/forgot-password.html',
                controller : namespaceUsers + '.UserCtrl as vm'
            })

            /**
             * Admin
             */
            //Call it the admin default
            .state('admin', {
                abstract: true,
                url: '/admin',
                templateUrl: 'scripts/modules/main/templates/admin.html'
            })
            .state('dashboard', {
                url: '/dashboard',
                parent : 'admin',
                views:{
                    'admin-ui':{
                        templateUrl: 'scripts/modules/dashboard/templates/dashboard.html',
                        controller : namespaceDashboard + '.DashboardCtrl'
                    }
                }
            })
            .state('users', {
                url: '/users',
                parent: 'dashboard',
                views : {
                    'admin-ui@admin' : {
                        templateUrl: 'scripts/modules/users/templates/index.html',
                        controller : namespaceUsers + '.UserCtrl as vm'
                    }
                }

            })
            .state('roles', {
                url: '/roles',
                parent: 'dashboard',
                views : {
                    'admin-ui@admin' : {
                        templateUrl: 'scripts/modules/settings/templates/roles.html',
                        controller : namespaceSettings + '.RoleCtrl as vm'
                    }
                }
            })
            .state('add-role', {
                url: '/add-role',
                parent: 'dashboard',
                views : {
                    'admin-ui@admin' : {
                        templateUrl: 'scripts/modules/settings/templates/add-role.html',
                        controller : namespaceSettings + '.RoleCtrl as vm'
                    }
                }
            })
            .state('edit-role', {
                url: '/edit-role/:roleId',
                parent: 'dashboard',
                views : {
                    'admin-ui@admin' : {
                        templateUrl: 'scripts/modules/settings/templates/roles/edit-role.html',
                        controller : namespaceSettings + '.RoleCtrl as vm'
                    }
                }
            })
            .state('settings', {
                url: '/settings',
                parent: 'dashboard',
                views : {
                    'admin-ui@admin' : {
                        templateUrl: 'scripts/modules/settings/templates/index.html',
                        controller : namespaceSettings + '.SettingsCtrl as vm'
                    }
                }
            });

          $urlRouterProvider.when('/admin', '/admin/dashboard');
          // if none of the above states are matched, use this as the fallback
          $urlRouterProvider.otherwise('home');

          $httpProvider.interceptors.push(['$q', '$location', '$rootScope', '$localStorage', 'urls',
            function ($q, $location, $rootScope, $localStorage, urls) {
                return {
                    'request': function (config) {
                        config.headers = config.headers || {};
                        if ($localStorage.digitalPortalToken) {
                            config.headers.Authorization = 'Bearer ' + $localStorage.digitalPortalToken;
                        }

                        return config;
                    },
                    'response': function(response) {
                        return response
                    },
                    'responseError': function (response) {
                        console.log(response);
                        if (response.status === 400 || response.status === 401 || response.status === 403) {
                            delete $localStorage.digitalPortalToken;
                            window.location = urls.site + '#signin';
                        }

                        return $q.reject(response);
                    }
                };
            }]);

        });

});