
requirejs.config({
    paths: {
        angular:          '../libs/angular/angular.min',
        angularAnimate:   '../libs/angular/angular-animate.min',
        angularSanitize:  '../libs/angular/angular-sanitize.min',
        uiRouter:         '../libs/angular-ui/angular-ui-router.min',
        ngMessages:       '../libs/angular/angular-messages',
        ngStorage:        '../libs/angular/ngStorage',
        loadingBar:       '../libs/angular/loading-bar',
        angularStrap:     '../libs/angular-strap.min'
    },
    shim: {
        angular : {exports : 'angular'},
        angularAnimate : {deps: ['angular']},
        angularSanitize : {deps: ['angular']},
        uiRouter : {deps: ['angular']},
        ngMessages : {deps: ['angular']},
        ngStorage : {deps: ['angular']},
        loadingBar : {deps: ['angular']},
        angularStrap : {deps: ['angular']}
    },
    priority: [
        'angular'
    ]
});

require([
        'angular',
        'namespace',
        'routes'
    ],
    function (angular, namespace) {
        angular.element(document).ready(function() {
             angular.bootstrap(document, [namespace]);
       });
    });