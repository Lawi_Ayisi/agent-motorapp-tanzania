/*global define, require */

define([
	'../module',
	'../namespace'], function (module, namespace) {

    'use strict';

    var ctrl = function($scope, Settings) {

    	var vm = this;

    	vm.saveSettings = function () {

    		vm.submitted = true;

    		if(vm.settingsForm.$valid)
    		{
    			Settings.add({ settings : vm.settings }, function(res) {

    			});
    		}
    	};

    	//Get all settings
    	Settings.getAll(function(res){
    		vm.settings = res;
    	});
    };

    ctrl.$inject = ['$scope', namespace + '.Settings'];

    var name = namespace + '.SettingsCtrl';

    return module.controller(name, ctrl);

});