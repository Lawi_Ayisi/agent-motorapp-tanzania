/*global define, require */

define([
	'../module',
	'../namespace'], function (module, namespace) {

    'use strict';

    var ctrl = function($scope, $state, $stateParams, Role, Settings) {

    	var vm = this;

    	vm.addRole = function () {
    		 vm.submitted = true;

    		 if(vm.roleForm.$valid)
    		 {
                        var role = {
                           name : vm.name,
                           display_name : vm.display_name,
                           description : vm.description
                        };
                 
    		 	 Settings.addRole(role, function(res) {

    		 	 }, function () {

    		 	 })
    		 }
    	};
        
        vm.editRole = function () {
                vm.submitted = true;

    		 if(vm.roleForm.$valid)
    		 {
                        var role = {
                           id : vm.role.id,
                           name : vm.role.name,
                           display_name : vm.role.display_name,
                           description : vm.role.description
                        };
                 
    		 	 Role.edit(role, function(res) {
                             if(res.status)
                             {
                                 $state.go('roles');
                             }
    		 	 }, function () {

    		 	 })
    		 }
        };
        
        if($stateParams.roleId)
        {
            Role.get($stateParams.roleId, function(res){
                vm.role = res;
            });
        }
        
        Role.getAll(function(res) {
            vm.roles = res;
        }); 

    };

    ctrl.$inject = ['$scope', '$state', '$stateParams', namespace + '.Role', namespace + '.Settings'];

    var name = namespace + '.RoleCtrl';

    return module.controller(name, ctrl);

});