/*global define, console */

define([
    '../module',
    '../namespace'
  ],
  function (module, namespace) {
    "use strict";

    var factory = function($http, urls) {
        return {
          get : function (roleId, success) {
              $http.get(urls.api + "roles" + roleId).success(success);
          },
          getAll : function (success){
              $http.get(urls.api + "roles").success(success);
          },
          edit : function(role, success, error) {
              $http.put(urls.api + "roles" + role.id, role).success(success)
          }
        };
    };

    factory.$inject = ['$http', 'urls'];

    var name = namespace + '.Role';

    return module.factory(name, factory);
});