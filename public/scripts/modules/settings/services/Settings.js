/*global define, console */

define([
    '../module',
    '../namespace'
  ],
  function (module, namespace) {
    "use strict";

    var factory = function($http, urls) {
        return {
          add : function (settings, success, error) {
              $http.post(urls.api + "settings", settings).success(success).error(error)
          },
          getAll : function (success,error){
              $http.get(urls.api + "settings").success(success)
          }
        };
    }

    factory.$inject = ['$http', 'urls'];

    var name = namespace + '.Settings';

    return module.factory(name, factory);
});