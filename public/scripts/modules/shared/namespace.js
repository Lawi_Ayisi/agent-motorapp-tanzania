define([
    '../../namespace'
], function (namespace) {
    'use strict';
    return namespace + ".shared";
});