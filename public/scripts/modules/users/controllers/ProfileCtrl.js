/*global define, require */

define([
	'../module',
	'../namespace'], function (module, namespace) {

    'use strict';

    var ctrl = function($scope, $state, $localStorage, Auth) {

    	var vm = this;      
        
        Auth.profile(function(res){
            vm.profile = res.entities[0];
        });
        
    	vm.logout = function () {
    		Auth.logout();
    	};
    };

    ctrl.$inject = ['$scope', '$state', '$localStorage', namespace + '.Auth'];

    var name = namespace + '.ProfileCtrl';

    return module.controller(name, ctrl);

});