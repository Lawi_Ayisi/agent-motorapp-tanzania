/*global define, require */

define([
	'../module',
	'../namespace'], function (module, namespace) {

    'use strict';

    var ctrl = function($scope, $state, $localStorage, Auth, UserService) {

    	var vm = this;

    	function successAuth(res) {
		      $localStorage.digitalPortalToken = res.token;

              //Get authenticated user kindly
              Auth.profile(function (res){
                    console.log(res);
              });

		      window.location = "/";
		      //$state.go('admin');
        }

    	vm.signin = function(){

    		vm.submitted = true;

    		if(vm.loginForm.$valid) {
                console.log('signing in...');
    			var formData = {
    				username : vm.username,
    				password : vm.password
    			};

    			Auth.signin(formData, successAuth, function (res) {
                    vm.message = res.message || 'Invalid credentials';
    			});
    		}
    	};

        vm.signup = function () {
            //var formData =
        };

        vm.forgotPassword = function() {
            vm.submitted = true;

            if(vm.pwdResetForm.$valid) {
                Auth.passwordForgot({'email':vm.email}, function(res){

                });
            }
        };

        UserService.getAll(function(res){
            vm.users = res;
        });
    };

    ctrl.$inject = ['$scope', '$state', '$localStorage', namespace + '.Auth', namespace + '.UserService'];

    var name = namespace + '.UserCtrl';

    return module.controller(name, ctrl);

});