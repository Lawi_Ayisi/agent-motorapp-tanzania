/*global define, console */

define([
    '../module',
    '../namespace'
  ],
  function (module, namespace) {
    "use strict";

    var factory = function ($http, $localStorage, urls) {

          function urlBase64Decode(str) {
              var output = str.replace('-', '+').replace('_', '/');
              switch (output.length % 4) {
                  case 0:
                      break;
                  case 2:
                      output += '==';
                      break;
                  case 3:
                      output += '=';
                      break;
                  default:
                      throw 'Illegal base64url string!';
              }
              return window.atob(output);
          }

          function getClaimsFromToken() {
              var token = $localStorage.digitalPportalToken;
              var user = {};
              if (typeof token !== 'undefined') {
                  var encoded = token.split('.')[1];
                  user = JSON.parse(urlBase64Decode(encoded));
              }
              return user;
          }

          var tokenClaims = getClaimsFromToken();

          return {
              signup: function (data, success, error) {
                  $http.post(urls.api + '/signup', data).success(success).error(error)
              },
              signin: function (data, success, error) {
                  $http.post(urls.api + 'login', data).success(success).error(error)
              },
              passwordReset: function (data, success, error) {
                  $http.post(urls.api + '/password/email', data).success(success).error(error)
              },
              signout: function (success) {
                  tokenClaims = {};
                  delete $localStorage.digitalPortalToken;
                  success();
              },
              getTokenClaims: function () {
                  return tokenClaims;
              },
              currentUser: function (success,error) {
                  $http.get(urls.api + '/me')
              },
              profile : function (success,error) {
                  $http.get(urls.api + '/me').success(success)
              },
              passwordForgot : function(data,success,error) {
                  $http.post('/passwords/forgot', data).success(success)
              },
            //   passwordReset : function(data, success, error) {
            //       $http.post('/passwords/reset?token=' + token).success(success)
            //   }
          };
    }

    factory.$inject = ['$http', '$localStorage', 'urls'];

    var name = namespace + '.Auth';

    return module.factory(name, factory);
});