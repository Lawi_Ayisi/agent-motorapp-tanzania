define([
        '../module',
        '../namespace'
    ],
    function (module, namespace) {
        "use strict";

        var factory = function ($http, $localStorage, urls) {

            return {
                add: function (data, success, error) {
                    $http.post(urls.api + 'users', data).success(success).error(error)
                },
                edit: function (data, success, error) {
                    $http.put(urls.api + 'users/' + data.id, data).success(success).error(error)
                },
                getAll: function (success, error) {
                    $http.get(urls.api + 'users').success(success)
                }
            };
        }

        factory.$inject = ['$http', '$localStorage', 'urls'];

        var name = namespace + '.UserService';

        return module.factory(name, factory);

    });