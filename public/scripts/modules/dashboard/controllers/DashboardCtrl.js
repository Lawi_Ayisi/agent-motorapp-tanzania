/*global define, require */

define([
	'../module',
	'../namespace'], function (module, namespace) {

    'use strict';

    var ctrl = function($scope) {

    };

    ctrl.$inject = ['$scope'];

    var name = namespace + '.DashboardCtrl';

    return module.controller(name, ctrl);

});