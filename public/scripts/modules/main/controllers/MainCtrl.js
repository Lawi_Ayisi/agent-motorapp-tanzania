/*global define, require */

define([
	'../module',
	'../namespace'], function (module, namespace) {

    'use strict';

    var ctrl = function($location, $scope) {
        var vm = this;

        vm.isActive = function (viewLocation) {
            return viewLocation === $location.path();
        };
    };

    ctrl.$inject = ['$location', '$scope'];

    var name = namespace + '.MainCtrl';

    return module.controller(name, ctrl);

});