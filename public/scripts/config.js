/*global define */

define(['angular'], function (angular) {
	'use strict';

	return angular.module('app.config', [])
		.constant('VERSION', '0.1')
		.constant('urls', {
            api: 'http://192.168.9.54:8200/api/',
            site: 'http://192.168.9.54:8200/'
        });

});