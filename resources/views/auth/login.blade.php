@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-sm-6">
            @if (count($errors) > 0)
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="/auth/login">
                {!! csrf_field() !!}

                <div class="form-group">
                    Email
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                </div>

                <div>
                    Password
                    <input type="password" name="password" class="form-control" id="password">
                </div>

                <div>
                    <input type="checkbox" name="remember"> Remember Me
                </div>

                <div>
                    <button type="submit" class="btn btn-danger">Login</button>
                </div>
            </form>
        </div>
    </div>

@endsection