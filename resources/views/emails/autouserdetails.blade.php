<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Account details</h2>

        <div>
            Thanks for making a quotation with Jubilee Insurance digital portal. We have created an account for you.<br/>
            Please use the following details to login after verifying your account:<br/>

            Email: {{ $quote['email']}}<br/>
            Password: {{ $quote['password']}}<br/>

            <p>Change your password to your preference after logging in.<br>

            <p>To activate your account
            <a href="{{ 'http://192.168.0.152/digital-portal/activate-account/' . $user->confirmation_code }}"> Click here</a><br/>
             

        </div>

    </body>
</html>