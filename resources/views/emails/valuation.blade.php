<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Valuation letter</title>
        <style>
            body {
                font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
            }

        </style>

    </head>
    <body>
        <p>Find attached your valuation letter.</p>

        <p>
            Yours, <br>
            Jubilee Insurance
        </p>

    </body>

</html>