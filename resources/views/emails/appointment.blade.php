<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    <p>{{ $provider }},</p>
    <p>To whom it may concern,</p>
    <p>Dear Sir/Madam, <br><br>
        I am {{ $name }} and would like to book an appointment in your health facility on {{ $date }} at {{ $time }}.
        You can find me on the number {{ $phone }}. <br>
        I look forward to your response.
    </p>

    <p>
        Regards, <br><br>
        {{ $name }}
    </p>

</div>

</body>
</html>