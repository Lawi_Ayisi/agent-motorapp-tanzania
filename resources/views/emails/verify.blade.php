<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Hi {{ $user->firstname }}</h2>

        <p>Welcome to Jubilee Insurance’s Online Portals. We are glad you took the time to register with us.</p>
        <p>To activate the account you have created, please click on the link below.</p>
        <p>This link will redirect you to the log-in page. Here you will be directed on how to proceed.</p>
        <p>
            <a href="{{ 'http://digital-portal.jubileeinsure.com/digital-portal/activate-account/' . $confirmation_code }}">Activate Account</a>
        </p>
        <p>Thank you for choosing Jubilee Insurance.</p>

    </body>
</html>