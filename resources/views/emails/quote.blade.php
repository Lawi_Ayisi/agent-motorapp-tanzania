<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->


    <!-- Bootstrap
    <link href="css/bootstrap.min.css" rel="stylesheet">-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<p>Dear {{ $name }}</p>
<p>Thank you for choosing Jubilee Insurance.</p>

<p>
    We have attached details of your application for motor insurance as submitted to us for your review. To verify
    these details and agree with our terms and conditions, please provide your agent with the unique code sent to
    your telephone number via SMS.
</p>
<p>
    In the event that the attachment contains errors or this e-mail has been sent to you without your consent,
    please call +254709949000 to report the incident.
</p>

<p>Please <a href="{{$policy_statement}}"> click here</a> to download the Policy Statement.</p>

<p>
    Regards, <br><br>
    Jubilee Insurance
</p>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins)
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<!-- Include all compiled plugins (below), or include individual files as needed
<script src="js/bootstrap.min.js"></script>-->
</body>
</html>
