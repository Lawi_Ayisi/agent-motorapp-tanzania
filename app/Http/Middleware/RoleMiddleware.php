<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role, $permission)
    {
        // if (Auth::guest()) {
        //     return redirect($urlOfYourLoginPage);
        // }

        if (! $request->user()->hasRole($role)) {
            return response()->json('Forbiden', 403);
        }

        if (! $request->user()->can($permission)) {
            return response()->json('Forbiden', 403);
        }

        return $next($request);
    }
}
