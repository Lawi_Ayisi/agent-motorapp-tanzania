<?php

//header('Access-Control-Allow-Origin: *');
//header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('api/', function () {
    return view('welcome');
});

Route::get('api/fb', function(SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb) {

    $helper = $fb->getJavaScriptHelper();

    try {
        $token = $helper->getAccessToken();
    } catch (Facebook\Exceptions\FacebookSDKException $e) {
        // Failed to obtain access token
        dd($e->getMessage());
    }

    return $token;
});
/*
Route::get('api/twitter', function() {
    // try {
    //      $response = Twitter::getUserTimeline(['screen_name' => 'JubileeInsKE', 'count' => 1, 'format' => 'json']);
    // }catch(Exception $e) {
    //     dd(Twitter::logs());
    // }
    $response = Twitter::getUserTimeline(['screen_name' => 'JubileeInsKE', 'count' => 1, 'format' => 'json']);

    return $response;

});*/


// Route::post('/min-premium', 'jick\unit_illustrator\src\controllers\settingsController@setMinPremium');
// Route::get('admin/', ['middleware' => [], function () {
//    return view('admin.dashboard');
// }]);

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

//Route::post('api/unit-illustrator', '\jick\Illustrator\controllers\IllustratorController@index');

Route::group(['middleware' => ['web']], function () {
    // Authentication routes...
    Route::get('/', function() {
        return view('layouts.spa');
    });
});

//Send an sms
Route::post('api/send-sms', function(Illuminate\Http\Request $request) {
    $v = Validator::make($request->all(), [
        'tel_no' => 'required',
        'message' => 'required'
    ]);

    if($v->fails()) {
        return response()->json([
            'data' => [
                'message' => $v->errors()->all()
            ]
        ]);
    };

    return Helper::sendSms($request->message, $request->tel_no);
});

Route::group(['middleware' => 'jwt.auth', 'prefix' => 'api/'], function (){
    //Authentication
    Route::post('/auth', 'userManagerController@authenticate');

    //Roles
    Route::post('manage-roles', 'userManagerController@manageRoles');
    Route::get('roles', 'userManagerController@listRoles');
    Route::post('assign-role', 'userManagerController@assignRole');
    Route::post('revoke-role', 'userManagerController@revokeRole');

    //Permissions
    Route::post('manage-permissions', 'userManagerController@managePermissions');
    Route::get('permissions', 'userManagerController@listPermissions');
    Route::post('assign-permission', 'userManagerController@assignPermission');
    Route::post('revoke-permission', 'userManagerController@revokePermission');

    //Roles & Permissions
    Route::post('assign-role-permission', 'userManagerController@assignRolePermission');
    Route::post('revoke-role-permission', 'userManagerController@revokeRolePermission');


    //User Roles
    Route::post('user-roles', 'userManagerController@filterUsersByRole');

});
