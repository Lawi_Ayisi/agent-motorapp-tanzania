<?php

namespace App\Http\Controllers;

use App\User;
//use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\RolePermission;
use Illuminate\Support\Facades\DB;

class userManagerController extends Controller
{
//    use ThrottlesLogins;

    // function __construct(){
    //   $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    // }

    function authenticate(Request $request) {
        $credentials = $request->only('username', 'password');

        try {
            if (! $token = \JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Invalid credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'Could not create token'], 500);
        }

        return response()->json(compact('token'));
    }


    function manageRoles(Request $request) {
        $role_name = $request->input('name');
        $count = count($role_name);

        $items = array();
        for($i = 0; $i < $count; $i++){
            $item = array(
                'NAME' => $role_name[$i]
            );

            $items[] = $item;
        }

        $role = $request->input('new_name');

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        $query = Role::where('NAME', $role_name)->first();

        if ($validator->fails()){
            return response()->json($validator->errors()->all());
        }

        if ($query != null && $role != null){
            $edit = Role::where('NAME', $role_name)->update(['NAME' => $role]);

            return response()->json($edit);
        } else {
//            $role = Role::create(['NAME' => $role_name]);
            if(is_array($role_name)) {
                Role::insert($items);
            }else{
                Role::create(['NAME' => $role_name]);
            }
            return response()->json($items);

        }
    }

    function listRoles() {
        // $roles = Role::with('permissions')->get();
        $roles = DB::table('roles')->select('*')->get();

        return response()->json($roles);
    }

    function managePermissions(Request $request) {
        $permission_name = $request->input('name');
        $count = count($permission_name);

        $items = array();
        for($i = 0; $i < $count; $i++){
            $item = array(
                'NAME' => $permission_name[$i]
            );

            $items[] = $item;
        }

        $permission = $request->input('new_name');

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        $query = Permission::where('NAME', $permission_name)->first();

        if ($validator->fails()){
            return response()->json($validator->errors()->all());
        }

        if ($query != null && $permission != null){
            $edit = Permission::where('NAME', $permission_name)->update(['NAME' => $permission]);

            return response()->json($edit);
        } else {
            Permission::insert($items);

            return response()->json($items);
        }

    }

    function listPermissions() {
        $permissions = Permission::all();

        return response()->json($permissions);
    }

    function assignRole(Request $request) {
        $id = $request->input('user_id');
        $user = User::whereId($id)->firstOrFail();
        $roles = $request->input('role');
        $count = count($roles);

        $items = array();
        for ($i =0; $i < $count; $i++){
            $item = array(
                'role' => $roles[$i]
            );

            $items[] = $item;
        }

        $validator = Validator::make($request->all(), [
//            'user' => 'required',
            'role' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json($validator->errors()->all());
        } else {
            $user->assignRole($items);
        }
    }

    function revokeRole(Request $request) {
        $id = $request->input('user_id');
        $user = User::whereId($id)->firstOrFail();
        $roles = $request->input('role');

        $validator = Validator::make($request->all(), [
//            'user' => 'required',
            'role' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json($validator->errors()->all());
        } else {
            if (is_array($roles)) {
                foreach ($roles as $role) {
                    $user->removeRole($role);
                }
            } else {
                $user->removeRole($roles);
            }

        }
    }

    function assignPermission(Request $request) {
        $id = $request->input('user_id');
        $user = User::whereId($id)->firstOrFail();
        $permissions = $request->input('permission');

        $validator = Validator::make($request->all(), [
//            'user' => 'required',
            'permission' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json($validator->errors()->all());
        } else {
            if (is_array($permissions)) {
                foreach ($permissions as $permission) {
                    $user->givePermissionTo($permission);
                }
            } else {
                $user->givePermissionTo($permissions);
            }

            $user->givePermissionTo($permissions);
        }
    }

    function revokePermission(Request $request) {
        $id = $request->input('user_id');
        $user = User::whereId($id)->firstOrFail();
        $permissions = $request->input('permission');

        $validator = Validator::make($request->all(), [
//            'user' => 'required',
            'permission' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json($validator->errors()->all());
        } else {
            if (is_array($permissions)) {
                foreach ($permissions as $permission) {
                    $user->revokePermissionTo($permission);
                }
            } else {
                $user->revokePermissionTo($permissions);
            }
        }
    }

    function assignRolePermission(Request $request){
        $permissions = $request->input('permission');
        $role = $request->input('role_id');

        $validator = Validator::make($request->all(), [
            'role_id' => 'required | integer',
            'permission' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json($validator->errors()->all());
        } else {
            $return = array();

            if (is_array($permissions)) {
                foreach ($permissions as $permission) {
                    $perm = Permission::where('NAME', $permission)->first()->id;

                    $query = RolePermission::where([['PERM_ID', $perm], ['ROLE_ID', $role]])->first();

                    if ($query != null) {
                     // return response()->json('The role already has permission to ' . $permission);
                    } else {
                      $new = new RolePermission();
                      $new->ROLE_ID = $role;
                      $new->PERM_ID = $perm;

                      $return['status'] = $new->save();
                    }
                }
            }

            return response()->json($return);
        }
    }

    function revokeRolePermission(Request $request){
        $id = $request->input('role_id');
        $role = Role::whereId($id)->firstOrFail();
        $permissions = $request->input('permission');

        $validator = Validator::make($request->all(), [
            'role_id' => 'required | integer',
            'permission' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json($validator->errors()->all());
        } else {
            if (is_array($permissions)) {
                foreach ($permissions as $permission) {
                    $role->revokePermissionTo($permission);
                }
            } else {
                $role->revokePermissionTo($permissions);
            }
        }
    }

    function filterUsersByRole(Request $request) {
        $role_id = $request->input('role_id');

        $validator = Validator::make($request->all(), [
            'role_id' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json($validator->errors()->all());

        } else {
            $user_roles = Role::findOrFail($role_id)->users()
                ->select('ID', 'NAME')
                ->lists('NAME', 'ID')
                ->all();

            return response()->json($user_roles);
        }
    }
}
