<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    protected $table = 'ROLE_HAS_PERMISSIONS';

    protected $fillable = ['PERM_ID', 'ROLE_ID', 'ID'];

    public $timestamps = false;
}
