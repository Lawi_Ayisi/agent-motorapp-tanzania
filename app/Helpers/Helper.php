<?php

namespace App\Helpers;

use DB;

use Rhumsaa\Uuid\Uuid;
use Rhumsaa\Uuid\Exception\UnsatisfiedDependencyException;

class Helper
{

    /**
    *Unformat a formatted number
    */
    static function number_unformat($number, $force_number = true, $dec_point = '.', $thousands_sep = ',') {
        if ($force_number) {
            $number = preg_replace('/^[^\d]+/', '', $number);
        } else if (preg_match('/^[^\d]+/', $number)) {
            return false;
        }
        $type = (strpos($number, $dec_point) === false) ? 'int' : 'float';
        $number = str_replace(array($dec_point, $thousands_sep), array('.', ''), $number);
        settype($number, $type);
        return $number;
    }

    /**
    *
    * Send an sms
    *
    *$params
    *@param Array $params List of messages and recipients
    */
    public static function sendSms($text, $recipient)
    {
        $client = new \infobip\api\client\SendMultipleTextualSmsAdvanced(new \infobip\api\configuration\BasicAuthConfiguration('JubileeKenya', 'Jubilee4321'));

        $destination = new \infobip\api\model\Destination();
        $destination->setTo($recipient);

        $message = new \infobip\api\model\sms\mt\send\Message();
        $message->setFrom('Jubinsure');
        $message->setDestinations([$destination]);
        $message->setText($text);
        //$message->setNotifyUrl(NOTIFY_URL);

        $requestBody = new \infobip\api\model\sms\mt\send\textual\SMSAdvancedTextualRequest();
        $requestBody->setMessages([$message]);

        $response = $client->execute($requestBody);

        return $response;
        //$last_sms = DB::connection('oracle2')->table('JHL_SMS_PROXY')->select('ID')->orderBy('ID', 'DESC')->first();

        // if(!is_array($message) AND empty($message) AND empty($recipient))
        // {
        //     return response()->json(['message' => 'Provide a message and at least one recipient']);
        // }

        // if(is_array($message) AND empty($message))
        // {
        //     return response()->json([
        //         'message' => 'Provide a message and at least one recipient'
        //     ]);
        // }

        // $data = array();

        // //If message is an array, there may be more than one gsms
        // if(is_array($message))
        // {
        //     foreach ($message as $param) {

        //         if(!isset($param['message'])) {
        //             return response()->json(['message' => 'Message must be provided']);
        //         }

        //         if(!isset($param['gsm'])) {
        //             return response()->json(['message' => 'Gsm must be provided']);
        //         }

        //         //Are gsms more than one
        //         if(is_array($param['gsm'])) {
        //             foreach ($param['gsm'] as $recipient) {
        //                 $data[] = array( 'PHONE' => $recipient, 'SENDER' => 'Jubinsure', 'TEXT' => $param['message']);
        //             }
        //         }else{
        //             $data[] = array( 'PHONE' => $param['gsm'], 'SENDER' => 'Jubinsure', 'TEXT' => $param['message']);
        //         }

        //     }
        // }else{
        //      $data[] = array( 'PHONE' => $recipient, 'SENDER' => 'Jubinsure', 'TEXT' => $message);
        // }

        // $inserted = DB::connection('oracle2')->table('JHL_SMS_PROXY')->insert($data);

        // return response()->json(['created' => $inserted]);
    }

    /**
    *Generate unique strings
    */
    public static function UUID($type='random') {

        $uuid;

        try{
            switch ($type) {

                default:
                     $uuid = Uuid::uuid4();
                    break;
            }

        } catch (UnsatisfiedDependencyException $e) {

        }

        return $uuid;
    }
}