<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Jick\agentsapp\models\Quote;
use Jick\Settings\models\Agent;
use Jick\users\models\User;

class SendDataToPremier extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'senddatatopremier';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send J motor Data to Premier';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo $this->userProfile();
    }

    public function userProfile()
    {
        $quotes = Quote::where('quote_id', 'like', '%P/ONL/%')
            ->where('uploaded_premia',0)->where('user_id','!=',0)
            ->get();

        foreach ($quotes as $res) {
            $brokerCode = Agent::where('user_id', $res->agent_id)->first()->agent_no;
            $brokerName = User::where('id', $res->agent_id)->first()->firstname . " " . User::where('id', $res->agent_id)->first()->lastname;
            $pinNo = User::where('id', $res->agent_id)->first()->id_no;


            $fName = null;
            $lName = null;
            $fullName = null;

            if ($res->user_id == 0) {
                $fName = null;
                $lName = null;
                $fullName = null;
            } elseif ($res->user_id != 0) {
                $fName = User::where('id', $res->user_id)->first()->firsname;
                $lName = User::where('id', $res->user_id)->first()->lasname;
                $fullName = $fName . " " . $lName;
            }

            $entities = DB::connection('premier_connect')->table('PM_ASSURED_APP')
                ->insert(array(
                    'ASSR_CODE' => '777',
                    'ASSR_FIRST_NAME' => $fName,
                    'ASSR_MIDDLE_NAME' => $lName,
                    'ASSR_LONG_NAME' => $fullName,
                    'ASSURED_BROKER_CODE' => $brokerCode,
                    'ASSR_BROKER_NAME' => $brokerName,
                    'ASSR_PIN_NO' => $pinNo,
                    'STATUS' => 'NEW',
                    'ASSR_REFERENCE ' => $res->quote_id
                ));

            if ($entities) {
                $this->info ('Data Saved Successfully');
                $update = Quote::where('id',$res->id)->update(array('uploaded_premia'=>1));
                if($update){
                    $this->info ('Quotes Record marked as uploaded to Premia');
                }else{
                    $this->info ('Failed to Upload to Premia');
                }
            } else {
                $this->info ('problem encountered while saving data');
            }
        }
    }
}
