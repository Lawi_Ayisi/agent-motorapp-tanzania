<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);
        $user = factory(App\User::class)->create([
                'name'      => 'Developer',
                'username'  => 'admin',
                'email'     => 'admin@example.com',
                'password'  => bcrypt('password'),
                'phone'     => '0710653477',
                'id_pp_no'  => '234234',
                'active'    => 0
        ]);
    }
}
