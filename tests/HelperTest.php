<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HelperTest extends TestCase
{
    /**
     * Test sending sms
     *
     * @return void
     */
    public function testSendSms()
    {
        $this->post('/api/send-sms')
             >seeJson([
                 'created' => true,
             ]);
    }
}
