<?php

namespace Jick\buyonline\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

use Jick\buyonline\models\Benefit;
use Jick\buyonline\models\MotorUsageType;
use Jick\buyonline\models\MotorVehicleUsageType;
use Jick\buyonline\models\Product;
use Jick\buyonline\models\ValuationCentre;
use Jick\buyonline\models\VehicleType;

class MotorSettingsController extends Controller
{
    public function getVehicleTypes()
    {
        $vtypes = VehicleType::select('id', 'name', 'value')->orderBy('name', 'asc')->get();
        return response()->json($vtypes);
    }

    public function saveVehicleType(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required',
            'value' => 'required',
            'usage_types' => 'required'
        ]);

        if($v->fails()) {
            return response()->json([
                'message' => 'Validation failed',
                'errors'  => $v->errors()->all()
            ], 422);
        }

        $vehType = VehicleType::firstOrNew(['name' => $request->name]);
        $vehType->VALUE = $request->value;

        if($vehType->save()) {

            if(!empty($request->usage_types)) {
                foreach($request->usage_types as $usage_type) {
                    //$usage_type = MotorUsageType::first($usage_type['id']);
                    //$vehType->usageTypes()->save($usage_type);
                    MotorVehicleUsageType::firstOrNew([
                        'vehicle_type_id'=>$vehType->id,
                        'motor_usage_type_id'=>$usage_type['id']
                    ])->save();
                }
            }
           // $vehType->usageTypes()->save($request->usage_types);

            return response()->json([
                'message' => 'Vehicle type saved successfully'
            ]);
        }
    }

    public function getUsageTypes($id)
    {
        $usage = MotorVehicleUsageType::join('motor_usage_types', 'motor_vehicle_usage_types.motor_usage_type_id', '=', 'motor_usage_types.id')
            ->join('vehicle_types', 'motor_vehicle_usage_types.vehicle_type_id', '=', 'vehicle_types.id')
            ->select('motor_usage_types.id', 'motor_usage_types.name', 'motor_usage_types.value')
            ->where('motor_vehicle_usage_types.vehicle_type_id', $id)
            ->get();

        return response()->json($usage);
    }

    public function saveUsageType(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required',
            'value' => 'required'
        ]);

        if($v->fails()) {
            return response()->json([
                'message' => 'Validation failed'
            ], 422);
        }

        $vehType = MotorUsageType::firstOrNew(['name' => $request->name]);
        $vehType->VALUE = $request->value;

        if($vehType->save()) {
            return response()->json([
                'message' => 'Vehicle usage type saved successfully'
            ]);
        }
    }

    public function getBenefits($productName='')
    {
        if($productName !== '') {
            $product = Product::where('LOWER(name)', $productName)->first();

            if(!empty($product)) {
                return Benefit::select('id', 'name', 'slug', 'description', 'product_id')
                            ->where('product_id', $product->id)->get();
            }

        }

        return Benefit::select('id', 'name', 'description', 'product_id')->get();
    }

    public function saveBenefit(Request $request)
    {

        $v = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'product' => 'required'
        ]);

        if($v->fails()) {
            return response()->json([
                'message' => 'Validation failed'
            ], 422);
        }

        $benefit = Benefit::firstOrNew(['name' => $request->name]);
        $benefit->description = $request->description;
        $benefit->product_id  = $request->product;
        $benefit->slug        = strtolower(str_replace(' ', '_', $request->name));

        if($benefit->save()) {
            return response()->json([
                'message' => 'Benefit saved successfully'
            ]);
        }
    }

    public function getValuationCentres()
    {

    }

    public function saveValuationCentre(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'longitude' => 'required',
            'latitude'  => 'required',
            'type' => 'required'
        ]);

        if($v->fails()) {
            return response()->json([
                'message' => 'Validation failed',
                'errors'  => $v->errors()->all()
            ], 422);
        }

        $vCentre = ValuationCentre::firstOrNew(['name' => $request->name]);
        $vCentre->address = $request->address;
        $vCentre->longitude = $request->longitude;
        $vCentre->longitude = $request->longitude;
        $vCentre->type      = $request->type;

        if($vCentre->save()) {
            return response()->json([
                'message' => 'Valuation centre saved successfully'
            ]);
        }
     }
}
