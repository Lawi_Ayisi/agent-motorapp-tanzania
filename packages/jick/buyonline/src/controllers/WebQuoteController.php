<?php

namespace Jick\buyonline\controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Jick\buyonline\models\AgeBracket;
use Jick\buyonline\models\Benefit;
use Jick\buyonline\models\Delivery_detail;
use Jick\buyonline\models\Motor_detail;
use Jick\buyonline\models\MotorRate;
use Jick\buyonline\models\MotorUsageType;
use Jick\buyonline\models\MotorVehicleUsageType;
use Jick\buyonline\models\Plan;
use Jick\buyonline\models\Product;
use Jick\buyonline\models\Quote;
use Jick\buyonline\models\QuoteBenefit;
use Jick\buyonline\models\VehicleType;
use Jick\policies\models\Policy;
use Jick\settings\models\Setting;
use Jick\users\models\User;
use Mail;
use PDF;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;

use Illuminate\Support\Facades\File;

//use JWTAuth;

class WebQuoteController extends Controller
{
    function index()
    {
        return $this->generate_policy_number('motor');
    }

    //** List all the policies **//
    function all_quotes()
    {
        return Quote::all();
    }

    //** Make quotation **//
    function store(Request $request)
    {
        $product = $request->product;

        //Validate, the product must be supplied
        $validator = Validator::make($request->all(), [
            'product' => 'required',
            'cover_type' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed',
                'errors' => $validator->errors()->all()
            ], 422);
        }
        //capture required details for a given product
        switch ($product) {
            case 'motor':
                //core parameters
                $cover_type = $request->cover_type;
                $use_type = $request->use_type;
                $veh_cat = $request->veh_cat;
                $veh_type = $request->veh_type;
                $weight = $request->weight;
                $sum_insured = $request->sum_insured;
                $passengers = $request->passengers;

                //optional benefits
                $bn_courtesy_car = $request->courtesy_car;
                $bn_excess_protector = $request->excess_protector;
                $bn_fatal_pa = $request->fatal_pa;
                $bn_forced_atm = $request->forced_atm;
                $bn_loss_of_keys = $request->loss_of_keys;
                $bn_loss_of_spare_wheel = $request->loss_of_spare_wheel;
                $bn_loss_personal_effects = $request->loss_personal_effects;
                $bn_out_of_station = $request->out_of_station;
                $bn_pvtr = $request->pvtr;
                $bn_aa_rescue = $request->aa_rescue;


                //create the array that contains the parameters to be passed to the calculate premium function
                $parameters = array(
                    'cover_type' => $cover_type,
                    'use_type' => $use_type,
                    'veh_cat' => $veh_cat,
                    'vehicle_type' => $veh_type,
                    'weight' => $weight,
                    'sum_insured' => $sum_insured,
                    'passengers_no' => $passengers,
                    'courtesy_car' => $bn_courtesy_car,
                    'excess_protector' => $bn_excess_protector,
                    'fatal_pa' => $bn_fatal_pa,
                    'forced_atm' => $bn_forced_atm,
                    'loss_of_keys' => $bn_loss_of_keys,
                    'loss_of_spare_wheel' => $bn_loss_of_spare_wheel,
                    'loss_personal_effects' => $bn_loss_personal_effects,
                    'out_of_station' => $bn_out_of_station,
                    'pvtr' => $bn_pvtr,
                    'aa_rescue' => $bn_aa_rescue
                );

                //call the function that calculates the motor quotation

                $premium = $this->generate_motor_premium($parameters);


                //Return the premium
                return response()->json(['data' => $premium]);

                break;

            default:
                # code...
                break;
        }
    }

    function premiumCalculator(Request $request)
    {
        $product = $request->product;

        //Validate, the product must be supplied
        $validator = Validator::make($request->all(), [
            'product' => 'required',
            'cover_type' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed',
                'errors' => $validator->errors()->all()
            ], 422);
        }
        //capture required details for a given product
        switch ($product) {
            case 'motor':
                //core parameters
                $cover_type = $request->cover_type;
                $use_type = $request->use_type;
                $veh_cat = $request->veh_cat;
                $veh_type = $request->veh_type;
                $weight = $request->weight;
                $sum_insured = $request->sum_insured;
                $passengers = $request->passengers;

                //optional benefits
                $bn_courtesy_car = $request->courtesy_car;
                $bn_excess_protector = $request->excess_protector;
                $bn_fatal_pa = $request->fatal_pa;
                $bn_forced_atm = $request->forced_atm;
                $bn_loss_of_keys = $request->loss_of_keys;
                $bn_loss_of_spare_wheel = $request->loss_of_spare_wheel;
                $bn_loss_personal_effects = $request->loss_personal_effects;
                $bn_out_of_station = $request->out_of_station;
                $bn_pvtr = $request->pvtr;
                $bn_aa_rescue = $request->aa_rescue;


                //create the array that contains the parameters to be passed to the calculate premium function
                $parameters = array(
                    'cover_type' => $cover_type,
                    'use_type' => $use_type,
                    'veh_cat' => $veh_cat,
                    'vehicle_type' => $veh_type,
                    'weight' => $weight,
                    'sum_insured' => $sum_insured,
                    'passengers_no' => $passengers,
                    'courtesy_car' => $bn_courtesy_car,
                    'excess_protector' => $bn_excess_protector,
                    'fatal_pa' => $bn_fatal_pa,
                    'forced_atm' => $bn_forced_atm,
                    'loss_of_keys' => $bn_loss_of_keys,
                    'loss_of_spare_wheel' => $bn_loss_of_spare_wheel,
                    'loss_personal_effects' => $bn_loss_personal_effects,
                    'out_of_station' => $bn_out_of_station,
                    'pvtr' => $bn_pvtr,
                    'aa_rescue' => $bn_aa_rescue
                );

                //call the function that calculates the motor quotation

                $premium = $this->generate_motor_premium($parameters);

//                $user = JWTAuth::parseToken()->authenticate();
//                $uid = $user['id'];
                $user = User::where('account_type', 'web')->first();
                $uid = $user->id;
                $code = DB::table('agents')->where('user_id', $uid)->first()->agent_no;

                $pd = [
                    'pdf' => url('buyonline/' . $code . '.pdf')
                ];


                $pdf = PDF::loadView('buyonline::provisional', ['data' => $premium]);
                $pdf->save('buyonline/' . $code . '.pdf');

                //Return the premium
                return response()->json(['data' => $premium, 'pdf_url' => $pd['pdf']]);

                break;

            default:
                # code...
                break;
        }
    }

    //** Function to store user and quotation details **//
    function save_quote(Request $request)
    {
//        $user = JWTAuth::parseToken()->authenticate();
//        $user_id = $user['id'];
        $user = User::where('account_type', 'web')->first();
        $user_id = $user->id;
        //validate for personal details i.e name,phone number, password
        $validator = Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => 'required',
            'email' => 'required | email',
            'premium' => 'required',
            'plan_name' => 'required',
            'product' => 'required',
            'id_pp' => 'required',
            'use_type' => 'required',
            'veh_type' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed, see errors below',
                'errors' => $validator->errors()->all()
            ], 422);
        }

        //Get the product name
        $product_name = $request->product;

        //Get the use type
        $use_type = $request->use_type;

        //Get the vehicle type
        $vehicle_type = $request->veh_type;

        //Save user details in the user table

        //create a username for them
        $username = substr($request->firstname, 0, 3) . "_" . $request->lastname;

        //create a password for them
        $password = str_random(5);

        //Call the function to auto register a user that returns user id
        $user = $this->autoRegister($request, $username, $password);

        //Lets get the levies and other values ready to display a quotation
        $quote = $this->generate_quote($request->premium);

        //email address
        $quote['email'] = $request->email;

        //get the name by joining first and lastname
        $quote['name'] = $request->firstname . ' ' . $request->lastname;

        $quote['password'] = $password;

        //Generate the policy number
        $result = $this->generate_policy_number($product_name, $vehicle_type, $use_type);//return $result;

        $policy_no = $result['quote_id'];

        //Add the policy number to the quote response array
        $quote['policy_no'] = $policy_no;

        //Add user id to the quote
        $quote['user_id'] = $user->id;

        //Add the product id
        $quote['product_id'] = $result['product_id'];

        //Add the plan name
        $quote['plan_name'] = $request->plan_name;

        //Full plan name
        $quote['plan_name_full'] = $this->get_cover_type($request->plan_name);

        //Add the plan id
        $plan_id = Plan::where('NAME', $quote['plan_name'])->first();
        $quote['plan'] = $plan_id->id;
        $quote['date'] = date("D, d M Y ");

        if ($request->courtesy_car != '') {
            $quote['cc'] = 'Courtesy Car<br/>';
        } else {
            $quote['cc'] = '';
        }
        if ($request->excess_protector != '') {
            $quote['ep'] = 'Excess Protector<br/>';
        } else {
            $quote['ep'] = '';
        }
        if ($request->fatal_pa != '') {
            $quote['fp'] = 'Fatal Personal Accident Cover<br/>';
        } else {
            $quote['fp'] = '';
        }
        if ($request->forced_atm != '') {
            $quote['fa'] = 'Forced ATM Withdrawal<br/>';
        } else {
            $quote['fa'] = '';
        }
        if ($request->loss_personal_effects != '') {
            $quote['lpe'] = 'Loss of Personal Effects<br/>';
        } else {
            $quote['lpe'] = '';
        }
        if ($request->out_of_station != '') {
            $quote['oos'] = 'Out of Station Accomodation<br/>';
        } else {
            $quote['oos'] = '';
        }
        if ($request->pvtr != '') {
            $quote['pvtr'] = 'Political Violence & Terrorism Risk<br/>';
        } else {
            $quote['pvtr'] = '';
        }
        if ($request->aa_rescue != '') {
            $quote['aa'] = 'AA Rescue<br/>';
        } else {
            $quote['aa'] = '';
        }

//        $confirmation = mt_rand(111111, 999999);
//        $client_phone = $request->phone;

        //save this quotation into the quotes table
        $quotation = new Quote;
        $quotation->QUOTE_ID = $quote['policy_no'];
        $quotation->PREMIUM = $quote['premium'];
        $quotation->PHCF = $quote['phcf'];
        $quotation->LEVY = $quote['training_levy'];
        $quotation->DUTY = $quote['stamp_duty'];
        $quotation->TOTAL_PREMIUM = $quote['total_premium'];
        $quotation->STATUS = 'quotation';
        $quotation->PRODUCT_ID = $quote['product_id'];
        $quotation->PLAN_ID = $quote['plan'];
        $quotation->USER_ID = $quote['user_id'];
        $quotation->id_pp = $request->id_pp;
        $quotation->agent_id = $user_id;

//        $quotation->client_code = $confirmation;
        //return $quotation;

        $policy = new Policy();
        $policy->policy_no = $quote['policy_no'];
        $policy->biz_line = 'general';
        $policy->user_id = $quote['user_id'];
        $policy->status = 0;
        $policy->total_premium = $quote['total_premium'];
        $policy->start_date = date('Y-m-d H:i:s');
        $policy->id_pp = $request->id_pp;
        $policy->end_date = date('Y-m-d', strtotime(date("Y-m-d", time()) . " + 364 day"));
        $policy->renewal_date = date('Y-m-d', strtotime(date("Y-m-d", time()) . " + 365 day"));

        $policy->save();

        //Generate the quotation pdf
        $pdf = PDF::loadView('buyonline::quote', ['quotation' => $quote, 'currency' => "Kshs", 'post' => $request->all()]);
        $pdf->save('buyonline/quotation_for_' . $request->email . '.pdf');

        //add the url to the quote
        $quote['pdf_url'] = url('buyonline/quotation_for_' . $request->email . '.pdf');

        //add the path to the quote
        $quote['pdf_path'] = public_path('buyonline/quotation_for_' . $request->email . '.pdf');

        //send only auto generated credentials if the user is new
        if ($user->user_type == 'new') {
            //send email with autoregistered  details
            Mail::send('emails.autouserdetails', ['quote' => $quote, 'user' => $user], function ($m) use ($quote) {

                $m->to($quote['email'], $quote['name'])->subject('Account Details');

            });
        }

        //send email with quotation details
//        Mail::send('emails.quote', ['quotation' => $quote, 'user' =>$user], function ($m) use ($quote) {
//          //  $user = JWTAuth::parseToken()->authenticate();
//            //$uid = $user['id'];
//            $user=User::where('account_type','web')->first();
//            $uid = $user->id;
//            $agent_email = User::where('id', $uid)->first()->email;
//
//            $m->to($quote['email'], $quote['name'])->subject('Your Quotation');
//            $m->cc($agent_email);
//            $m->attach($quote['pdf_path']);
//        });


        if ($quotation->save()) {
//            Helper::sendSms('Dear, '.$request->lastname.' your code is: '.$confirmation.' To verify your details, please view the e-mail we just sent you and provide this code to your agent. Jubilee Insurance', $client_phone);

            return response()->json([
                'message' => 'Quote saved successfully',
                'quotation' => $quote
            ]);
        } else {
            return response()->json([
                'message' => 'Quote saving failed'
            ]);
        }

        if ($policy->save()) {
            return response()->json([
                'message' => 'The policy has been saved',
                'reference' => $quote['policy_no']
            ]);
        } else {
            return response()->json([
                'message' => 'Policy could not be saved'
            ]);
        }
        // dd($quote);
    }


    //** Function to auto register a user that returns user id **//
    private function autoRegister($request, $username, $password)
    {
        $user_exists = User::where('email', $request->email)->first();

        if ($user_exists == null) {
            $user = User::firstOrNew(['email' => $request->email]);

            $user->FIRSTNAME = $request->firstname;
            $user->LASTNAME = $request->lastname;
            $user->PHONE = $request->phone;
            $user->id_no = $request->id_pp;
            $user->USERNAME = $username;
            $user->password = bcrypt($password);
            $user->active = 0;

            //handle account activation
            $confirmation_code = str_random(30);
            $user->confirmation_code = $confirmation_code;

            if ($user->save()) {
                $user->user_type = 'new';
                return $user;
            }
        } else {
            $user_exists->user_type = 'existing';
            return $user_exists;
        }

    }

    public function confirmCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'quote_id' => 'required',
            'client_code' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'The Client Code is required.'
            ], 422);
        };

        $quote = $request->quote_id;
        $code = $request->client_code;

        $coverStart = $request->cover_start;
        if (!is_null($coverStart) || !isEmpty($coverStart)) {
            //update  cover start on on motor details
            Motor_detail::where('quote_id', $quote)->update(['cover_start' => $this->dateFormat($coverStart)]);
        }

        $query = Quote::where('quote_id', $quote)->first()->client_code;
        if ($query == $code) {
            return response()->json([
                'message' => 'You can now proceed to buying the policy.'
            ]);
        } else {
            return response()->json([
                'message' => 'The code provided is incorrect.'
            ], 422);
        }
    }

    public function save_extra_details(Request $request)
    {
        $confirmation = mt_rand(111111, 999999);
        $client_phone = $request->phone;

        //validate extra details
        $validator = Validator::make($request->all(), [
            'policy_number' => 'required',
            'car_make' => 'required',
            'car_model' => 'required',
//            'cover_start' => 'required',
            'manufacture_year' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed, see errors below',
                'errors' => $validator->errors()->all()
            ], 422);
        }

        $motor_details = Motor_detail::firstOrNew(['quote_id' => $request->policy_number]);
        $motor_details->make = $request->car_make;
        $motor_details->model = $request->car_model;
        $motor_details->purchase_year = $request->manufacture_year;
        $motor_details->cover_start = $request->cover_start;

        if ($motor_details->save()) {
            //check email to get userID
            if (!is_null($this->checkUserID($request->email)) || !isEmpty($this->checkUserID($request->email))) {
                Quote::where('quote_id', $request->policy_number)->update([
                    'client_code' => $confirmation,
                    'user_id' => $this->checkUserID($request->email)
                ]);
                //update user data
                User::where('id', $this->checkUserID($request->email))->update([
                    'phone' => substr($request->phone, 1),
                    'dob' => $this->dateFormat($request->dob),
                    'physical_addr' => $request->residential_address,
                    'id_no' => $request->id_number,
                ]);
            } else {
                //create Client user and update quote Records
                $email = $request->email;
                $phone = substr($request->phone, 1);
                $firstname = $request->firstname;
                $lastname = $request->lastname;
                $dob = $this->dateFormat($request->dob);
                $gender = $request->gender;
                $idNumber = $request->id_number;
                $residence = $request->residential_address;
                $userID = $this->createClientRecord($firstname, $lastname, $email, $phone, $idNumber, $dob, $gender, $residence);

                Quote::where('quote_id', $request->policy_number)->update([
                    'client_code' => $confirmation,
                    'user_id' => $userID
                ]);
            }
            Quote::where('quote_id', $request->policy_number)->update(['client_code' => $confirmation]);

            Helper::sendSms('Dear, ' . $request->firstname . ' your code is: ' . $confirmation . ' To verify your details, please view the e-mail we just sent you and provide this code to your agent. Jubilee Insurance', $client_phone);
//            $user = JWTAuth::parseToken()->authenticate();
//            $uid = $user['id'];
            $user = User::where('account_type', 'web')->first();
            $uid = $user->id;
            $agent_c = DB::table('agents')->where('user_id', $uid)->first();

            if ($agent_c != null) {
                $agent_code = DB::table('agents')->where('user_id', $uid)->first()->agent_no;
                $agent_fname = User::where('id', $uid)->first()->firstname;
                $agent_lname = User::where('id', $uid)->first()->lastname;
                $agent_tel = User::where('id', $uid)->first()->phone;
                $agent_name = $agent_fname . ' ' . $agent_lname;
                $agent_email = User::where('id', $uid)->first()->email;

//            $make = CarMake::where('id', $request->car_make)->first()->name;
//            $model = DB::table('models')->where('id', $request->car_model)->first()->name;

                if ($request->courtesy_car != '') {
                    $cc = 'Courtesy Car<br/>';
                } else {
                    $cc = '';
                }
                if ($request->excess_protector != '') {
                    $ep = 'Excess Protector<br/>';
                } else {
                    $ep = '';
                }
                if ($request->fatal_pa != '') {
                    $fp = 'Fatal Personal Accident Cover<br/>';
                } else {
                    $fp = '';
                }
                if ($request->forced_atm != '') {
                    $fa = 'Forced ATM Withdrawal<br/>';
                } else {
                    $fa = '';
                }
                if ($request->loss_personal_effects != '') {
                    $lpe = 'Loss of Personal Effects<br/>';
                } else {
                    $lpe = '';
                }
                if ($request->out_of_station != '') {
                    $oos = 'Out of Station Accomodation<br/>';
                } else {
                    $oos = '';
                }
                if ($request->pvtr != '') {
                    $pvtr = 'Political Violence & Terrorism Risk<br/>';
                } else {
                    $pvtr = '';
                }
                if ($request->aa_rescue != '') {
                    $aa = 'AA Rescue<br/>';
                } else {
                    $aa = '';
                }

                $policy_statement = url('agentsapp/policy_wordings.pdf');

                $data = [
                    'make' => $request->car_make,
                    'model' => $request->car_model,
                    'email' => $request->email,
                    'policy_statement' => $policy_statement,
                    'agent_name' => $agent_name,
                    'agent_code' => $agent_code,
                    'agent_email' => $agent_email,
                    'agent_number' => $agent_tel,
                    'name' => $request->firstname . ' ' . $request->lastname,
                    'dob' => $request->dob,
                    'tel' => $request->phone,
                    'gender' => $request->gender,
                    'nationality' => $request->nationality,
                    'reg_no' => $request->reg_number,
                    'residential' => $request->residential_address,
                    'estimated' => $request->sum_insured,
                    'type' => $request->veh_type,
                    'use' => $request->use_type,
                    'owner' => $request->firstname . ' ' . $request->lastname,
                    'cover' => $request->cover_type,
                    'quote' => $request->policy_number,
                    'premium' => $request->premium,
                    'amount' => $request->total_premium_amount,
                    'phcf' => $request->phcf,
                    'stamp' => $request->stamp_duty,
                    'training' => $request->training_levy,
                    'date' => date("D, d M Y "),
                    'pvtr' => $pvtr,
                    'oos' => $oos,
                    'lpe' => $lpe,
                    'fa' => $fa,
                    'cc' => $cc,
                    'fp' => $fp,
                    'ep' => $ep,
                    'aa' => $aa
                ];

                $pdf = PDF::loadView('buyonline::index', $data);
                $pdf->save('buyonline/quotation_for_' . $request->email . '.pdf');

                $body = [
                    'agent_email' => User::where('id', $uid)->first()->email,
                    'client_email' => $request->email,
                    'name' => $request->firstname,
                    'pdf' => public_path('buyonline/quotation_for_' . $request->email . '.pdf')
                ];

                Mail::send('emails.quote', $data, function ($m) use ($body) {
                    $m->to($body['client_email'], $body['name'])->subject('Your Quotation');
                    $m->cc($body['agent_email']);
                    $m->attach($body['pdf']);
                });

                return response()->json([
                    'message' => 'Motor details saved successfully'
                ], 200);
            } else {
                return response()->json([
                    'message' => 'Your agent code is missing from your profile, please contact the administrator.'
                ], 422);
            }
        } else {
            return response()->json([
                'message' => 'Saving failed, try again'
            ], 422);
        }

    }

    //check user id through email look up and return null or userid object
    public function checkUserID($email)
    {
        $userId = null;
        $user = User::where('email', $email)->get();
        if ($user->count() > 0) {
            //user exists
            $userId = User::where('email', $email)->first()->id;
        }
        return $userId;
    }

    //create client record where client does not exists
    public function createClientRecord($firstname, $lastname, $email, $phone, $idNumber, $dob, $gender, $residence)
    {
        $user = User::firstOrNew(['email' => $email]);
        $user->firstname = $firstname;
        $user->lastname = $lastname;
        $user->email = $email;
        $user->username = $email;
        $user->phone = $phone;
        $user->account_type = "customer";
        $user->id_no = $idNumber;
        $user->dob = $dob;
        $user->physical_addr = $residence;
        $user->save();

        $userId = $user->id;
        return $userId;
    }

    public function dateFormat($parsedDate)
    {
        //form d-m-yy to yy-m-d
        $date = date('Y-m-d', strtotime($parsedDate));
        return $date;
    }

    //** Function to return a value of a given setting when the name is passed **//
    private function get_settingVal($name)
    {
        $settings = Setting::where(['module' => 'motor', 'name' => $name])
            ->first();
        return !empty($settings) ? $settings->setting_value : '';

    }

    //** Function to return the name of the cover type **//
    private function get_cover_type($cover_type_val = '')
    {
        $return = '';

        switch ($cover_type_val === 'tpo') {
            case 'tpo':
                $return = 'Third Party Only';
                break;
            case 'tpft':
                $return = 'Third Party Fire & Theft';
                break;
            case 'comprehensive':
                $return = 'Comprehensive';
                break;
        }

        return $return;
    }

    //** Function to display all quotes for a given user **//
    function list_quotes()
    {
//        $user = JWTAuth::parseToken()->authenticate();
//        $user_id = $user['id'];
        $user = User::where('account_type', 'web')->first();
        $user_id = $user->id;

        $my_quotes = Quote::where('user_id', $user_id)->get();//where('username',$user)->first();

        return response()->json($my_quotes);
    }


    //** Function to save delivery details **//
    function delivery_details(Request $request)
    {
        //validate
        $validator = Validator::make($request->all(), [
            'quote_id' => 'required',
            'physical_address' => 'required',
//            'postal_address' => 'required',
            'delivery_type' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed, see errors below',
                'errors' => $validator->errors()->all()
            ], 422);
        }
        $delivery = Delivery_detail::firstOrNew(['quote_id' => $request->quote_id]);
        $delivery->physical_address = $request->physical_address;
        $delivery->postal_address = $request->postal_address;
        $delivery->delivery_type = $request->delivery_type;


        if ($delivery->save()) {
            return response()->json([
                'message' => 'Delivery details saved successfully'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Saving failed, try again'
            ], 422);
        }
    }


    //** Generate a policy number **//
    private function generate_policy_number($product_name, $veh_type, $use_type)
    {
        switch ($product_name) {
            case 'motor':
                //evaluate the sub class code to be used based on the use and vehicle types
                $sub_class = "";
                if ($veh_type === 'motorcycle') {
                    $sub_class = 2005;
                } else if ($use_type === 'private') {
                    $sub_class = 2010;
                } else if ($use_type === 'own_goods') {
                    $sub_class = 2040;
                } else if ($use_type === 'gen_cartage') {
                    $sub_class = 2065;
                } else {
                    $sub_class = 2045;
                }

                //Get the current year
                $cur_year = date('Y');

                //Get the id of the product from products table
                $product = Product::where(['name' => 'motor'])->first();
                if (!empty($product)) {
                    $product_id = $product->id;
                    $quotes = Quote::where('product_id', $product_id)->orderBy('created_at', 'asc')->get();

                    $last_quote_id;
                    foreach ($quotes as $quote) {//return $quote->quote_id;
                        $last_quote_id = $quote->quote_id;
                    }

                    if (count($quotes) > 0) {
                        $last_quote_policy_no_segs = explode('/', $last_quote_id);//return $last_quote_id;

                        $quote_id = "P/ONL/" . $sub_class . "/" . $cur_year . "/" . sprintf("%05s", $last_quote_policy_no_segs[4] + 1);
                    } else {
                        $quote_id = "P/ONL/" . $sub_class . "/" . $cur_year . "/" . sprintf("%05s", 1);
                    }

                    $return = array('quote_id' => $quote_id,
                        'product_id' => $product_id);

                    return $return;

                } else {
                    return response()->json([
                        'message' => 'No products, kindly add products in the systems'
                    ], 422);
                }
                break;

            default:
                # code...
                break;
        }
    }


    /**
     *
     * Function to calculate the motor premium and generate premium values
     *
     * @param type $input
     *
     */

    private function generate_motor_premium($params = null)
    {
        $premium = 0;
        $return = array();

        //return $params;
        //Check post params and save them in variables
        $cover_type = isset($params['cover_type']) ? $params['cover_type'] : '';
        $use_type = isset($params['use_type']) ? $params['use_type'] : '';
        $veh_cat = isset($params['veh_cat']) ? $params['veh_cat'] : '';
        $veh_type = isset($params['vehicle_type']) ? $params['vehicle_type'] : '';
        $weight = isset($params['weight']) ? (double)$params['weight'] : '';
        $sum_insured = isset($params['sum_insured']) ? (double)$params['sum_insured'] : '';
        $passengers = isset($params['passengers_no']) ? (double)$params['passengers_no'] : '';

        $bn_courtesy_car = isset($params['courtesy_car']) ? $params['courtesy_car'] : '';
        $bn_excess_protector = isset($params['excess_protector']) ? $params['excess_protector'] : '';
        $bn_fatal_pa = isset($params['fatal_pa']) ? $params['fatal_pa'] : '';
        $bn_forced_atm = isset($params['forced_atm']) ? $params['forced_atm'] : '';
        $bn_loss_of_keys = isset($params['loss_of_keys']) ? $params['loss_of_keys'] : '';
        $bn_loss_of_spare_wheel = isset($params['loss_of_spare_wheel']) ? $params['loss_of_spare_wheel'] : '';
        $bn_loss_personal_effects = isset($params['loss_personal_effects']) ? $params['loss_personal_effects'] : '';
        $bn_out_of_station = isset($params['out_of_station']) ? $params['out_of_station'] : '';
        $bn_pvtr = isset($params['pvtr']) ? $params['pvtr'] : '';
        $bn_aa_rescue = isset($params['aa_rescue']) ? $params['aa_rescue'] : '';


        //Get the settings from the get_settingVal method that fetches from the table
        $tpo_motorcycle = $this->get_settingVal('tpo_motorcycle');
        $tpo_saloon_private = $this->get_settingVal('tpo_saloon_private');
        $tpo_saloon_taxiself = $this->get_settingVal('tpo_saloon_taxiself');
        $tpo_saloon_taxichauffer9p = $this->get_settingVal('tpo_saloon_taxichauffer9p');
        $tpo_saloon_taxichauffer25p = $this->get_settingVal('tpo_saloon_taxichauffer25p');
        $tpo_saloon_taxi_pass_legal = $this->get_settingVal('tpo_saloon_taxi_pass_legal');
        $tpo_saloon_driving = $this->get_settingVal('tpo_saloon_driving');

        $tpo_owngoods_weight3 = $this->get_settingVal('tpo_owngoods_weight3');//Weight upto 3tons
        $tpo_owngoods_weight8 = $this->get_settingVal('tpo_owngoods_weight8');//Weight upto 8tons
        $tpo_owngoods_weight10 = $this->get_settingVal('tpo_owngoods_weight10');//Weight upto 10tons

        $tpo_gencartage_weight20 = $this->get_settingVal('tpo_gencartage_weight20');//Weight upto 20tons
        $tpo_gencartage_weight30 = $this->get_settingVal('tpo_gencartage_weight30');//Weight upto 3tons
        $tpo_gencartage_perextra = $this->get_settingVal('tpo_gencartage_perextra');

        $driv_skul_heavy7 = $this->get_settingVal('driv_skul_heavy7');
        $driv_skul_heavy_over7 = $this->get_settingVal('driv_skul_heavy_over7');

        $tpo_agric = $this->get_settingVal('tpo_agric');
        $tpo_ambulance = $this->get_settingVal('tpo_ambulance');
        $tpo_cranes = $this->get_settingVal('tpo_cranes');

        $tpo_tanker10 = $this->get_settingVal('tpo_tanker10');
        $tpo_tanker20 = $this->get_settingVal('tpo_tanker20');
        $tpo_tanker30 = $this->get_settingVal('tpo_tanker30');
        $tpo_tanker_perextra = $this->get_settingVal('tpo_tanker_perextra');

        $tpft_motorcycle_r = $this->get_settingVal('tpft_motorcycle_r');
        $tpft_motorcycle_min = $this->get_settingVal('tpft_motorcycle_min');
        $comp_motorcycle_r = $this->get_settingVal('comp_motorcycle_r');
        $comp_motorcycle_min = $this->get_settingVal('comp_motorcycle_min');

        $tpft_agric_r = $this->get_settingVal('tpft_agric_r');
        $tpft_agric_min = $this->get_settingVal('tpft_agric_min');
        $comp_agric_r = $this->get_settingVal('comp_agric_r');
        $comp_agric_min = $this->get_settingVal('comp_agric_min');

        $tpft_tanker_r = $this->get_settingVal('tpft_tanker_r');
        $tpft_tanker_min = $this->get_settingVal('tpft_tanker_min');
        $comp_tanker_r = $this->get_settingVal('comp_tanker_r');
        $comp_tanker_min = $this->get_settingVal('comp_tanker_min');

        //Special Vehicles category 2 : Ambulances and fire fighters
        $tpft_special2_r = $this->get_settingVal('tpft_special2_r');
        $tpft_special2_min = $this->get_settingVal('tpft_special2_min');
        $comp_special2_r = $this->get_settingVal('comp_special2_r');
        $comp_special2_min = $this->get_settingVal('comp_special2_min');

        //Special vehicles category 3 : cranes, excavators, forklift and roller
        $tpft_special3_r = $this->get_settingVal('tpft_special3_r');
        $tpft_special3_min = $this->get_settingVal('tpft_special3_min');
        $comp_special3_r = $this->get_settingVal('comp_special3_r');
        $comp_special3_min = $this->get_settingVal('comp_special3_min');

        $tpft_institutional_r = $this->get_settingVal('tpft_institutional_r');
        $tpft_institutional_min = $this->get_settingVal('tpft_institutional_min');
        $comp_institutional_r = $this->get_settingVal('comp_institutional_r');
        $comp_institutional_min = $this->get_settingVal('comp_institutional_min');

        $tpft_driving_private_r = $this->get_settingVal('tpft_driving_private_r');
        $tpft_driving_private_min = $this->get_settingVal('tpft_driving_private_min');
        $comp_driving_private_r = $this->get_settingVal('comp_driving_private_r');
        $comp_driving_private_min = $this->get_settingVal('comp_driving_private_min');

        $tpft_driving_heavy_r = $this->get_settingVal('tpft_driving_heavy_r');
        $tpft_driving_heavy_min = $this->get_settingVal('tpft_driving_heavy_min');
        $comp_driving_heavy_r = $this->get_settingVal('comp_driving_heavy_r');
        $comp_driving_heavy_min = $this->get_settingVal('comp_driving_heavy_min');

        $tpft_own_goods_r = $this->get_settingVal('tpft_own_goods_r');
        $tpft_own_goods_min = $this->get_settingVal('tpft_own_goods_min');
        $comp_own_goods_r = $this->get_settingVal('comp_own_goods_r');
        $comp_own_goods_min = $this->get_settingVal('comp_own_goods_min');

        $tpft_gen_cartage_r = $this->get_settingVal('tpft_gen_cartage_r');
        $tpft_gen_cartage_min = $this->get_settingVal('tpft_gen_cartage_min');
        $comp_gen_cartage_r = $this->get_settingVal('comp_gen_cartage_r');
        $comp_gen_cartage_min = $this->get_settingVal('comp_gen_cartage_min');

        $tpft_saloon_private_r = $this->get_settingVal('tpft_saloon_private_r');
        $tpft_saloon_private_min = $this->get_settingVal('tpft_saloon_private_min');
        $comp_saloon_private_r = $this->get_settingVal('comp_saloon_private_r');
        $comp_saloon_private_min = $this->get_settingVal('comp_saloon_private_min');

        $tpft_taxi_self_r = $this->get_settingVal('tpft_taxi_self_r');
        $tpft_taxi_self_min = $this->get_settingVal('tpft_taxi_self_min');
        $comp_taxi_self_r = $this->get_settingVal('comp_taxi_self_r');
        $comp_taxi_self_min = $this->get_settingVal('comp_taxi_self_min');

        $tpft_taxi_chauffer_r = $this->get_settingVal('tpft_taxi_chauffer_r');
        $tpft_taxi_chauffer_min = $this->get_settingVal('tpft_taxi_chauffer_min');
        $comp_taxi_chauffer_r = $this->get_settingVal('comp_taxi_chauffer_r');
        $comp_taxi_chauffer_min = $this->get_settingVal('comp_taxi_chauffer_min');

        $tpft_saloon_driving_school_r = $this->get_settingVal('$tpft_saloon_driving_school_r');
        $tpft_saloon_driving_school_min = $this->get_settingVal('tpft_saloon_driving_school_min');
        $comp_saloon_driving_school_r = $this->get_settingVal('comp_saloon_driving_school_r');
        $comp_saloon_driving_school_min = $this->get_settingVal('comp_saloon_driving_school_min');

        /*
        ** Optional benefit constants added from the extended settings
        */
        $bn_courtesy_car_rate = $this->get_settingVal('r_courtesy_car');

        if ($use_type == 'private' OR $veh_type == 'motorcycle') {
            $bn_excess_protector_rate = $this->get_settingVal('pr_excess_protector');
        } elseif ($use_type == 'own_goods' || $use_type == 'gen_cartage') {
            $bn_excess_protector_rate = $this->get_settingVal('pr_excess_protector');
        }

        $bn_fatal_pa_rate = $this->get_settingVal('r_fatal_pa');
        $bn_forced_atm_rate = $this->get_settingVal('r_forced_atm');
        $bn_loss_of_keys_rate = $this->get_settingVal('r_loss_of_keys');
        $bn_loss_of_spare_wheel_rate = $this->get_settingVal('r_loss_of_sparewheel');
        $bn_loss_personal_effects_rate = $this->get_settingVal('r_loss_personal_effects');
        $bn_out_of_station_rate = $this->get_settingVal('r_out_of_station');


        if ($use_type == 'private' OR $veh_type == 'motorcycle') {
            $bn_pvtr_rate = $this->get_settingVal('pr_pvtr');
        } elseif ($use_type == 'own_goods' || $use_type == 'gen_cartage') {
            $bn_pvtr_rate = $this->get_settingVal('pr_pvtr');
        }

        $bn_aa_rescue_rate = $this->get_settingVal('r_aa_rescue');

        //Limits
        $bn_forced_atm_limit = $this->get_settingVal('forced_atm_limit');
        $bn_out_of_station_limit = $this->get_settingVal('out_of_station_limit');

        if ($use_type == 'private' OR $veh_type == 'motorcycle') {
            $bn_fatal_pa_limit = $this->get_settingVal('pr_fatal_pa_limit');
        } elseif ($use_type == 'own_goods' || $use_type == 'gen_cartage') {
            $bn_fatal_pa_limit = $this->get_settingVal('cr_fatal_pa_limit');
        }

        $bn_loss_personal_effects_limit = $this->get_settingVal('loss_personal_effects_limit');

        // //Minimum values
        $bn_excess_protector_min = $this->get_settingVal('excess_protector_min');
        $bn_pvtr_min = $this->get_settingVal('pvtr_min');

        //Re-Assign values to some benefits from post that had no textboxes

        //Calculating the rates of the optional benefit selected
        if (!empty($bn_courtesy_car)) {
            $bn_courtesy_car = $bn_courtesy_car_rate;


            $return['benefits']['courtesy_car'] = $bn_courtesy_car;
        }

        if (isset($bn_excess_protector_rate) AND !empty($bn_excess_protector)) {
            $bn_excess_protector = ($bn_excess_protector_rate / 100) * $sum_insured;

            if ($bn_excess_protector < $bn_excess_protector_min) {
                $bn_excess_protector = $bn_excess_protector_min;
            }

            $return['benefits']['excess_protector'] = $bn_excess_protector;
        }

        if (!empty($bn_fatal_pa)) {
            $bn_fatal_pa = $bn_fatal_pa_rate; //($bn_fatal_pa_rate/100) * $bn_fatal_pa;
            $return['benefits']['fatal_pa'] = $bn_fatal_pa;
        }

        if (!empty($bn_forced_atm)) {
            $bn_forced_atm = ($bn_forced_atm_rate / 100) * $bn_forced_atm_limit;
            $return['benefits']['forced_atm'] = $bn_forced_atm;
        }

        if ($bn_loss_of_keys > 0) {
            $bn_loss_of_keys = ($bn_loss_of_keys_rate / 100) * $bn_loss_of_keys;
        }

        if ($bn_loss_of_spare_wheel > 0) {
            $bn_loss_of_spare_wheel = ($bn_loss_of_spare_wheel_rate / 100) * $bn_loss_of_spare_wheel;
        }

        if (!empty($bn_loss_personal_effects)) {
            $bn_loss_personal_effects = ($bn_loss_personal_effects_rate / 100) * $bn_loss_personal_effects_limit;
            $return['benefits']['loss_personal_effects'] = $bn_loss_personal_effects;
        }

        if (!empty($bn_out_of_station)) {
            $bn_out_of_station = ($bn_out_of_station_rate / 100) * $bn_out_of_station_limit;
            $return['benefits']['out_of_station'] = $bn_out_of_station;
        }

        if (isset($bn_pvtr_rate) AND !empty($bn_pvtr)) {
            $bn_pvtr = ($bn_pvtr_rate / 100) * $sum_insured;

            if ($bn_pvtr < $bn_pvtr_min) {
                $bn_pvtr = $bn_pvtr_min;
            }

            $return['benefits']['pvtr'] = $bn_pvtr;
        }

        if (!empty($bn_aa_rescue)) {
            $bn_aa_rescue = $bn_aa_rescue_rate;

            $return['benefits']['aa_rescue'] = $bn_aa_rescue;
        }


        /*Function to calculate tpft premiums
        //@parameters, the rate..minimum premium and sum insuerd
        */

        function help_premium($rate, $min, $sum_insured)
        {
            $p = ($rate / 100) * $sum_insured;
            if ($p < $min) {
                $p = $min;
            }

            return $p;
        }


        /*Function to calculate comprehensive premiums
        //@parameters, the rate..minimum premium and sum insuerd
        */

        function help_premium_comp($rate, $min, $sum_insured, $bn_courtesy_car, $bn_excess_protector, $bn_fatal_pa, $bn_forced_atm, $bn_loss_of_keys, $bn_loss_of_spare_wheel, $bn_loss_personal_effects, $bn_out_of_station, $bn_pvtr, $bn_aa_rescue = '')
        {
            $p = ($rate / 100) * $sum_insured;


            if ($p < $min) {
                $p = $min;
            }

            $benefits = (double)$bn_courtesy_car + (double)$bn_excess_protector + (double)$bn_fatal_pa + (double)$bn_forced_atm + (double)$bn_loss_of_keys + (double)$bn_loss_of_spare_wheel + (double)$bn_loss_personal_effects + (double)$bn_out_of_station + (double)$bn_pvtr;

            $b_premium = (double)$p + (double)$benefits;
            $b_premium_discounted = ($p * 0.95) + $benefits;
            $premium_discounted = $p * 0.95;


            return [
                'premium' => $p,
                'b_premium' => $b_premium,
                'b_premium_discounted' => $b_premium_discounted,
                'premium_discounted' => $premium_discounted
            ];
        }


        switch ($cover_type) {
            case 'tpo':

                if ($veh_type === 'motorcycle')//Special Vehicles category 4 : MotorCycles
                {
                    $premium = $tpo_motorcycle;

                } elseif ($veh_type === 'crane' ||
                    $veh_type === 'excavator' ||
                    $veh_type === 'forklift' ||
                    $veh_type === 'roller'
                ) {//Special vehicles category 3 : cranes, excavators, forklift and roller
                    $premium = $tpo_cranes;
                } elseif ($veh_type === 'ambulance' ||
                    $veh_type === 'firefighter'
                ) {//Special Vehicles category 2 : Ambulances and fire fighters
                    $premium = $tpo_ambulance;
                } elseif ($veh_type === 'harvestor' || $veh_type === 'tractor') {//Special Vehicles category 1 : Harvester and tractor
                    $premium = $tpo_agric;
                }

                //For tankers
                if ($veh_type === 'tanker' and !empty($weight)) {
                    if ($weight <= 10) {
                        $premium = $tpo_tanker10;
                    } elseif ($weight > 10 AND $weight <= 20) {
                        $premium = $tpo_tanker20;
                    } elseif ($weight > 20 AND $weight <= 30) {
                        $premium = $tpo_tanker30;
                    } elseif ($weight > 30) {
                        $premium = $tpo_tanker30 + (($weight - 30) * $tpo_tanker_perextra);
                    }
                }


                if ($veh_type == 'saloon') {
                    switch ($use_type) {
                        case 'private' :
                            $premium = $tpo_saloon_private;

                            break;

                        case 'taxi-self' :
                            $premium = $tpo_saloon_taxiself;

                            break;

                        case 'taxi-chauffer' :
                            if ($passengers > 0 && $passengers <= 9) {
                                $premium = ($tpo_saloon_taxichauffer9p) + ($tpo_saloon_taxi_pass_legal * $passengers);
                            } elseif ($passengers > 9 && $passengers <= 25) {
                                $premium = ($tpo_saloon_taxichauffer25p) + ($tpo_saloon_taxi_pass_legal * $passengers);
                            }

                            break;

                        case 'drivingskul'
                        :
                            $premium = $tpo_saloon_driving;

                            break;

                        default:

                            break;
                    }
                }

                if ($veh_type == 'lorry' || $veh_type == 'truck' || $veh_type == 'trailer' || ($veh_type == 'van' AND $use_type == 'own_goods')) {
                    switch ($use_type) {
                        case 'own_goods' :

                            if ($weight > 0 && $weight <= 3) {
                                $premium = $tpo_owngoods_weight3;
                            } elseif ($weight > 3 && $weight <= 8) {
                                $premium = $tpo_owngoods_weight8;
                            } elseif ($weight > 8 && $weight <= 10) {
                                $premium = $tpo_owngoods_weight10;
                            }

                            break;

                        case 'gen_cartage' :

                            if ($weight > 0 && $weight <= 3) {
                                $premium = $tpo_owngoods_weight3;
                            } elseif ($weight > 3 && $weight <= 8) {
                                $premium = $tpo_owngoods_weight8;
                            } elseif ($weight > 8 && $weight <= 20) {
                                $premium = $tpo_gencartage_weight20;
                            } elseif ($weight > 20 && $weight <= 30) {
                                $premium = $tpo_gencartage_weight30;
                            } elseif ($weight > 30) {
                                $extra_w = $weight - 30;
                                $premium = $tpo_gencartage_weight30 + ($extra_w * $tpo_gencartage_perextra);
                            }

                            break;

                        case 'driv_skul' :
                            if ($weight > 0 && $weight <= 7) {
                                $premium = $driv_skul_heavy7;
                            } elseif ($weight > 7) {
                                $premium = $driv_skul_heavy_over7;
                            }

                            break;
                    }

                }

                if ($veh_type == 'bus' || $veh_type == 'van') {
                    switch ($use_type) {
                        case 'institutional' :
                            if ($passengers > 0 && $passengers <= 9) {
                                $premium = 7500;
                            } elseif ($passengers > 9 && $passengers <= 25) {
                                $premium = 15000;
                            } elseif ($passengers > 25) {
                                $premium = 20000 + (500 * $passengers);
                            }

                            break;

                        case 'driv_skul' :

                            $premium = 10000;

                            break;
                    }
                }

                break;

            case 'tpft':

                if ($veh_type == 'motorcycle') {
                    $premium = help_premium($tpft_motorcycle_r, $tpft_motorcycle_min, $sum_insured);
                } elseif ($veh_type == 'harvester' || $veh_type == 'tractor') {
                    $premium = help_premium($tpft_agric_r, $tpft_agric_min, $sum_insured);

                } elseif ($veh_type == 'tanker') {
                    $premium = help_premium($tpft_tanker_r, $tpft_tanker_min, $sum_insured);
                } elseif ($veh_type == 'crane' || $veh_type == 'forklift' || $veh_type == 'roller' || $veh_type == 'excavator') {//Special Vehicles category 3

                    $premium = help_premium($tpft_special3_r, $tpft_special3_min, $sum_insured);

                } elseif ($veh_type == 'ambulance' || $veh_type == 'firefighter') {//Special Vehicles category 2 : Ambulances and fire fighters

                    $premium = help_premium($tpft_special2_r, $tpft_special2_min, $sum_insured);

                } elseif ($veh_type == 'bus' || ($veh_type == 'van' AND $use_type != 'own_goods')) {
                    switch ($use_type) {
                        case 'institutional' :
                            $premium = help_premium($tpft_institutional_r, $tpft_institutional_min, $sum_insured);
                            break;

                        case 'driv_skul' :
                            $premium = help_premium($tpft_driving_private_r, $tpft_driving_private_min, $sum_insured);
                            break;
                    }

                } elseif ($veh_type == 'lorry' || $veh_type == 'trailer' || $veh_type == 'truck' || ($veh_type == 'van' AND $use_type == 'own_goods')) {
                    switch ($use_type) {
                        case 'own_goods' :

                            $premium = help_premium($tpft_own_goods_r, $tpft_own_goods_min, $sum_insured);
                            break;

                        case 'gen_cartage' :

                            $premium = help_premium($tpft_gen_cartage_r, $tpft_gen_cartage_min, $sum_insured);
                            break;

                        case 'driv_skul' :
                            $premium = help_premium($tpft_driving_heavy_r, $tpft_driving_heavy_min, $sum_insured);

                            break;
                    }
                } elseif ($veh_type == 'saloon') {
                    switch ($use_type) {
                        case 'private' :

                            $premium = help_premium($tpft_saloon_private_r, $tpft_saloon_private_min, $sum_insured);

                            break;

                        case 'taxi-self' :

                            $premium = help_premium($tpft_taxi_self_r, $tpft_taxi_self_min, $sum_insured);

                            break;

                        case 'taxi-chauffer' :

                            $premium = help_premium($tpft_taxi_chauffer_r, $tpft_taxi_chauffer_min, $sum_insured);

                            break;

                        case 'drivingskul'  :

                            $premium = help_premium($tpft_saloon_driving_school_r, $tpft_saloon_driving_school_min, $sum_insured);

                            break;

                        default:

                            break;
                    }
                }

                break;

            case 'comprehensive':

                $benefits = array();
                $comp_veh_rate = 0;
                $comp_veh_min = 0;

                if ($veh_type == 'motorcycle') {
                    $comp_veh_rate = $comp_motorcycle_r;
                    $comp_veh_min = $comp_motorcycle_min;
                } elseif ($veh_type == 'harvester' || $veh_type == 'tractor') {
                    $comp_veh_rate = $comp_agric_r;
                    $comp_veh_min = $comp_agric_min;

                } elseif ($veh_type == 'tanker') {
                    $comp_veh_rate = $comp_tanker_r;
                    $comp_veh_min = $comp_tanker_min;
                    //$premium = help_premium_comp($comp_tanker_r,,$sum_insured, $bn_courtesy_car, $bn_excess_protector, $bn_fatal_pa, $bn_forced_atm, $bn_loss_of_keys, $bn_loss_of_spare_wheel, $bn_loss_personal_effects, $bn_out_of_station, $bn_pvtr);
                } elseif ($veh_type == 'crane' || $veh_type == 'forklift' || $veh_type == 'roller' || $veh_type == 'excavator') {//Special Vehicles category 3

                    $comp_veh_rate = $comp_special3_r;
                    $comp_veh_min = $comp_special3_min;
                    //$premium = help_premium_comp(,$comp_special3_min,$sum_insured, $bn_courtesy_car, $bn_excess_protector, $bn_fatal_pa, $bn_forced_atm, $bn_loss_of_keys, $bn_loss_of_spare_wheel, $bn_loss_personal_effects, $bn_out_of_station, $bn_pvtr);

                } elseif ($veh_type == 'ambulance' || $veh_type == 'firefighter') {//Special Vehicles category 2 : Ambulances and fire fighters
                    $comp_veh_rate = $comp_special2_r;
                    $comp_veh_min = $comp_special2_min;
                    //$premium = help_premium_comp(,,$sum_insured, $bn_courtesy_car, $bn_excess_protector, $bn_fatal_pa, $bn_forced_atm, $bn_loss_of_keys, $bn_loss_of_spare_wheel, $bn_loss_personal_effects, $bn_out_of_station, $bn_pvtr);

                } elseif ($veh_type == 'bus' || ($veh_type == 'van' AND $use_type != 'own_goods')) {
                    switch ($use_type) {
                        case 'institutional' :
                            $comp_veh_rate = $comp_institutional_r;
                            $comp_veh_min = $comp_institutional_min;
                            //$premium = help_premium_comp(,,$sum_insured, $bn_courtesy_car, $bn_excess_protector, $bn_fatal_pa, $bn_forced_atm, $bn_loss_of_keys, $bn_loss_of_spare_wheel, $bn_loss_personal_effects, $bn_out_of_station, $bn_pvtr);
                            break;

                        case 'driv_skul' :
                            $comp_veh_rate = $comp_driving_private_r;
                            $comp_veh_min = $comp_driving_private_min;
                            //$premium = help_premium_comp($comp_driving_private_r,$comp_driving_private_min,$sum_insured, $bn_courtesy_car, $bn_excess_protector, $bn_fatal_pa, $bn_forced_atm, $bn_loss_of_keys, $bn_loss_of_spare_wheel, $bn_loss_personal_effects, $bn_out_of_station, $bn_pvtr);
                            break;
                    }

                } elseif ($veh_type == 'lorry' || $veh_type == 'trailer' || $veh_type == 'truck' || ($veh_type == 'van' AND $use_type == 'own_goods')) {
                    switch ($use_type) {
                        case 'own_goods' :
                            $comp_veh_rate = $comp_own_goods_r;
                            $comp_veh_min = $comp_own_goods_min;
                            //$premium = help_premium_comp(,,$sum_insured, $bn_courtesy_car, $bn_excess_protector, $bn_fatal_pa, $bn_forced_atm, $bn_loss_of_keys, $bn_loss_of_spare_wheel, $bn_loss_personal_effects, $bn_out_of_station, $bn_pvtr, $bn_aa_rescue);
                            break;

                        case 'gen_cartage' :
                            $comp_veh_rate = $comp_gen_cartage_r;
                            $comp_veh_min = $comp_gen_cartage_min;
                            //$premium = help_premium_comp(,,$sum_insured, $bn_courtesy_car, $bn_excess_protector, $bn_fatal_pa, $bn_forced_atm, $bn_loss_of_keys, $bn_loss_of_spare_wheel, $bn_loss_personal_effects, $bn_out_of_station, $bn_pvtr, $bn_aa_rescue);
                            break;

                        case 'driv_skul' :
                            $comp_veh_rate = $comp_driving_heavy_r;
                            $comp_veh_min = $comp_driving_heavy_min;
                            //$premium = help_premium_comp( $comp_driving_heavy_r, $comp_driving_heavy_min,$sum_insured, $bn_courtesy_car, $bn_excess_protector, $bn_fatal_pa, $bn_forced_atm, $bn_loss_of_keys, $bn_loss_of_spare_wheel, $bn_loss_personal_effects, $bn_out_of_station, $bn_pvtr);

                            break;
                    }
                } elseif ($veh_type == 'saloon') {
                    switch ($use_type) {
                        case 'private' :
                            $comp_veh_rate = $comp_saloon_private_r;
                            $comp_veh_min = $comp_saloon_private_min;
                            $premium = help_premium_comp($comp_veh_rate, $comp_veh_min, $sum_insured, $bn_courtesy_car, $bn_excess_protector, $bn_fatal_pa, $bn_forced_atm, $bn_loss_of_keys, $bn_loss_of_spare_wheel, $bn_loss_personal_effects, $bn_out_of_station, $bn_pvtr, $bn_aa_rescue);
                            // help_premium_comp($rate,$min,$sum_insured,  $bn_courtesy_car, $bn_excess_protector, $bn_fatal_pa, $bn_forced_atm, $bn_loss_of_keys, $bn_loss_of_spare_wheel, $bn_loss_personal_effects, $bn_out_of_station, $bn_pvtr, $bn_aa_rescue='')

                            break;

                        case 'taxi-self' :
                            $comp_veh_rate = $comp_taxi_self_r;
                            $comp_veh_min = $comp_taxi_self_min;
                            // $premium = help_premium_comp(,,$sum_insured, $bn_courtesy_car, $bn_excess_protector, $bn_fatal_pa, $bn_forced_atm, $bn_loss_of_keys, $bn_loss_of_spare_wheel, $bn_loss_personal_effects, $bn_out_of_station, $bn_pvtr);

                            break;

                        case 'taxi-chauffer' :

                            $comp_veh_rate = $comp_taxi_chauffer_r;
                            $comp_veh_min = $comp_taxi_chauffer_min;
                            //$premium = help_premium_comp(,,$sum_insured, $bn_courtesy_car, $bn_excess_protector, $bn_fatal_pa, $bn_forced_atm, $bn_loss_of_keys, $bn_loss_of_spare_wheel, $bn_loss_personal_effects, $bn_out_of_station, $bn_pvtr);

                            break;

                        case 'drivingskul'  :
                            $comp_veh_rate = $comp_saloon_driving_school_r;
                            $comp_veh_min = $comp_saloon_driving_school_min;
                            //$premium = help_premium_comp(,,$sum_insured, $bn_courtesy_car, $bn_excess_protector, $bn_fatal_pa, $bn_forced_atm, $bn_loss_of_keys, $bn_loss_of_spare_wheel, $bn_loss_personal_effects, $bn_out_of_station, $bn_pvtr);

                            break;

                        default:

                            break;
                    }
                }

                $data = help_premium_comp($comp_veh_rate, $comp_veh_min, $sum_insured, $bn_courtesy_car, $bn_excess_protector, $bn_fatal_pa, $bn_forced_atm, $bn_loss_of_keys, $bn_loss_of_spare_wheel, $bn_loss_personal_effects, $bn_out_of_station, $bn_pvtr);


                $premium = $data['premium'];

                $return['b_premium'] = $data['b_premium'];
                $return['b_premium_discounted'] = $data['b_premium_discounted'];
                $return['cover_type'] = $cover_type;
                $return['phcf'] = (double)(0.25 / 100) * $data['b_premium'];
                $return['training_levy'] = (double)(0.2 / 100) * $data['b_premium'];
                $return['stamp_duty'] = 40;
                $return['total_premium'] = ($data['b_premium'] + $return['training_levy'] + $return['stamp_duty'] + $return['phcf']);
                break;

            default:
                # code...
                break;
        }

        $return['premium'] = (double)$premium;

        //For motor tpo cover no discount applies
        if (isset($cover_type) && $cover_type != 'tpo') {
            $return['discounted_premium'] = (double)$premium * 0.95;
        }

        $return['cover_type'] = $cover_type;

        return $return;

    }


    /**
     * generate_quote
     *
     * generates values used to display quote
     *
     * @access  public
     * @param  type  name
     * @return  type
     */
    function generate_quote($params = null)
    {
        // print_r($params);die();

        if (isset($params['cover_type']) && $params['cover_type'] == 'tpo') {
            $premium = (double)$params['premium'];
        } else if (isset($params['cover_type']) && $params['cover_type'] == 'comprehensive') {
            $premium = (double)$params['b_premium'];
        } else {
            $premium = (double)$params['premium'];//$premium = (double)$params['premium'] * 0.95;//without benefits
        }

        //$premium = isset($params['discounted_premium']) ? (double)$params['discounted_premium'] : (double)$params['premium'];

        $b_premium = $premium;//(double)$params['b_premium_discounted'];

        $phcf = (double)(0.25 / 100) * $b_premium;
        $training_levy = (double)(0.2 / 100) * $b_premium;
        $stamp_duty = 40;

        $return = array();

        if (isset($params['b_premium'])) {
            //$b_premium = (double)$params['b_premium'];//Premium plus benefits

            $total_premium = $b_premium + $phcf + $training_levy + $stamp_duty;

            if (isset($params['benefits']['aa_rescue'])) {
                $total_premium += (double)$params['benefits']['aa_rescue'];
            }

            $return['b_premium'] = $b_premium;
        } else {
            $total_premium = $premium + $phcf + $training_levy + $stamp_duty;
        }


        $return = array_merge($return, [
            'total_premium' => $total_premium,
            'premium' => $premium,
            'phcf' => $phcf,
            'training_levy' => $training_levy,
            'stamp_duty' => $stamp_duty,

        ]);


        //Some reset value from post
        $return['benefits'] = array();

        if (isset($params['benefits']) AND !empty($params['benefits'])) {

            //array_merge($return, $params['benefits']);
            $return['benefits'] = $params['benefits'];

        }

        return $return;
    }


    public function getQuoteForPrivateCars(Request $request)
    {
//        $user = JWTAuth::parseToken()->authenticate();
//        $user_id = $user['id'];
        $user = User::where('account_type', 'web')->first();
        $user_id = $user->id;

        $year_of_manufacture = $request->manufacture_year;
        $car_value = $request->sum_insured;
        $benefits = json_decode($request->benefits);
        $cover_type = $request->cover_type;
//        $sub_class = $request->sub_class;
        $sub_class = $request->use_type;//usage type

        $b_premium = $this->getPremiumCarPrivate($year_of_manufacture, $car_value);

        if ($b_premium < 20000) {
            $b_premium = 20000;
        }

        if (!empty($benefits)) {
            $benefits = $this->getBenefitList($benefits, $car_value);
        }

        $total_benefit_costs = 0;
        $total_benefit_costs_taxable = 0;

        if (!empty($benefits)) {
            foreach ($benefits as $benefitCost) {
                if (trim($benefitCost['name']) != trim("AA 24 Hour Road Rescue")) {
                    $total_benefit_costs_taxable = $total_benefit_costs + $benefitCost['cost'];
                }
                $total_benefit_costs = $total_benefit_costs + $benefitCost['cost'];
            }
        }

        $pvtr = (double)(0.25 / 100) * ($b_premium + $total_benefit_costs_taxable);
        $levy = (double)(0.2 / 100) * ($b_premium + $total_benefit_costs_taxable);
        $stamp_duty = 40;

        $total_premium = ($b_premium + $levy + $stamp_duty + $pvtr + $total_benefit_costs);

        $quote_id = $this->generate_policy_number("motor", "saloon", $sub_class);
        $plan_id = $this->get_cover_type_id($cover_type);

        $quote_ref = $quote_id['quote_id'];

        $quote = new Quote();
        $quote->premium = $b_premium;
        $quote->quote_id = $quote_ref;
        $quote->premium = $b_premium;
        $quote->phcf = $pvtr;
        $quote->levy = $levy;
        $quote->duty = $stamp_duty;
        $quote->total_premium = $total_premium;
        $quote->status = 'quotation';
        $quote->product_id = 1;
        $quote->plan_id = $plan_id;
        $quote->user_id = "";
        $quote->id_pp = "";
        $quote->agent_id = $user_id;

        $quote->save();

        $lastInserted_id = $this->getQuote($quote_ref);

        if (!empty($benefits)) {
            foreach ($benefits as $benefit) {

                $benefit_name = $this->get_benefit_id($benefit['name']);

                $quoteBenefit = new QuoteBenefit();
                $quoteBenefit->quote_id = $lastInserted_id;
                $quoteBenefit->benefit_id = $benefit_name;
                $quoteBenefit->applied_limit = $benefit['limit'];
                $quoteBenefit->cost = $benefit['cost'];

                $quoteBenefit->save();
            }
        }

        //generate quote

        $pdf_data = [
            'plan' => $cover_type,
            'usage_type' => $sub_class,
            'year_of_manufacture' => $year_of_manufacture,
            'b_premium' => $b_premium,
            'b_premium_discounted' => $b_premium,
            'cover_type' => $cover_type,
            'training_levy' => $levy,
            'phcf' => $pvtr,
            'stamp_duty' => $stamp_duty,
            'total_premium' => $total_premium,
            'benefit_list' => $benefits,
            'quote_ref' => $quote_ref
        ];

        $pdf = PDF::loadView('agentsapp::private_car_quote', $pdf_data);

        $policy_no = str_replace("/", "_", $quote_ref);

        $doc_name = "private_car_quote_for_'.$policy_no.'.pdf'";

        if (File::exists(public_path('agentsapp/' . $doc_name . '.pdf'))) {
            File::delete(public_path('agentsapp/' . $doc_name . '.pdf'));
        }

        $pdf->save('agentsapp/private_car_quote_for_' . $policy_no . '.pdf');

        $path = url('agentsapp/private_car_quote_for_' . $policy_no . '.pdf');


        $data = [
            'b_premium' => $b_premium,
            'b_premium_discounted' => $b_premium,
            'cover_type' => $cover_type,
            'training_levy' => $levy,
            'phcf' => $pvtr,
            'stamp_duty' => $stamp_duty,
            'total_premium' => $total_premium,
            'benefit_list' => $benefits,
            'quote_ref' => $quote_ref,
            'quote_id' => $lastInserted_id,
            'quote_link' => $path
        ];

        return response()->json([
            'data' => $data
        ]);
    }

    private function get_cover_type_id($cover_name)
    {
        $cover_id = Plan::select('id')
            ->where('name', '<=', $cover_name)
            ->value('id');

        return $cover_id;

    }


    private function get_benefit_id($name)
    {
        $id = Benefit::select('id')
            ->where('name', '=', $name)
            ->value('id');

        return $id;

    }


    private function getQuote($quote_id)
    {

        $id = Quote::select('id')
            ->where('quote_id', '=', $quote_id)
            ->value('id');

        return $id;

    }

    private function getBenefitList($benefitFromApp, $car_value)
    {
        $benefits = array();

        foreach ($benefitFromApp as $benefitIds) {

            $benefitArray = ((Array)$benefitIds);

            $benefitId = $benefitArray['id'];

            $limit_amount = $benefitArray['limitAmount'];
            $limit_changed = true;

            $benefit = Benefit::select('name', 'category', 'benefit_limit', 'rate', 'rate_application')
                ->where("id", $benefitId)->first();

            if ($limit_amount <= $benefit->benefit_limit) {
                $limit_amount = $benefit->benefit_limit;
                $limit_changed = false;
            }

            $cost = $this->calculateRateBenefits($benefit->benefit_limit, $limit_amount, $benefit->rate, $benefit->category, $benefit->rate_application, $car_value, $limit_changed);
            $benefitArray = array("name" => $benefit->name, "limit" => $limit_amount, "cost" => $cost);
            $benefits[] = $benefitArray;
        }
        return $benefits;
    }


    private function getPremiumCarPrivate($year, $car_value)
    {
        $age_id = $this->getAgeBracket($year);

        // echo $age_id;
        $rates = MotorRate::select("rate", "min_sum_assured", "max_sum_assured", "has_no_max")
            ->where('age_bracket_id', '=', $age_id)
            ->get();

        $rate_to_use = "";

        foreach ($rates as $rate) {
            if ($rate->has_no_max) {

                if ($car_value >= $rate->min_sum_assured) {
                    $rate_to_use = $rate->rate;
                }
            } else {
                if ($car_value >= $rate->min_sum_assured && $car_value <= $rate->max_sum_assured) {
                    $rate_to_use = $rate->rate;
                }
            }
        }

        return (($rate_to_use / 100) * $car_value);
    }


    private function getAgeBracket($year)
    {
        $car_age = (date('Y') - $year);

        $age_bracket_id = AgeBracket::select('id')
            ->where('minimum_years', '<=', $car_age)
            ->where('max_years', '>=', $car_age)
            ->value('id');

        return $age_bracket_id;
    }


    private function calculateRateBenefits($benefit_limit, $limit_amount, $benefit_rate, $category, $rate_application, $car_value, $limit_changed)
    {
        if (($limit_changed && trim($category) == trim("Standard")) || (trim($category) == trim("Optional"))) {
            if (trim($rate_application) == trim("ON LIMIT")) {
                $amount = ($benefit_rate / 100) * $limit_amount;
                if ($amount > 3000) {
                    return $amount;
                } else {
                    return 3000;
                }
            } else if (trim($rate_application) == trim("ON SUM INSURED")) {
                $amt = ($benefit_rate / 100) * $car_value;
                if ($amt < 2500) {
                    return 2500;
                } else {
                    return ($benefit_rate / 100) * $car_value;
                }
            } else if (trim($rate_application) == trim("ON EXTRA")) {
                if ($limit_changed) {
                    $extra = ($limit_amount - $benefit_limit);
                    return ($benefit_rate / 100) * $extra;
                } else {
                    return 0;
                }
            } else if (trim($rate_application) == trim("FLATRATE")) {
                return $benefit_rate;
            }
        } else {
            return 0;
        }
    }


    public function sendUserMailAndSaveQuote(Request $request)
    {
//        $user = JWTAuth::parseToken()->authenticate();
//        $user_id = $user['id'];
        $user = User::where('account_type', 'web')->first();
        $user_id = $user->id;

        //create a username for them
        $username = substr($request->firstname, 0, 3) . "_" . $request->lastname;

        //create a password for them
        $password = str_random(5);

        //Call the function to auto register a customer that returns user object
        $client = $this->autoRegister($request, $username, $password);

        //add the path to the quote
        $quote['pdf_path'] = public_path('agentsapp/policy_wordings.pdf');

    }

    public function typeOfCovers(){
        $plans = Plan::select('name','details as cover_description','id as cover_id')->get();
        return response()->json($plans);
    }
    public function vehicleTypes(){
        $data = VehicleType::select('name','value as vehicle_value','id as vehicle_type_id')->get();
        return response()->json($data);
    }
    public function vehicleUsageTypes(){
        $data = MotorUsageType::select('name','value as usage_value','id as vehicle_usage_type_id')->get();
        return response()->json($data);
    }
    public function coverBenefits(){
        $data = Benefit::select('name','slug','description')->where('product_id',1)->get();
        return response()->json($data);
    }

}
