<?php

namespace jick\buyonline;

use Illuminate\Support\ServiceProvider;

class BuyonlineServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
      $this->loadViewsFrom(__DIR__.'/views', 'buyonline');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //Load controllers
        $this->app->make('Jick\buyonline\controllers\MotorSettingsController');
        $this->app->make('Jick\buyonline\controllers\QuoteController');
        $this->app->make('Jick\buyonline\controllers\WebQuoteController');

        //Load models
        $this->app->make('Jick\buyonline\models\Benefit');
        $this->app->make('Jick\buyonline\models\Quote');
        $this->app->make('Jick\buyonline\models\MotorUsageType');
        $this->app->make('Jick\buyonline\models\Product');
        $this->app->make('Jick\buyonline\models\VehicleType');

        include __DIR__.'/routes.php';
    }
}
