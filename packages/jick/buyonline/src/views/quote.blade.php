<!DOCTYPE html><html lang="en">
<head>    <meta charset="UTF-8">
  <title>Quote</title>
  <style>
  table, p { margin: 15px 0; font-family: Verdana, sans-serif; }
  p { padding: 10px; }
  table td { padding: 10px; }
  table, tr { width: 100%; }
  table.w-30 td { width: 35%; }
  table thead.w-50 td { width: 50%; }
  .text-center { text-align: center; }
  td.text-uppercase { text-transform: uppercase; color: #BA0C2F; }
  td.text-right { text-align: right; }
  td.fs-28 { font-size: 28px; }
  tr.title { background: #BA0C2F; color: #FFF; }
  td.cap { text-transform: capitalize; }
  .description { border: 1px solid #BA0C2F; }
  .description h5 { background: #BA0C2F; padding: 10px; margin-top: 0; }
  </style>
</head>
<body>
  <table>
    <thead class="w-50">
      <tr>
        <td>
          <img src='/var/www/site/img/logo.png' alt="">        
        </td>        <td class="text-uppercase text-right  fs-28">QUOTE ESTIMATE</td>    </tr>    </thead>    <tbody>    <tr><td></td><td class="text-right">Quote Number: <?php echo $quotation['policy_no']; ?> </td></tr>    <tr><td></td><td class="text-right">Date: <?php echo $quotation['date']; ?></td></tr>    <tr class="title"><td>From</td><td></td></tr>    <tr><td>Jubilee Insurance Company of Kenya</td><td></td></tr>    <tr><td>P.O BOX 30376-00100</td><td></td></tr>    <tr><td>Wabera St, Nairobi Kenya</td><td></td></tr>    <tr><td>0709949000</td><td></td></tr>    </tbody></table><div class="description">    <h5>Description</h5>    <p>        The quote provided below is for the motor insurance package for <?php echo $quotation['name']; ?>    </p></div><table class="w-30">    <thead>    </thead>    <tbody>    <tr class="title"><td>Quote Details</td><td>Breakdown</td></tr>    <tr><td><strong>Type of Cover</strong></td><td class="cap"><?php echo $quotation['plan_name_full']; ?></td></tr>    <tr><td><strong>Basic Premium</strong></td><td><?php echo number_format($quotation['premium']); ?></td></tr>    <tr><td><strong>PHCF</strong></td><td><?php echo number_format($quotation['phcf']); ?></td></tr>    <tr><td><strong>Training Levy</strong></td><td><?php echo number_format($quotation['training_levy']); ?></td></tr>    <tr><td><strong>Stamp Duty</strong></td><td><?php echo number_format($quotation['stamp_duty']); ?></td></tr>    <tr class="grey"><td><strong>Gross Premium Estimate</strong></td><td><?php echo number_format($quotation['total_premium']); ?></td></tr>    </tbody></table><div class="description">    <h5>Additional Benefits</h5>    <div style="padding-left: 10px; ">        <?php echo $quotation['pvtr']; ?>        <?php echo $quotation['oos']; ?>        <?php echo $quotation['fa']; ?>        <?php echo $quotation['lpe']; ?>        <?php echo $quotation['aa']; ?>        <?php echo $quotation['cc']; ?>        <?php echo $quotation['fp']; ?>    </div></div><p><strong>Note</strong>: This estimate is not a contract or a bill. It is provides the estimated amount before valuation.    Thus the value might change based on the valuation report.</p><p class="text-center"><strong>To schedule for your car valuation, please contact us</strong></p></body></html>
