<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Quote</title>
    <style>        table, p {
            margin: 15px 0;
            font-family: Verdana, sans-serif;
        }

        p {
            padding: 10px;
        }

        table td {
            padding: 10px;
        }

        table, tr {
            width: 100%;
        }

        table.w-30 td {
            width: 35%;
        }

        table thead.w-50 td {
            width: 50%;
        }

        .text-center {
            text-align: center;
        }

        td.text-uppercase {
            text-transform: uppercase;
            color: #BA0C2F;
        }

        td.text-right {
            text-align: right;
        }

        td.fs-28 {
            font-size: 28px;
        }

        tr.mt-30 {
            margin-top: 30px;
        }

        tr.title {
            background: #BA0C2F;
            color: #FFF;
        }

        tr.grey {
            background: #EEE;
        }

        td.cap {
            text-transform: capitalize;
        }

        .description {
            border: 1px solid #BA0C2F;
        }

        .description h5 {
            background: #BA0C2F;
            padding: 10px;
            margin-top: 0;
        }    </style>
</head>
<body>
<table>
    <thead class="w-50">
    <tr>
        <td><img src='/var/www/site/img/logo.png' alt=""></td>
        <td class="text-uppercase text-right  fs-28">QUOTE ESTIMATE</td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td></td>
        <td class="text-right">Quote Number: {{ $quote }} </td>
    </tr>
    <tr>
        <td></td>
        <td class="text-right">Date: {{ $date }} </td>
    </tr>
    <tr class="title">
        <td>From</td>
    </tr>
    <tr>
        <td>Jubilee Insurance Company of Kenya</td>
        <td></td>
    </tr>
    <tr>
        <td>P.O BOX 30376-00100</td>
        <td></td>
    </tr>
    <tr>
        <td>Wabera St, Nairobi Kenya</td>
        <td></td>
    </tr>
    <tr>
        <td>0709949000</td>
        <td></td>
    </tr>
    </tbody>
</table>
<table class="w-30">
    <thead></thead>
    <tbody>
    <tr class="title">
        <td>Client Details</td>
        <td>Agent Details</td>
    </tr>
    <tr>
        <td><strong>Full Name</strong>: {{ $name }}</td>
        <td><strong>Name</strong>: {{ $agent_name }}</td>
    </tr>
    <tr>
        <td><strong>Date of Birth</strong>: {{ $dob }}</td>
        <td><strong>Code</strong>: {{ $agent_code }}</td>
    </tr>
    <tr>
        <td><strong>Gender</strong>: {{ $gender }}</td>
        <td><strong>Tel No</strong>: {{ $agent_number }}</td>
    </tr>
    <tr>
        <td><strong>Residential Address</strong>: {{ $residential }}</td>
        <td><strong>Email</strong>: {{ $agent_email }}</td>
    </tr>
    <tr>
        <td><strong>Nationality</strong>: {{ $nationality }}</td>
        <td></td>
    </tr>
    <tr>
        <td><strong>Tel No</strong>: {{ $tel }}</td>
        <td></td>
    </tr>
    <tr>
        <td><strong>Email</strong>: {{ $email }}</td>
        <td></td>
    </tr>
    </tbody>
</table>
<table class="w-30 details">
    <thead></thead>
    <tbody>
    <tr class="title">
        <td>Car Details</td>
    </tr>
    <tr>
        <td><strong>Estimated Market Value</strong>: {{ $estimated }}</td>
    </tr>
    <tr>
        <td class="cap"><strong>Type</strong>: {{ $type }}</td>
    </tr>
    <tr>
        <td class="cap"><strong>Primary Use</strong>: {{ $use }}</td>
    </tr>
    <tr>
        <td><strong>Owner</strong>: {{ $name }}</td>
    </tr>
    <tr>
        <td><strong>Registration No</strong>: {{ $reg_no }}</td>
    </tr>
    <tr>
        <td class="cap"><strong>Make</strong>: {{ $make }}</td>
    </tr>
    <tr>
        <td><strong>Model</strong>: {{ $model }}</td>
    </tr>
    </tbody>
</table>
<table class="w-30">
    <thead></thead>
    <tbody>
    <tr class="title">
        <td>Quote Details</td>
        <td>Breakdown</td>
    </tr>
    <tr>
        <td><strong>Type of Cover</strong>:</td>
        <td class="cap">{{ $cover }}</td>
    </tr>
    <tr>
        <td><strong>Basic Premium</strong></td>
        <td>{{ $premium }}</td>
    </tr>
    <tr>
        <td><strong>PHCF</strong></td>
        <td>{{ $phcf }}</td>
    </tr>
    <tr>
        <td><strong>Training Levy</strong></td>
        <td>{{ $training }}</td>
    </tr>
    <tr>
        <td><strong>Stamp Duty</strong></td>
        <td>{{ $stamp }}</td>
    </tr>
    <tr class="grey">
        <td><strong>Gross Premium Estimate</strong></td>
        <td><strong>{{ $amount }}</strong></td>
    </tr>
    </tbody>
</table>
<div class="description"><h5>Additional Benefits</h5>
    <div style="padding-left: 10px; ">
        {{ $pvtr }}
        {{ $oos }}
        {{ $fa }}
        {{ $lpe }}
        {{ $aa }}
        {{ $cc }}
        {{ $fp }}
        {{ $ep }}
    </div>
</div>
<p><strong>Note</strong>: This estimate is not a contract or a bill. It is provides the estimated amount before
    valuation. Thus the value might change based on the valuation report.</p>
<p class="text-center"><strong>To schedule for your car valuation, please contact us</strong></p></body>
</html>
