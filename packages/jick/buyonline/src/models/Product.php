<?php
namespace Jick\buyonline\models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {
    protected $fillable = ['name'];
}