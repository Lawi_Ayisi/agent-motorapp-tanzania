<?php
namespace Jick\buyonline\models;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $fillable = [
        'id',
        'quote_id',
        'premium',
        'phcf',
        'levy',
        'duty',
        'total_premium',
        'status',
        'product_id',
        'plan_id',
        'user_id'
    ];

}