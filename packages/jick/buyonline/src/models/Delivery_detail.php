<?php
namespace Jick\buyonline\models;

use Illuminate\Database\Eloquent\Model;

class Delivery_detail extends Model
{

    protected $fillable = [
        'physical_address',
        'ppstal_address',
        'delivery_type',
        'quote_id'
    ];

}