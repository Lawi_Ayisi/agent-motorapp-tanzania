<?php
namespace Jick\buyonline\models;

use Illuminate\Database\Eloquent\Model;

class Benefit extends Model
{
    protected $fillable = ['name', 'description', 'product_id'];
}