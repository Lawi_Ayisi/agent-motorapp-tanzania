<?php
namespace Jick\buyonline\models;

use Illuminate\Database\Eloquent\Model;

class MotorUsageType extends Model
{
    protected $fillable = ['name', 'value'];
}