<?php
namespace Jick\buyonline\models;

use Illuminate\Database\Eloquent\Model;

class MotorRate extends Model {
    protected $fillable = ['min_sum_assured','max_sum_assured','rate'];
}