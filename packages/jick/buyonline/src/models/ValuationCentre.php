<?php
namespace Jick\buyonline\models;

use Illuminate\Database\Eloquent\Model;

class ValuationCentre extends Model
{
    protected $fillable = [
        'name',
        'address',
        'longitude',
        'latitude',
        'type'
    ];
}