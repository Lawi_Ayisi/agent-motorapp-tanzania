<?php
namespace Jick\buyonline\models;

use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{

    protected $fillable = ['name', 'value'];

    public function usageTypes() {

        return $this->belongsToMany(
            'Jick\buyonline\models\MotorUsageType',
            'motor_vehicle_usage_types'
        );
    }
}