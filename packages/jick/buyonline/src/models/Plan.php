<?php
namespace Jick\buyonline\models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model {
    protected $fillable = ['name'];
}