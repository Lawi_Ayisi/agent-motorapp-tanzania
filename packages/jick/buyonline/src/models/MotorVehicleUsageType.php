<?php
namespace Jick\buyonline\models;

use Illuminate\Database\Eloquent\Model;

class MotorVehicleUsageType extends Model {

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = ['vehicle_type_id', 'motor_usage_type_id'];
}