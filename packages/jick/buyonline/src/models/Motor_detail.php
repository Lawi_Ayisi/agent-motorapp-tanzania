<?php
namespace Jick\buyonline\models;

use Illuminate\Database\Eloquent\Model;

class Motor_detail extends Model {
    protected $fillable = [
        'make',
        'model',
        'purchase_year',
        'cover_start',
        'quote_id'
    ];
}