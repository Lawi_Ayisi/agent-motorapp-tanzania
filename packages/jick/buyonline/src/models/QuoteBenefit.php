<?php
/**
 * Created by PhpStorm.
 * User: Evans.Wanyama
 * Date: 7/6/2017
 * Time: 6:43 PM
 */

namespace Jick\buyonline\models;
use Illuminate\Database\Eloquent\Model;


class QuoteBenefit extends Model{

    protected $fillable = [
        'quote_id',
        'benefit_id',
        'applied_limit',
        'cost'
    ];

} 