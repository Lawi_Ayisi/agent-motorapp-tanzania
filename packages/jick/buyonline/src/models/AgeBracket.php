<?php
namespace Jick\buyonline\models;

use Illuminate\Database\Eloquent\Model;

class AgeBracket extends Model
{
    protected $fillable = ['label'];
}