<?php

//Protected routes
Route::group(['middleware' => 'jwt.auth', 'prefix' => 'api/'], function () {

    Route::post('settings/motor-usage-type', 'jick\buyonline\controllers\MotorSettingsController@saveUsageType');
    Route::post('settings/veh-type', 'jick\buyonline\controllers\MotorSettingsController@saveVehicleType');
    Route::post('confirm-code', 'jick\buyonline\controllers\QuoteController@confirmCode');
    Route::post('save_quote', 'jick\buyonline\controllers\QuoteController@sendUserMailAndSaveQuote');

    //Product benefits
    Route::post('settings/product-benefits', 'jick\buyonline\controllers\MotorSettingsController@saveBenefit');

    //List all quotes for a particular logged in user
    Route::get('myquotes', 'jick\buyonline\controllers\QuoteController@list_quotes');
    Route::post('/premium-calculator', 'jick\buyonline\controllers\QuoteController@premiumCalculator');

    //


});

Route::group(['prefix' => 'api/'], function () {
    Route::get('veh-type', 'jick\buyonline\controllers\MotorSettingsController@getVehicleTypes');
    Route::get('usage-type/{id}', 'jick\buyonline\controllers\MotorSettingsController@getUsageTypes');
    Route::get('motor-usage-type', 'jick\buyonline\controllers\MotorSettingsController@getUsageTypes');
    Route::get('product-benefits/{productName}', 'jick\buyonline\controllers\MotorSettingsController@getBenefits');

    Route::post('delivery_details', 'jick\buyonline\controllers\QuoteController@delivery_details');
    Route::post('motor_details', 'jick\buyonline\controllers\QuoteController@save_extra_details');
    Route::get('all-quotes', 'jick\buyonline\controllers\QuoteController@all_quotes');
    Route::resource('quote', 'jick\buyonline\controllers\QuoteController');
    Route::post('quote-car-private', 'jick\buyonline\controllers\QuoteController@getQuoteForPrivateCars');

    //test send email to underwriters
    Route::post('email-underwriters','jick\buyonline\controllers\QuoteController@emailUnderwriters');
    Route::post('email-underwriters2','jick\files\controllers\FileController@sendEmailToUnderwriters');
});


Route::group(['prefix' => 'api/web/'], function () {
    Route::resource('quote', 'jick\buyonline\controllers\WebQuoteController');
    Route::post('quote-car-private', 'jick\buyonline\controllers\WebQuoteController@getQuoteForPrivateCars');

    Route::post('cover-types', 'jick\buyonline\controllers\WebQuoteController@typeOfCovers');//type of covers
    Route::post('vehicle-types', 'jick\buyonline\controllers\WebQuoteController@vehicleTypes');//vehicle types
    Route::post('vehicle-usage-types', 'jick\buyonline\controllers\WebQuoteController@vehicleUsageTypes');//type of covers
    Route::post('benefits', 'jick\buyonline\controllers\WebQuoteController@coverBenefits');//tbenefits
});

Route::post('user/make-web-agent', function () {
    $email = "web@jubileekenya.com";
    $agent_code = 998870;
    $password = "jubilee123!";

    $user = \Jick\users\models\User::firstOrNew(['email' => $email]);
    $user->firstname = "web";
    $user->lastname = "web";
    $user->username = "web";
    $user->email = $email;
    $user->phone = "";
    $user->confirmation_code = null;
    $user->account_type = "web";
    $user->active = 1;
    $user->password = bcrypt($password);

    //user saved, create user ganet account as web
    if ($user->save()) {
        echo "User record created <br/>";
        $agent = \Jick\Settings\models\Agent::firstOrNew(['agent_no' => $agent_code, 'user_id' => $user->id]);
        $agent->agent_no = $agent_code;
        $agent->agency = "web";
        $agent->user_id = $user->id;
        $agent->company_name = "Jubilee Kenya Ltd";
        $agent->save();

    }

    echo "web user created successfully";

});