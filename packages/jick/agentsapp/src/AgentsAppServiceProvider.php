<?php

namespace jick\agentsapp;

use Illuminate\Support\ServiceProvider;

class AgentsAppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'agentsapp');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //Load controllers
        $this->app->make('Jick\agentsapp\controllers\AgentsDashboardController');
        $this->app->make('Jick\agentsapp\controllers\ClientListController');
        $this->app->make('Jick\agentsapp\controllers\PoliciesListController');
        $this->app->make('Jick\agentsapp\controllers\ContactsController');
        $this->app->make('Jick\agentsapp\controllers\AppointmentsController');
        $this->app->make('Jick\agentsapp\controllers\CommissionsController');
        $this->app->make('Jick\agentsapp\controllers\NotificationsController');
        $this->app->make('Jick\agentsapp\controllers\PaymentController');
        $this->app->make('Jick\agentsapp\controllers\UserController');
        $this->app->make('Jick\agentsapp\controllers\PreInspectionController');

        //Load models
        $this->app->make('Jick\agentsapp\models\Policy');

        include __DIR__.'/routes.php';
    }
}
