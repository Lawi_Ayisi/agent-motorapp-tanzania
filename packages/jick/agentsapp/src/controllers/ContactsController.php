<?php
/**
 * Created by PhpStorm.
 * User: Evans.Wanyama
 * Date: 12/29/2016
 * Time: 10:15 AM
 */

namespace Jick\agentsapp\controllers;

use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Jick\agentsapp\models\AgentContact;

class ContactsController extends Controller{

    public function getContactsList()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $contacts = AgentContact::join('users', 'agent_contacts.user_id', '=', 'users.id')
            ->select('agent_contacts.contact_name','agent_contacts.contact_phone_number')
            ->where([
                ['agent_contacts.user_id', $user_id]
            ])->get();

        return response()->json($contacts);
    }

    public function saveContact(Request $request){

        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $contact = new AgentContact();
        $contact->user_id = $user_id;
        $contact->contact_name = $request->input('contact_name');
        $contact->contact_phone_number = $request->input('contact_phone_number');

        if ($contact->save()) {
            return response()->json([
                'message' => 'Contact saved'
            ]);
        } else {
            return response()->json('Something went wrong, could not save ');
        }
    }
}