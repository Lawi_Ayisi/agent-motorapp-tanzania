<?php
/**
 * Created by PhpStorm.
 * User: evans.wanyama
 * Date: 12/21/2016
 * Time: 11:55 AM
 */

namespace Jick\agentsapp\controllers;

use JWTAuth;
use App\Http\Controllers\Controller;

use Jick\agentsapp\models\Policy;
use Jick\agentsapp\models\Quote;

class AgentsDashboardController extends Controller{

    public function getTotalPolicies($userId){
        return Quote::where('agent_id', $userId)->where('uploaded', 0)->get()->count();
    }

    public function getTotalClients($userId){
        $users = Quote::where('agent_id', $userId)->where('uploaded', 0)->select('user_id')->orderBy('user_id')->distinct('user_id')->get();
        return count($users);
    }

    public function getTotalCommission($userId){
        return $this->getGrossCommission($userId);
    }

    public function getTotalActionPoints($userId){
        return $this->getActionPoints($userId);
    }

    public function dashboardStats(){

        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        return response()->json([
            'total_policies' => $this->getTotalPolicies($user_id),
            'target_policies' => 200,
            'total_clients' => $this->getTotalClients($user_id),
            'target_clients' => 200,
            'total_commission' => $this->getTotalCommission($user_id),
            'target_commission' => 2000000,
            'total_action_points' => $this->getTotalActionPoints($user_id),
            'target_action_points' => $this->getTotalPolicies($user_id)
        ]);
    }

    private function getGrossCommission($userId){
        //get total premiums
        $totalPremiumCommission = Quote::where('agent_id', $userId)
                              ->whereMonth('created_at', '=', date('m'))
                               ->sum('total_premium');

        $grossCommission =  ($totalPremiumCommission * 0.1);
        $gwt = ($grossCommission*0.1);
        return ($grossCommission - $gwt);
    }

    private function getActionPoints($userId){
        $actionPoints = Policy::where('agent_id', $userId)
            ->where('renewal_date', '<', date('Y-m-d', strtotime("+60 days")).' 00:00:00')
            ->where('renewal_date', '<', date('Y-m-d').' 00:00:00')
            ->count();
        return $actionPoints;
    }

}