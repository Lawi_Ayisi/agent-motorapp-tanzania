<?php
/**
 * Created by PhpStorm.
 * User: evans.wanyama
 * Date: 1/3/2017
 * Time: 3:53 PM
 */

namespace Jick\agentsapp\controllers;

use Illuminate\Support\Facades\File;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use PDF;

use Jick\agentsapp\models\Quote;


class CommissionsController extends Controller{

    public function getAgentMonthlyCommission(Request $request){

        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $year = $request->input('year');
        $month =  date_parse($request->input('month'));

        //get total premiums
        $totalPremiumCommission = Quote::where('agent_id', $user_id)
            ->whereYear('created_at', '=', $year)
                ->whereMonth('created_at', '=', $month['month'])
            ->sum('total_premium');
        $grossCommission =  ($totalPremiumCommission * 0.1);
        $gwt = ($grossCommission*0.1);
        $due = ($grossCommission - $gwt);

        $pdf_url = $this->downloadCommissionStatement($request);

        return array("commission_earned"=>$grossCommission, "wht"=>$gwt,"commissions_due"=>$due,"commissions_paid"=>0.0,"pdf_url"=> $pdf_url);
    }

    public function downloadCommissionStatement(Request $request){

        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $year = $request->input('year');
        $month =  date_parse($request->input('month'));

        $quotes = Quote::where('agent_id', $user_id)
            ->whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month['month'])
            ->get();


        $data = [
            'quotes' => $quotes
        ];


        $pdf = PDF::loadView('agentsapp::commission', $data);

        $doc_name = "commission_statement_for_'.$request->email.'.pdf'";

        if (File::exists(public_path('agentsapp/'.$doc_name.'.pdf'))){
            File::delete(public_path('agentsapp/'.$doc_name.'.pdf'));
        }

        $pdf->save('agentsapp/commission_statement_for_'.$request->email.'.pdf');

       $path = 'agentsapp/commission_statement_for_'.$request->email.'.pdf';

        return $path;
    }

} 