<?php
/**
 * Created by PhpStorm.
 * User: evans.wanyama
 * Date: 12/21/2016
 * Time: 12:44 PM
 */

namespace Jick\agentsapp\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Jick\agentsapp\models\Benefit;
use Jick\agentsapp\models\MotorUsageType;
use Jick\agentsapp\models\Policy;
use Jick\agentsapp\models\Quote;
use Jick\agentsapp\models\Risk;
use Jick\agentsapp\models\User;
use JWTAuth;
use PDF;

class PoliciesListController extends Controller
{

    public function getPoliciesList()
    {

        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $policies = Quote::join('plans', 'quotes.plan_id', '=', 'plans.id')
            ->join('products', 'plans.product_id', '=', 'products.id')
            ->select('quotes.quote_id', 'quotes.user_id', 'quotes.id_pp', 'plans.name as plan', 'products.name as product')
            ->where([
                ['quotes.agent_id', $user_id]
            ])
            ->where([
                ['quotes.uploaded', 0]
            ])
            ->orderBy('quotes.id_pp')
            ->get();

        $policiesArray = array();

        foreach ($policies as $policy) {
            $client = array("client_name" => $this->getClientsName($policy->user_id), "policyNo" => $policy->quote_id, "client_id" => $policy->id_pp, "plan" => $policy->plan, "products" => $policy->product);
            $policiesArray[] = $client;
        }

        return response()->json($policiesArray);
    }

    public function getActionPoints()
    {

        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $actionPoints = Policy::select('policy_no', 'renewal_date', 'id_pp', 'user_id')
            ->where('agent_id', '=', $user_id)
            ->get();
        // ->where('renewal_date', '<', date('Y-m-d') . ' 00:00:00')

        $policiesArray = array();

        foreach ($actionPoints as $policy) {
            $client = array("name" => $this->getClientsName($policy->user_id), "policy_no" => $policy->policy_no, "status" => "RENEW", "renewal_date" => $policy->renewal_date);
            $policiesArray[] = $client;
        }

        return response()->json($policiesArray);
    }

    private function getClientsName($id)
    {
        $name = User::selectRaw('CONCAT(firstname," ",lastname) as client_name')
            ->where('id', $id)
            ->value('client_name');
        return $name;
    }

    public function getPolicyDetails(Request $request)
    {

        $policyNo = $request->input('policy_id');


        $policies = Policy::select('policy_no', 'renewal_date', 'id_pp', 'user_id')
            ->where('policy_no', '=', $policyNo)
            ->get();

        $cars = $this->getCarUnderPolicy($policyNo);

        $client = "";

        foreach ($policies as $policy) {
            $client = array("client" => $this->getClientsName($policy->user_id), "policy_no" => $policy->policy_no, "renewal_date" => $policy->renewal_date, "user_id" => $policy->user_id, "vehicles" => $cars);
        }
        return response()->json($client);

    }


    private function getCarUnderPolicy($policyNo)
    {

        $risks = Risk::select('car_make', 'reg_no', 'car_premium', 'cover_type')
            ->where('policy_no', '=', $policyNo)
            ->get();
        return $risks;

    }

    public function getRenewalQuote(Request $request)
    {

        $policyNo = $request->input('policy_id');

        $policies = Quote::join('plans', 'plans.id', '=', 'quotes.plan_id')
            ->select('quotes.quote_id', 'quotes.premium', 'quotes.phcf', 'quotes.levy', 'quotes.duty', 'quotes.total_premium', 'plans.name')
            ->where('quote_id', '=', $policyNo)
            ->get();

        $client = "";

        $pdf_url = $this->generateRenewalQuote($policyNo);

        foreach ($policies as $policy) {
            $client = array("plan" => $policy->name, "basic_premium" => $policy->premium, "policy_no" => $policy->quote_id, "phcf" => $policy->phcf, "duty" => $policy->duty, "levy" => $policy->levy, "total_premium" => $policy->total_premium, "pdf_url" => $pdf_url);
        }
        return response()->json($client);

    }


    public function generateRenewalQuote($policyNo)
    {

        $policies = Quote::join('plans', 'plans.id', '=', 'quotes.plan_id')
            ->join('risks', 'risks.policy_no', '=', 'quotes.quote_id')
            ->select('quotes.quote_id', 'risks.car_value', 'quotes.premium', 'quotes.phcf', 'quotes.levy', 'quotes.duty', 'quotes.total_premium', 'plans.name')
            ->where('quote_id', '=', $policyNo)
            ->get();

        $data = [
            'policies' => $policies
        ];

        $pdf = PDF::loadView('agentsapp::renewal_quote', $data);

        $policy_no = str_replace("/", "_", $policyNo);

        $doc_name = "renewal_quote_for_'.$policy_no.'.pdf'";

        if (File::exists(public_path('agentsapp/' . $doc_name . '.pdf'))) {
            File::delete(public_path('agentsapp/' . $doc_name . '.pdf'));
        }

        $pdf->save('agentsapp/renewal_quote_for_' . $policy_no . '.pdf');

        $path = 'agentsapp/renewal_quote_for_' . $policy_no . '.pdf';

        return $path;

    }


    public function getBenefitsList(Request $request)
    {

        $sub_class = $request->input('sub_class');
        $cover_type = $request->input('cover_type');

        //get sub_class id
        $usage_type_id = MotorUsageType::select("id")
            ->where("value", $sub_class)
            ->get()
            ->first();

        $benefits = Benefit::select('id', 'name', 'category','benefit_limit', 'description')
            ->where("cover_type", "=", $cover_type)
            ->where("sub_class_id", "=", $usage_type_id['id'])
            ->get();

        $data = [
            'data' => $benefits
        ];
        return response()->json($data);
    }


    public function getBenefitDetails(Request $request)
    {
        $benefit_id = $request->input('benefit_id');
        $benefit = Benefit::where("id", "=", $benefit_id)
            ->get()
            ->first();
        return response()->json($benefit);
    }

}