<?php
/**
 * Created by PhpStorm.
 * User: evans.wanyama
 * Date: 12/21/2016
 * Time: 12:45 PM
 */
namespace Jick\agentsapp\controllers;

use Jick\agentsapp\models\AgentAppointment;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class AppointmentsController extends Controller{

    public function getAppointmentList()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $appointments = AgentAppointment::join('users', 'agent_appointments.client_id', '=', 'users.id')
            ->select('users.firstname','users.lastname','agent_appointments.appointment_date','agent_appointments.appointment_time')
            ->where([
                ['agent_appointments.user_id', $user_id]
            ])->get();

        return response()->json($appointments);
    }

    public function saveAppointment(Request $request){

        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $appointment = new AgentAppointment();
        $appointment->user_id = $user_id;
        $appointment->client_id = $request->input('client_id');
        $appointment->appointment_date = $request->input('appointment_date');
        $appointment->appointment_time = $request->input('appointment_time');
        $appointment->notes = $request->input('notes');

        if ($appointment->save()) {
            return response()->json([
                'message' => 'Appointment saved'
            ]);
        } else {
            return response()->json('Something went wrong, could not save ');
        }
    }

}