<?php
/**
 * Created by PhpStorm.
 * User: evans.wanyama
 * Date: 12/21/2016
 * Time: 12:44 PM
 */
namespace Jick\agentsapp\controllers;

use Illuminate\Support\Facades\DB;
use JWTAuth;
use App\Http\Controllers\Controller;

use Jick\agentsapp\models\Quote;
use Jick\agentsapp\models\User;
use Illuminate\Http\Request;


class ClientListController extends Controller{

    public function getClientsList(){

        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $policies = Quote::join('users', 'users.id', '=', 'quotes.user_id')
            ->select('quotes.user_id','users.phone', DB::raw('CONCAT(firstname," ",lastname) as client_name'))
            ->where([
                ['quotes.agent_id', $user_id]
            ])
            ->where([
                ['quotes.uploaded', 0]
            ])
            ->distinct('quotes.user_id')
            ->get();

        $policiesArray =  array();

        foreach($policies as $policy){

            $phone = ($policy -> phone);

            if(($phone == null) || ($phone == "")){
                $phone =  "No phone number";
            }

            $client = array("client_name"=>$this->getClientsName($policy -> user_id),"phone"=> $phone,"user_id"=> $policy -> user_id);

            $policiesArray[] = $client;
        }

        return response()->json($policiesArray);
    }

    private function getClientsName($id){
        $name = User::selectRaw('CONCAT(firstname," ",lastname) as client_name')
            ->where('id',$id)
            ->value('client_name');
        return $name;
    }

    public function  getClientIdentities(Request $request){

        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $user_id = $request->input('user_id');

        $user = User::where([
            ['id', $user_id]
        ])->get();

        return response()->json($user);
    }
}

