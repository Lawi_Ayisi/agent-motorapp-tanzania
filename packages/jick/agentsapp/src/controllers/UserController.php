<?php
/**
 * Created by PhpStorm.
 * User: evans.wanyama
 * Date: 1/31/2017
 * Time: 12:28 PM
 */

namespace Jick\agentsapp\controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Jick\agentsapp\models\User;
use Jick\buyonline\models\Quote;
use Jick\Settings\models\Agent;
use JWTAuth;


class UserController extends Controller
{

    public function updateUserDetails(Request $request)
    {

        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $user = User::where('id', $user_id)->first();

        if ($request->input('title') != "") {
            $user->title = $request->input('title');
        }
        if ($request->input('first_name') != "") {
            $user->firstname = $request->input('first_name');
        }
        if ($request->input('last_name') != "") {
            $user->lastname = $request->input('last_name');
        }
        if ($request->input('phone') != "") {
            $user->phone = $request->input('phone');
        }
        if ($request->input('id_no') != "") {
            $user->id_no = $request->input('id_no');
        }
        if ($request->input('passport_no') != "") {
            $user->passport_no = $request->input('passport_no');
        }
        if ($request->input('kra_pin') != "") {
            $user->kra_pin = $request->input('kra_pin');
        }
        if ($request->input('nationality') != "") {
            $user->nationality = $request->input('nationality');
        }
        if ($request->input('city') != "") {
            $user->city = $request->input('city');
        }
        if ($request->input('postal_address') != "") {
            $user->postal_addr = $request->input('postal_address');
        }
        if ($request->input('postal_code') != "") {
            $user->postal_code = $request->input('postal_code');
        }
        if ($request->input('company') != "") {
            $user->company = $request->input('company');
        }
        if ($request->input('physical_addr') != "") {
            $user->physical_addr = $request->input('physical_addr');
        }
        if ($user->save()) {
            return response()->json([
                'message' => 'Details Updated'
            ]);
        } else {
            return response()->json('Something went wrong, could not save ');
        }

    }

    public function updateUserDetailsByAgent(Request $request)
    {

        $user_id = $request->input('user_id');
        $policy_number = $request->input('policy_id');
        $client_phone = $request->input('phone');
        $first_name = $request->input('first_name');

        $user = User::where('id', $user_id)->first();

        if (!($this->checkIfEmailExists($request->input('email'), $user_id))) {

            if ($request->input('title') != "") {
                $user->title = $request->input('title');
            }
            if ($request->input('first_name') != "") {
                $user->firstname = $request->input('first_name');
            }
            if ($request->input('last_name') != "") {
                $user->lastname = $request->input('last_name');
            }
            if ($request->input('email') != "") {
                $user->email = $request->input('email');
            }
            if ($request->input('phone') != "") {
                $user->phone = $request->input('phone');
            }
            if ($request->input('id_no') != "") {
                $user->id_no = $request->input('id_no');
            }
            if ($request->input('passport_no') != "") {
                $user->passport_no = $request->input('passport_no');
            }
            if ($request->input('kra_pin') != "") {
                $user->kra_pin = $request->input('kra_pin');
            }
            if ($request->input('nationality') != "") {
                $user->nationality = $request->input('nationality');
            }
            if ($request->input('city') != "") {
                $user->city = $request->input('city');
            }
            if ($request->input('postal_address') != "") {
                $user->postal_addr = $request->input('postal_address');
            }
            if ($request->input('postal_code') != "") {
                $user->postal_code = $request->input('postal_code');
            }
            if ($request->input('company') != "") {
                $user->company = $request->input('company');
            }
            if ($request->input('physical_addr') != "") {
                $user->physical_addr = $request->input('physical_addr');
            }
            if ($user->save()) {
                //send sms with code
                $confirmation = mt_rand(111111, 999999);
                Quote::where('quote_id', $policy_number)->update(['client_code' => $confirmation]);
                Helper::sendSms('Dear, ' . $first_name . ' your code is: ' . $confirmation . ' To verify your details, please view the e-mail we just sent you and provide this code to your agent. Jubilee Insurance', $client_phone);

                return response()->json([
                    'message' => 'Details Updated'
                ]);
            } else {
                return response()->json('Something went wrong, could not save ');
            }
        } else {

            return response()->json([
                'message' => 'This email already is being used by another user'
            ]);

        }

    }

    private function checkIfEmailExists($email, $user_id)
    {
        $user = User::where('email', $email)
            ->whereNotIn('id', [$user_id])
            ->first();

        if ($user === null) {
            return false;
        } else {
            return true;
        }
    }


    public function getUserByIdno(Request $request)
    {
        $idno = $request->input('customer_id');

        if ($idno) {

            $user = User::where('id_no', $idno)
                ->get()
                ->first();

            if ($user) {
                return response()->json($user);
            } else {
                return response()->json([
                    'message' => 'No details'
                ]);
            }

        }
    }


    public function registerUser(Request $request){

        $confirmation_code = rand(9999,999999999);

        $fname = $request->input('first_name');
        $oname = $request->input('other_name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $agency_name = $request->input('name');
        $agent_code = $request->input('agent_code');
        $agency = $request->input('agency');
        //$is_manager = $request->input('is_manager');

        //update users table
        $user = new User();
        $user->firstname = $fname;
        $user->lastname = $oname;
        $user->username = $email;
        $user->email = $email;
        $user->phone = $phone;
        $user->confirmation_code = $confirmation_code;

        if($user->save()) {

            $code = $user->confirmation_code;
            //send confirmation code;
            Helper::sendSms('Dear, ' . $fname . ' use this code to activate your account: ' . $code, $phone);
            $agent = new Agent();
            $agent->agent_no = $agent_code;
            $agent->agency = $agency;
            $agent->_company_name = $agency_name;

        }

    }

    public function confirmCode(Request $request){
    }
} 