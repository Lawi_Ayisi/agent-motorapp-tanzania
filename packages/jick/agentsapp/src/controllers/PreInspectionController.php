<?php
/**
 * Created by PhpStorm.
 * User: evans.wanyama
 * Date: 12/21/2016
 * Time: 12:44 PM
 */

namespace Jick\agentsapp\controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Jick\agentsapp\models\Benefit;
use Jick\agentsapp\models\MotorUsageType;
use Jick\agentsapp\models\Policy;
use Jick\agentsapp\models\PreInspection;
use Jick\agentsapp\models\PreInspectionImage;
use Jick\agentsapp\models\Quote;
use Jick\agentsapp\models\Risk;
use Jick\agentsapp\models\User;
use JWTAuth;
use PDF;

class PreInspectionController extends Controller
{

    public function savePreInspectionData(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $data = PreInspection::firstOrNew(['vehicle_reg' => $request->regNo,'car_model'=>$request->carModel]);
        $data->user_id = $user_id;
        $data->vehicle_reg = $request->regNo;
        $data->car_make = $request->carMake;
        $data->car_model = $request->carModel;
        $data->year_manufacture = $request->yearManuf;
//        $data->date_photos = $request->photoDate;
        $data->date_photos = Carbon::now()->toDateString();
        $data->time_photos = Carbon::now()->toTimeString();
//        $data->insured = $request->insured;
        $data->latitude = $request->latitude;
        $data->longitude = $request->longitude;
        $data->notes = $request->notes;
        $save = $data->save();

        if ($save) {
            $status = "success";
            $message = "Pre Inspection Data has been saved Successfully";
            $preInspID = $data->id;
        } else {
            $status = "failure";
            $message = "Pre Inspection Data has NOT been saved, Please Try again later";
            $preInspID = null;
        }

        return response()->json(array('status' => $status, 'message' => $message, 'inspectionID' => $preInspID));

    }

    public function savePreInspectionImage(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $file = base64_decode($request->image);
        $folderName = '/uploads/pre-inspection/';
        $safeName = $request->inspectionID . '_' . $request->imageSide . '.' . 'png';
        $destinationPath = public_path() . $folderName;
        File::isDirectory($destinationPath) or File::makeDirectory($destinationPath, 0777, true, true);//check if directory exists or create it
        $fileName = public_path() . '/uploads/pre-inspection/' . $safeName;
        file_put_contents($fileName, $file);

        $data = PreInspectionImage::firstOrNew(['pre_inspection_id' => $request->inspectionID]);
        $data->pre_inspection_id = $request->inspectionID;
        if ($request->imageSide == "image_front") {
            $data->image_front = $folderName . $safeName;
        } elseif ($request->imageSide == "image_rear") {
            $data->image_rear = $folderName . $safeName;
        } elseif ($request->imageSide == "image_right") {
            $data->image_right = $folderName . $safeName;
        } elseif ($request->imageSide == "image_left") {
            $data->image_left = $folderName . $safeName;
        } elseif ($request->imageSide == "image_chasis") {
            $data->image_chasis = $folderName . $safeName;
        } elseif ($request->imageSide == "image_logbook") {
            $data->image_logbook = $folderName . $safeName;
        }
        $save = $data->save();

        if ($save) {
            $status = "success";
            $message = "Pre Inspection Image has been saved Successfully";
        } else {
            $status = "failure";
            $message = "Pre Inspection Image has NOT been saved, Please try again later";
        }
        return response()->json(array('status' => $status, 'message' => $message, "imageSide" => $request->imageSide));
    }

    public function loadPreInspectionData(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $preInspection = PreInspection::where('id', $request->inspectionID)->get();
        if ($preInspection->count() > 0) {
            $data = PreInspection::join('pre_inspection_image', 'pre_inspection.id', '=', 'pre_inspection_image.pre_inspection_id')
                ->where('pre_inspection.id',$request->inspectionID)
                ->where('pre_inspection.user_id',$user_id)
                ->first();

            $status = "success";
            $message = "Pre Inspection Data found";
            $data = $data;
        } elseif ($preInspection->count() < 1) {
            $status = "failure";
            $message = "Pre Inspection Data does not exists";
            $data = null;
        }
        return response()->json(array('status' => $status, 'message' => $message, "data" => $data));
    }

    public function emailPreInspectionData(Request $request){
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $preInspection = PreInspection::where('id', $request->inspectionID)->get();
        if ($preInspection->count() > 0) {
            $data = PreInspection::join('pre_inspection_image', 'pre_inspection.id', '=', 'pre_inspection_image.pre_inspection_id')
                ->join('users','pre_inspection.user_id','=','users.id')
                ->where('pre_inspection.id',$request->inspectionID)
                ->where('pre_inspection.user_id',$user_id)
                ->select('pre_inspection.*','pre_inspection_image.*','users.email','users.phone','users.firstname','users.lastname')
                ->first();

            $emailMessage = $this->sendEmail($data);

            $status = "success";
            $message = "Your Details have been received Successfully. Thank you.";
            $data = $data;
        } elseif ($preInspection->count() < 1) {
            $status = "failure";
            $message = "Problem while submitting Pre Inspection Data";
            $data = null;
        }
        return response()->json(array('status' => $status, 'message' => $message, "data" => $data,"emailMessage"=>$emailMessage));
    }

    public function sendEmail($dataz){

        $data = [
            'name' => $dataz->firstname .' '. $dataz->lastname,
            'email' => $dataz->email,
            'phone' => $dataz->phone,
            'make' => $dataz->car_make,
            'model' => $dataz->car_model,
            'vehicle_reg' => $dataz->vehicle_reg,
            'year_manufacture' => $dataz->year_manufacture,
            'date_photos' => $dataz->date_photos,
            'time_photos' => $dataz->time_photos,
            'insured' => $dataz->insured,
            'image_front'=>$dataz->image_front,
            'image_rear'=>$dataz->image_rear,
            'image_right'=>$dataz->image_right,
            'image_left'=>$dataz->image_left,
            'image_chasis'=>$dataz->image_chasis,
            'image_logbook'=>$dataz->image_logbook,
            'latitude'=>$dataz->latitude,
            'longitude'=>$dataz->longitude,
            'notes'=>$dataz->notes,
        ];

        $path= realpath('');

        Mail::send('agentsapp::email.pre_inspection', $data, function ($m) use($data,$path) {

            $m->replyTo($data['email'], $data['name'])->subject('Pre Inspection');
//            $m->from('general@jubileeinsurance.com')->subject('Pre Inspection');

            $mailSettings = config('app.email_setting');
            if ($mailSettings == "live") {
                //Change valuation email address from @jubileekenya to @jubileetanzania
                $m->to('valuation@jubileetanzania.co.tz', 'Jubilee Insurance - Valuations');
                $m->cc($data['email'], $data['name']);
            } elseif ($mailSettings == "dev") {
                $m->to('david.thige@jubileekenya.com', 'Jubilee Insurance - Valuations');
            }



            $m->attach($path.$data['image_front']);
            $m->attach($path.$data['image_rear']);
            $m->attach($path.$data['image_right']);
            $m->attach($path.$data['image_left']);

            if(!is_null($data['image_chasis'])){
                $m->attach($path.$data['image_chasis']);
            }
            if(!is_null($data['image_logbook'])){
                $m->attach($path.$data['image_logbook']);
            }

        });

        return "email sent successfully";
    }
}