<?php
/**
 * Created by PhpStorm.
 * User: Evans.Wanyama
 * Date: 1/16/2017
 * Time: 12:10 PM
 */

namespace Jick\agentsapp\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Jick\agentsapp\models\Payment;
use Jick\agentsapp\models\Policy;
use Jick\agentsapp\models\Quote;
use JWTAuth;


class PaymentController extends Controller
{
    public function savePayment(Request $request)
    {

        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $payment = new Payment();
        $payment->reference = $request->input('ref');
        $payment->transaction_id = $request->input('transaction_no');
        $payment->transaction_type = $request->input('transaction_type');
        $payment->amount = $request->input('amount');
        $payment->status = $request->input('status');

        if ($payment->save()) {
            $this->updateRenewalDate($request->input('ref'));
            return response()->json([
                'message' => 'Record saved'
            ]);
        } else {
            return response()->json('Something went wrong, could not save ');
        }
    }


    private function updateRenewalDate($policyId)
    {

        $new_renewal_date = date('Y-m-d', strtotime('+1 years'));

        $policy = Policy::where('policy_no', $policyId)
            ->update(['end_date' => $new_renewal_date, 'renewal_date' => $new_renewal_date]);

        $quote = Quote::where('quote_id', $policyId)
            ->update(['uploaded' => false]);

    }
} 