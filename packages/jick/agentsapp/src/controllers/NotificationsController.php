<?php
/**
 * Created by PhpStorm.
 * User: evans.wanyama
 * Date: 1/3/2017
 * Time: 3:53 PM
 */

namespace Jick\agentsapp\controllers;

use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Jick\agentsapp\models\NotificationDetails;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;


class NotificationsController extends Controller{

    public function saveUserNotificationDetails(Request $request){

        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $notificationDetails = NotificationDetails::where('user_id',$user_id)->first();

        if (is_null($notificationDetails)) {

            $notificationDetails = new NotificationDetails();
            $notificationDetails->user_id = $user_id;
            $notificationDetails->token = $request->input('token');
            $notificationDetails->save();
            if ($notificationDetails->save()) {
                return response()->json([
                    'message' => 'Record saved'
                ]);
            } else {
                return response()->json('Something went wrong, could not save ');
            }

        }else{

            $notificationDetails->token = $request->input('token');
            $notificationDetails->save();
            if ($notificationDetails->save()) {
                return response()->json([
                    'message' => 'Record saved'
                ]);
            } else {
                return response()->json('Something went wrong, could not save ');
            }

        }

    }


    public function sendSingleUserNotification(){

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('Will this shit really work?');
        $notificationBuilder->setBody('Hello from laravel FCM test, cool plugin implemented by Evans')
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'my_data']);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        //get user firebase token
        $token = $this->getUserToken();

        //echo $token;

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        //return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify();

        //return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();

        echo "message send";
    }

    private function getUserToken(){

        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        return NotificationDetails::where('user_id',$user_id)->value('token');
    }

}