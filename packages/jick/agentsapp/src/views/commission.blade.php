<html>

    <body>

        <div>
        <p>Your commission Statement</p>
        <table width="100%" border="1" cellpadding="5" cellspacing="0">
            <tr>
            <th>Policy Number</th>
            <th>Premium</th>
             <th>Commission</th>
              <th>Tax</th>
               <th>Net Commission</th>
            </tr>
            @foreach ($quotes as $commission)
                <tr>
                    <td>{{ $commission->quote_id }}</td>
                    <td>{{ $commission->total_premium }}</td>
                    <td>{{ ($commission->total_premium) * 0.1}}</td>
                    <td>{{ (($commission->total_premium) * 0.1)*0.1}}</td>
                    <td>{{ (($commission->total_premium) * 0.1) - ((($commission->total_premium) * 0.1)*0.1) }}</td>
                </tr>
            @endforeach
            </table>
        </div>
    </body>
</html>