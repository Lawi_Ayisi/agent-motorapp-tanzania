<html>

    <body>

        <div>
        <p>Quote Details</p>
         <h6>Quote Reference: {{$quote_ref}}</h6>
        <h6>Plan: {{$plan}}</h6>
        <h6>Usage Type:  {{$usage_type}}</h6>
        <h6>Year of Manufacture: {{$year_of_manufacture}}</h6>
        <table width="100%" border="1" cellpadding="5" cellspacing="0">
            <tr>
            <th>Cover Description</th>
            <th>Sum Insured/Limit</th>
             <th>Cost</th>
            </tr>
            <tr>
            <td>Basic Premium</td>
             <td></td>
             <td>{{$b_premium}}</td>
            </tr>

            @if(!empty($benefit_list))
                       @foreach ($benefit_list as $benefit)
                            <tr>
                            <td>{{$benefit['name']}}</td>
                            <td>{{$benefit['limit']}}</td>
                            <td>{{$benefit['cost']}}</td>
                            </tr>
                        @endforeach
            @endif



             <tr>
                                                <td>PHCF</td>
                                                 <td></td>
                                                 <td>{{$phcf}}</td>
                                                </tr>




             <tr>
                        <td>Training Levy</td>
                         <td></td>
                         <td>{{$training_levy}}</td>
                        </tr>

                          <tr>
                                                <td>Stamp Duty</td>
                                                 <td></td>
                                                 <td>{{$stamp_duty}}</td>
                                                </tr>

                                                  <tr>
                                                                                                <td>Total Premium Estimate</td>
                                                                                                 <td></td>
                                                                                                 <td>{{$total_premium}}</td>
                                                                                                </tr>


            </table>
            </div>
            </body>
            </html>