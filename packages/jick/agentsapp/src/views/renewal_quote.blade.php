<html>

    <body>

        <div style="text-align: center;">Logo</div>

        <div>
        <p style="margin-bottom: 10px;">We thank you for your continued patronage and wish to inform you that your policy above is
           due for renewal on 29/03/2017 and the renewal terms are as stated below.We also wish to
           bring to your attention the requirement of Section 156 sub-section (2) of the Insurance Act
           Cap 487 that the renewal premium must be paid on or before the renewal date.Please note
           that the Company shall only assume risk upon receipt of the full premium.
        </p>
        <h4>MOTOR VALUATION</h4>
        <p>
          <b>Motor valuation is mandatory for comprehensive covers. Please ensure your vehicle(s) is
           valued prior renewal. Please contact our valuation officers to arrange for valuation on 0700
           330610, 0700 330600 and 0700 330611 or Center Manager on 0720468734 or 0733743761;
           Email: valuations@jubileeinsurance.com
          </b>
         </p>

        <p>Car details</p>
        <table width="100%" border="1" cellpadding="5" cellspacing="0">
            <tr>
                 <th>Cover Description</th>
                 <th>Sum Insured/Limit</th>
                 <th>Basic Premium</th>
                 <th>Levies</th>
                 <th>Gross Premium</th>
            </tr>
            @foreach ($policies as $policy)
                 <tr>
                    <td>{{ $policy->name }}</td>
                    <td>{{ $policy->car_value }}</td>
                    <td>{{ $policy->total_premium }}</td>
                    <td>{{ $policy->levy }}</td>
                    <td>{{($policy->total_premium + $policy->levy)}}</td>
                 </tr>
             @endforeach
        </table>

        <p style="text-decoration: underline;"><b>Optional covers provided at additional premium for comprehensive covers only
           (please ignore if you already have purchased any of the below):-</b></p>

           <ol>
              <li>Courtesy Car (Private Cars Only) - Kshs.3,000/- additional (Limit Kshs.30,000/- for 10 days with 3 days excess)</li>
              <li>Excess Protector - 0.25% of Sum Insured minimum Kshs.2,500/-(Private Vehicles), 0.5% of Sum Insured minimum Kshs.3,000/- (Commercial Vehicles)</li>
              <li>Terrorism,Sabotage & Political Violence Risks - 0.25% of Sum Insured (Private Cars) or 0.35% of Sum Insured(Commercial Vehicles) minimum Kshs.2,500/-</li>
              <li>Road Rescue Services(Private Cars Only) - Kshs.6,960/-(INFAMA) or Kshs.3,000/-(AA) additional</li>
           </ol>

           <p>Otherwise subject to terms and conditions of the policy.</p>

           <p>We look forward to your renewal instructions and kindly don't hesitate to contact us if you have any queries or clarifications</p>

        </div>
    </body>
</html>