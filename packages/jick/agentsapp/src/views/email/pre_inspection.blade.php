<strong>Dear Valuer</strong>, <br><br>

You have Pre-Inspection Request from: <br/><br/>
{{$name}} <br/>
{{$phone}} <br/>
{{--Is Car Insured? <strong>@if($insured) Yes @else No @endif</strong> <br/>--}}
Car Make: <strong>{{$make}}</strong> <br/>
Car Model: <strong>{{$model}}</strong> <br/>
Registration No.: <strong>{{$vehicle_reg}}</strong> <br/>
Year of Manufacture: <strong>{{$year_manufacture}}</strong> <br/>
Date of taking Photos: <strong>{{$date_photos}}</strong> <br/>
Time of taking Photos: <strong>{{$time_photos}}</strong> <br/>
Latitude: <strong>{{$latitude}}</strong> <br/>
Longitude: <strong>{{$longitude}}</strong> <br/>
Additional Notes: <strong>{{$notes}}</strong> <br/>

<?php
$link = "http://www.google.com/maps/place/".$latitude.",".$longitude;
?>
<a href="{{$link}}" target="_blank">Photo Capture Location</a><br/><br/>
Attached are my image snapshots <br><br>


<strong>Yours faithfully</strong>, <br>
<strong>{{$name}}</strong>
{{--<strong>JUBILEE INSURANCE COMPANY OF KENYA LTD</strong>.--}}