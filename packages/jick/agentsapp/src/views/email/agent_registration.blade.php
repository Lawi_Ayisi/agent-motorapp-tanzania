<strong>Greetings</strong>, <br><br>

We have a new agent Registered with the following Information through the Mobile App: <br/><br/>

Name: <strong>{{$name}}</strong> <br/>
Email: <strong>{{$email}}</strong> <br/>
Phone No.: <strong>{{$phone}}</strong> <br/>
Agency Name.: <strong>{{$agencyName}}</strong> <br/>
Agent Code.: <strong>{{$agentCode}}</strong> <br/>
Agency: <strong>{{$agency}}</strong> <br/><br/>

<?php
$link = url('web/activate/agent/'.$email);
?>
If valid &nbsp;<a href="{{$link}}" target="_blank">Click here to Activate this user</a><br/>
or copy this link {{ url('web/activate/agent/'.$email)}}<br/><br/><br/>


<strong>Yours faithfully</strong>, <br>
<strong>General App</strong>
{{--<strong>JUBILEE INSURANCE COMPANY OF KENYA LTD</strong>.--}}