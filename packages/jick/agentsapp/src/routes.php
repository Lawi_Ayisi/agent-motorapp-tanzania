<?php

//Protected routes
Route::group(['middleware' => 'jwt.auth', 'prefix' => 'api/'], function() {
    Route::get('dashboard-stats', 'jick\agentsapp\controllers\AgentsDashboardController@dashboardStats');
    Route::get('agent-client-list', 'jick\agentsapp\controllers\ClientListController@getClientsList');
    Route::get('agent-client-list-names', 'jick\agentsapp\controllers\ClientListController@getClientIdentities');
    Route::post('agent-client-details-by-id', 'jick\agentsapp\controllers\ClientListController@getClientIdentities');
    Route::get('agent-policies-list', 'jick\agentsapp\controllers\PoliciesListController@getPoliciesList');
    Route::post('customer-policy-details', 'jick\agentsapp\controllers\PoliciesListController@getPolicyDetails');
    Route::post('policy-renewal-quote', 'jick\agentsapp\controllers\PoliciesListController@getRenewalQuote');
    Route::get('agent-action-points', 'jick\agentsapp\controllers\PoliciesListController@getActionPoints');
    Route::post('benefits-list', 'jick\agentsapp\controllers\PoliciesListController@getBenefitsList');
    Route::post('benefits-details', 'jick\agentsapp\controllers\PoliciesListController@getBenefitDetails');
    Route::get('agent-contacts', 'jick\agentsapp\controllers\ContactsController@getContactsList');
    Route::post('save-agent-contact', 'jick\agentsapp\controllers\ContactsController@saveContact');
    Route::get('agent-appointments', 'jick\agentsapp\controllers\AppointmentsController@getAppointmentList');
    Route::post('save-agent-appointment', 'jick\agentsapp\controllers\AppointmentsController@saveAppointment');
    Route::post('agent-commission', 'jick\agentsapp\controllers\CommissionsController@getAgentMonthlyCommission');
    Route::post('agent-commission-statement', 'jick\agentsapp\controllers\CommissionsController@getAgentMonthlyCommission');
    Route::post('save-payment', 'jick\agentsapp\controllers\PaymentController@savePayment');
    Route::post('save-firebase-details', 'jick\agentsapp\controllers\NotificationsController@saveUserNotificationDetails');
    Route::post('send-notification', 'jick\agentsapp\controllers\NotificationsController@sendSingleUserNotification');
    Route::post('update-agent-details', 'jick\agentsapp\controllers\UserController@updateUserDetails');
    Route::post('get-user', 'jick\agentsapp\controllers\UserController@getUserByIdno');
    Route::post('update-user-details', 'jick\agentsapp\controllers\UserController@updateUserDetails');
    Route::post('update-user-details-agent', 'jick\agentsapp\controllers\UserController@updateUserDetailsByAgent');

    Route::post('save-pre-inspection-data', 'jick\agentsapp\controllers\PreInspectionController@savePreInspectionData');
    Route::post('save-pre-inspection-image', 'jick\agentsapp\controllers\PreInspectionController@savePreInspectionImage');
    Route::post('load-pre-inspection-data', 'jick\agentsapp\controllers\PreInspectionController@loadPreInspectionData');
    Route::post('email-pre-inspection-data', 'jick\agentsapp\controllers\PreInspectionController@emailPreInspectionData');

});
