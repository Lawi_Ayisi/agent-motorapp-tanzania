<?php
/**
 * Created by PhpStorm.
 * User: Evans.Wanyama
 * Date: 12/29/2016
 * Time: 11:24 AM
 */

namespace Jick\agentsapp\models;

use Illuminate\Database\Eloquent\Model;

class AgentContact extends Model
{
    protected $table = 'agent_contacts';

    protected $fillable = [''];
}