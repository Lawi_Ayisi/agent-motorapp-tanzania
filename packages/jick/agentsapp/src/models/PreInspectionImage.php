<?php

namespace Jick\agentsapp\models;

/**
 * Created by PhpStorm.
 * User: Evans.Wanyama
 * Date: 6/12/2017
 * Time: 3:37 PM
 */

use Illuminate\Database\Eloquent\Model;

class PreInspectionImage extends Model
{
    protected $table = 'pre_inspection_image';

    protected $fillable = [''];
}