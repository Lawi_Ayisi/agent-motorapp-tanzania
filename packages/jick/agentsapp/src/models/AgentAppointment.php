<?php
/**
 * Created by PhpStorm.
 * User: Evans.Wanyama
 * Date: 12/29/2016
 * Time: 11:26 AM
 */

namespace Jick\agentsapp\models;

use Illuminate\Database\Eloquent\Model;

class AgentAppointment extends Model
{
    protected $table = 'agent_appointments';

    protected $fillable = [''];
}