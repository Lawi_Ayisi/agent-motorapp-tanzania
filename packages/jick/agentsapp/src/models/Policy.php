<?php
    namespace Jick\agentsapp\models;

    use Illuminate\Database\Eloquent\Model;

    class Policy extends Model
    {
        protected $table = 'policies';

        protected $fillable = [''];
    }
    