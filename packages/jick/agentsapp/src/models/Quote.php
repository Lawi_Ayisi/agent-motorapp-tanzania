<?php
namespace Jick\agentsapp\models;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    public $incrementing = false;

    protected $table = 'quotes';

    protected $fillable = [
        'quote_id',
        'premium',
        'phcf',
        'levy',
        'duty',
        'total_premium',
        'status',
        'product_id',
        'plan_id',
        'user_id'
    ];

}