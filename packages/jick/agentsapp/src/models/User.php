<?php

namespace Jick\agentsapp\models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'username',
        'firstname',
        'lastname',
        'email',
        'phone',
        'id_no',
        'password',
        'agent_no',
        'active'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    function social() {
        return $this->hasMany('Jick\users\models\Social');
    }
}