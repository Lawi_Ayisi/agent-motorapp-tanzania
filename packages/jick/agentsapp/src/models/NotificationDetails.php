<?php
/**
 * Created by PhpStorm.
 * User: Evans.Wanyama
 * Date: 1/25/2017
 * Time: 11:16 AM
 */

namespace Jick\agentsapp\models;

use Illuminate\Database\Eloquent\Model;


class NotificationDetails extends Model{

    protected $table = 'mobile_notification_details';

    protected $fillable = [''];

} 