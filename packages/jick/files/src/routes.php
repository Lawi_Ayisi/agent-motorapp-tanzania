<?php

Route::group(['prefix' => 'api/file'], function() {

    Route::post('/', 'jick\files\controllers\FileController@store');

    Route::delete('/{filename}', 'jick\files\controllers\FileController@destroy');
});
