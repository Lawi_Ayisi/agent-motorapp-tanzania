<?php

namespace Jick\files\controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Jick\agentsapp\models\Quote;
use Jick\buyonline\models\Delivery_detail;
use Jick\users\models\User;
use Validator;

use Helper;

use jick\files\models\File;

use jick\files\UploadHandler;
use Mail;


class FileController extends Controller
{
    /**
     *
     * Get all files
     *
     */
    public function index()
    {

    }

    public function store(Request $request)
    {

        $file = $request->file('file');

        if (is_array($file)) {
            foreach ($file as $file) {
                $v = Validator::make([$file], [
                    'file' => 'required|image|mimes:jpeg,mp4',
                    'category' => 'required',
                    'filetype' => 'required',
                    'foreign_key' => 'required'
                ]);

                if ($v->fails()) {
                    return response()->json([
                        'message' => $v->errors()->all(),
                        'error' => true
                    ]);
                }

                $resp = $this->saveFile($request, $file);
                $response['fileIds'][] = $resp['fileId'];
            }
            $response['message'] = 'Files uploaded successfully';
        } else {

            $v = Validator::make($request->all(), [
                'file' => 'required|image|mimes:jpeg,jpg,mp4',
                'category' => 'required',
                'filetype' => 'required',
                'foreign_key' => 'required'
            ]);

            if ($v->fails()) {
                return response()->json([
                    'message' => $v->errors()->all(),
                    'error' => true
                ]);
            }

            $response = $this->saveFile($request, $file);
        }

        return response()->json($response);

    }

    private function saveFile($request, $file)
    {

        //generate a unique id and append the original name
        $filename = Helper::UUID() . $file->getClientOriginalName();
        $response = null;
        //Save the image details in the database
        if ($fileDb = File::create([
            'name' => $file->getClientOriginalName(),
            'filename' => $filename,
            'filetype' => $request->filetype,
            'filesize' => $file->getSize(),
            'mimetype' => $file->getMimeType(),
            'url' => url('files/' . $filename),
            'category' => $request->category,
            'extension' => $file->getClientOriginalExtension(),
            'foreign_id' => $request->foreign_key, //policy number
            'document' => $request->document
        ])) {

            //Move file to the correct location
            $file->move('files', $filename);

            $response = [
                'message' => 'File uploaded successfully',
                'id' => $fileDb->id,
                'url' => $fileDb->url
            ];
        }

        $this->sendEmailToUnderwriters($request->foreign_key);

        return $response;
    }

    public function destroy($filename)
    {
        unlink(public_path("files/$filename"));

        //remove from the db too
        File::where('filename', $filename)->delete();
    }

    public function sendEmailToUnderwriters($policyNumber)
    {
        $policy_number = $policyNumber;


        $uid = Quote::where('quote_id', $policy_number)->first()->agent_id;
        $customerId = Quote::where('quote_id', $policy_number)->first()->user_id;

        if (File::where(['foreign_id' => $policy_number, 'document' => 'national_id', 'category' => 'quote'])->exists()) {
            $nid = File::where(['foreign_id' => $policy_number, 'document' => 'national_id', 'category' => 'quote'])->first()->url;
            $n = 1;
        } else {
            return response()->json('The national ID was not uploaded.');
        }

        if (File::where(['foreign_id' => $policy_number, 'document' => 'kra_pin', 'category' => 'quote'])->exists()) {
            $kra = File::where(['foreign_id' => $policy_number, 'document' => 'kra_pin', 'category' => 'quote'])->first()->url;
            $k = 1;
        } else {
            return response()->json('The KRA PIN was not uploaded.');
        }

        if (File::where(['foreign_id' => $policy_number, 'document' => 'driving_licence', 'category' => 'quote'])->exists()) {
            $licence = File::where(['foreign_id' => $policy_number, 'document' => 'driving_licence', 'category' => 'quote'])->first()->url;
            $l = 1;
        } else {
            return response()->json('The Driving Licence was not uploaded.');
        }

        if (File::where(['foreign_id' => $policy_number, 'document' => 'log_book', 'category' => 'quote'])->exists()) {
            $logbook = File::where(['foreign_id' => $policy_number, 'document' => 'log_book', 'category' => 'quote'])->first()->url;
            $b = 1;
        } else {
            return response()->json('The Log Book was not uploaded.');
        }

        $f = User::where('id', $uid)->first()->firstname;
        $ls = User::where('id', $uid)->first()->lastname;
        $customerFname = User::where('id', $customerId)->first()->firstname;
        $customerLname = User::where('id', $customerId)->first()->lastname;
        $email = User::where('id', $customerId)->first()->email;
//        $branch = Agent::where('user_id',$uid)->first()->branch_id;
//        $underwriter = DB::table('underwriters')->where('branch_id', $branch)->first();
        $delivery = Delivery_detail::where('quote_id', $policy_number)->first();
        $content = [
            'policy' => $policy_number,
            'id' => $nid,
            'kra' => $kra,
            'licence' => $licence,
            'logbook' => $logbook,
            'pdf' => public_path('buyonline/quotation_for_' . $this->emailReplace($email) . '.pdf'),
//            'underwriter_name' => $underwriter->name,
//            'underwriter_email' => $underwriter->email
        ];

        $data = [
            'agent' => $f . ' ' . $ls,
            'insured' => $customerFname . ' ' . $customerLname,
            'date' => date("D, d M Y "),
            'quote' => $policy_number,
            'reg' => null, //$request->reg_number,
            'address' => $delivery->physical_address,
            'type' => $delivery->delivery_type
        ];

        $mailSettings = config('app.email_setting');
        if ($mailSettings == "live") { //proced to sendign data to premia if its in live environment
            if ($n = 1 && $k = 1 && $l = 1 && $b = 1) {
                Mail::send('emails.underwriter', $data, function ($m) use ($content) {
//                    $m->to($content['underwriter_email'], $content['underwriter_name'])->subject('New Cover - ' .$content['policy']);
                    $m->to('mobilesales@jubileekenya.com', 'Mobile Sales')->subject('New Cover - ' . $content['policy']);
                    $m->cc('Michael.Kaburu@jubileekenya.com', 'Michael Kaburu');
                    $m->cc('Elias.Kokonya@jubileekenya.com', 'Elias Kokonya');
                    $m->cc('David.Thige@jubileekenya.com', 'David Thige');
                    $m->attach($content['pdf']);
                    $m->attach($content['id']);
                    $m->attach($content['kra']);
                    $m->attach($content['licence']);
                    $m->attach($content['logbook']);
                });
            }
        }

    }

    public function emailReplace($email)
    {
        $email = str_replace('.', '_', $email);
        return $email;
    }
}