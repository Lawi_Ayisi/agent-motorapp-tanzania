<?php

namespace jick\files\models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name',
        'filename',
        'category',
        'document',
        'type',
        'filesize',
        'url',
        'thumbnail_url',
        'description',
        'extension',
        'mimetype',
        'foreign_id'
    ];
}