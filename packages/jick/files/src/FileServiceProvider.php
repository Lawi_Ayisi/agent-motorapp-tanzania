<?php

namespace jick\files;

use Illuminate\Support\ServiceProvider;

class FileServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //Load controllers
        $this->app->make('Jick\files\controllers\FileController');

        //Load models
        $this->app->make('Jick\files\models\File');

        include __DIR__.'/routes.php';
    }
}