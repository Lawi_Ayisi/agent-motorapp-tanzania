<?php

namespace jick\permissions;

use Illuminate\Support\ServiceProvider;

class PermissionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'permissions');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //Load controllers
        $this->app->make('Jick\permissions\controllers\PermissionsController');

        //Load models
        $this->app->make('Jick\permissions\models\Permission');

        include __DIR__.'/routes.php';
    }
}
