<?php

Route::group(['prefix' => 'api/', 'middleware' => 'jwt.auth'], function() {
	Route::resource('permissions', 'jick\permissions\controllers\PermissionsController');
});