<?php

namespace Jick\permissions\models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    //
    protected $fillable = [
        'name'
    ];
}