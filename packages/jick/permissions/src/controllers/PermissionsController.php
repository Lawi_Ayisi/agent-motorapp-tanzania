<?php

namespace Jick\permissions\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Jick\permissions\models\Permission;
use Validator;

class PermissionsController extends Controller
{
    function index()
    {
        return Permission::all();
    }

    function show($id)
    {
        return Permission::find($id);
    }

    function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:permissions'
        ]);

        if($validator->fails()) {
            return response()->json([
                'message' => 'Sorry, an error occured preventing saving',
                'status' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }

        $role = Permission::firstOrNew(['name'=> $request->name]);

        if($role->save())
        {
            return response()->json([
                'message' => 'Permission saved successfully'
            ]);
        }
        //return response()->json($setting->save());
    }

}