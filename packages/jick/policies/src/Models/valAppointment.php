<?php
    namespace Jick\Policies\Models;

    use Illuminate\Database\Eloquent\Model;


    class valAppointment extends Model
    {
        protected $table = 'valappointments';

        protected $fillable = ['user_id', 'valc_id', 'val_date'];
    }

