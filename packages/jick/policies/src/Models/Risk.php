<?php
    namespace Jick\Policies\Models;

    use Illuminate\Database\Eloquent\Model;

    class Risk extends Model
    {
        protected $table = 'risks';

        protected $fillable = [''];
    }
    