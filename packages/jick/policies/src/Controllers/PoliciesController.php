<?php

    namespace jick\policies\Controllers;



    use App\Http\Controllers\Controller;

    use Barryvdh\DomPDF\Facade as PDF;

    use Illuminate\Http\Request;

    use Illuminate\Support\Facades\Mail;

    use jick\files\models\File;
    use Jick\policies\Models\Policy;

    use Jick\policies\Models\valAppointment;

    use Illuminate\Support\Facades\Input;

    use Illuminate\Support\Facades\Validator;

    use jick\Content\Models\ValCenter;

    use jick\files\UploadHandler;

    use Jick\users\models\User;

    use JWTAuth;



    class PoliciesController extends Controller

    {

        function bookAppointment(Request $request) {


            $validator = Validator::make($request->all(), [

                'valc_id' => 'required',

                'val_date' => 'required'

             ]);



            if ($validator->fails()) {


                return response()->json([

                'message' => 'Your request could not be completed.',

                'status' => 0,

                'errors' => $validator->errors()->all()

                ], 422);


            } else {


                $user = JWTAuth::parseToken()->authenticate();

                $uid = $user['id'];



                $f = User::where('id', $uid)->first()->firstname;

                $l = User::where('id', $uid)->first()->lastname;





//                if ($request->cCarVal != '') { $cc = 'Courtesy Car<br/>'; } else { $cc = ''; }
//                if ($request->exProVal != '') { $ep = 'Excess Protector<br/>'; } else { $ep = ''; }
//                if ($request->fpacVal != '') { $fp = 'Fatal Personal Accident Cover<br/>'; } else { $fp = ''; }
//                if ($request->facwVal != '') { $fa = 'Forced ATM Withdrawal<br/>'; } else { $fa= ''; }
//                if ($request->lopeVal != '') { $lpe = 'Loss of Personal Effects<br/>'; } else { $lpe = ''; }
//                if ($request->oosaVal != '') { $oos = 'Out of Station Accomodation<br/>'; } else { $oos = ''; }
//                if ($request->pvtrVal != '') { $pvtr = 'Political Violence & Terrorism Risk<br/>'; } else { $pvtr = ''; }
//                if ($request->aarVal != '') { $aa = 'AA Rescue<br/>'; } else { $aa = ''; }



                $data = [

                    'agent' => $f.' '.$l,

                    'insured' => $request->firstname.' '.$request->lastname,

                    'date' => date("D, d M Y "),
                    'reg' => $request->reg_number,

                    'pvtr' => $request->cCarVal,
                    'oos' => $request->oosaVal,
                    'lpe' => $request->lopeVal,
                    'fa' => $request->facwVal,
                    'cc' => $request->cCarVal,
                    'fp' => $request->fpacVal,
                    'ep' => $request->exProVal,
                    'aa' => $request->aarVal

                ];


                $pdf = PDF::loadView('policies::valuation', $data);

                $pdf->save('policies/valuation_letter_'.$request->email.'.pdf');

                $body = [

                    'agent_email' => User::where('id', $uid)->first()->email,

                    'client_email' => $request->email,

                    'name' => $request->firstname,

                    'pdf' => public_path('policies/valuation_letter_'.$request->email.'.pdf')

                ];

                Mail::send('emails.valuation', $data, function ($m) use ($body) {

                    $m->to($body['client_email'], $body['name'])->subject('Valuation Letter');

                    $m->cc($body['agent_email']);

                    $m->attach($body['pdf']);

                });

                $new = new valAppointment();

                $new->user_id = $uid;

                $new->valc_id = $request->valc_id;

                $new->val_date = $request->val_date;

                $save = $new->save();

            }

            if ($save) {

                return response()->json('Successfully requested for an appointment');

            } else {
                return response()->json('Something went wrong, your request could not be completed');

            }

        }



        function viewAppointments() {

            $user = JWTAuth::parseToken()->authenticate();

            $user_id = $user['id'];



            $appointments = valAppointment::where('user_id', $user_id)->get();



            return response()->json($appointments);

        }



        function editAppointment(Request $request) {

            $app_id = $request->input('app_id');

            $valctr = $request->input('valc_c');



            $update = valAppointment::where('id', $app_id)->update([

                'valc_id' => $valctr,

                'updated_at' => date('Y-m-d H:i:s')

            ]);



            if ($update) {

                return response()->json('Your appointment has been successfully updated.');



            } else {

                return response()->json('Your request could not be completed.');



            }

        }



        function getUserPolicies(){

            $user = JWTAuth::parseToken()->authenticate();

            $user_id = $user['id'];



            // $policy = Policy::where('user_id', $user_id)->first()->policy_no;



            $policies = Policy::join('risks', 'policies.policy_no', '=', 'risks.policy_no')

                ->join('quotes', 'policies.policy_no', '=', 'quotes.quote_id')

                ->join('plans', 'quotes.plan_id', '=', 'plans.id')

                ->select('policies.*', 'risks.*', 'plans.details')

                ->where('policies.user_id', $user_id)

                ->get();



            if ($policies != null) {

              return response()->json($policies);

            } else {

              return response()->json([

                      'message' => 'We could not find any policies registered to you.'

                  ]);

            }



        }



        function editPolicy(Request $request){

            // $policy = $request->input('policy_id');



            // $validator = Validator::make($request->all(), [

            //     'policy_id' => 'required'

            // ]);



            // if ($validator->fails()) {

            //     return response()->json([

            //         'message' => 'Your request could not be completed.',

            //         'status' => 0,

            //         'errors' => $validator->errors()->all()

            //     ], 422);

            // } else {

            //     $update = Policy::where('ID', $policy)->update([]);



            //     return response()->json('You have successfully updated the policy');

            // }

        }









    }
