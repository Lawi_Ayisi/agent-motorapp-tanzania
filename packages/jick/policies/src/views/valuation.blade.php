<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Valuation</title>
    <style>
        body { font-family: Verdana, sans-serif; }
        td, p, ul { padding: 15px; text-align: left; }
        ul { margin-top: -40px; margin-left: 25px; }
        table { margin-left: 15px; margin-right: 15px; }
    </style>

</head>

<body>
<!--table>
    <tr><td><strong>DATE</strong>:  </td></tr>
    <tr><td><strong>INSURED</strong>:  </td></tr>
    <tr><td><strong>QUOTATION NO</strong>:  <td></tr>
    <tr><td><strong>AGENT</strong>: </td></tr>

</table-->

<p><strong>RE: VEHICLE INSPECTION & ANTI-THEFT DEVICE NOTICE <br>
        VEHICLE REG. MARKS: </strong> {{ $reg }} </p>

<p>Thank you for placing your motor insurance with us. <br>
    As a requirement, please have your vehicle inspected free of charge at any of the following approved centers:
</p>

<table style="border: 1px solid #646464;">
    <tr style="border: 1px solid #646464;">
        <td><strong>JUBILEE SERVICE CENTER</strong></td>
        <td>
            Situated along Enterprise Road, Off Mombasa Road near General Motors (Warehouse number 7).
            No booking is required but you can contact Valuation Officers on <strong>0700 330610, 0700 330600,
                0700330608 and 0700 330611 or Center Manager on 0723 166170; Email: valuations@jubileeinsurance.com
            </strong>

        </td>

    </tr>
    <tr>
        <td><strong>REGENT AUTOMOBILE VALUERS</strong></td>
        <td>
            Situated at Lutheran House <strong>(Tel No. 020 -2603906, 0722608210)</strong>, or Westlands Branch,
            Muthithi road Next to Aga Khan Diagnostic Centre <strong>(Tel No. 020 – 2603916, 0711599665)</strong>.
            No booking required but clients are reserved on first-come first-served basis.

        </td>
    </tr>
    <tr>
        <td><strong>INFAMA CENTER</strong></td>
        <td>
            Situated along North Airport Road off Mombasa Road opposite Cool IT Storage (<strong>4902000</strong> or
            <strong>0714674354</strong>) and at <strong>INFAMA Customer Convenience Center, TOYOTSU AUTOMART</strong>
            on Popo road Opposite <strong>KEBS (0704670636, 0750737283)</strong>. Inspection is carried out from
            Monday to Friday from <strong>8:00 a.m. to 3:00 p.m.</strong> and on Saturdays from
            <strong>8.00 a.m. to 12.00 noon</strong>. To book, email booking@infamagroup.com

        </td>

    </tr>

</table>
<br><br><br>

<p><strong style="text-decoration: underline">Important:</strong></p>
<ul>
    <li>Please provide the Valuations Officer with proof of ownership of the vehicle (a copy of log book or
        importation documents).</li>
    <li>Inspection must be carried out within <strong>Thirty (30) Calendar </strong>days of the commencement of the policy failure
        to which the cover will automatically reduce to <strong>Third Party Only</strong>.</li>
    <li>Ensure that you carry this letter with you to avoid being charged.</li>

</ul>

<p>Thank you for entrusting us with your insurance.</p>
<p><strong>Yours,</strong></p>
<p>
    <strong>
        General Business, <br>
        Jubilee Insurance
    </strong>
</p>

</body>

</html>