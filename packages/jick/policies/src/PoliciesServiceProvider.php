<?php
    namespace Jick\policies;

    use Illuminate\Support\ServiceProvider;

    class PoliciesServiceProvider extends ServiceProvider
    {
        public function boot() {
            $this->loadViewsFrom(__DIR__.'/views', 'policies');
        }

        public function register() {
            //Load controllers
            $this->app->make('Jick\policies\Controllers\PoliciesController');

            //Load models
            $this->app->make('Jick\policies\Models\Policy');
            $this->app->make('Jick\policies\Models\valAppointment');
            $this->app->make('Jick\policies\Models\Risk');

            include __DIR__.'/routes.php';
        }
    }