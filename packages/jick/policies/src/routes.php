<?php
    Route::group(['middleware' => 'jwt.auth', 'prefix' => 'api/'], function() {
        Route::get('policies', 'jick\policies\controllers\PoliciesController@getUserPolicies');
        Route::get('appointments', 'jick\policies\controllers\PoliciesController@viewAppointments');
        Route::post('book-appointment', 'jick\policies\controllers\PoliciesController@bookAppointment');
        Route::post('edit-appointment', 'jick\policies\controllers\PoliciesController@editAppointment');
        Route::get('claim-no', 'jick\claims\controllers\ClaimsController@makeClaim');
    });