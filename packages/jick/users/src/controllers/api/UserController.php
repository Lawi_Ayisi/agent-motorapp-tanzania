<?php

namespace Jick\users\controllers\api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;
use Socialite;
use Mail;
use Illuminate\Support\Facades\Input;

use Auth;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Helper;

use Jick\users\models\User;
use Jick\roles\models\Role;
use Jick\users\models\VerifyUser;
use Jick\users\models\Social;

class UserController extends Controller
{
     /**
      *
      * Get all users
      *
      */
    public function index(Request $request)
    {
        // $user = ['name'=>'Mike',
        //             'email' => 'expertprograma@gmail.com',
        //             'code' => 12345];
        //   $r =  Mail::send('emails.verify', ['user' => $user], function ($m) use ($user) {
        //     //$m->from('hello@app.com', 'Your Application');

        //     $m->to($user['email'], $user['name'])->subject('Your Reminder!');
        // });

        // //   Mail::send('emails.verify', ['confirmation_code' => '1234567'], function($message) {
        // //             $message->to('mike.nyakundi@jubileekenya.com', 'Michira')
        // //                 ->subject('Verify your email address');
        // //         });


        //     return $r;
        //$resp = Helper::sendSms('Enter the code: '.rand (0,100000).' to load your policies', '+254713621589');
        //return $this->verifyUser('col',;// rand (0,100000);
        return User::with('roles')->orderBy('created_at', 'desc')->paginate(Input::get('limit', 10));
    }


      /**
      *
      * Get one user
      *
      */
    public function show($id)
    {
        return User::with('roles')->find($id);
    }


    /**
     *
     * Update a user
     *
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'roleId' => 'required|numeric'
        ]);

        if($validator->fails()) {
            return response()->json([
                'message' => 'Form must be filled out.',
                'status' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }

        $user = User::find($id);

       $role = Role::find($request->roleId);

       //Assign role to the user if not assigned yet
       if( ! $user->hasRole($role->name))
       {
            $user->assignRole($role->name);
       }

       if($user->save())
       {
           return response()->json([
               'message' => 'User updated successfully',
               'status'  => 1
           ]);
       }
    }

    /**
      *
      * update user profile
      *
      */
    public function profile($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'email' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'username' => 'required',
            'phone' => 'required',
            'id_no' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'message' => 'Sorry, an error occured preventing updating',
                'status' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }

         $result = User::where('id', $id)
                    ->update(['username' => $request->username,
                        //'email' => $request->email,
                        //'username' => $request->name,
                        'phone' => $request->phone,
                        'id_no' => $request->id_pp_no
                        ]);
        if($result)
        {
            return response()->json([
                'message' => 'User profile successfully updated'
            ]);
        }
        else
        {
             return response()->json([
                'message' => 'User profile not updated'
            ]);
        }
    }

    /**
      *
      * Delete a user
      *
      */
   public function destroy($id)
    {
        $result = User::destroy($id);

        if($result)
        {
            return response()->json([
                'message' => 'User successfully deleted'
            ]);
        }
        else
        {
             return response()->json([
                'message' => 'User not deleted'
            ]);
        }
    }


    /**
     *
     * Login
     *
     */
     public function login(Request $request)
     {
         // grab credentials from the request
         $credentials = $request->only('email', 'password');

         $validator = Validator::make($request->all(), [
             'email' => 'required',
             'password' => 'required'
         ]);


         if($validator->fails()) {
             return response()->json([
                 'message' => 'Email and Password fields must be filled out',
                 'status' => 0,
                 'errors' => $validator->errors()->all()
             ], 422);
         }

         //Support for different logins
         //$login_type =

         try {
             // attempt to verify the credentials and create a token for the user
             if (! $token = JWTAuth::attempt($credentials)) {
                 return response()->json(['message' => 'Invalid Credentials'], 401);
             }

             //Is the user active
             $user = Auth::user();

             if( ! $user->active) {
                 return response()->json(['message' => 'Please activate your account first'], 401);
             }



         } catch (JWTException $e) {
             // something went wrong whilst attempting to encode the token
             return response()->json(['message' => 'could_not_create_token'], 500);
         }

          if(!$request->login_type) {

             // all good so return the token
                 $luser = User::with('roles')->where('email', $request->email)->first();

                 $login_response = [
                   'message' => 'Login Successful',
                   'token' => $token,
                   'user'  => $luser
                 ];

          }
          else if($this->verifyUser($request->login_type, $user) === true) {
                  // all good so return the token
                  $luser = User::with('roles')->where('email', $request->email)->first();

                  $login_response = [
                    'message' => 'Login Successful',
                    'token' => $token,
                    'user'  => $luser
                  ];

          }
          else{
            $luser = User::with('roles')->where('email', $request->email)->first();

            $login_response = [
              'token' => $token,
              'user'  => $luser
            ];

          }

          return response()->json(['data' => $login_response]);

     }

    /**
    * confirm the OTP
    */
    public function confirmOTP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required'
        ]);


        if($validator->fails()) {
            return response()->json([
                'message' => 'OTP field must be supplied',
                'status' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }

        //Get the one time password
            $otp = $request->otp;

        //Get the logged in user
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

            $response = VerifyUser::where(['otp' => $otp,'user_id' => $user_id])->first();


            if($response != null)
            {

                //update the status
                $response->status = 'verified';

                //overide the code
                $response->otp = 0;

                $response->save();

                return response()->json(['message' =>'code verified']);

            }
            else{
                return response()->json([
                'message' => 'The code entered is invalid',
            ], 422);
            }
    }

    /**
    *Verify user
    */
      private function verifyUser($login_type,$user)
      {
            //Get the user id
            $user_id = $user->id;

            //Get the user phone number
            $user_phone = $user->phone;

            if($login_type == 'col')
            {
                $response = VerifyUser::where(['module' => $login_type, 'user_id' => $user_id,'status' => 'verified'])->first();

                if($response != null)
                {
                    return true;

                }
                elseif(!$user_phone) {
                    return true;
                }
                else{
                    //generate the random integer
                    $otp = rand (0,100000);

                    //check whether the otp generated already exists in the db
                    $response = VerifyUser::where(['otp' => $otp])->first();

                    if($response != null){
                        $otp = rand (0,100000);
                    }

                    //insert the otp into the database
                    $verify = VerifyUser::firstOrNew(['user_id' => $user_id]);

                     if($response == null){
                        //send the sms message
                        //Helper::sendSms('Enter the code: '.$otp.' to load your policies', '+254713621589');
                    }

                    //send the sms message
                    //Helper::sendSms('Enter the code: '.$otp.' to load your policies', '+254713621589');


                    $verify->module = $login_type;
                    $verify->otp = $verify->otp ? $verify->otp : $otp;
                    $verify->status = 'unverified';

                    $verify->save();

                    //send the sms message
                    // Helper::sendSms('Enter the code: '.$otp.' to load your policies', '+254713621589');

                    //Return a response
                    return false;
                }
            }
            else{

                return true;
            }
      }
    /**
     *
     * Register action
     *
     *@return json
     *
     */
    public function register(Request $request)
    {
        // /return getallheaders();
        $credentials = $request->all();

        //Generate an account activation code
        $confirmation_code = str_random(30);

        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:users,username',
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|unique:users,email|email',
            'phone' => 'required',
            'id_no' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'message' => 'Validation Failed',
                'active' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }

        try {

            //Create a password
            $credentials['password'] = bcrypt($credentials['password']);

            $user = User::firstOrNew(['email' => $request->email]);

            //Set user to be inactive
            $user->active = 0;

            //Assign the confirmation code
            $user->confirmation_code = $confirmation_code;

            $user->fill($credentials);

            $status = $user->save();

        } catch (Exception $e) {
            return response()->json(['message' => 'User already exists.', 'status' => 0], HttpResponse::HTTP_CONFLICT);
        }

        $token = JWTAuth::fromUser($user);

        //Add token to the user array
        $user['token'] = $token;

        //Send account activation mail on successfull registration
        if($status === true) {

            Mail::send('emails.verify', ['confirmation_code' => $confirmation_code], function($message) {
                $message->to(Input::get('email'), Input::get('username'))
                    ->subject('Verify your email address');
            });
        }

        return response()->json([
            'message' => 'Registration successfull. Please check your email for instructions to activate your account.',
            'status' => $status,
            //'user' => $user
        ]);
    }


    /**
     *
     * Check if the user is unique using email or username
     *
     */
    public function checkUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'source' => 'required|in:username,email',
            'value'  => 'required'
        ]);

        if($validator->fails())
        {
            return response()->json([
                'message' => 'Not allowed',
                'status' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }

        $source = $request->source;
        $value  = $request->value;

        $user = User::where($source, $value)->first();

        if( User::where($source, $value)->count() > 0)
        {
            return response()->json([
                'message' => "$source taken"//ucfirst($source)." has already been taken"
            ], 400); //Not unique
        }
        else
        {
            return response()->json([
                'message' => ucfirst($source)." is unique"
            ]); //unique
        }
    }

    /**
    *
    *Function to do the account activation
    *@returns json
    *
    */
    public function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
                return response()->json([
                'message' => 'No confirmation code',
            ]);
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if ( ! $user)
        {
                return response()->json([
                'message' => 'Invalid activation code',
            ]);
        }

        $user->active = 1;
        $user->confirmation_code = null;
        $user->save();

         return response()->json([
            'message' => 'You have successfully verified your account.',
        ]);
    }


    public function getSocialRedirect( $provider )
    {
        $providerKey = \Config::get('services.' . $provider);
        if(empty($providerKey))
            return view('pages.status')
                ->with('error','No such provider');

       // return response()->json($providerKey);

        return Socialite::driver( $provider )->redirect();

    }

    public function getSocialHandle( $provider, Request $request)
    {
        if ($request->has('redirectUri')) {
            config()->set("services.{$provider}.redirect", $request->get('redirectUri'));
        }

        try {
            $user = Socialite::driver( $provider )->stateless()->user();//->userFromToken($request->id_token);
        } catch(Exception $e) {
            return $e->getMessage();
        }

        $socialUser = null;

        //Check is this email present
        $userCheck = User::where('email', '=', $user->email)->first();

        //return response()->json($user);
        if(!empty($userCheck))
        {
            $socialUser = $userCheck;
        }
        else
        {
            $sameSocialId = Social::where(array(
                'social_id' => $user->id,
                'provider' => $provider ))
                ->first();

            if(empty($sameSocialId))
            {
                //There is no combination of this social id and provider, so create new one
                $newSocialUser = new User;
                $newSocialUser->email              = $user->email;
                $name = explode(' ', $user->name);
                $newSocialUser->firstname         = $name[0];
                $newSocialUser->lastname          = $name[1];
                //$newSocialUser->name               = $user->name;
                $newSocialUser->username           = $name[0].$name[1];

                //We trust you. Autoactivate
                $newSocialUser->active             = 1;
                $newSocialUser->save();

                $socialData = new Social;
                $socialData->social_id = $user->id;
                $socialData->provider= $provider;
                $newSocialUser->social()->save($socialData);

                // Add role
                $role = Role::whereName('user')->first();
                $newSocialUser->assignRole($role);

                $socialUser = $newSocialUser;
            }
            else
            {
                //Load this existing social user
                $socialUser = $sameSocialId->user;
            }

        }

        $token = JWTAuth::fromUser($socialUser);

        return response()->json([
            'message' => 'Login Successful',
            'token' => $token,
            'user'  => $socialUser
        ]);

        // $this->auth->login($socialUser, true);

        // if( $this->auth->user()->hasRole('user'))
        // {
        //     return redirect()->route('user.home');
        // }

        // if( $this->auth->user()->hasRole('administrator'))
        // {
        //     return redirect()->route('admin.home');
        // }

        //return \App::abort(500);
    }

    public function refreshToken() {
        $token = JWTAuth::getToken();
        $newToken = JWTAuth::refresh($token);

        return response()->json(['token' => $newToken]);
    }

}
