<?php

namespace Jick\users\controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function postEmail(Request $request)
     {
        $v = Validator::make($request->only('email'), ['email' => 'required|email']);

        if($v->fails())
        {
            return response()->json(['message' => 'Email address is absent']);
        }

        $broker = $this->getBroker();

        $response = Password::broker($broker)->sendResetLink(
            $this->getSendResetLinkEmailCredentials($request),
            $this->resetEmailBuilder()
        );


        // $response = $this->passwords->sendResetLink($request->only('email'), function($m)
        // {
        //     $m->subject($this->getEmailSubject());
        // });

        switch ($response)
        {
            case Password::RESET_LINK_SENT:
                $message = "Password recovery link has been send to your email address";
               // Toastr::success($message, $title = null, $options = []);
                return response()->json(trans($response));

            case Password::INVALID_USER:
                return response()->json(trans($response));
        }
     }
}
