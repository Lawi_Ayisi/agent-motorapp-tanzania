<?php

namespace Jick\users\controllers;

use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use Jick\Settings\models\Agent;
use Validator;
use Socialite;
use Mail;
use Illuminate\Support\Facades\Input;

use Auth;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Helper;

use Jick\users\models\User;
use Jick\roles\models\Role;
use Jick\users\models\VerifyUser;
use Jick\users\models\Social;

class UserController extends Controller
{
    /**
     *
     * Get all users
     *
     */
    public function index(Request $request)
    {
        // $user = ['name'=>'Mike',
        //             'email' => 'expertprograma@gmail.com',
        //             'code' => 12345];
        //   $r =  Mail::send('emails.verify', ['user' => $user], function ($m) use ($user) {
        //     //$m->from('hello@app.com', 'Your Application');

        //     $m->to($user['email'], $user['name'])->subject('Your Reminder!');
        // });

        // //   Mail::send('emails.verify', ['confirmation_code' => '1234567'], function($message) {
        // //             $message->to('mike.nyakundi@jubileekenya.com', 'Michira')
        // //                 ->subject('Verify your email address');
        // //         });


        //     return $r;
        //$resp = Helper::sendSms('Enter the code: '.rand (0,100000).' to load your policies', '+254713621589');
        //return $this->verifyUser('col',;// rand (0,100000);
        return User::with('roles')->orderBy('created_at', 'desc')->paginate(Input::get('limit', 10));
    }


    /**
     *
     * Get one user
     *
     */
    public function show($id)
    {
        return User::with('roles')->find($id);
    }


    /**
     *
     * Update a user
     *
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'roleId' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Form must be filled out.',
                'status' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }

        $user = User::find($id);

        $role = Role::find($request->roleId);

        //Assign role to the user if not assigned yet
        if (!$user->hasRole($role->name)) {
            $user->assignRole($role->name);
        }

        if ($user->save()) {
            return response()->json([
                'message' => 'User updated successfully',
                'status' => 1
            ]);
        }
    }

    /**
     *
     * update user profile
     *
     */
    public function profile($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'email' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'username' => 'required',
            'phone' => 'required',
            'id_no' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Sorry, an error occurred preventing updating',
                'status' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }

        $result = User::where('id', $id)
            ->update(['username' => $request->username,
                //'email' => $request->email,
                //'username' => $request->name,
                'phone' => $request->phone,
                'id_no' => $request->id_pp_no
            ]);
        if ($result) {
            return response()->json([
                'message' => 'User profile successfully updated'
            ]);
        } else {
            return response()->json([
                'message' => 'User profile not updated'
            ]);
        }
    }

    /**
     *
     * Delete a user
     *
     */
    public function destroy($id)
    {
        $result = User::destroy($id);

        if ($result) {
            return response()->json([
                'message' => 'User successfully deleted'
            ]);
        } else {
            return response()->json([
                'message' => 'User not deleted'
            ]);
        }
    }


    /**
     *
     * Login
     *
     */
    public function login(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);


        if ($validator->fails()) {
            return response()->json([
                'message' => 'Email and Password fields must be filled out',
                'status' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }

        //Support for different logins
        //$login_type =

        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['message' => 'Invalid Credentials'], 401);
            }

            //Is the user active
            $user = Auth::user();

            if (!$user->active) {
//                return response()->json(['message' => 'Please activate your account first'], 401);
                return response()->json(['message' => 'Your Account needs activation, please contact your Manager'], 401);
            }


        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['message' => 'could_not_create_token'], 500);
        }

        if (!$request->login_type) {

            // all good so return the token
            $luser = User::with('roles')->where('email', $request->email)->first();

            $login_response = [
                'message' => 'Login Successful',
                'token' => $token,
                'user' => $luser
            ];

        } else if ($this->verifyUser($request->login_type, $user) === false) {
            // all good so return the token
            $luser = User::with('roles')->where('email', $request->email)->first();

            $login_response = [
                'message' => 'otp sent',
                'token' => $token,
                'user' => $luser,
                'otp' => 1
            ];

        } else {
            $luser = User::with('roles')->where('email', $request->email)->first();

            $login_response = [
                'message' => 'Login Successful',
                'token' => $token,
                'user' => $luser
            ];

        }

        return response()->json(['data' => $login_response]);

    }

    /**
     * confirm the OTP
     */
    public function confirmOTP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required'
        ]);


        if ($validator->fails()) {
            return response()->json([
                'message' => 'OTP field must be supplied',
                'status' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }

        //Get the one time password
        $otp = $request->otp;

        //Get the logged in user
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $response = VerifyUser::where(['otp' => $otp, 'user_id' => $user_id])->first();


        if ($response != null) {

            //update the status
            $response->status = 'verified';

            //overide the code
            $response->otp = 0;

            $response->save();

            return response()->json(['message' => 'code verified']);

        } else {
            return response()->json([
                'message' => 'The code entered is invalid',
            ], 422);
        }
    }

    /**
     *
     * Verify a user.
     * @return {bool} true/false
     */
    private function verifyUser($login_type, $user)
    {
        //Get the user id
        $user_id = $user->id;

        //Get the user phone number
        $user_phone = $user->phone;

        if ($login_type == 'col') {
            $response = VerifyUser::where([
                'module' => $login_type,
                'user_id' => $user_id,
                'status' => 'verified'
            ])->first();

            if ($response != null) {
                return true;

            } elseif (!$user_phone) {
                return true;
            } else {

                //generate the random integer
                $otp = rand(0, 100000);

                //check whether the otp generated already exists in the db
                $response = VerifyUser::where(['otp' => $otp])->first();

                if ($response != null) {
                    $otp = rand(0, 100000);
                }

                //send the sms message
                Helper::sendSms('Enter the code: ' . $otp . ' to load your policies', $user_phone);

                //insert the otp into the database
                $verify = VerifyUser::firstOrNew(['user_id' => $user_id]);

                $verify->module = $login_type;
                $verify->otp = $otp;
                $verify->status = 'unverified';

                $verify->save();

                //Return a response
                return false;
            }
        } else {

            return true;
        }
    }

    /**
     *
     * Register action
     *
     * @return json
     *
     */
    public function mobileRegister(Request $request)
    {
        $credentials = $request->all();

        //Generate an account activation code
        $confirmation = mt_rand(111111, 999999);

        $validator = Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|unique:users,email|email',
            'phone' => 'required|unique:users',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation Failed',
                'active' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }

        try {

            //Create a password
            $credentials['password'] = bcrypt($credentials['password']);

            $user = User::firstOrNew(['email' => $request->email]);

            //Set user to be inactive
            $user->active = 0;

            //Assign the confirmation code
            $user->confirmation_code = $confirmation;

            $user->fill($credentials);

            $user->username = $request->email;

            $status = $user->save();

        } catch (Exception $e) {
            return response()->json(['message' => 'User already exists.', 'status' => 0], HttpResponse::HTTP_CONFLICT);
        }

        $token = JWTAuth::fromUser($user);

        //Add token to the user array
        $user['token'] = $token;

        //Send account activation mail on successfull registration
        if ($status === true) {
            Helper::sendSms('Enter the code: ' . $confirmation . ' to verify your account', $request->phone);
        }

        return response()->json([
            'message' => 'Registration successful. Use the code sent to your phone to verify your account.',
            'status' => $status,
            //'user' => $user
        ]);
    }

    public function mobileConfirmUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'confirm_code' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'All fields are required.',
                'status' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        } else {
            $phone = $request->phone;
            $code = $request->confirm_code;

            if (User::where('phone', $phone)->exists()) {
                $v = User::where('phone', $phone)->first()->confirmation_code;
                $u = User::where('phone', $phone)->first()->get();
                $a = User::where('phone', $phone)->first()->active;

                if ($a == 1) {
                    return response()->json([
                        'message' => 'Your account is active.',
                        'status' => 3
                    ]);
                }

                if ($v != null || $u != null) {
                    if ($v == $code) {
                        $activate = User::where('phone', $phone)->update(['active' => 1, 'confirmation_code' => '']);
                        if ($activate) {
                            return response()->json([
                                'message' => 'Your account has been activated.',
                                'status' => 0
                            ]);
                        } else {
                            return response()->json([
                                'message' => 'Your account was not activated, try again later.',
                                'status' => 2
                            ], 422);
                        }

                    } else {
                        return response()->json([
                            'message' => 'Invalid Credentials.',
                            'status' => 1
                        ], 422);
                    }

                }
            } else {
                return response()->json([
                    'message' => 'Invalid credentials.',
                    'status' => 1
                ], 422);
            }
        }
    }


    public function register(Request $request)
    {
        // /return getallheaders();
        $credentials = $request->all();

        //Generate an account activation code
        $confirmation_code = str_random(30);

        $validator = Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|unique:users,email|email',
            'phone' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation Failed',
                'active' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }

        try {

            //Create a password
            $credentials['password'] = bcrypt($credentials['password']);

            $user = User::firstOrNew(['email' => $request->email]);

            //Set user to be inactive
            $user->active = 0;

            //Assign the confirmation code
            $user->confirmation_code = $confirmation_code;

            $user->fill($credentials);

            $user->username = $request->email;

            $status = $user->save();

        } catch (Exception $e) {
            return response()->json(['message' => 'User already exists.', 'status' => 0], HttpResponse::HTTP_CONFLICT);
        }

        $token = JWTAuth::fromUser($user);

        //Add token to the user array
        $user['token'] = $token;

        //Send account activation mail on successfull registration
        if ($status === true) {

            Mail::send('emails.verify', ['confirmation_code' => $confirmation_code, 'user' => $user], function ($message) {
                $message->to(Input::get('email'), Input::get('username'))
                    ->subject('Verify your email address');
            });
        }

        return response()->json([
            'message' => 'Registration successfull. Please check your email for instructions to activate your account.',
            'status' => $status,
            //'user' => $user
        ]);
    }


    /**
     *
     * Check if the user is unique using email or username
     *
     */
    public function checkUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'source' => 'required|in:username,email',
            'value' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Not allowed',
                'status' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }

        $source = $request->source;
        $value = $request->value;

        $user = User::where($source, $value)->first();

        if (User::where($source, $value)->count() > 0) {
            return response()->json([
                'message' => "$source taken"//ucfirst($source)." has already been taken"
            ], 400); //Not unique
        } else {
            return response()->json([
                'message' => ucfirst($source) . " is unique"
            ]); //unique
        }
    }

    /**
     *
     *Function to do the account activation
     * @returns json
     *
     */
    public function confirm($confirmation_code)
    {
        if (!$confirmation_code) {
            return response()->json([
                'message' => 'No confirmation code',
            ]);
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if (!$user) {
            return response()->json([
                'message' => 'Invalid activation code',
            ]);
        }

        $user->active = 1;
        $user->confirmation_code = null;
        $user->save();

        return response()->json([
            'message' => 'You have successfully verified your account.',
        ]);
    }


    public function getSocialRedirect($provider)
    {
        $providerKey = \Config::get('services.' . $provider);
        if (empty($providerKey))
            return view('pages.status')
                ->with('error', 'No such provider');

        // return response()->json($providerKey);

        return Socialite::driver($provider)->redirect();

    }

    public function getSocialHandle($provider, Request $request)
    {
        if ($request->has('redirectUri')) {
            config()->set("services.{$provider}.redirect", $request->get('redirectUri'));
        }

        try {
            $user = Socialite::driver($provider)->stateless()->user();//->userFromToken($request->id_token);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        $socialUser = null;

        //Check is this email present
        $userCheck = User::where('email', '=', $user->email)->first();

        //return response()->json($user);
        if (!empty($userCheck)) {
            $socialUser = $userCheck;
        } else {
            $sameSocialId = Social::where(array(
                'social_id' => $user->id,
                'provider' => $provider))
                ->first();

            if (empty($sameSocialId)) {
                //There is no combination of this social id and provider, so create new one
                $newSocialUser = new User;
                $newSocialUser->email = $user->email;
                $name = explode(' ', $user->name);
                $newSocialUser->firstname = $name[0];
                $newSocialUser->lastname = $name[1];
                //$newSocialUser->name               = $user->name;
                $newSocialUser->username = $name[0] . $name[1];

                //We trust you. Autoactivate
                $newSocialUser->active = 1;
                $newSocialUser->save();

                $socialData = new Social;
                $socialData->social_id = $user->id;
                $socialData->provider = $provider;
                $newSocialUser->social()->save($socialData);

                // Add role
                $role = Role::whereName('user')->first();
                $newSocialUser->assignRole($role);

                $socialUser = $newSocialUser;
            } else {
                //Load this existing social user
                $socialUser = $sameSocialId->user;
            }

        }

        $token = JWTAuth::fromUser($socialUser);

        return response()->json([
            'message' => 'Login Successful',
            'token' => $token,
            'user' => $socialUser
        ]);

        // $this->auth->login($socialUser, true);

        // if( $this->auth->user()->hasRole('user'))
        // {
        //     return redirect()->route('user.home');
        // }

        // if( $this->auth->user()->hasRole('administrator'))
        // {
        //     return redirect()->route('admin.home');
        // }

        //return \App::abort(500);
    }

    public function refreshToken()
    {
        $token = JWTAuth::getToken();
        $newToken = JWTAuth::refresh($token);

        return response()->json(['token' => $newToken]);
    }

    //Register Customer
    public function registerCustomer(Request $request)
    {
        $user = User::firstOrNew(['email' => $request->email]);
        $user->firstname = $request->firstName;
        $user->lastname = $request->lastName;
        $user->id_no = $request->idNumber;
        $user->phone = $request->phoneNumber;
        $user->email = $request->email;
        $user->username = $request->email;
        $user->password = bcrypt($request->password);
        $user->active = 1;
        $user->account_type = $request->account_type;
        $save = $user->save();
        if ($save) {
            $mesg = "Record Saved";
        } else {
            $mesg = "Error trying to register, Please try again later! If this persists contact Customer Care.";
        }
        return response()->json([
            'message' => $mesg
        ]);
    }

    public function registerAgent(Request $request)
    {
        $confirmation_code = rand(0, 100000);

        $fname = $request->firstName;
        $oname = $request->lastName;
        $email = $request->email;
        $phone = $request->phoneNumber;
        $agency_name = $request->agencyName;
        $agent_code = $request->agentCode;
        $checkAgentCode = $this->verifyAgentCode($agent_code);
        $agency = $request->agency;
        //$is_manager = $request->input('is_manager');

        $user = User::firstOrNew(['email' => $email]);
        $user->firstname = $fname;
        $user->lastname = $oname;
        $user->username = $email;
        $user->email = $email;
        $user->phone = $phone;
        if ($checkAgentCode) {//we have agent code verified, activate user and do not send email for verification
            $user->active = 1;
        }
        $user->confirmation_code = $confirmation_code;
        $user->account_type = $request->account_type;

        if ($user->save()) {

            $code = $user->confirmation_code;
            //send confirmation code;
            Helper::sendSms('Dear, ' . $fname . ' use this code to activate your account: ' . $code, $phone);
//            $agent = new Agent();
            $agent = Agent::firstOrNew(['agent_no' => $agent_code, 'user_id' => $user->id]);
            $agent->agent_no = $agent_code;
            $agent->agency = $agency;
            $agent->user_id = $user->id;
            $agent->company_name = $agency_name;

            if (!$checkAgentCode) {// we dint get this agent code, then send email to Dan for verification
                $emailMessage = $this->sendEmail($request);
            }
            if ($agent->save()) {
                $status = "success";
                $message = "Agent Account has been created successfully, proceed to activate account";
            }
        } else {
            $status = "failure";
            $message = "Agent Account has not been created";
        }

        return response()->json([
            'status' => $status, 'message' => $message
        ]);
    }

    public function sendEmail($dataz)
    {

        $data = [
            'name' => $dataz->firstName . ' ' . $dataz->lastName,
            'email' => $dataz->email,
            'phone' => $dataz->phoneNumber,
            'agencyName' => $dataz->agencyName,
            'agentCode' => $dataz->agentCode,
            'agency' => $dataz->agency,
        ];

        $path = realpath('');

        Mail::send('agentsapp::email.agent_registration', $data, function ($m) use ($data, $path) {

            $m->replyTo($data['email'], $data['name'])->subject('Agent Account Registration - General');
//            $m->from('general@jubileeinsurance.com')->subject('Pre Inspection');

            $mailSettings = config('app.email_setting');
            if ($mailSettings == "live") {
                $m->to('Daniel.Omondi@Jubileekenya.com', 'Daniel Omondi');
                $m->cc('Elias.Kokonya@jubileekenya.com', 'Elias Kokonya');
                $m->cc('david.thige@jubileekenya.com', 'David Thige');
            } elseif ($mailSettings == "dev") {
                $m->to('david.thige@jubileekenya.com', 'David Thige');
            }
        });

        return "email sent successfully";
    }

    public function confirmAgentAccount(Request $request)
    {
        $email = $request->email;
        $code = $request->code;

        $check = User::where(['email' => $email, 'confirmation_code' => $code])->get();
        if ($check->count() > 0) {
            $updateUser = User::where(['email' => $email, 'confirmation_code' => $code])
                ->update(array('confirmation_code' => null));
            if ($updateUser) {
                $status = "success";
                $message = "Confirmation Code found";
            }
        } elseif ($check->count() < 1) {
            $status = "failure";
            $message = "Confirmation Code not found";
        }

        return response()->json([
            'status' => $status, 'message' => $message
        ]);
    }

    public function createAgentPassword(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $isPasswordReset = $request->passReset; //true or false as string

        $check = User::where(['email' => $email])->get();
        if ($check->count() > 0) {
            $mPassword = bcrypt($password);
            if ($isPasswordReset == "true") {//is is true
                $updateUserPass = User::where(['email' => $email])
                    ->update(array('password' => $mPassword, 'active' => 1)); // activate agent to enable login
            } else {//is false
                $updateUserPass = User::where(['email' => $email])
                    ->update(array('password' => $mPassword, 'active' => 0)); // activate agent to enable login
            }
            if ($updateUserPass) {
                $status = "success";
                $message = "Password Created Successfully for " . $email;
            }
        } elseif ($check->count() < 1) {
            $status = "failure";
            $message = "Failed to create password for " . $email;
        }

        return response()->json([
            'status' => $status, 'message' => $message
        ]);
    }

    public function formatPhoneNumber($phone_number)
    {
        $number = '';
        $phone_number = str_replace(' ', '', $phone_number);
        $code = 254;
        $offset = 0;
        if (strlen($phone_number) == 13) {
            $offset = 4;
            $code = substr($phone_number, 1, 3);
        } elseif (strlen($phone_number) == 12) {
            $offset = 3;
            $code = substr($phone_number, 0, 3);
        } elseif (strlen($phone_number) == 10) {
            $offset = 1;
        }

        //Phone number variation
        $unique = substr($phone_number, $offset);
        $number = $code . $unique;

        return $number;
    }

    public function resetPassword(Request $request)
    {
        $email = $request->email;
        $confirmation_code = rand(0, 100000);
        //check if user exists
        $userCnt = User::where('email', $email)->count();
        if ($userCnt > 0) {
            $user = User::firstOrNew(['email' => $email]);
            $user->confirmation_code = $confirmation_code;
            if ($user->save()) {
                $code = $user->confirmation_code;
                $fname = $user->firstname;
                $phone = $user->phone;
                //send confirmation code;
                Helper::sendSms('Dear, ' . $fname . ', Enter the code: ' . $code . ' to Reset your Account', $phone);

                $status = "success";
                $message = "Reset Code has been sent through SMS";
            } else {
                $status = "failure";
                $message = "Reset Code was not sent";
            }
        } else {
            $status = "failure";
            $message = "This email address does not exist";
        }
        return response()->json(['status' => $status, 'message' => $message]);
    }

    public function verifyAgentCode($agentKode)
    {
        try {
            //$agentCode = '150022';
            $agentCode = $agentKode;
            $tableUAT = 'PREMIA.VW_AGENT_INFO';
            $tablePROD = 'PREMIA.VW_AGENT_INFO';

            $entities = DB::connection('premier_connect_uat')->table($tableUAT)
                ->where('AGENT_CODE', $agentCode)->get();
//            ->where('CUST_CODE', $agentCode)->get();

//        $entities = DB::connection('premier_connect_uat')->select('SELECT * FROM PREMIA.VW_AGENT_INFO WHERE CUST_CODE = \'150022\'');

            if ($entities) {
                return 1;
            } else {
                return 0;
            }
        } catch (QueryException $e) {
           return Helper::sendSms('PREMIA.VW_AGENT_INFO '.$e->getMessage(), '254726913264');
        } catch (\Exception $e) {
          return  Helper::sendSms('PREMIA.VW_AGENT_INFO '.$e->getMessage(), '254726913264');
        }
    }
}
