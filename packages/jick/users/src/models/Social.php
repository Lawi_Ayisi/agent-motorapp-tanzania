<?php

namespace Jick\users\models;

use Illuminate\Database\Eloquent\Model;

class Social extends Model {

    protected $table = 'social_logins';

    public function user()
    {
        return $this->belongsTo('Jick\users\models\User');
    }
}