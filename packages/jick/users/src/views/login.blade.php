@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-sm-6">


            <form method="POST" action="/auth/login" id="signin">


                <div class="form-group">
                    Email
                    <input type="text" name="username" class="form-control">
                </div>

                <div>
                    Password
                    <input type="password" name="password" class="form-control" id="password">
                </div>

                <div>
                    <input type="checkbox" name="remember"> Remember Me
                </div>

                <div>
                    <button type="submit" class="btn btn-danger">Login</button>
                </div>
            </form>
        </div>
    </div>

@endsection