<?php

Route::get('login', function () {
    return view('users::login');
});


Route::get('register', function () {
    return view('users::login');
});

Route::group(['middleware' => 'auth', 'prefix' => 'admin/'], function () {
    Route::get('users', 'Jick\users\controllers\UserController@index');
});


Route::get('password-gene', function () {
    echo bcrypt('benjamin123');
    //dd(DB::connection('medical_online')->getPdo());
//    $entities = DB::connection('medical_online')->table('MIDBENTITY')->limit(5)->get();
//    return $entities;

});

Route::get('premia', function () {
//    dd("here");
    //dd(DB::connection('premier_connect')->getPdo());
//    $entities = DB::connection('premier_connect')->table('PM_ASSURED_APP')->limit(5)->get();

    $quotes = \Jick\agentsapp\models\Quote::where('quote_id', 'like', '%P/ONL/%')
        ->where('uploaded_premia',0)->where('user_id','!=',0)
        ->get();

    foreach ($quotes as $res) {
        $brokerCode = \Jick\Settings\models\Agent::where('user_id', $res->agent_id)->first()->agent_no;
        $brokerName = \Jick\users\models\User::where('id', $res->agent_id)->first()->firstname . " " . \Jick\users\models\User::where('id', $res->agent_id)->first()->lastname;
        $pinNo = \Jick\users\models\User::where('id', $res->agent_id)->first()->id_no;


        $fName = null;
        $lName = null;
        $fullName = null;

        if ($res->user_id == 0) {
            $fName = null;
            $lName = null;
            $fullName = null;
        } elseif ($res->user_id != 0) {
            $fName = \Jick\users\models\User::where('id', $res->user_id)->first()->firstname;
            $lName = \Jick\users\models\User::where('id', $res->user_id)->first()->lastname;
            $fullName = $fName . " " . $lName;
        }

        $tableUAT = 'PM_ASSURED_APP';
        $tablePROD = 'PREMIA.PM_ASSURED_APP';
        $entities = DB::connection('premier_connect_prod')->table($tablePROD)
            ->insert(array(
                'ASSR_CODE' => '777',
                'ASSR_FIRST_NAME' => $fName,
                'ASSR_MIDDLE_NAME' => $lName,
                'ASSR_LONG_NAME' => $fullName,
                'ASSURED_BROKER_CODE' => $brokerCode,
                'ASSR_BROKER_NAME' => $brokerName,
                'ASSR_PIN_NO' => $pinNo,
                'STATUS' => 'NEW',
                'ASSR_REFERENCE ' => $res->quote_id
            ));

        if ($entities) {
            echo "Data Saved Successfully <br>";
            //update quotes table uploaded_premia to 1
            $update = \Jick\agentsapp\models\Quote::where('id',$res->id)->update(array('uploaded_premia'=>1));
            if($update){
                echo "Quotes Record marked as uploaded to Premia <br>";
            }else{
                echo "Failed to Upload to Premia <br>";
            }
        } else {
            echo "problem encountered while saving data <br>";
        }
    }

//    print "================\n\n".$quotes;
});
/**
 * Api routes
 */
Route::group(['prefix' => 'api/'], function () {

    Route::post('login', 'Jick\users\controllers\UserController@login');
    Route::post('register', 'Jick\users\controllers\UserController@register');
    Route::post('m-register', 'Jick\users\controllers\UserController@mobileRegister');
    Route::post('m-confirm', 'Jick\users\controllers\UserController@mobileConfirmUser');
    Route::post('update-user', 'Jick\users\controllers\UserController@update');

    // Check if user is unique
    Route::post('check-user', 'Jick\users\controllers\UserController@checkUser');

    // Confirm the otp sent
    Route::post('otp-confirm', 'Jick\users\controllers\UserController@confirmOTP');

    // Password reset link request routes...
    Route::post('password/email', 'Jick\users\controllers\PasswordController@postEmail');

    // Password reset routes...
    Route::post('password/reset', 'Jick\users\controllers\PasswordController@postReset');

    //Confirm user account
    Route::get('register/verify/{confirmationCode}', ['as' => 'confirmation_path', 'uses' => 'Jick\users\controllers\UserController@confirm']);


    $s = 'social.';
    Route::get('/social/redirect/{provider}', ['as' => $s . 'redirect', 'uses' => 'Jick\users\controllers\UserController@getSocialRedirect']);
    Route::post('/social/handle/{provider}', ['as' => $s . 'handle', 'uses' => 'Jick\users\controllers\UserController@getSocialHandle']);


    //Authentication required
    Route::group(['middleware' => 'jwt.auth'], function () {

        Route::put('profile/{id}', 'Jick\users\controllers\UserController@profile');

        Route::group(['middleware' => []], function () {
            Route::get('users', 'Jick\users\controllers\UserController@index');
            Route::resource('users', 'Jick\users\controllers\UserController');
        });

        //Refresh token on request
        Route::get('refresh-token', 'Jick\users\controllers\UserController@refreshToken');
    });

    //maitho
    Route::post('register-customer', 'Jick\users\controllers\UserController@registerCustomer');
    Route::post('register-agent', 'Jick\users\controllers\UserController@registerAgent');
    Route::post('confirm-agent-account', 'Jick\users\controllers\UserController@confirmAgentAccount');
    Route::post('create-agent-password', 'Jick\users\controllers\UserController@createAgentPassword');

    Route::post('reset-password', 'Jick\users\controllers\UserController@resetPassword');
    Route::get('verify-agent-code', 'Jick\users\controllers\UserController@verifyAgentCode');



});

Route::get('web/activate/agent/{email}', function ($email) {
    //Daniel Omondi and Elias Kokonya Activating Agents if Information is Valid
//    echo $email;
    $check = \Jick\users\models\User::where(['email' => $email])->get();
    if ($check->count() > 0) {
        $updateUser = \Jick\users\models\User::where(['email' => $email])
            ->update(array('confirmed' => 1,'active'=> 1));
        if ($updateUser) {
            $status = "success";
            $message = "agent has been activated Successfully";
        }
    } elseif ($check->count() < 1) {
        $status = "failure";
        $message = "User not found";
    }

    Print $status."<br/>";
    print $message;
});
