<?php
    namespace Jick\settings\model;

    use Illuminate\Database\Eloquent\Model;

    class SOS extends Model
    {
        protected $table = 'sos';

        protected $fillable = ['name', 'type', 'location', 'phone', 'alt_phone'];
    }