<?php
    namespace Jick\Settings\models;

    use Illuminate\Database\Eloquent\Model;

    class CarPart extends Model
    {
        protected $table = 'PARTS';

        protected $fillable = ['NAME'];
    }