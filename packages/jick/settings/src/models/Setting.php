<?php

namespace Jick\settings\models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    //
    protected $fillable = [
        'name',
        'module',
        'value'

    ];

    public function getKeyName(){
        return "name";
    }
}