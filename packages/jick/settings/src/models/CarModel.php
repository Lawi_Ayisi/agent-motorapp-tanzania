<?php
    namespace Jick\Settings\models;

    use Illuminate\Database\Eloquent\Model;

    class CarModel extends Model
    {
        protected $table = 'MODEL';

        protected $fillable = ['NAME', 'MAKE_ID', 'YEAR'];
    }