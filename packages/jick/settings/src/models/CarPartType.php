<?php
    namespace Jick\Settings\models;

    use Illuminate\Database\Eloquent\Model;

    class CarPartType extends Model
    {
        protected $table = 'PARTTYPE';

        protected $fillable = ['NAME', 'PART_ID'];
    }