<?php
    namespace Jick\settings\models;

    use Illuminate\Database\Eloquent\Model;

    class FAQ extends Model
    {
        protected $table = 'FAQS';

        protected $fillable = ['QUESTION', 'ANSWER', 'PRODUCT_ID'];
    }