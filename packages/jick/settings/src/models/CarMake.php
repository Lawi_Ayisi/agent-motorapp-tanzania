<?php
    namespace Jick\Settings\models;

    use Illuminate\Database\Eloquent\Model;

    class CarMake extends Model
    {
        protected $table = 'MAKES';

        protected $fillable = ['NAME'];
    }