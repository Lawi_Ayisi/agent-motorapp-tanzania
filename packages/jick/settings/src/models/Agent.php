<?php
namespace Jick\Settings\models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $table = 'agents';
    
    protected $fillable = ['agent_no', 'user_id','agency','company_name'];
}