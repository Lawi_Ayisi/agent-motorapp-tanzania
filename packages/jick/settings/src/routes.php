<?php

Route::group(['middleware' => [
	'jwt.auth',
	//'role:admin,View Backend'
], 'prefix' => 'api/'], function() {
	Route::resource('settings', 'jick\settings\controllers\SettingsController');
	Route::post('add/agent', 'jick\settings\controllers\SettingsController@addAgent');
});
