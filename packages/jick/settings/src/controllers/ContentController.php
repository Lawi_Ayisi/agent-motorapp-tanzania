<?php
    namespace Jick\settings\controllers;

    use App\Http\Controllers\Controller;
    use Guzzle\Http\Message\Request;
    use Illuminate\Support\Facades\Validator;
    use Jick\settings\model\SOS;
    use Jick\Settings\models\CarMake;
    use Jick\Settings\models\CarModel;
    use Jick\Settings\models\CarPart;
    use Jick\Settings\models\CarPartType;
    use Jick\settings\models\Offer;

    class ContentController extends Controller
    {
        //SOS
        function showSOS() {
            $sos = SOS::all();

            return response()->json($sos);
        }

        function manageSOS(Request $request) {
            $name = $request->input('sos');
            $type = $request->input('type_id');
            $location = $request->input('location');
            $phone = $request->input('phone');
            $alt = $request->input('alt_phone');

            $validator = Validator::make($request->all(), [
                'name' => 'required | unique',
                'type_id' => 'required | integer',
                'location' => 'required',
                'phone' => 'required | unique'
            ]);

            $query = SOS::where([
                ['NAME', $name],
                ['TYPE', $type]
            ])->first();

            if ($validator->fails()){
                return response()->json($validator->errors()->all());
            } elseif ($query != null) {
                SOS::where([
                    ['NAME', $name],
                    ['TYPE', $type]
                ])->update([
                    'NAME' => $name,
                    'TYPE' => $type,
                    'LOCATION' => $location,
                    'PHONE' => $phone,
                    'ALT_PHONE' => $alt
                ]);
            } else {
                $new = new SOS();
                $new->NAME = $name;
                $new->TYPE = $type;
                $new->LOCATION = $location;
                $new->PHONE = $phone;
                $new->ALT_PHONE = $alt;

                $new->save();
            }
        }

        function searchSOS(Request $request) {
            $type = $request->input('type_id');

            $validator = Validator::make($request->all(), [
                'type_id' => 'required'
            ]);

            if ($validator->fails()){
                return response()->json($validator->errors()->all());
            } else {
                $sos = SOS::where('TYPE', $type)->get();

                return response()->json($sos);
            }
        }

        function deleteSOS(Request $request) {
            $sid = $request->input('sos_id');

            $validator = Validator::make($request->all(), [
                'sos_id' => 'required'
            ]);

            if ($validator->fails()){
                return response()->json('No record has been selected');
            } else {
                SOS::where('ID', $sid)->delete();
            }
        }

        //Offers

        function showOffers() {
            $offers = Offer::all();

            return response()->json($offers);
        }

        function manageOffer(Request $request) {
            $title = $request->input('title');
            $body = $request->input('body');
            $product = $request->input('product_id');
            $expires = $request->input('expires_at');

            $query = Offer::where([
                ['title' => $title],
                ['product_id', $product]
            ])->first();

            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'body' => 'required',
                'product_id' => 'integer',
                'expires_at' => 'required | date'
            ]);

            if ($validator->fails()){
                return response()->json($validator->errors()->all());
            } elseif ($query != null) {
                Offer::where([
                    ['TITLE', $title],
                    ['PRODUCT_ID', $product]
                ])->update([
                    'TITLE' => $title,
                    'BODY' => $body,
                    'PRODUCT_ID' => $product,
                    'EXPIRES_AT' => $expires
                ]);

                return response()->json('Tou have edited a offer: ' . $title);

            } else {
                $new = new Offer();
                $new->TITLE = $title;
                $new->BODY = $body;
                $new->PRODUCT_ID = $product;
                $new->EXPIRES_AT = $expires;

                $new->save();

                return response()->json('You have successfully created an offer: ' . $title);
            }
        }

        function deleteOffer(Request $request) {
            $validator = Validator::make($request->all(), [
                'id' => 'required'
            ]);

            if ($validator->fails()){
                return response()->json($validator->errors()->all());
            } else {
                Offer::destroy($request->id);
            }
        }

        //Car make
        function showCarMake() {
            $make = CarMake::with('models');

            return response()->json($make);
        }

        function manageCarMake(Request $request) {
            $name = $request->input('name');
            $new_name = $request->input('new_name');

            $validator = Validator::make($request->all(), [
                'name' => 'required | unique'
            ]);

            $query = CarMake::where('NAME', $name)->first();

            if ($validator->fails()){
                return response()->json($validator->errors()->all());
            } elseif ($query != null) {
                CarMake::where('NAME', $name)->update([
                    'NAME' => $new_name
                ]);

                return response()->json('You have changed: ' . $name . ' to: ' .$new_name);

            } else {
                $new = new CarMake();
                $new->NAME = $name;

                $new->save();

                return response()->json('You have added a car make: ' . $name);
            }
        }

        function deleteCarMake(Request $request) {
            $validator = Validator::make($request->all(), [
                'id' => 'required | integer'
            ]);

            $query = CarModel::where('CARMAKE_ID', $request->id)->first();

            if ($validator->fails()){
                return response()->json($validator->errors()->all());
            } elseif ($query != null) {
                return response()->json('You cannot delete a parent record');
            } else {
                CarMake::destroy($request->id);

                return response()->json('You have successfully deleted record #' .$request->id);
            }

        }

        function manageCarModel(Request $request) {
            $name = $request->input('name');
            $make = $request->input('make_id');
            $new_name = $request->input('new_name');

            $validator = Validator::make($request->all(), [
                'name' => 'required | unique',
                'make_id' => 'required | integer'
            ]);

            $query = CarModel::where('NAME', $name)->first();

            if ($validator->fails()){
                return response()->json($validator->errors()->all());
            } elseif ($query != null) {
                CarModel::where('NAME', $name)->update([
                    'NAME' => $new_name,
                    'CARMAKE_ID' => $make
                ]);

                return response()->json('You have changed: ' . $name . ' to: ' . $new_name . ' and make to #' .$make);

            } else {
                $new = new CarModel();
                $new->NAME = $name;
                $new->CARMAKE_ID = $make;

                $new->save();

                return response()->json('You have added a car model: ' . $name);
            }
        }

        function deleteCarModel(Request $request) {
            $validator = Validator::make($request->all(), [
                'id' => 'required | integer'
            ]);

            if ($validator->fails()){
                return response()->json($validator->errors()->all());
            } else {
                CarModel::destroy($request->id);

                return response()->json('You have successfully deleted record #' .$request->id);
            }
        }

        function showCarParts() {
            $parts = CarPart::with('parttypes');

            return response()->json($parts);
        }

        function manageCarParts(Request $request) {
            $name = $request->input('name');
            $new_name = $request->input('new_name');

            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ]);

            $query = CarPart::where('NAME', $name)->first();

            if ($validator->fails()){
                return response()->json($validator->errors()->all());
            } elseif ($query != null) {
                CarPart::where('NAME', $name)->update(['NAME' => $new_name, 'UPDATED_AT' => date('Y-m-d H:i:s')]);
            } else {
                $new = new CarPart();
                $new->NAME = $name;

                $new->save();

                return response()->json('You have added a new part ' . $name);
            }
        }

        function deleteCarPart(Request $request) {
            $validator = Validator::make($request->all(), [
                'id' => 'required'
            ]);

            $query = CarPartType::where('PART_ID', $request->id)->first();

            if ($validator->fails()) {
                return response()->json($validator->errors()->all());

            } elseif ($query != null) {
                return response()->json('You cannot delete a parent part');

            } else {
                CarPart::destroy($request->id);

                return response()->json('You have successfully deleted a part');
            }
        }

        function manageCarPartTypes(Request $request) {
            $name = $request->input('name');
            $part = $request->input('part_id');
            $new_name = $request->input('new_name');

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'part_id' => 'required | integer'
            ]);

            $query = CarPartType::where('NAME', $name)->first();

            if ($validator->fails()){
                return response()->json($validator->errors()->all());

            } elseif ($query != null) {
                CarPartType::where('NAME', $name)->update(['NAME' => $new_name, 'PART_ID' => $part, 'UPDATED_AT' => date('Y-m-d H:i:s')]);

            } else {
                $new = new CarPartType();
                $new->NAME = $name;
                $new->PART_ID = $part;

                $new->save();

                return response()->json('You have added a new part type ' . $name);
            }
        }

        function deleteCarPartType(Request $request) {
            $validator = Validator::make($request->all(), [
                'id' => 'required'
            ]);

            if ($validator->fails()){
                return response()->json($validator->errors()->all());

            } else {
                CarPartType::destroy($request->id);

                return response()->json('You have successfully deleted a part');
            }
        }

    }
