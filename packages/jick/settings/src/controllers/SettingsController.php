<?php

namespace Jick\Settings\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Jick\Settings\models\Agent;
use Jick\Settings\models\Setting;
use Jick\users\models\User;
use Illuminate\Support\Facades\Validator;

class SettingsController extends Controller
{
    function index()
    {
        return Setting::all();
    }
    
    public function addAgent(Request $request) {
        $validator = Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => 'required',
            'agent_no' => 'required',
            'email' => 'required'
        ]);
    
        if($validator->fails()) {
            return response()->json([
                'message' => 'Sorry, an error occured preventing saving',
                'status' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }
        
        $user = new User();
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->agent_no);
        $user->active = 1;
        $user->confirmed = 1;
        
        $save = $user->save();
        
        if($save) {
            $agent = new Agent();
            $agent->agent_no = $request->agent_no;
            $agent->user_id = User::where(['email' => $request->email, 'firstname' => $request->firstname])->first()->id;
            
            $agent->save();
            return response()->json([
                'message' => 'Agent Created'
            ]);
        } else {
            return response()->json([
                'message' => 'Something went wrong'
            ]);
        }
    }

    function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'settings' => 'required',
            //'value' => 'required',
            //'module' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'message' => 'Sorry, an error occured preventing saving',
                'status' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }

        if(is_array($request->settings)) {
            foreach($request->settings as $setting) {
                $oldSetting = Setting::where(['name'=> $setting['name']])->first();

                if(!empty($oldSetting)) {//update
                    $status = Setting::where(['name'=> $setting['name']])
                                    ->update(['value' => $setting['value']]);
                }else{
                    $status = Setting::create($setting);
                }
            }
        }

        return response()->json(['message' => 'Settings saved successfully']);
    }

    function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'name' => 'required',
            'value' => 'required',
            'module' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'message' => 'Sorry, an error occured preventing updating',
                'status' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }

         $result = Setting::where('id', $request->id)
                    ->update(['name' => $request->name,
                        'value' => $request->value,
                        'module' => $request->module
                        ]);
        if($result)
        {
            return response()->json([
                'message' => 'Settings successfully updated'
            ]);
        }
        else
        {
             return response()->json([
                'message' => 'Settings not updated'
            ]);
        }
    }


    function destroy(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'message' => 'Sorry, an error occured preventing deletion',
                'status' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }

         $result = Setting::destroy($request->id);


        if($result)
        {
            return response()->json([
                'message' => 'Settings successfully deleted'
            ]);
        }
        else
        {
             return response()->json([
                'message' => 'Settings not deleted'
            ]);
        }
    }

}
