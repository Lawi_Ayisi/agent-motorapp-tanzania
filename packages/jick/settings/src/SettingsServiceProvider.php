<?php

namespace jick\settings;

use Illuminate\Support\ServiceProvider;

class SettingsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
      $this->loadViewsFrom(__DIR__.'/views', 'settings');

       /* $this->publishes([
            __DIR__.'/views' => base_path('resources/views/jick/users'),
        ]);*/
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //Load controllers
        $this->app->make('Jick\settings\controllers\SettingsController');
        $this->app->make('Jick\settings\controllers\ContentController');

        //Load models
        $this->app->make('Jick\settings\models\Setting');
        $this->app->make('Jick\settings\models\FAQ');
        $this->app->make('Jick\settings\models\Offer');
        $this->app->make('Jick\settings\models\Agent');

        include __DIR__.'/routes.php';
    }
}
