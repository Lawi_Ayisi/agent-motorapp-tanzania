<?php
namespace jick\settings\facades;

use jick\settings\models\Setting;

class SettingsLib {

    function get($name) {

        $value = Setting::where('name', '=', $name)->first();

        return $value;
    }
}