<?php

Route::group(['prefix' => 'api/'], function() {
	Route::resource('unit_illustrator', 'jick\unit_illustrator\controllers\Unit_illustratorController');

	//Admin

	Route::post(
		'settings/unit-illustrator/min-premium',
		'jick\unit_illustrator\controllers\SettingsController@setMinPremium'
	);
	Route::post(
		'settings/unit-illustrator/set-min-premium',
		'jick\unit_illustrator\controllers\SettingsController@setMinPremium'
	);
	Route::post(
		'settings/unit-illustrator/add-qx',
		'jick\unit_illustrator\controllers\SettingsController@addQX'
	);
});