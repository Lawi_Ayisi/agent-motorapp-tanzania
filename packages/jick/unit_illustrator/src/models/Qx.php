<?php

namespace Jick\Unit_illustrator\models;

use Illuminate\Database\Eloquent\Model;

class Qx extends Model
{
    protected $table = 'qx';

    protected $fillable = ['x', 'qx', 'result', 'user_id'];

    public $timestamps = false;
}