<?php

namespace Jick\Unit_illustrator\models;

use Illuminate\Database\Eloquent\Model;

class Rider_conversion_rate extends Model
{
    protected $table = 'rider_conversion_rates';

    protected $fillable = ['frequency', 'rate', 'start', 'end'];

    public $timestamps = false;
}