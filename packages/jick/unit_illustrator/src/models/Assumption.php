<?php

namespace Jick\Unit_illustrator\models;

use Illuminate\Database\Eloquent\Model;

class Assumption extends Model
{
    protected $table = 'assumptions';

    protected $fillable = ['name', 'value'];

    public $timestamps = false;
}