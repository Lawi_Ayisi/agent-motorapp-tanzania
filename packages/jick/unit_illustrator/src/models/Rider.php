<?php
/**
 * Created by PhpStorm.
 * User: Mnyakundi
 * Date: 6/27/2016
 * Time: 15:16
 */
namespace Jick\Unit_illustrator\models;

use Illuminate\Database\Eloquent\Model;

class Rider extends Model
{
   protected $table = 'riders';

   protected $fillable = ['name', 'rate', 'unit', 'initials'];

   public $timestamps = false;

}