<?php

namespace Jick\Unit_illustrator\models;

use Illuminate\Database\Eloquent\Model;

class Surrender_benefit extends Model
{
    protected $table = 'surrender_benefits';

    protected $fillable = ['name', 'value'];

    public $timestamps = false;
    
}