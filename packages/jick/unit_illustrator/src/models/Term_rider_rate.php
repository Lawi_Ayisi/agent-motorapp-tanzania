<?php

namespace Jick\Unit_illustrator\models;

use Illuminate\Database\Eloquent\Model;

class Term_rider_rate extends Model
{
    protected $table= 'trr';

    protected $fillable = ['age', 'term', 'rate'];

    public $timestamps = false;
    
}