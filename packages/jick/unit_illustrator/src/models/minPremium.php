<?php

namespace Jick\Unit_illustrator\models;

use Illuminate\Database\Eloquent\Model;

class minPremium extends Model
{
    protected $table = 'min_premium';

    protected $fillable = ['interval', 'premium'];

    public $timestamps = false;
}