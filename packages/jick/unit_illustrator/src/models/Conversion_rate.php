<?php

namespace Jick\Unit_illustrator\models;

use Illuminate\Database\Eloquent\Model;

class Conversion_rate extends Model
{
    protected $table = 'conversion_r';

    protected $fillable = ['rider_name', 'frequency', 'value'];

    public $timestamps = false;
}