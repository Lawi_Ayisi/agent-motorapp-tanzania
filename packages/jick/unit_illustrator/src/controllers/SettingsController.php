<?php

namespace Jick\Unit_illustrator\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Jick\Unit_illustrator\models\assumption;
use Jick\Unit_illustrator\models\qx;
use Jick\Unit_illustrator\models\conversion_rate;
use Jick\Unit_illustrator\models\rider_conversion_rate;
use Jick\Unit_illustrator\models\term_rider_rate;
use Jick\Unit_illustrator\models\rider;
use Jick\Unit_illustrator\models\surrender_benefit;
use Jick\Unit_illustrator\models\minPremium;
use Validator;
use App;

class SettingsController extends Controller
{
    function setMinPremium(Request $request){
        $plan = $request->input('plan');
        $interval = $request->input('interval');
        $premium = $request->input('premium');

        $validator = Validator::make($request->all(), [
            'plan' => 'required',
            'interval' => 'required',
            'premium' => 'required'
        ]);

        $query = minPremium::where([
            ['PLAN', $plan],
            ['INTERVAL', $interval]
        ])->first();

        if ($validator->fails()) {
            return response()->json($validator->errors()->all());

        } elseif ($query != null) {
            minPremium::where([
                ['PLAN', $plan],
                ['INTERVAL', $interval]
            ])->update([
                'PREMIUM' => $premium
                // 'UPDATED_AT' => date('Y-m-d H:i:s')
            ]);

            // Activity::log([
            //     'contentId'   => 1,
            //     'contentType' => 'App Settings',
            //     'action'      => 'Edit',
            //     'description' => 'Edit Minimum Premium',
            //     'details'     => 'Plan: ' . $plan . ' Interval: ' . $interval . ', Premium: ' .$premium
            // ]);

            return response()->json('You have edited record');

        } else {
            $new = new minPremium();
            $new->PLAN = $plan;
            $new->INTERVAL = $interval;
            $new->PREMIUM = $premium;

            // Activity::log([
            //     'contentId'   => 1,
            //     'contentType' => 'App Settings',
            //     'action'      => 'Create',
            //     'description' => 'Created Minimum Premium',
            //     'details'     => 'Plan: ' . $plan . ' Interval: ' . $interval . ', Premium: ' .$premium
            // ]);

            $new->save();
            return response()->json('You have created a new record');

        }

    }

    // function addMinPremium(Request $request){
    //     $plan = $request->input('plan');
    //     $interval = $request->input('interval');
    //     $premium = $request->input('premium');

    //     $query = minPremium::where([
    //         ['PLAN', $plan],
    //         ['INTERVAL', $interval]
    //     ])->first();

    //     if ($query != null){
    //         // dd('Cannot create conflicting records.');
            

    //     } else {
    //         $new = new minPremium();
    //         $new->PLAN = $plan;
    //         $new->INTERVAL = $interval;
    //         $new->PREMIUM = $premium;

    //         // Activity::log([
    //         //     'contentId'   => 1,
    //         //     'contentType' => 'App Settings',
    //         //     'action'      => 'Create',
    //         //     'description' => 'Created Minimum Premium',
    //         //     'details'     => 'Plan: ' . $plan . ' Interval: ' . $interval . ', Premium: ' .$premium
    //         // ]);

    //         $new->save();

    //     }
    // }

    function addQX(Request $request) {
        $x = $request->input('x');
        $qx = $request->input('qx');
        $result = $request->input('result');

        $validator = Validator::make($request->all(), [
            'x' => 'required',
            'qx' => 'required',
            'result' => 'required'
        ]);

        $query = qx::where([
            ['X', $x],
            ['QX', $qx]
        ])->first();

        if ($validator->fails()) {
            return response()->json($validator->errors()->all());

        } elseif ($query != null){
            qx::where([
                ['X', $x],
                ['QX', $qx]
            ])->update([
                'RESULT' => $result,
                'UPDATED_AT' => date('Y-m-d H:i:s')
            ]);

            // Activity::log([
            //     'contentId'   => 1,
            //     'contentType' => 'App Settings',
            //     'action'      => 'Edit',
            //     'description' => 'Edited a QX',
            //     'details'     => 'X: ' . $x . ' QX: ' . $qx . ' Result: ' . $result,
            // ]);
            return response()->json('You have edited a record');

        } else {
            $new = new qx();
            $new->X = $x;
            $new->QX = $qx;
            $new->RESULT = $result;

            // Activity::log([
            //     'contentId'   => 1,
            //     'contentType' => 'App Settings',
            //     'action'      => 'Create',
            //     'description' => 'Created a new QX',
            //     'details'     => 'X: ' . $x . ' QX: ' . $qx . ' Result: ' . $result,
            // ]);

            $new->save();
            return response()->json('You have created a new record');
        }
    }



    // function editQX(Request $request) {
    //     $qid = $request->input('id');
    //     $x = $request->input('x');
    //     $qx = $request->input('qx');
    //     $result = $request->input('result');

    //     $validator = Validator::make($request->all(), [
    //         'x' => 'required',
    //         'qx' => 'required',
    //         'result' => 'required'
    //     ]);

    //     if($validator->fails()){
    //         return response()->json($validator->errors()->all());
    //     } else {
    //         qx::where('id', $qid)
    //             ->update([
    //                 'X' => $x,
    //                 'QX' => $qx,
    //                 'RESULT' => $result,
    //                 'UPDATED_AT' => date('Y-m-d H:i:s')
    //             ]);

    //         Activity::log([
    //             'contentId'   => 1,
    //             'contentType' => 'App Settings',
    //             'action'      => 'Edit',
    //             'description' => 'Edited a QX',
    //             'details'     => 'X: ' . $x . ' QX: ' . $qx . ' Result: ' . $result,
    //         ]);
    //     }
    // }

    function addTRR(Request $request) {
        $age = $request->input('age');
        $term = $request->input('term');
        $rate = $request->input('rate');

        $validator = Validator::make($request->all(), [
            'age' => 'required | integer',
            'term' => 'required | integer',
            'rate' => 'required | numeric'
        ]);

        $query = term_rider_rate::where([
            ['AGE', $age],
            ['TERM', $term]
        ])->first();

        if ($validator->fails()){
            return response()->json($validator->errors()->all());

        } elseif ($query != null){
            term_rider_rate::where([
                    ['AGE', $age],
                    ['TERM', $term]
                ])
                ->update([
                    'RATE' => $rate,
                    'UPDATED_AT' => date('Y-m-d H:i:s')
                ]);

            // Activity::log([
            //     'contentId' => 1,
            //     'contentType' => 'App Settings',
            //     'action' => 'Edit',
            //     'description' => 'Edited a Term Rider Rate',
            //     'details' => 'Age: ' . $age . ', Term: ' . $term . ', Rate: ' .$rate
            // ]);
            return response()->json('You have edited a record');

        } else {
            $new = new term_rider_rate();
            $new->AGE = $age;
            $new->TERM = $term;
            $new->RATE = $rate;

            // Activity::log([
            //     'contentId' => 1,
            //     'contentType' => 'App Settings',
            //     'action' => 'Edit',
            //     'description' => 'Edited a Term Rider Rate',
            //     'details' => 'Age: ' . $age . ', Term: ' . $term . ', Rate: ' .$rate
            // ]);

            $new->save();
            return response()->json('You have created a new record');
        }
    }

    // function editTRR(Request $request) {
    //     $age = $request->input('age');
    //     $term = $request->input('term');
    //     $rate = $request->input('rate');

    //     $validator = Validator::make($request->all(), [
    //         'age' => 'required | integer',
    //         'term' => 'required | integer',
    //         'rate' => 'required | numeric'
    //     ]);

    //     if ($validator->fails()){
    //         return response()->json($validator->errors()->all());
    //     } else {
    //         term_rider_rate::where([
    //                 ['AGE', $age],
    //                 ['TERM', $term]
    //             ])
    //             ->update([
    //                 'RATE' => $rate,
    //                 'UPDATED_AT' => date('Y-m-d H:i:s')
    //             ]);

    //         // Activity::log([
    //         //     'contentId' => 1,
    //         //     'contentType' => 'App Settings',
    //         //     'action' => 'Edit',
    //         //     'description' => 'Edited a Term Rider Rate',
    //         //     'details' => 'Age: ' . $age . ', Term: ' . $term . ', Rate: ' .$rate
    //         // ]);
    //     }
    // }

    function addConRates(Request $request) {
        $name = $request->input('rider_name');
        $frequency = $request->input('frequency');
        $value = $request->input('value');

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'frequency' => 'required',
            'value' => 'required'
        ]);

        $query = conversion_rate::where([
            ['RIDER_NAME', $name],
            ['FREQUENCY', $frequency]
        ])->first();

        if($validator->fails()){
            return response()->json($validator->errors()->all());

        } elseif ($query != null) {
            conversion_rate::where([
                ['RIDER_NAME', $name],
                ['FREQUENCY', $frequency]
            ])
                ->update([
                    'RIDER_NAME' => $name,
                    'FREQUENCY' => $frequency,
                    'VALUE' => $value,
                    'UPDATED_AT' => date('Y-m-d H:i:s')
                ]);
                // Activity::log([
            //     'contentId' => 1,
            //     'contentType' => 'App Settings',
            //     'action' => 'Edit',
            //     'description' => 'Edited Conversion Rate',
            //     'details' => 'Name: ' . $name . ', Frequency: ' .$frequency. ', Value: ' .$value
            // ]);
            
            return response()->json('You have edited a record');

        } else {
            $new = new conversion_rate();
            $new->RIDER_NAME = $name;
            $new->FREQUENCY = $frequency;
            $new->VALUE = $value;

            // Activity::log([
            //     'contentId'   => 1,
            //     'contentType' => 'App Settings',
            //     'action'      => 'Create',
            //     'description' => 'Created a new Conversion Rate',
            //     'details'     => 'Rider name: ' . $name . 'Frequency: ' . $frequency . ' Value: ' . $value,
            // ]);

            $new->save();
            return response()->json('You have created a new record');
        }
    }

    // function editConRates(Request $request) {
    //     $crid = $request->input('id');
    //     $name = $request->input('rider_name');
    //     $frequency = $request->input('frequency');
    //     $value = $request->input('value');

    //     $validator = Validator::make($request->all(), [
    //         'rider_name' => 'required',
    //         'frequency' => 'required',
    //         'value' => 'required'
    //     ]);

    //     if($validator->fails()){
    //         return response()->json($validator->errors()->all());
    //     } else {
    //         conversion_rate::where('id', $crid)
    //             ->update([
    //                 'RIDER_NAME' => $name,
    //                 'FREQUENCY' => $frequency,
    //                 'VALUE' => $value,
    //                 'UPDATED_AT' => date('Y-m-d H:i:s')
    //             ]);

    //         // Activity::log([
    //         //     'contentId' => 1,
    //         //     'contentType' => 'App Settings',
    //         //     'action' => 'Edit',
    //         //     'description' => 'Edited Conversion Rate',
    //         //     'details' => 'Name: ' . $name . ', Frequency: ' .$frequency. ', Value: ' .$value
    //         // ]);
    //     }
    // }

    function addAssumption(Request $request) {
        $name = $request->input('name');
        $value = $request->input('value');

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'value' => 'required'
        ]);

        $query = Assumption::where([
            ['NAME', $name]
        ])->first();

        if($validator->fails()){
            return response()->json($validator->errors()->all());

        } elseif ($query != null) {
            assumption::where('name', $name)
                ->update([
                    'NAME' => $name,
                    'VALUE' => $value,
                    'UPDATED_AT' => date('Y-m-d H:i:s')
                ]);

            // Activity::log([
            //     'contentId' => 1,
            //     'contentType' => 'App Settings',
            //     'action' => 'Edit',
            //     'description' => 'Edited Assumption',
            //     'details' => 'Name: ' . $name . ', Value: ' .$value
            // ]);
            return response()->json('You have edited a record');

        } else {
            $new = new assumption();
            $new->NAME = $name;
            $new->VALUE = $value;

            // Activity::log([
            //     'contentId'   => 1,
            //     'contentType' => 'App Settings',
            //     'action'      => 'Create',
            //     'description' => 'Created a new Assumption',
            //     'details'     => 'Name: ' . $name . ' Value: ' . $value,
            // ]);

            $new->save();
            return response()->json('You have created a new record');
        }
    }

    // function editAssumption(Request $request) {
    //     $aid = $request->input('id');
    //     $name = $request->input('name');
    //     $value = $request->input('value');

    //     $validator = Validator::make($request->all(), [
    //         'name' => 'required',
    //         'value' => 'required'
    //     ]);

    //     if($validator->fails()){
    //         return response()->json($validator->errors()->all());
    //     } else {
    //         assumption::where('id', $aid)
    //             ->update([
    //                 'NAME' => $name,
    //                 'VALUE' => $value,
    //                 'UPDATED_AT' => date('Y-m-d H:i:s')
    //             ]);

    //         // Activity::log([
    //         //     'contentId' => 1,
    //         //     'contentType' => 'App Settings',
    //         //     'action' => 'Edit',
    //         //     'description' => 'Edited Assumption',
    //         //     'details' => 'Name: ' . $name . ', Value: ' .$value
    //         // ]);
    //     }
    // }

    function addRConrates(Request $request) {
        $frequency = $request->input('frequency');
        // $description = $request->input('description');
        $rate = $request->input('rate');
        $start = $request->input('start');
        $end = $request->input('end');

        $validator = Validator::make($request->all(), [
            'frequency' => 'required',
            // 'description' => 'required',
            'rate' => 'required',
            'start' => 'required',
            'end' => 'required'
        ]);

        $query = riderConversionRate::where([
            ['FREQUENCY', $frequency],
            ['START', $start],
            ['END', $end]
        ])->first();

        if($validator->fails()){
            return response()->json($validator->errors()->all());

        } elseif ($query != null) {
           rider_conversion_rate::where([
                ['FREQUENCY', $frequency],
                ['START', $start],
                ['END', $end]
           ])->update([
                    'RATE' => $rate,
                    'UPDATED_AT' => date('Y-m-d H:i:s')
                ]);

            return response()->json('You have edited a record');

            // Activity::log([
            //     'contentId' => 1,
            //     'contentType' => 'App Settings',
            //     'action' => 'Edit',
            //     'description' => 'Edited Rider Conversion Rates',
            //     'details' => 'Frequency: ' .$frequency. ', Rate: ' .$rate. ', Start: ' .$start. ', End: ' .$end
            // ]);

        } else {
            $new = new rider_conversion_rate();
            $new->FREQUENCY = $frequency;
            // $new->description = $description;
            $new->RATE = $rate;
            $new->START = $start;
            $new->END = $end;

            // Activity::log([
            //     'contentId'   => 1,
            //     'contentType' => 'App Settings',
            //     'action'      => 'Create',
            //     'description' => 'Created a new Rider Conversion Rate',
            //     'details'     => 'Frequency: ' . $frequency . ' Rate: ' . $rate . ' Start: ' . $start . ' End: ' .$end,
            // ]);

            $new->save();
            return response()->json('You have created a new record');
        }

    }

    // function editRConRates(Request $request) {
    //     $rcid = $request->input('id');
    //     $frequency = $request->input('frequency');
    //     $rate = $request->input('rate');
    //     $start = $request->input('start');
    //     $end = $request->input('end');

    //     $validator = Validator::make($request->all(), [
    //         'frequency' => 'required',
    //         'rate' => 'required',
    //         'start' => 'required',
    //         'end' => 'required'
    //     ]);

    //     if($validator->fails()){
    //         return response()->json($validator->errors()->all());
    //     } else {
    //         rider_conversion_rate::where('id', $rcid)
    //             ->update([
    //                 'FREQUENCY' => $frequency,
    //                 'RATE' => $rate,
    //                 'START' => $start,
    //                 'END' => $end,
    //                 'UPDATED_AT' => date('Y-m-d H:i:s')
    //             ]);

    //         // Activity::log([
    //         //     'contentId' => 1,
    //         //     'contentType' => 'App Settings',
    //         //     'action' => 'Edit',
    //         //     'description' => 'Edited Rider Conversion Rates',
    //         //     'details' => 'Frequency: ' .$frequency. ', Rate: ' .$rate. ', Start: ' .$start. ', End: ' .$end
    //         // ]);
    //     }
    // }

    function addSurrenderBenefits(Request $request) {
        $name = $request->input('name');
        $value = $request->input('value');

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'value' => 'required | integer'
        ]);

        $query = SurrenderBenefit::where([
            ['NAME', $name]
        ])->first();

        if ($validator->fails()){
            return response()->json($validator->errors()->all());

        } elseif ($query != null) {
            Surrender_benefit::where('NAME', $name)
                ->update([
                    'VALUE' => $value,
                    'UPDATED_AT' => date('Y-m-d H:i:s')
                ]);

            // Activity::log([
            //     'contentId' => 1,
            //     'contentType' => 'App Settings',
            //     'action' => 'Edit',
            //     'description' => 'Edited a Surrender Benefit',
            //     'details' => 'Name: ' .$name. ', Value: ' .$value
            // ]);
            return response()->json('You have edited a record');

        } else {
            $new = new Surrender_benefit();
            $new->NAME = $name;
            $new->VALUE = $value;

            $new->save();

            // Activity::log([
            //     'contentId' => 1,
            //     'contentType' => 'App Settings',
            //     'action' => 'Create',
            //     'description' => 'Create a new Surrender Benefit',
            //     'details' => 'Name: ' .$name. ', Value: ' .$value
            // ]);
            return response()->json('You have created a new record');
        }
    }

    // function editSurrenderBenefit(Request $request){
    //     $sid = $request->input('id');
    //     $name = $request->input('name');
    //     $value = $request->input('value');

    //     $validator = Validator::make($request->all(), [
    //         'name' => 'required',
    //         'value' => 'required | integer'
    //     ]);

    //     if ($validator->fails()){
    //         return response()->json($validator->errors()->all());
    //     } else {
    //         Surrender_benefit::where('id', $sid)
    //             ->update([
    //                 'NAME' => $name,
    //                 'VALUE' => $value,
    //                 'UPDATED_AT' => date('Y-m-d H:i:s')
    //             ]);

    //         // Activity::log([
    //         //     'contentId' => 1,
    //         //     'contentType' => 'App Settings',
    //         //     'action' => 'Edit',
    //         //     'description' => 'Edited a Surrender Benefit',
    //         //     'details' => 'Name: ' .$name. ', Value: ' .$value
    //         // ]);
    //     }
    // }

    function addRider(Request $request){
        $name = $request->input('name');
        $rate = $request->input('rate');
        $unit = $request->input('unit');
        $initials = $request->input('initials');

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'rate' => 'required | numeric',
            'unit' => 'required',
            'initials' => 'required'
        ]);

        $query = rider::where([
            ['UNIT', $unit],
            ['INITIALS', $initials]
        ])->first();

        if ($validator->fails()) {
            return response()->json($validator->errors()->all());

        } elseif ($query != null) {
            rider::where([
                    ['UNIT', $unit], 
                    ['INITIALS', $initials]
                ])->update([
                    'NAME' => $name,
                    'RATE' => $rate,
                    'UNIT' => $unit,
                    'INITIALS' => $initials,
                    'UPDATED_AT' => date('Y-m-d H:i:s')
                ]);

            // Activity::log([
            //     'contentId' => 1,
            //     'contentType' => 'App Settings',
            //     'action' => 'Edit',
            //     'description' => 'Edited a Rider',
            //     'details' => 'Name: ' . $name . ', Rate: ' . $rate . ', Unit: ' . $unit . ', Initial: ' .$initials
            // ]);
            return response()->json('You have edited a record');

        } else {
            $new = new rider();
            $new->NAME = $name;
            $new->RATE = $rate;
            $new->UNIT = $unit;
            $new->INITIALS = $initials;

            // Activity::log([
            //     'contentId' => 1,
            //     'contentType' => 'App Settings',
            //     'action' => 'Create',
            //     'description' => 'Created a Rider',
            //     'details' => 'Name: ' . $name . ', Rate: ' . $rate . ', Unit: ' . $unit . ', Initial: ' .$initials
            // ]);

            $new->save();
            return response()->json('You have created a new record');
        }

    }

    // function editRider(Request $request){
    //     $rid = $request->input('rid');
    //     $name = $request->input('name');
    //     $rate = $request->input('rate');
    //     $unit = $request->input('unit');
    //     $initials = $request->input('initials');

    //     $validator = Validator::make($request->all(), [
    //         'rid' => 'required | integer',
    //         'name' => 'required',
    //         'rate' => 'required | numeric',
    //         'unit' => 'required',
    //         'initials' => 'required'
    //     ]);

    //     if ($validator->fails()) {
    //         return response()->json($validator->errors()->all());
    //     } else {
    //         rider::where('id', $rid)->update([
    //             'NAME' => $name,
    //             'RATE' => $rate,
    //             'UNIT' => $unit,
    //             'INITIALS' => $initials,
    //             'UPDATED_AT' => date('Y-m-d H:i:s')
    //         ]);

    //         // Activity::log([
    //         //     'contentId' => 1,
    //         //     'contentType' => 'App Settings',
    //         //     'action' => 'Edit',
    //         //     'description' => 'Edited a Rider',
    //         //     'details' => 'Name: ' . $name . ', Rate: ' . $rate . ', Unit: ' . $unit . ', Initial: ' .$initials
    //         // ]);
    //     }

    // }
}

