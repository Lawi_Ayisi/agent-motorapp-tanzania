<?php

namespace Jick\Unit_illustrator\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Jick\Unit_illustrator\models\assumption;
use Jick\Unit_illustrator\models\qx;
use Jick\Unit_illustrator\models\conversion_rate;
use Jick\Unit_illustrator\models\rider_conversion_rate;
use Jick\Unit_illustrator\models\term_rider_rate;
use Jick\Unit_illustrator\models\rider;
use Jick\Unit_illustrator\models\surrender_benefit;
use Validator;
use App;
use PDF;

//use App\Helpers\Helper;

class Unit_illustratorController extends Controller
{
    function index()
    {
        return Assumption::where('name', 6 > 5 ? 'Allocation Yr 5' : 'Allocation Yr 4')->first();
        $pdf = PDF::loadView('unit_illustrator::index', ['yuiyui'=>'ghgj']);

        return response()->json($pdf->save('invoice.pdf'));
      //return Surrender_benefit::all();//Conversion_rate::where([['rider_name','ADB Rider'],['frequency','monthly']])->first();//Term_rider_rate::all();
    }

    function store(Request $request)
    {
        //Assign variables
        $age = $request->age;
        $term = $request->term;
        $frequency = strtolower($request->frequency);
        $periodic_premium = $request->periodic_premium;
        $sum_assured = 0;
        $paid_premium = 0;

        //Variables from the riders
        $tar_amount = isset($request->tar_amount) ? $request->tar_amount : 0;

        $cah_set = $request->cah_set;
        $aah_set = $request->aah_set;
        $tpd_set = $request->tpd_set;
        $adb_set = $request->adb_set;
        $wp_set = $request->wp_set;

        //Personal details
        $email_addr = $request->email_addr;



        $illustration = array();

        for($year=1; $year<= $term; $year++)
        {
            //Factor in the top ups values get
            $top_up = $request->top_up;

            $top_up_amt = 0;
            for ($t=0; $t< sizeof($top_up); $t++)
            {
                if($top_up[$t]['year'] == $year)
                {
                    $top_up_amt += $top_up[$t]['amount'];
                }

            }

            $allocation_rate = Assumption::where('name', $year > 5 ? 'Allocation Yr 5' : 'Allocation Yr '.$year)->first();

            $monthly_allocated_premium = $periodic_premium * ($year >= 6 ? 1 : $allocation_rate->value);
            $allocated_premium = 0;

            //Get previous year's fund
            $previous_yr_fund_after = $year > 1 ?  $illustration[$year-2]['fund_after'] : 0;
            $realistic_pyfa = $year > 1 ?  $illustration[$year-2]['realistic_fund_after'] : 0;
            $pessimistic_pyfa = $year > 1 ?  $illustration[$year-2]['pessimistic_fund_after'] : 0;


            $previous_yr_fund_after = $this->number_unformat($previous_yr_fund_after);
            $realistic_pyfa = $this->number_unformat($realistic_pyfa);
            $pessimistic_pyfa = $this->number_unformat($pessimistic_pyfa);
            //$policy_fee = $this->number_unformat( $policy_fee);

            switch($frequency){
                case 'annually':
                        $yearly_premium = $periodic_premium;
                        $allocated_premium = $monthly_allocated_premium * 1;
                        $policy_fee = $year > 1 ? $policy_fee*1.1 : 200 * 12;
                        $periodic = $this->periodic_illustration(1,$age,$monthly_allocated_premium,$periodic_premium,$policy_fee,$previous_yr_fund_after,$realistic_pyfa,$pessimistic_pyfa,$top_up_amt,$yearly_premium);
                    break;
                case 'semi-annually':
                        $yearly_premium = $periodic_premium * 2;
                        $allocated_premium = $monthly_allocated_premium * 2;
                        $policy_fee = $year > 1 ? $policy_fee*1.1 : 200 * 6;
                        $periodic = $this->periodic_illustration(2,$age,$monthly_allocated_premium,$periodic_premium,$policy_fee,$previous_yr_fund_after,$realistic_pyfa,$pessimistic_pyfa,$top_up_amt,$yearly_premium);
                    break;
                case 'quarterly':
                        $yearly_premium = $periodic_premium * 4;
                        $allocated_premium = $monthly_allocated_premium * 4;
                        $policy_fee = $year > 1 ? $policy_fee*1.1 : 200 * 3;
                        $periodic = $this->periodic_illustration(4,$age,$monthly_allocated_premium,$periodic_premium,$policy_fee,$previous_yr_fund_after,$realistic_pyfa,$pessimistic_pyfa,$top_up_amt,$yearly_premium);
                    break;
                case 'monthly':
                        $yearly_premium = $periodic_premium * 12;
                        $allocated_premium = $monthly_allocated_premium * 12;
                        $policy_fee = $year > 1 ? $policy_fee*1.1 : 200 * 1;
                        $periodic = $this->periodic_illustration(12,$age,$monthly_allocated_premium,$periodic_premium,$policy_fee,$previous_yr_fund_after,$realistic_pyfa,$pessimistic_pyfa,$top_up_amt,$yearly_premium);
                    break;
            }

            //Get the premium pain as at this year
                $paid_premium += $yearly_premium;

            //Calculate the surrender values
            $surrender_name = '';

            if($year == 1)
            {
               $surrender_name = 'SB_1';
            }
            else if($year == 2)
            {
               $surrender_name = 'SB_2';
            }
            else if($year == 3)
            {
               $surrender_name = 'SB_3';
            }
            else if($year == 4)
            {
               $surrender_name = 'SB_4';
            }
            else if($year >= 5 )
            {
               $surrender_name = 'SB_5';
            }

            $surrender_rate = Surrender_benefit::where('name',$surrender_name)->first()->value;
            $surrender_amount = $surrender_rate/100 * $this->number_unformat($periodic['annual_fund_after']);
            $real_surrender_amount = $surrender_rate/100 * $this->number_unformat($periodic['realistic_annual_fund_after']);
            $pess_surrender_amount = $surrender_rate/100 * $this->number_unformat($periodic['pessimistic_annual_fund_after']);


            //Get the premium plus any riders if selected
                $sa = $this->getSumAssured($age,$yearly_premium);

            //Calculate the rider amounts that depend on the sum assured
                $adb_amount = $adb_set === 'yes' ? $sa : 0;
                $tpd_amount = $tpd_set === 'yes' ? $sa : 0;
                $aah_amount = $aah_set === 'yes' ? $sa * 0.4 : 0;
                $cah_amount = $cah_set === 'yes' ? $sa * 0.4 : 0;

           //Minimum of both aah_amount and cah_amount is 250,000
                $aah_amount = ($aah_amount > 250000) && $aah_amount != 0 ? 250000 : $aah_amount;
                $cah_amount = $cah_amount > 250000 && $cah_amount != 0 ? 250000 : $cah_amount;


            $total_payable_premium = $this->get_total_premium($tar_amount,$adb_amount,$tpd_amount,$aah_amount,$cah_amount,$wp_set,$age,$term,$frequency,$periodic_premium);


            $term_illustration = array(
                'year' => $year,
                'paid_premium' => number_format($yearly_premium),
                'cumulative_premium' => number_format($paid_premium),
                'surrender_value' => number_format($surrender_amount),
                'pess_surrender_value' => number_format($pess_surrender_amount),
                'real_surrender_value' => number_format($real_surrender_amount),
                'allocated_premium' => number_format($allocated_premium),
                'pf_bos_mc' => $periodic['annual_total_charges'],
                'fund_before' => $periodic['annual_fund_before'],
                'fund_mngt_fee' => $periodic['annual_fund_mngt_fee'],
                'fund_after' => $periodic['annual_fund_after'],
                'realistic_fund_after' => $periodic['realistic_annual_fund_after'],
                'pessimistic_fund_after' => $periodic['pessimistic_annual_fund_after'],
                'total_payable_premium' => $total_payable_premium,
                'sum_assured' => $sa,
                'periodic' => $periodic['periodic']

            );



            $illustration[] = $term_illustration;

            //Increase the age after every year
            $age++;

            //Increase the policy fee by 10% every other year
            //$policy_fee *= 1.1;

        }

        //Generate pdf and pass url as response
        $return_value;

        if(isset($email_addr))
        {
           $pdf = PDF::loadView('unit_illustrator::index', ['illustration'=>$illustration,'post'=> $request->all()]);

           $pdf->save('unit_illustrator/illustration_'.$email_addr.'.pdf');

        //    return view('unit_illustrator::index', ['illustration'=>$illustration,'post'=> $request->all()]);

           $return_value = [
            'illustration' => $illustration,
            'pdf_url' => url('unit_illustrator/illustration_'.$email_addr.'.pdf') ];
        }
        else{
            $return_value = ['illustration' => $illustration];
        }

        //return $pdf->stream();

        return response()->json($return_value);

    }



    //** Function to calculate and return periodic values for the illustration **//

    private function periodic_illustration($frequency,$age,$monthly_allocated,$periodic_premium,$policy_fee,$previous_yr_fund_after,$realistic_pyfa,$pessimistic_pyfa,$top_up_amt,$yearly_premium)
    {
        $report = array();
        $fund_after = $previous_yr_fund_after != 0 ? $previous_yr_fund_after : 0;
        $real_fund_after = $previous_yr_fund_after != 0 ? $realistic_pyfa : 0;
        $pess_fund_after = $previous_yr_fund_after != 0 ? $pessimistic_pyfa : 0;

        $annual_total_charges = 0;


        //*** Calculate sum assured ***//
        $sa = $this->getSumAssured($age,$yearly_premium);


        for($i = 1; $i <= $frequency; $i++){

            $qx_rate = Qx::where('x', $age)->first();

            //Get the mortality charge per month
            $mortality_charge = (($qx_rate->result/1000)*($sa-$fund_after))/$frequency;
            $real_mortality_charge = (($qx_rate->result/1000)*($sa-$real_fund_after))/$frequency;
            $pess_mortality_charge = (($qx_rate->result/1000)*($sa-$pess_fund_after))/$frequency;

            $mortality_charge = $mortality_charge>0 ? $mortality_charge : 0;
            $real_mortality_charge = $real_mortality_charge>0 ? $real_mortality_charge : 0;
            $pess_mortality_charge = $pess_mortality_charge>0 ? $pess_mortality_charge : 0;



            //Calculate the dib offer spread
            $bos = ($monthly_allocated - $policy_fee - $mortality_charge) * 0.05;
            $real_bos = ($monthly_allocated - $policy_fee - $real_mortality_charge) * 0.05;
            $pess_bos = ($monthly_allocated - $policy_fee - $pess_mortality_charge) * 0.05;

            //Get the Fund Growth Periodic
            //Optimistic
            $fgp = pow((1 + 12/100),(1/$frequency));

            //Realistic

            $fgp_realistic = pow((1 + 10/100),(1/$frequency));

            //Pessimistic

            $fgp_pessimistic = pow((1 + 8/100),(1/$frequency));




            /** FUND BEFORE **/

            //Calculate the optimistic fund before the fund management fee
            $fund_before = ($fund_after + $monthly_allocated - $policy_fee - $mortality_charge - $bos) * $fgp;

            //Calculate the realistic fund before the fund management fee
            $realistic_fund_before = ($real_fund_after + $monthly_allocated - $policy_fee - $real_mortality_charge - $real_bos) * $fgp_realistic;

            //Calculate the pessimistic fund before the fund management fee
            $pessimistic_fund_before = ($pess_fund_after + $monthly_allocated - $policy_fee - $pess_mortality_charge - $pess_bos) * $fgp_pessimistic;



            /** FUND MANAGEMENT FEES **/

            //Add the top up amount to the fund before
            //$fund_before = $fund_before +$top_up_amt;

            //Calculate fund management fee ( 2% of the fund before fees, at the end of the year)
            $fund_mngt_fee = ($i==$frequency) ? 2/100*$fund_before : 0;

            //Realistic
            $realistic_fund_mngt_fee = ($i==$frequency) ? 2/100*$realistic_fund_before : 0;

            //Pessimistic
            $pessimistic_fund_mngt_fee = ($i==$frequency) ? 2/100*$pessimistic_fund_before : 0;




            /** FUND AFTER **/

            //Calculate the fund after the management fee
            $fund_after = $fund_before -  $fund_mngt_fee;

            //Calculate the realistic fund after the management fee
            $real_fund_after = $realistic_fund_before -  $realistic_fund_mngt_fee;

            //Calculate the pessimistic fund after the management fee
            $pess_fund_after = $pessimistic_fund_before -  $pessimistic_fund_mngt_fee;






            //Calculate all charges
            $all_charges = $policy_fee + $mortality_charge + $bos;

            //calculate annual total charges
            $annual_total_charges += $all_charges;

            $periodic_report = array(
                'age' => $age,
                'month' => $i,
                'premium' => $periodic_premium,
                'allocated_premium' => $monthly_allocated,
                'policy_fee' => number_format($policy_fee),
                'mortality_charge' =>number_format($mortality_charge),
                'bid_offer_spread' => number_format($bos),
                'fund_before' => number_format($fund_before),
                'realistic_fund_before' => number_format($realistic_fund_before),
                'pessimistic_fund_before' => number_format($pessimistic_fund_before),
                'fund_mngt_fee' => number_format($fund_mngt_fee),
                'realistic_fund_mngt_fee' => number_format($realistic_fund_mngt_fee),
                'pessimistic_fund_mngt_fee' => number_format($pessimistic_fund_mngt_fee),
                'realistic_fund_after' => number_format($real_fund_after),
                'pessimistic_fund_after' => number_format($pess_fund_after),
                'fund_after' => number_format($fund_after),
                'all_charges' => number_format($all_charges)
            );

            $report[$i] = $periodic_report;

        }

        //add top up amounts if any to fund before
        $topup_fund_before = $this->number_unformat($periodic_report['fund_before']) + $top_up_amt;
        $real_topup_fund_before = $this->number_unformat($periodic_report['realistic_fund_before']) + $top_up_amt;
        $pess_topup_fund_before = $this->number_unformat($periodic_report['pessimistic_fund_before']) + $top_up_amt;


        /** TOP UP CHARGED **/


        //add 2% of the top up amount to the fund management fee
        $top_up_charge = (2/100*$top_up_amt) + $this->number_unformat($periodic_report['fund_mngt_fee']);

        //add 2% of the top up amount to the fund management fee
        $realistic_top_up_charge = (2/100*$top_up_amt) + $this->number_unformat($periodic_report['realistic_fund_mngt_fee']);

        //add 2% of the top up amount to the fund management fee
        $pessimistic_top_up_charge = (2/100*$top_up_amt) + $this->number_unformat($periodic_report['pessimistic_fund_mngt_fee']);



        /** FUND AFTER **/


        //Calculate the fund after considering if there is top up charge
        $topup_fund_after = $topup_fund_before - $top_up_charge;

        //Calculate the realistic fund after considering if there is top up charge
        $realistic_topup_fund_after = $real_topup_fund_before - $realistic_top_up_charge;

        //Calculate the pessimistic fund after considering if there is top up charge
        $pessimistic_topup_fund_after = $pess_topup_fund_before - $pessimistic_top_up_charge;



        return [
            'periodic' => $report,
            'annual_fund_after' => number_format($topup_fund_after),
            'realistic_annual_fund_after' => number_format($realistic_topup_fund_after),
            'pessimistic_annual_fund_after' => number_format($pessimistic_topup_fund_after),
            'annual_fund_before' => number_format($topup_fund_before),
            'annual_fund_mngt_fee' => number_format($top_up_charge),
            'annual_total_charges' =>  number_format($annual_total_charges)
        ];
    }


    //** Function to calculate the total premium based on the factors chosen */
    private function get_total_premium($tar_amount,$adb_amount,$tpd_amount,$aah_amount,$cah_amount,$wp_set,$age,$term,$frequency,$periodic_premium)
    {

        //**Let's get the TAR premium**//

        $tr_rate = Term_rider_rate::where(['age'=> $age,'term'=> $term])->first();
        $tar_rate = !empty($tr_rate) ? $tr_rate->rate : 0;

        //Get the rider factor from the rider amount given and the frequency from the conversion_rates table
        if($tar_amount != 0){
            $rider_factor = Rider_conversion_rate::where('START','<=', $tar_amount)
                ->where('END','>=', $tar_amount)
                ->where('frequency',$frequency)->first()->rate;

            $tar_premium = $tar_rate/1000*$tar_amount*$rider_factor;
        }
        else{
            $tar_premium = 0;
        }


        //***Let's get the ABD rider premium***//

        //Get the rider factor from conversion rate table
        $adb_factor = Conversion_rate::where(['rider_name'=>'ADB Rider','frequency'=>$frequency])->first()->value;

        //Get the ADB rate from the riders table
        $adb_rate = Rider::where('initials','ADB')->first()->rate;

        $adb_premium = $adb_rate/1000*$adb_amount*$adb_factor;



        //***Let's get the TPD rider premium***//

        //Get the rider factor from conversion rate table
        $tpd_factor = Conversion_rate::where(['rider_name'=>'TPD Rider','frequency'=>$frequency])->first()->value;

        //Get the TPD rate from the riders table
        $tpd_rate = Rider::where('initials','TPD')->first()->rate;

        $tpd_premium = $tpd_rate/1000*$tpd_amount*$tpd_factor;



        //***Let's get the AAH rider premium***//

        //Get the rider factor from conversion rate table
        $aah_factor = Conversion_rate::where(['rider_name'=>'AAH Rider','frequency'=>$frequency])->first()->value;

        //Get the TPD rate from the riders table
        $aah_rate = Rider::where('initials','AAH')->first()->rate;

        $aah_premium = $aah_rate/1000*$aah_amount*$aah_factor;


        //***Let's get the CAH rider premium***//

        //Get the rider factor from conversion rate table
        $cah_factor = Conversion_rate::where(['rider_name'=>'CAH Rider','frequency'=>$frequency])->first()->value;

        //Get the TPD rate from the riders table
        $cah_rate = Rider::where('initials','CAH')->first()->rate;

        $cah_premium = $cah_rate/1000*$cah_amount*$cah_factor;



        //***Let's get the Waiver of premium***//

        //Get the WP rate from the riders table
        $wp_rate = Rider::where('initials','WP')->first()->rate;

        //Check whether WP is set
            $wp_premium = $wp_set == 'yes' ? ($wp_rate*$periodic_premium) : 0;

        //Sum all these rider premiums and return them
        //Dont add the riders if age is above 60
        $topup_premium = 0;

        if($age <= 60) {
            $topup_premium = $wp_premium + $cah_premium + $aah_premium + $tpd_premium + $adb_premium + $tar_premium;
            //$topup_premium = $tar_amount != 0 ? ($wp_premium + $cah_premium + $aah_premium + $tpd_premium + $adb_premium + $tar_premium) : 0;
        }

        //Add the rider premiums to the periodic premium to get the new payable premium
        $total_rider_premium = $topup_premium + $periodic_premium;


        //Set the rider premiums to an array
        $rider_premiums = array(
            'tar_premium' => number_format($tar_premium),
            'adb_premium' =>number_format($adb_premium),
            'tpd_premium' =>number_format($tpd_premium),
            'aah_premium' =>number_format($aah_premium),
            'cah_premium' =>number_format($cah_premium),
            'wp_premium' => number_format($wp_premium),
            'wp_emium' => number_format($cah_amount),
            'total_rp' =>number_format($total_rider_premium),
            'cah_amount' => number_format($cah_amount),
            'aah_amount' => number_format($aah_amount)
        );
        return $rider_premiums;
    }


    //** Function to get the sum assured *//
    private function getSumAssured($age,$yearly_premium)
    {
        //if age is less than 60 multiply yearly premium by 10 otherwise multiply by three
            $sa = 0;
            if($age < 60)
            {
                $sa = $yearly_premium * 10;
            }
            else
            {
                $sa = $yearly_premium * 3;
            }

            return $sa;

    }

    //Function to unformat a number that is number formatted
    function number_unformat($number, $force_number = true, $dec_point = '.', $thousands_sep = ',') {
        if ($force_number) {
            $number = preg_replace('/^[^\d]+/', '', $number);
        } else if (preg_match('/^[^\d]+/', $number)) {
            return false;
        }
        $type = (strpos($number, $dec_point) === false) ? 'int' : 'float';
        $number = str_replace(array($dec_point, $thousands_sep), array('.', ''), $number);
        settype($number, $type);
        return $number;
    }


    function update(Request $request)
    {

        // $validator = Validator::make($request->all(), [
        //     'id' => 'required',
        //     'name' => 'required',
        //     'value' => 'required',
        //     'module' => 'required'
        // ]);

        // if($validator->fails()) {
        //     return response()->json([
        //         'message' => 'Sorry, an error occured preventing updating',
        //         'status' => 0,
        //         'errors' => $validator->errors()->all()
        //     ], 422);
        // }

        //  $result = Setting::where('id', $request->id)
        //             ->update(['name' => $request->name,
        //                 'value' => $request->value,
        //                 'module' => $request->module
        //                 ]);
        // if($result)
        // {
        //     return response()->json([
        //         'message' => 'Settings successfully updated'
        //     ]);
        // }
        // else
        // {
        //      return response()->json([
        //         'message' => 'Settings not updated'
        //     ]);
        // }
    }


    function delete(Request $request)
    {

        // $validator = Validator::make($request->all(), [
        //     'id' => 'required'
        // ]);

        // if($validator->fails()) {
        //     return response()->json([
        //         'message' => 'Sorry, an error occured preventing deletion',
        //         'status' => 0,
        //         'errors' => $validator->errors()->all()
        //     ], 422);
        // }

        //  $result = Setting::destroy($request->id);


        // if($result)
        // {
        //     return response()->json([
        //         'message' => 'Settings successfully deleted'
        //     ]);
        // }
        // else
        // {
        //      return response()->json([
        //         'message' => 'Settings not deleted'
        //     ]);
        // }
    }



}