<?php

namespace jick\unit_illustrator;

use Illuminate\Support\ServiceProvider;

class Unit_illustratorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'unit_illustrator');

       /* $this->publishes([
            __DIR__.'/views' => base_path('resources/views/jick/users'),
        ]);*/
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //Load controllers
        $this->app->make('Jick\unit_illustrator\controllers\Unit_illustratorController');
        $this->app->make('Jick\unit_illustrator\controllers\SettingsController');

        //Load models
        $this->app->make('Jick\unit_illustrator\models\Assumption');
        $this->app->make('Jick\unit_illustrator\models\Qx');
        $this->app->make('Jick\unit_illustrator\models\Conversion_rate');
        $this->app->make('Jick\unit_illustrator\models\Rider_conversion_rate');
        $this->app->make('Jick\unit_illustrator\models\Term_rider_rate');
        $this->app->make('Jick\unit_illustrator\models\Rider');
        $this->app->make('Jick\unit_illustrator\models\Surrender_benefit');
        $this->app->make('Jick\unit_illustrator\models\minPremium');

        include __DIR__.'/routes.php';
    }
}