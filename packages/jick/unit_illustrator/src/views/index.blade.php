<!DOCTYPE html>
<html>
    <head>
        <!--<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">-->
        <style rel="stylesheet">
            body{
                font-size:8px;
                /*border: 1px solid #000;
                padding-left: 5px 5px 5px -5px;*/
            }
            table {
                border-collapse: collapse;
                width:100%;
            }
            td, th {
                border: 1px solid black;
            }
            .container{
                width:700px;
            }
            .header{
                background-color: #c10435;
                height:85px;
                font-size: 15px;                
            }
            .jub_logo{
                float:left;
                width: 200px;
                height:80px; 
            }
            .header_text{
                float:left;
                width:500px; 
                color:#fff;
            }
            .below_header{
               clear:both;
               text-align:center;
               margin-bottom:5px; 
            }
            .basic_details{
                margin-bottom:0px;
            }
            .basic_header{
                background-color:#9C9C9C;
                color:#fff;
                height:20px;
                width:700px;
                padding-top:-10px;
            }
            .basic_left{
               float:left;
               width:200px; 
            }
            .basic_middle{
                float:left; 
                width:150px;
                height:50px; 
            }
            .basic_right{
                float:left; 
                width:250px
            }
            .coverage_details{
                clear:both;
                margin-bottom:60px;
            }
            .coverage_header{
                background-color:#9C9C9C;
                color:#fff;
                padding-top:-10px;
                height:20px;
            }
            .coverage_left{
               float:left;
               width:250px; 
            }
            .coverage_middle{
                float:left; 
                width:100px;
                height:50px; 
            }
            .coverage_right{
                float:left; 
                /*width:300px*/
            }
            .benefits{
                clear:both;
            }
            .benefits_header{
                background-color:#9C9C9C;
                color:#fff;
                height:20px;
                padding-top:-10px;
                margin-bottom:2px;
            }
            .illustrator{
                clear:both;
            }
            .illustrator_header{
                background-color:#9C9C9C;
                color:#fff;
                height:20px;
                padding-top:-10px;
                margin-bottom:2px;
            }
            
            .notes{
                clear:both;
            }
            .notes_header{
                background-color:#9C9C9C;
                color:#fff;
                height:20px;
                margin-bottom: 10px;
                margin-top: 20px;
                padding-top:-10px;               
                
            }
            ._middle{
                width:10px;
                height:10px; 
            }
            ._right{
                margin-top: 15px;
                page-break-after:always;
                /*float: left;*/
            }            
            ._left{
            }
            
            .above_signature{
                clear:both;
            }
            
            .sign_header{
                background-color:#9C9C9C;
                color:#fff;
                height:20px;
                margin-bottom: 20px;
                margin-top: 10px;
                padding-top:-10px;
            }
            .sign_left{
               float:left;
               width:350px; 
            }
            .sign_middle{
                float:left; 
                width:50px;
                height:50px; 
            }
            .sign_right{
                float:left;
                width:300px; 
            }
            .client_sign{
                border-bottom:1px solid #000;
            }
            
            .declare_header{
                background-color:#9C9C9C;
                color:#fff;
                height:20px;
                margin-bottom: 20px;
                margin-top: 20px;
                padding-top:-10px;
            }
            .declare_words{
                margin-bottom:40px;
            }
            .declare_left{
               float:left;
               width:350px; 
               margin-top: 40px;
            }
            .declare_middle{
                float:left; 
                width:50px;
                height:100px; 
            }
            .declare_right{
                float:left;
                width:300px; 
            }
            .declarations{
                clear:both;
            }
            .branch{
                clear:both;
                margin-bottom: 10px;
                margin-top: 20px;
            }            
            .official{
                clear:both;
                margin-bottom: 20px;
                margin-top: 20px;
            }
            .official_header{
                background-color:#9C9C9C;
                color:#fff;
                height:20px;
                margin-bottom: 20px;
                margin-top: 20px;
                padding-top:-10px;
            }
            .footer {
                width: 100%;
                position: fixed;
            }
            .footer {
                border-top:1px solid #9C9C9C;
                padding-top: 5px;
                margin-top:5px;
                bottom: 15px;                
                text-align:center;
                font-size:8px;
            }
            .pagenum:before {
                content: counter(page);
            }
        </style>
    </head>
    <body>
        <div class="footer">
                Illustration Version No: <?php echo date('M Y');?><br>
                Date on which Illustration Report is prepared: <?php echo date('d-M-Y');?><br>
                Illustration Validity End Date:  <br>
                Page <span class="pagenum"></span>
        </div>
        <div class="container">
            <div class="header">
                <div class="jub_logo">
                    <img src="img/j_logo.jpg" alt="Logo" height="78" width="160">
                </div>
                <div class="header_text">
                     <h2>Jubilee Insurance Company Limited</h2>
                </div>
            </div>
            <div class="below_header">
                            Illustration of Benefits for<br>(Jwealth Education/Savings/Retirement)<br>
                                    Prepared for <?php echo $post['name']; ?>
            </div>
            <div class="basic_details">
                <div class="basic_header">
                   <h4>Basic Details:</h4> 
                </div>
                <div class="basic_left">
                    <table border=0>
                        <tr>
                            <td><strong>Name of Life Assured:</strong></td>
                            <td><?php echo $post['name']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Date of Birth:</strong></td>
                            <td><?php echo $post['dob']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Age:</strong></td>
                            <td><?php echo $post['age']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Gender:</strong></td>
                            <td><?php echo $post['gender']; ?></td>
                        </tr>
                    </table>
                </div>
                <div class="basic_middle">
                    
                </div>
                <div class="basic_right">
                    <table border=0>
                        <tr>
                            <td><strong>Expected Commencement Date:</strong></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><strong>Reference Number:</strong></td>
                            <td></td>
                        </tr>
                    </table>                    
                </div>
            </div>
            <div class="coverage_details">
                <div class="coverage_header">
                   <h4>Coverage Details:</h4> 
                </div>
                <div class="coverage_left">                    
                    <strong>Currency of Plan:</strong> KES.<br>
                    <strong>Name of Plan:</strong>(Jwealth Edu/Savings/Retirement) Plan<br>
                    <strong>Premium Payment Term:</strong> <?php echo $post['term']; ?> Years<br>
                    <strong>Policy Term:</strong> <?php echo $post['term']; ?> Years<br>
                    <strong>Indexation:</strong> N/A<br>
                    <strong>Fund type(M.Market/Growth/Equity)</strong>	
                </div>
                <div class="coverage_middle">
                    
                </div>
                <div class="coverage_right">
                    <strong>Mode of Premium Payment:</strong> <?php echo ucfirst($post['frequency']); ?><br>
                    <strong>Cover Multiple:</strong> <br>
                    <strong>Initial Basic Premium(<?php echo ucfirst($post['frequency']); ?>):</strong> KES. <?php echo number_format($post['periodic_premium']); ?><br>
                    <strong>Initial Total Premium(<?php echo ucfirst($post['frequency']); ?>):</strong> KES. <?php echo $illustration[0]['total_payable_premium']['total_rp']; ?><br>
                    <strong>Initial Maximum Sum Assured:</strong> KES. <?php echo number_format($illustration[0]['sum_assured']); ?>
                </div>
            </div>
            <div class="benefits">
                <table border=0>
                    <tr style="text-decoration:underline; text-align:left; font-weight:bold">
                        <td>Description</td>
                        <td>Benefit Code</td>
                        <td>Benefit/Sum Assured</td>
                        <td>Term</td>
                        <td>Premium</td>
                    </tr>
                    <tr>
                        <td> Main Plan</td>
                        <td> - </td>
                        <td> KES. <?php echo number_format($illustration[0]['sum_assured']); ?> </td>
                        <td> <?php echo $post['term']; ?> Years</td>
                        <td> KES. <?php echo number_format($post['periodic_premium']); ?> </td>
                    </tr>
                    <?php 
                        if(isset($post['tar_amount']) && $post['tar_set'] == 'yes')
                        {                    
                              echo "<tr>
                                        <td> Term Assurance Rider </td>
                                        <td> TAR </td>
                                        <td> KES.".number_format($post['tar_amount'])."</td>
                                        <td>".$post['term']." Years</td>
                                        <td> KES.".$illustration[0]['total_payable_premium']['tar_premium']."</td>
                                    </tr>";
                        
                        }
                    ?>
                    <?php 
                        if(isset($post['adb_set']) && $post['adb_set'] == 'yes')
                        {                    
                              echo "<tr>
                                        <td> Accidental Death Benefit</td>
                                        <td> ADB </td>
                                        <td> KES.".number_format($illustration[0]['sum_assured'])."</td>
                                        <td>".$post['term']." Years</td>
                                        <td> KES.".$illustration[0]['total_payable_premium']['adb_premium']."</td>
                                    </tr>";
                        
                        }
                    ?>
                    <?php 
                        if(isset($post['tpd_set']) && $post['tpd_set'] == 'yes')
                        {                    
                              echo "<tr>
                                        <td> Total & Permanent Disability</td>
                                        <td> TPD </td>
                                        <td> KES.".number_format($illustration[0]['sum_assured'])."</td>
                                        <td>".$post['term']." Years</td>
                                        <td> KES.".$illustration[0]['total_payable_premium']['tpd_premium']."</td>
                                    </tr>";
                        
                        }
                    ?>
                    <?php
                        if(isset($post['aah_set']) && $post['aah_set'] == 'yes')
                        {                    
                              echo "<tr>
                                        <td> Adult Accident Hospitalization Rider </td>
                                        <td> AAH </td>
                                        <td> KES.".$illustration[0]['total_payable_premium']['aah_amount']."</td>
                                        <td>".$post['term']." Years</td>
                                        <td> KES.".$illustration[0]['total_payable_premium']['aah_premium']."</td>
                                    </tr>";
                        
                        }
                    ?>
                    <?php 
                        if(isset($post['cah_set']) && $post['cah_set'] == 'yes')
                        {                    
                              echo "<tr>
                                        <td> Child Accident Hospitalization Rider</td>
                                        <td> CAH </td>
                                        <td> KES.".$illustration[0]['total_payable_premium']['cah_amount']."</td>
                                        <td>".$post['term']." Years</td>
                                        <td> KES.".$illustration[0]['total_payable_premium']['cah_premium']."</td>
                                    </tr>";
                        
                        }
                    ?>
                    <?php 
                        if(isset($post['wp_set']) && $post['wp_set'] == 'yes')
                        {                    
                              echo "<tr>
                                        <td> Waiver of Premmium </td>
                                        <td> WP </td>
                                        <td> KES.".number_format($post['periodic_premium'])."</td>
                                        <td>".$post['term']." Years</td>
                                        <td> KES.".$illustration[0]['total_payable_premium']['wp_premium']."</td>
                                    </tr>";
                        
                        }
                    ?>
                  </table>
                
            </div>
            <div class="illustrator">
                <div class="illustrator_header">
                   <h4>Illustrative Values:</h4> 
                </div>
                    <table border="1" style="">
                        <tr>
                            <th colspan="3"></th>
                            <th colspan="2">Expected Cash Value (8%)</th>
                            <th colspan="2">Expected Cash Value (10%)</th>
                            <th colspan="2">Expected Cash Value (12%)</th>
                        </tr>
                        <tr>
                            <th>Year</th>
                            <th>Premium Paid</th>
                            <th>Cumulative Premium Paid</th>
                            <th>Main Plan</th>
                            <th>Surrender Value</th>
                            <th>Main Plan</th>
                            <th>Surrender Value</th>
                            <th>Main Plan</th>
                            <th>Surrender Value</th>
                        </tr>                        
                            <?php
                            foreach($illustration as $illustrate)
                            {


                                echo "<tr>
                                            <td align='right'>".$illustrate['year']."</td>
                                            <td align='right'>".$illustrate['paid_premium']."</td>
                                            <td align='right'>".$illustrate['cumulative_premium']."</td>
                                            <td align='right'>".$illustrate['pessimistic_fund_after']."</td>
                                            <td align='right'>".$illustrate['pess_surrender_value']."</td>
                                            <td align='right'>".$illustrate['realistic_fund_after']."</td>
                                            <td align='right'>".$illustrate['real_surrender_value']."</td>
                                            <td align='right'>".$illustrate['fund_after']."</td>
                                            <td align='right'>".$illustrate['surrender_value']."</td>
                                        </tr>";


                            }
                            ?>
                    </table>               
                </div>
                <div class="notes">
                    <div class="notes_header">
                    <h4>Notes</h4> 
                    </div>
                    <div class="_left">
                        Charges*: <br>                
                            <table>
                                <tr>
                                    <th width='60%'>Decription</th><th>Level</th>
                                </tr>
                                <tr>
                                    <td>Management Charge</td><td>2% per year of fund value</td>
                                </tr>
                                <tr>
                                    <td>Bid-Offer Spread</td><td>5%</td>
                                </tr>
                                <tr>
                                    <td>Policy Fee</td><td>Nil</td>
                                </tr>
                                <tr>
                                    <td>Mortality, Rider and other Risk Charges</td>
                                    <td>Applied on Sum at Risk on attained age basis</td>
                                </tr>
                                <tr>
                                    <td>Administrative Charge</td>
                                    <td>KES. 200 per month</td>
                                </tr>
                                <tr>
                                    <td>Fund Switching/Redirection Charge:
                                        - First free switch per policy year - Subsequent Switching/Redirections within a policy
                                            Year charges							

                                    </td>
                                    <td>
                                       1st Free of Charge, KES. 1000 per subsequent switch in policy year Switch/<Redirection></Redirection>
                                    </td>
                                </tr>
                            </table> 
                    </div>
                    <div class="_right">
                          - Allocation Percentages: <br>                 
                                <table>
                                    <tr>
                                        <th width='60%'>Year</th><th>Allocation Percentages</th>
                                    </tr>
                                    <tr>
                                        <td>1st Year</td><td align="right">60%</td>
                                    </tr>
                                    <tr>
                                        <td>2nd Year</td><td align="right">80%</td>
                                    </tr>
                                    <tr>
                                        <td>3rd Year</td><td align="right">85%</td>
                                    </tr>
                                    <tr>
                                        <td>3rd Year<br>4th Year</td><td align="right">90%<br>95%</td>
                                    </tr>
                                    <tr>
                                        <td>6th Year Onwards</td>
                                        <td align="right">100%</td>
                                    </tr>
                                </table>                 
                    </div>
                </div>
                <div class="above_signature">
                    <b>These charges are applicable for all policyholder funds<b><p>
                    - An age-based Mortality Charge applies for the life insurance risk each year and is dependant on the sum-at-risk. No charge applies in years where there is no sum-at-risk.<br>
                    - Administrative Charge is KES. 200 per month.<br>
                    - All charges mentioned in the above notes are reviewable at the discretion of the Company
                </div>
                <div class="signatures"  style="">
                   <div class="sign_header">
                        <h4>SIGNATURES</h4> 
                    </div> 
                    <div class="sign_left">
                        <div class="client_sign"></div>
                        <div><center>Client Signature</center></div>
                        <div style="margin-top:10px">
                            Name:  <span style="border-bottom: 1px solid black; padding-left: 235px">&nbsp;</span>                                 
                        </div>
                        <div style="margin-top:10px">
                            Date:  <span style="border-bottom: 1px solid black; padding-left: 240px">&nbsp;</span>                                 
                        </div>
                    </div>
                    <div class="sign_middle">
                    </div>
                    <div class="sign_right">
                        <div class="client_sign"></div>
                        <div><center>Signature of Sales Representative</center></div>
                        <div style="margin-top:10px">
                            Name:  <span style="border-bottom: 1px solid black; padding-left: 235px">&nbsp;</span>                                 
                        </div>
                        <div style="margin-top:10px">
                            Date:  <span style="border-bottom: 1px solid black; padding-left: 240px">&nbsp;</span>                                 
                        </div>
                    </div>
                </div>
                <div class="above_signature">
                    
                    - The “Cumulative Main Plan Basic Premiums Paid” mentioned in the tables above is net of non-linked supplementary rider premiums which are deducted from the total main plan premium
                      mentioned in the “Coverage Details” section. However, for linked rider(s), the risk charges are deducted monthly from surrender values.<br>
                    - Death Benefit is Sum Assured or Cash Value, whichever is higher. For illutration purpose, it is assumed that death occurs at the end of the year<br>
                    - Maturity Benefit is the prevailing Cash Value at the time of Maturity<br>
                    - Cash Value is the Unit Account value of the Policy. After three Policy Years, Surrender Value is equal to Cash Value.<br>
                    - The illustrative values do not take into account any partial surrenders<br>
                    - The rate of return assumptions mentioned in the tables above are before deduction of the fund Management charges. These charges have been deducted from the rate
                      of return assumptions prior to determining the Illustrative Values in the tables above<br>
                    - The Death Benefit/Surrender Values given above are based on assumptions. The actual values can be higher or lower than the one illustrated above depending upon the performance
                      of the underlying investments of the Company<br>
                    - The underlying investments of the plan are In Money Market Fund and/or Balanced/ Growth Fund and/or Equity Fund<br>
                    - Surrender Value given above are net of all charges. Policy will acquire Surrender Value after completion of two full Policy Years.<br>
                    - The Main Plan Death Benefit and Surrender Value shown above are rounded to the nearest thousand Kenya Shillings.<br>
                    - This is an approved illustration of the Company. Any other illustration which contradicts this illustration should not be given any consideration and should be reported to the Company<br>
                    - Please refer to the Policy Document for detailed Terms & Conditions
                    
                </div>
                <div class="declarations">
                   <div class="declare_header">
                        <h4>DECLARATIONS</h4>                        
                    </div>
                    <div class="declare_words">
                        <u>Declaration by Sales Representative:</u>
                        I confirm that I have not made any verbal, written or electronic presentation which is in contradiction to this illustration. I also confirm that I have not misled or deceived the prospective client in 
                        any way. In case of any misconduct on my part, the Company and I shall be responsible for any loss to the prospective client.
                    </div>
                    <div class="declare_left">
                        <div class="client_sign"></div>
                        <div><center>Signature</center></div>
                        <div style="margin-top:10px">
                            Name:  <span style="border-bottom: 1px solid black; padding-left: 235px">&nbsp;</span>                                 
                        </div>
                        <div style="margin-top:10px">
                            Code:  <span style="border-bottom: 1px solid black; padding-left: 240px">&nbsp;</span>                                 
                        </div>
                    </div>
                    <div class="declare_middle">
                    </div>
                    <div class="declare_right">                        
                        <div style="margin-top:10px">
                            Designation:  <span style="border-bottom: 1px solid black; padding-left: 200px">&nbsp;</span>                                 
                        </div>
                        <div style="margin-top:10px">
                            Date:  <span style="border-bottom: 1px solid black; padding-left: 245px">&nbsp;</span><br>                                 
                        </div>
                    </div>
                </div>
                <div class="declarations">
                    <div class="declare_words">
                        <u>Declaration by Client:</u>
                        I have studied the above illustration and notes carefully and understood them fully. I also confirm that no other illustration verbal, written or electronic in contradiction to this illustration has been 
                        given to me. . I also understand that past performance of Jubilee’s funds is not necessarily a guide to future performance. Any forecast made is not necessarily indicative of future or likely performance 
                        of the funds and Jubilee will not incur any liability for the same
                    </div>
                    <div class="declare_left">
                        <div class="client_sign"></div>
                        <div><center>Signature</center></div>
                        <div style="margin-top:10px">
                            Name:  <span style="border-bottom: 1px solid black;"><?php echo $post['name']; ?></span>                                 
                        </div>
                        <div style="margin-top:10px">
                            Address:  <span style="border-bottom: 1px solid black; padding-left: 240px">&nbsp;</span>                                 
                        </div>
                    </div>
                    <div class="declare_middle">
                    </div>
                    <div class="declare_right"> 
                        <div style="margin-top:10px">
                            CNIC:  <span style="border-bottom: 1px solid black; padding-left: 225px">&nbsp;</span>                                 
                        </div>                       
                        <div style="margin-top:10px">
                            Date:  <span style="border-bottom: 1px solid black; padding-left: 230px">&nbsp;</span>                                 
                        </div>
                        <div style="margin-top:10px">
                            Contact Number:  <span style="border-bottom: 1px solid black; padding-left: 150px">&nbsp;</span>                                 
                        </div>
                    </div>
                </div>                
                <div class="branch">
                    <hr>
                    <u>Branch Management Review</u><br>
                    <div class="declare_left">
                        <div class="client_sign"></div>
                        <div><center>Signature</center></div>
                        <div style="margin-top:10px">
                              Name:  <span style="border-bottom: 1px solid black; padding-left: 240px">&nbsp;</span>                               
                        </div>
                        <div style="margin-top:10px">
                            Date:  <span style="border-bottom: 1px solid black; padding-left: 240px">&nbsp;</span>                                 
                        </div>
                    </div>                    
                </div>
                <div class="official">
                   <div class="official_header">
                        <h4>FOR OFFICE USE ONLY</h4>                        
                    </div>
                    <div class="declare_left">
                        <div><b>Medical Requirements: Non Medical</div>
                        <div style="margin-top:10px">
                            <b>TSAR</b>:  <span style="border-bottom: 1px solid black; padding-left: 235px">&nbsp;</span><br>
                            <b>FIB SAR</b>:  <span style="border-bottom: 1px solid black; padding-left: 215px">&nbsp;</span><br>
                            <b>FIB SA</b>:  <span style="border-bottom: 1px solid black; padding-left: 225px">&nbsp;</span><br>
                                                             
                        </div>
                    </div>
                    <div class="declare_middle">
                    </div>
                    <div class="declare_right">                        
                        <div style="margin-top:10px">
                            Sales Person:  <span style="border-bottom: 1px solid black; padding-left: 200px">&nbsp;</span>                                 
                        </div>
                        <div style="margin-top:10px">
                            Manager:  <span style="border-bottom: 1px solid black; padding-left: 220px">&nbsp;</span><br>                                 
                        </div>
                        <div style="margin-top:10px">
                            Branch Name:  <span style="border-bottom: 1px solid black; padding-left: 190px">&nbsp;</span>                                 
                        </div>
                        <div style="margin-top:10px">
                            S. Person's Code:  <span style="border-bottom: 1px solid black; padding-left: 170px">&nbsp;</span><br>                                 
                        </div>
                        <div style="margin-top:10px">
                           Manager's Code:  <span style="border-bottom: 1px solid black; padding-left: 170px">&nbsp;</span><br>                                 
                        </div>
                        
                        <div style="margin-top:10px">
                            City:  <span style="border-bottom: 1px solid black; padding-left: 250px">&nbsp;</span><br>                                 
                        </div>
                    </div>
                </div>
                                <?php //print_r($illustration[0]); ?>
        </div>
            
</body>
</html>