<?php

use Jick\policies\Models\Risk;

Route::group(['prefix' => 'api/'], function() {
	Route::post('payment', function(Illuminate\Http\Request $request) {

		$validator = Validator::make($request->all(), [
			'fname' => 'required',
			'lname' => 'required',
			'email' => 'required',
			'phone' => 'required',
			'reference' => 'required'
		]);

		if($validator->fails()) {
			return response()->json($validator->errors()->all());
		}

		// $risk = new Risk();
		// $risk->car_make = $request->input('car_make');
		// $risk->car_model = $request->input('car_model');
		// $risk->dom = $request->input('dom');
		// $risk->reg_no = $request->input('reg_no');
		// $risk->policy_no = $request->input('reference');

		// $risk->save();

		$params = [
            'description' => '',
            'type' => 'MERCHANT',
            'ref' => $request->reference,
            'fname' => $request->fname,
            'lname' => $request->lname,
            'email' => $request->email,
						'phone' => $request->phone
        ];

        $iframe_src = Pesapal::request($params);

		return $iframe_src;

        // return '<iframe src="'.$iframe_src.'" width="100%" height="620px" scrolling="auto" frameBorder="0">
        //     <p>Unable to load the payment page</p>
        // </iframe>';
	});

	Route::get('payment', function() {
		return Pesapal::handleResponse();
	});

	Route::post('save-policy', 'jick\payments\controllers\PaymentController@savePolicy');
});
