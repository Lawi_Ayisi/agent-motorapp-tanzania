<?php

namespace jick\payments;

use Illuminate\Support\ServiceProvider;

class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'payments');

       /* $this->publishes([
            __DIR__.'/views' => base_path('resources/views/jick/users'),
        ]);*/
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //Load controllers
        $this->app->make('Jick\payments\controllers\PaymentController');

        //Load models
        $this->app->make('Jick\payments\models\Payment');

        include __DIR__.'/routes.php';
    }
}