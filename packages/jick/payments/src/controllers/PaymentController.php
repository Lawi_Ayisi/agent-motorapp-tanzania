<?php

namespace Jick\payments\controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Jick\payments\models\Payment;
use Jick\policies\Models\Policy;
use App\Http\Requests;
use Pesapal;
use Validator;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'payinf';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $params = [
            'description' => '',
            'type' => 'MERCHANT',
            'ref' => rand(1000, 1000000),
            'fname' => 'joe',
            'lname' => 'WK',
            'email' => 'waiguru.jk@gmail.com'
        ];

        $iframe_src = Pesapal::request($params);

        // return '<iframe src="'.$iframe_src.'" width="100%" height="620px" scrolling="auto" frameBorder="0">
        //     <p>Unable to load the payment page</p>
        // </iframe>';
        return $iframe_src;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];
        //store a payment

        //validate
        $validator = Validator::make($request->all(), [
                'reference' => 'required',
                'transaction_id' => 'required',
                'transaction_type' => 'required',
                'amount' => 'required',
                'status' => 'required',
                'callback_url' => 'required'
            ]);

            if($validator->fails()) {
                return response()->json([
                    'message' => 'Validation failed, see errors below',
                    'errors' => $validator->errors()->all()
                ], 422);
            }

        $payment = Payment::firstOrNew(['reference'=> $request->reference]);
        $payment->transaction_id = $request->transaction_id;
        $payment->transaction_type = $request->transaction_type;
        $payment->amount = $request->amount;
        $payment->status = $request->status;

        $policy = new Policy();
        $policy->policy_no = $request->reference;
        $policy->biz_line = $request->biz_line;
        $policy->user_id = $user_id;
        $policy->status = 0;
        $policy->total_premium = $request->amount;
        $policy->start_date = date('Y-m-d H:i:s');
        $policy->end_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 364 day"));
        $policy->renewal_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 365 day"));

        $policy->save();

         if($payment->save()) {
                return response()->json([
                    'message' => 'Payment details saved successfully'
                ], 200);
            }
            else{
                return response()->json([
                    'message' => 'Saving failed, try again'
                ], 422);
            }
    }

    public function savePolicy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'reference' => 'required',
            'transaction_id' => 'required',
            'transaction_type' => 'required',
            'amount' => 'required',
            'status' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'data' => [
                    'message' => 'Validation failed, see errors below',
                    'errors' => $validator->errors()->all()
                ]
            ], 422);
        }

        $payment = Payment::firstOrNew(array('reference' => $request->reference));

        $payment->transaction_id = $request->transaction_id;
        $payment->transaction_type = $request->transaction_type;
        $payment->amount = $request->amount;
        $payment->status = $request->status;

        if($payment->save()) {
            return response()->json([
                'message' => 'Payment details saved successfully'
            ], 200);
        }
        else{
            return response()->json([
                'message' => 'Saving failed, try again'
            ], 422);
        }
    }
}
