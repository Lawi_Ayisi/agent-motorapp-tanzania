<?php

namespace Jick\payments\models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public $incrementing = false;
    // 
    protected $fillable = [
        'reference',
        'transaction_id',
        'transaction_type',
        'amount',
        'status'
    ];

}
