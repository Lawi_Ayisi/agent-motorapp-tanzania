<?php

namespace jick\claims;

use Illuminate\Support\ServiceProvider;

class ClaimServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'claims');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //Load controllers
        $this->app->make('Jick\claims\controllers\ClaimsController');

        //Load models
        $this->app->make('Jick\claims\models\Claim');
        $this->app->make('Jick\claims\models\ClaimWitness');
        $this->app->make('Jick\claims\models\GeneralClaimDetail');
        $this->app->make('Jick\claims\models\Instruction_checklist');
        $this->app->make('Jick\claims\models\PersonInjured');
        $this->app->make('Jick\claims\models\PropertyDamage');
        $this->app->make('Jick\claims\models\CarDamage');
        $this->app->make('Jick\claims\models\PoliceDetail');

        include __DIR__.'/routes.php';
    }
}
