<?php
namespace Jick\claims\models;

use Illuminate\Database\Eloquent\Model;
use jick\files\models\File;
use Jick\Policies\Models\Risk;

class Claim extends Model
{
    protected $fillable = [
        'nature_of_loss',
        'description',
        'location',
        'status',
        'policy_id',
        'user_id',
        'claim_driver_name',
        'claim_driver_id',
        'claim_driver_licence',
        'claim_driver_phone'
    ];

    /**
     *Claim status constants
     */
    public static $pending = 'AWAITING_APPROVAL';
    //public static $approved = ''

    public $incrementing = false;

    public function witnesses() {
        $witness = $this->hasMany(ClaimWitness::class, 'claim_no', 'claim_no');
        return $witness;
    }

    public function properties() {
        $property = $this->hasMany(PropertyDamage::class, 'claim_no', 'claim_no');
        return $property;
    }

    public function persons() {
        $person = $this->hasMany(PersonInjured::class, 'claim_no', 'claim_no');
        return $person;
    }

    public function parts() {
        $part = $this->hasMany(CarDamage::class, 'claim_no', 'claim_no');
        return $part;
    }

    public function photos() {
        $photo = $this->hasMany(File::class, 'foreign_id', 'claim_no');
        return $photo;
    }

    public function drivers() {
        $driver = $this->hasMany(GeneralClaimDetail::class, 'claim_no', 'claim_no');
        return $driver;
    }

    public function details() {
        $detail = $this->hasMany(Risk::class, 'policy_no', 'policy_no');
        return $detail;
    }

}
