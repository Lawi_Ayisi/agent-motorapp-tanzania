<?php
namespace Jick\claims\models;

use Illuminate\Database\Eloquent\Model;

class CarDamage extends Model
{
    protected $table = 'car_damage';

    protected $fillable = ['claim_id', 'part_id'];
}