<?php
namespace Jick\claims\models;

use Illuminate\Database\Eloquent\Model;

class PoliceDetail extends Model
{
    protected $table = 'police_details';

    protected $fillable = ['police_name', 'police_station', 'claim_no'];
}
