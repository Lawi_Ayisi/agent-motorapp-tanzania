<?php
namespace Jick\claims\models;

use Illuminate\Database\Eloquent\Model;

class GeneralClaimDetail extends Model
{
        protected $fillable = [
            'driver_name',
            'driver_phone',
            'driver_license_no',
            'veh_reg_no',
            'veh_policy_no',
            'claim_id'
        ];

        protected $table = 'general_claim_details';

}