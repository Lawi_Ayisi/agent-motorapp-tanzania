<?php
namespace Jick\claims\models;

use Illuminate\Database\Eloquent\Model;

class ClaimWitnessStatement extends Model
{
        protected $fillable = [
            'name',
            'description',
            'claim_witness_id'
        ];

}