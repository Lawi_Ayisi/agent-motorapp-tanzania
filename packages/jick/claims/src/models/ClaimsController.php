<?php

namespace Jick\claims\controllers;

use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;
use PDF;

use Jick\claims\models\Claim;
use Jick\claims\models\Instruction_checklist;
use Jick\claims\models\ClaimWitness;
use Jick\claims\models\ClaimWitnessStatement;
use Jick\claims\models\PersonInjured;
use Jick\claims\models\PropertyDamage;
use Jick\claims\models\GeneralClaimDetail;
use Jick\claims\models\CarDamage;
use Jick\claims\models\PoliceDetail;
use Jick\policies\Models\Policy;

class ClaimsController extends Controller
{
  //** Generate a claim reference number **//
    private function generate_claim_number($policy_no,$biz_line)
    {
            //Get the current year
            $cur_year = date('Y');
            switch ($biz_line) {
                case 'general':
                         $policy_segments = explode('/', $policy_no);

                         $sub_class = $policy_segments[2];

                        //Get the id of the product from products table

                            $claims = Claim::all();//return $claims;
                            $last_claim;
                            foreach ($claims as $claim) {
                                $last_claim = $claim->claim_no;
                            }

                            if(count($claims) > 0)
                            {
                                $last_claim_segs = explode('/', $last_claim);

                                $claim_no = "C/ONL/".$sub_class."/".$cur_year."/".sprintf("%05s", $last_claim_segs[4] + 1);
                            }
                            else{
                                $claim_no = "C/ONL/".$sub_class."/".$cur_year."/". sprintf("%05s", 1);
                            }

                            return $claim_no;
                                break;

                default:
                    # code...
                    break;
            }



    }

    function initiateClaim(Request $request) {
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];
        //Business line
        $biz_line = $request->input('biz_line');
        //Policy ID
        $policy = $request->input('policy_no');
        //Claim number
        $claim_no = $this->generate_claim_number($policy, $biz_line);
        //Risk
        $risk = $request->input('risk');
        //Description
        $description = $request->input('description');

        $v = Validator::make($request->all(), [
            'risk' => 'required',
            'policy_no' => 'required',
            'description' => 'required',
            'biz_line' => 'required',
            'location' => 'required'
        ]);

        if($v->fails()) {
            return response()->json([
                'message' => 'Validation failed',
                'errors'  => $v->errors()->all()
            ]);
        }

        $claim = new Claim();
        $claim->claim_no = $claim_no;
        $claim->nature_of_loss = $risk;
        $claim->description = $description;
        $claim->location = $request->input('location');
        $claim->status = Claim::$pending;
        $claim->policy_no = $policy;
        $claim->user_id = $user_id;
        $claim->claim_driver_name = $request->claim_driver_name;
        $claim->claim_driver_id = $request->claim_driver_id;
        $claim->claim_driver_licence = $request->claim_driver_licence;
        $claim->claim_driver_phone = $request->claim_driver_phone;

        $save_claim = $claim->save();

        $cid = Claim::where('claim_no', $claim_no)->first()->id;

        if ($save_claim) {
            return response()->json([
                'message' => 'Claim saved',
                'reference' => $claim_no,
                'claim_id' => $cid
            ]);
        } else {
            return response()->json('Something went wrong, could not save the claim');
        }
    }

    function claimDriver(Request $request) {
        $driver = $request->input('driver_name');
        $count_driver = count($driver);

        $items_driver = array();
        for ($i=0; $i < $count_driver; $i++) {
            $item_driver = [
                'driver_name' => $request->input('driver_name')[$i],
                'driver_phone' => $request->input('driver_phone')[$i],
                'driver_license_no' => $request->input('driver_license_no')[$i],
                'veh_reg_no' => $request->input('driver_veh_reg_no')[$i],
                'veh_policy_no' => $request->input('driver_veh_policy_no')[$i],
                'claim_no' => $request->claim_no
            ];

            $items_driver[] = $item_driver;
        }

        $save_driver = GeneralClaimDetail::insert($items_driver);

        if ($save_driver) {
            return response()->json([
                'message' => 'Claim saved',
                'reference' => $request->claim_no
            ]);
        } else {
            return response()->json('Something went wrong, could not save the claim');
        }
    }

    function claimPersons(Request $request) {
        $person = $request->input('person_name');
        $count_persons = count($person);

        $items_person = array();
        for ($i=0; $i < $count_persons; $i++) {
            $item_person = [
                'name' => $request->person_name[$i],
                'email' => $request->person_email[$i],
                'id_no' => $request->person_id_no[$i],
                'passport_no' => $request->person_passport_no[$i],
                'postal_address' => $request->person_postal[$i],
                'physical_address' => $request->person_address[$i],
                'phone' => $request->person_phone[$i],
                'claim_no' => $request->claim_no
            ];

            $items_person[] = $item_person;
        }

        $save_person = PersonInjured::insert($items_person);

        if ($save_person) {
            return response()->json([
                'message' => 'Details saved',
                'reference' => $request->claim_no
            ]);
        } else {
            return response()->json('Something went wrong, could not save the claim');
        }
    }

    function claimProperty(Request $request) {
        $property = $request->input('property_name');
        $count_property = count($property);

        $items_property = array();
        for ($i=0; $i < $count_property; $i++) {
            $item_property = [
                'name' => $request->property_name[$i],
                'property_description' => $request->property_description[$i],
                'claim_no' => $request->claim_no
            ];

            $items_property[] = $item_property;
        }

        $save_property = PropertyDamage::insert($items_property);

        if ($save_property) {
            return response()->json([
                'message' => 'Property details saved',
                'reference' => $request->claim_no
            ]);
        } else {
            return response()->json('Something went wrong, could not save the claim');
        }

    }

    function claimWitnesses(Request $request) {
        $witness = $request->witness_name;
        $count_witness = count($witness);

        $items_witness = array();
        for ($i=0; $i < $count_witness; $i++) {
            $item_witness = [
                'name' => $request->witness_name[$i],
                'email' => $request->witness_email[$i],
                'id_no' => $request->witness_id_no[$i],
                'passport_no' => $request->witness_passport_no[$i],
                'postal_address' => $request->witness_postal[$i],
                'physical_address' => $request->witness_address[$i],
                'phone' => $request->witness_phone[$i],
                'claim_no' => $request->claim_no
            ];

            $items_witness[] = $item_witness;
        }

        $save_witness = ClaimWitness::insert($items_witness);

        if ($save_witness) {
            return response()->json([
                'message' => 'Witness details saved',
                'reference' => $request->claim_no
            ]);
        } else {
            return response()->json('Something went wrong, could not save the claim');
        }
    }

    function claimDamage(Request $request) {
        $part = $request->part_id;
        $count_part = count($part);

        $items_part = array();
        for ($i=0; $i < $count_part ; $i++) {
            $item_part = [
                'claim_no' => $request->claim_no,
                'part_id' => $request->part_id[$i]
            ];

            $items_part[] = $item_part;
        }

        $save_part = CarDamage::insert($items_part);

        if ($save_part) {
            return response()->json([
                'message' => 'Cars damage saved',
                'reference' => $request->claim_no
            ]);
        } else {
            return response()->json('Something went wrong, could not save the claim');
        }
    }

    function claimPolice(Request $request) {
        $police = new PoliceDetail();
        $police->police_name = $request->input('police_name');
        $police->police_station = $request->input('police_station');
        $police->claim_no = $request->claim_no;

        $save_police = $police->save();

        if ($save_police) {
            return response()->json([
                'message' => 'Police details saved',
                'reference' => $request->claim_no
            ]);
        } else {
            return response()->json('Something went wrong, could not save the claim');
        }
    }

    function editClaim(Request $request) {
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $claim_id = $request->input('claim_no');

        //Policy ID
        $policy = $request->input('policy_id');

        //Persons Injured
        $person_name = $request->input('person_name');
        $person_id_no = $request->input('person_id_no');
        $person_passport_no = $request->input('person_passport_no');
        $person_phone = $request->input('person_phone');
        $person_email = $request->input('person_email');
        $person_postal = $request->input('person_postal');
        $person_address = $request->input('person_address');

        //Property damaged
        $property_name = $request->input('property_name');
        $property_description = $request->input('property_description');

        //Witnesses
        $witness_name = $request->input('witness_name');
        $witness_id_no = $request->input('witness_id_no');
        $witness_passport_no = $request->input('witness_passport_no');
        $witness_phone = $request->input('witness_phone');
        $witness_stmt = $request->file('witness_stmt');
        $witness_email = $request->input('witness_email');
        $witness_postal = $request->input('witness_postal');
        $witness_address = $request->input('witness_address');


        //Save witnesses
        $witness = $request->input('witness_name');
        $count_witness = count($witness);

        $items_witness = array();
        for ($i=0; $i < $count_witness; $i++) {
            $item_witness = [
                'name' => $witness_name[$i],
                'email' => $witness_email[$i],
                'id_no' => $witness_id_no[$i],
                'passport_no' => $witness_passport_no[$i],
                'postal_address' => $witness_postal[$i],
                'physical_address' => $witness_address[$i],
                'phone' => $witness_phone[$i],
                'claim_no' => $claim_id
            ];

            $items_witness[] = $item_witness;

            $save_witness = ClaimWitness::firstOrCreate($item_witness);
        }

        //Add PoliceDetail
        $police = new PoliceDetail();
        $police->police_name = $request->input('police_name');
        $police->police_station = $request->input('police_station');
        $police->claim_no = $claim_id;



        //Save parts
        $part = $request->input('part_id');
        $count_part = count($part);

        $items_part = array();
        for ($i=0; $i < $count_part ; $i++) {
            $item_part = [
                'claim_no' => $claim_id,
                'part_id' => $request->input('part_id')[$i]
            ];

            $items_part[] = $item_part;

            $save_part = CarDamage::firstOrCreate($item_part);
        }

        //Save Persons Injured
        $person = $request->input('person_name');
        $count_persons = count($person);

        $items_person = array();
        for ($i=0; $i < $count_persons; $i++) {
            $item_person = [
                'name' => $person_name[$i],
                'email' => $person_email[$i],
                'id_no' => $person_id_no[$i],
                'passport_no' => $person_passport_no[$i],
                'postal_address' => $person_postal[$i],
                'physical_address' => $person_address[$i],
                'phone' => $person_phone[$i],
                'claim_id' => $claim_id
            ];

            $items_person[] = $item_person;

            $save_person = PersonInjured::firstOrCreate($item_person);
        }

        //Save Property damage
        $property = $request->input('property_name');
        $count_property = count($property);

        $items_property = array();
        for ($i=0; $i < $count_property; $i++) {
            $item_property = [
                'property_name' => $request->input('property_name')[$i],
                'property_description' => $request->input('property_description')[$i],
                'claim_no' => $claim_id
            ];

            $items_property[] = $item_property;

            $save_property = PropertyDamage::firstOrCreate($item_property);
        }

        //Save Drivers Involved
        $driver = $request->input('driver_name');
        $count_driver = count($driver);

        $items_driver = array();
        for ($i=0; $i < $count_driver; $i++) {
            $item_driver = [
                'driver_name' => $request->input('driver_name')[$i],
                'driver_phone' => $request->input('driver_phone')[$i],
                'driver_license_no' => $request->input('driver_license_no')[$i],
                'veh_reg_no' => $request->input('driver_veh_reg_no')[$i],
                'veh_policy_no' => $request->input('driver_veh_policy_no')[$i],
                'claim_no' => $claim_id
            ];

            $items_driver[] = $item_driver;

            $save_driver = GeneralClaimDetail::firstOrCreate($item_driver);
        }


        if ($save_witness && $save_person && $save_property && $save_driver) {
            return response()->json('Successfully saved your claim');
        } else {
            return response()->json('Something went wrong, could not save the claim');
        }

    }

    function getPendingClaims() {
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $pending = Claim::join('risks', 'claims.policy_no', '=', 'risks.policy_no')
            ->select('risks.*', 'claims.*')
            ->with('witnesses')
            ->with('properties')
            ->with('drivers')
            ->with('parts')
            ->with('persons')
            ->with('photos')
            ->where([
                ['claims.status', 'AWAITING_APPROVAL'],
                ['claims.user_id', $user_id]
              ])
            ->get();

        if(!$pending) {
            return response()->json('You do not have any claims pending approval');
        } else {
            return response()->json($pending);
        }
    }

    function getUserClaims() {
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $claims = Claim::join('risks', 'claims.policy_no', '=', 'risks.policy_no')
            ->select('risks.*', 'claims.*')
            ->with('witnesses')
            ->with('properties')
            ->with('drivers')
            ->with('parts')
            ->with('persons')
            ->with('photos')
            ->where([
                ['claims.user_id', $user_id]
            ])
            ->get();

        if(!$claims) {
            return response()->json('You do not have any claims');
        } else {
            return response()->json($claims);
        }

    }

    function getClaimDetails($id){
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        // $policy = Policy::where('user_id', $user_id)->first()->policy_id;

        $claim = Claim::join('risks', 'claims.policy_no', '=', 'risks.policy_no')
            ->select('risks.*', 'claims.*')
            ->with('witnesses')
            ->with('properties')
            ->with('drivers')
            ->with('parts')
            ->with('persons')
            ->with('photos')
            ->where([
              ['claims.user_id', $user_id],
              ['claims.id', $id]
            ])
          ->get();

        if(!$claim) {
            return response()->json('Nothing found');
        } else {
            return response()->json($claim);
        }
    }


    //function to check for claim status
    function claim_status()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user['id'];

        $my_claims = Claim::where('user_id',$user_id)->select('status')->get();
        return response()->json($my_claims);

    }

    /**
     *Get all instructions
     */
    function instruction_checklist(Request $request)
    {
        $nature_loss = $request->nature_id;

        $checklist = Instruction_checklist::where('nature_id',$nature_loss)->get();
        return response()->json($checklist);

    }

    /**
     *Add an instruction
     */
    function addInstruction(Request $request)
    {

    }
}
