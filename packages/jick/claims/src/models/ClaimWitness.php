<?php
namespace Jick\claims\models;

use Illuminate\Database\Eloquent\Model;

class ClaimWitness extends Model
{
        protected $fillable = [
            'name',
            'email',
            'phone',
            'id_no',
            'passport_no',
            'phone',
            'physical_address',
            'postal_address',
            'claim_id'
        ];

        function statements() {
            return $this->hasOne('Jick\claims\models\ClaimWitnessStatement');
        }

}