<?php
    namespace Jick\claims\models;

    use Illuminate\Database\Eloquent\Model;

    class PropertyDamage extends Model
    {
        protected $table = 'claim_property_damaged';

        protected $fillable = ['name', 'description', 'claim_id'];
    }