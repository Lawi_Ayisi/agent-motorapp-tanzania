<?php
namespace Jick\claims\models;

use Illuminate\Database\Eloquent\Model;

class Instruction_checklist extends Model
{
        protected $table = 'INSTRUCTION_CHECKLIST';
        
        protected $fillable = [
        'instruction',
        'nature_id'
    ];

}