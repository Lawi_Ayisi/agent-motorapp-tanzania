<?php

    namespace Jick\claims\models;

    use Illuminate\Database\Eloquent\Model;

    class PersonInjured extends Model
    {
        protected $table = 'claim_persons_injured';

        protected $fillable = ['name', 'email', 'id_no', 'passport_no', 'postal_address', 'physical_address', 'phone', 'claim_no'];
    }