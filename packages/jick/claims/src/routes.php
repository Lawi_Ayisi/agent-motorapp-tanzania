<?php

//Protected routes
Route::group(['middleware' => 'jwt.auth', 'prefix' => 'api/'], function() {
    // Route::resource('claims', 'jick\claims\controllers\ClaimsController');
    Route::post('claim_status', 'jick\claims\controllers\ClaimsController@claim_status');

    Route::post('add-witness', 'jick\claims\controllers\ClaimsController@saveWitness');
    Route::post('register-claim', 'jick\claims\controllers\ClaimsController@saveClaim');
    Route::get('pending/claims', 'jick\claims\controllers\ClaimsController@getPendingClaims');
    Route::get('claims', 'jick\claims\controllers\ClaimsController@getUserClaims');
    Route::get('claim/details/{id}', 'jick\claims\controllers\ClaimsController@getClaimDetails');

    Route::post('make/claim/initiate', 'jick\claims\controllers\ClaimsController@initiateClaim');
    Route::post('make/claim/driver', 'jick\claims\controllers\ClaimsController@claimDriver');
    Route::post('make/claim/persons', 'jick\claims\controllers\ClaimsController@claimPersons');
    Route::post('make/claim/property', 'jick\claims\controllers\ClaimsController@claimProperty');
    Route::post('make/claim/witnesses', 'jick\claims\controllers\ClaimsController@claimWitnesses');
    Route::post('make/claim/damage', 'jick\claims\controllers\ClaimsController@claimDamage');
    Route::post('make/claim/police', 'jick\claims\controllers\ClaimsController@claimPolice');
});
