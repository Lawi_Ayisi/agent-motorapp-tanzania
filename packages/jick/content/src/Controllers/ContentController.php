<?php
    namespace jick\content\Controllers;

    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Input;
    use Illuminate\Support\Facades\Mail;
    use Illuminate\Support\Facades\Validator;
    use jick\Content\Models\Alert;
    use jick\Content\Models\FAQ;
    use jick\Content\Models\IDictionary;
    use jick\Content\Models\Location;
    use jick\Content\Models\SOS;
    use jick\Content\Models\CarMake;
    use jick\Content\Models\CarModel;
    use jick\Content\Models\CarPart;
    use jick\Content\Models\CarPartType;
    use jick\Content\Models\Offer;
    use jick\Content\Models\ValCenter;
    use jick\Content\Models\Garage;
    use jick\Content\Models\Branch;
    use App\User;
    use Tymon\JWTAuth\Facades\JWTAuth;

    class ContentController extends Controller
    {
        //SOS
        function showSOS() {
            $sos = SOS::all();

            return response()->json($sos);
        }

        function manageSOS(Request $request) {
            $name = $request->input('name');
            $new_name = $request->input('new_name');
            $type = $request->input('type_id');
            $location = $request->input('location');
            $latitude = $request->input('latitude');
            $longitude = $request->input('longitude');
            $phone = $request->input('phone');
            $alt = $request->input('alt_phone');

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'type_id' => 'required',
                'location' => 'required',
                'longitude' => 'required',
                'latitude' => 'required',
                'phone' => 'required'
            ]);

            $query = SOS::where([
                ['NAME', $name]
            ])->first();

            if ($validator->fails()){
                return response()->json($validator->errors()->all());
            } elseif ($query != null) {
                SOS::where('NAME', $name)->update([
                    'NAME' => $new_name,
                    'TYPE' => $type,
                    'LOCATION' => $location,
                    'LONGITUDE' => $longitude,
                    'LATITUDE' => $latitude,
                    'PHONE' => $phone,
                    'ALT_PHONE' => $alt
                ]);
            } else {
                $new = new SOS();
                $new->NAME = $new_name;
                $new->TYPE = $type;
                $new->LOCATION = $location;
                $new->LONGITUDE = $longitude;
                $new->LATITUDE = $latitude;
                $new->PHONE = $phone;
                $new->ALT_PHONE = $alt;

                $save = $new->save();

                if ($save) {
                    return response()->json('Your request was successfully completed');
                } else {
                    return response()->json('Something went wrong, your request could not be completed');
                }
            }
        }

        function searchSOS(Request $request) {
            $type = $request->input('type_id');

            $validator = Validator::make($request->all(), [
                'type_id' => 'required'
            ]);

            if ($validator->fails()){
                return response()->json($validator->errors()->all());
            } else {
                $sos = SOS::where('TYPE', 'LIKE', '%' . $type . '%')->get();

                return response()->json($sos);
            }
        }

        function deleteSOS(Request $request) {
            $sid = $request->input('sos_id');

            $validator = Validator::make($request->all(), [
                'sos_id' => 'required'
            ]);

            if ($validator->fails()){
                return response()->json('No record has been selected');
            } else {
                $delete = SOS::where('ID', $sid)->delete();

                if ($delete) {
                    return response()->json('You have successfully deleted SOS #' .$sid);
                } else {
                    return response()->json('Could not deleted SOS #' .$sid);
                }
            }
        }

        //Offers
        function showOffers() {
            $offers = Offer::all();

            return response()->json($offers);
        }

        function manageOffer(Request $request) {
            $title = $request->input('title');
            $body = $request->input('body');
            $product = $request->input('product_id');
            $status = $request->input('status');
            $expires = $request->input('expires_at');

            $count = count($title);

            // $query = Offer::where(
            //     ['TITLE' => $title],
            //     ['PRODUCT_ID', $product]
            // )->first();

            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'body' => 'required',
                'expires_at' => 'required'
            ]);

            if ($validator->fails()){
                return response()->json($validator->errors()->all());
            // } elseif ($query != null) {
            //     Offer::where([
            //         ['TITLE', $title],
            //         ['PRODUCT_ID', $product]
            //     ])->update([
            //         'TITLE' => $title,
            //         'BODY' => $body,
            //         'PRODUCT_ID' => $product,
            //         'EXPIRES_AT' => $expires
            //     ]);
            //
            //     return response()->json('You have edited a offer: ' . $title);

            } else {
                $items = array();
                for ($i = 0; $i < $count; $i++) {
                    $item = [
                        'TITLE' => $title[$i],
                        'BODY' => $body[$i],
                        'PRODUCT_ID' => $product[$i],
                        'STATUS' => $status[$i],
                        'EXPIRES_AT' => $expires[$i],
                        'CREATED_AT' => date('Y-m-d H:i:s'),
                        'UPDATED_AT' => date('Y-m-d H:i:s')
                    ];

                    $items[] = $item;
                }

                $save = Offer::insert($items);

                if ($save) {
                    return response()->json('You have successfully created ' .$count. ' offer(s)');
                } else {
                    return response()->json('Something went wrong, we could not complete your request');
                }
            }
        }

        function deleteOffer(Request $request) {
            $oid = $request->input('offer_id');
            $validator = Validator::make($request->all(), [
                'offer_id' => 'required'
            ]);

            if ($validator->fails()){
                return response()->json($validator->errors()->all());
            } else {
                $delete = Offer::destroy($oid);

                if ($delete) {
                    return response()->json('You have successfully deleted Offer #' .$oid);
                } else {
                    return response()->json('Could not deleted Offer #' .$oid);
                }
            }
        }

        //Insurance dictionary
        function showDictionary() {
            $terms = IDictionary::all();

            return response()->json($terms);
        }

        function manageDictionary(Request $request) {
            $term = $request->input('term');
            $new_term = $request->input('new_term');
            $definition = $request->input('definition');
            $product = $request->input('product_id');

            $count = count($term);

            $validator = Validator::make($request->all(), [
                'term' => 'required',
                'definition' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors()->all());
            }

            $query = IDictionary::where('TERM', $term)->first();

            if ($query != null) {
                IDictionary::where('TERM', $term)->update([
                    'TERM' => $new_term,
                    'DEFINITION' => $definition,
                    'PRODUCT_ID' => $product
                ]);
            } else {
                $items = array();
                for ($i = 0; $i < $count; $i++) {
                    $item = [
                        'TERM' => $term[$i],
                        'DEFINITION' => $definition[$i],
                        'PRODUCT_ID' => $product[$i],
                        'CREATED_AT' => date('Y-m-d H:i:s'),
                        'UPDATED_AT' => date('Y-m-d H:i:s')
                    ];

                    $items[] = $item;
                }

                $save = IDictionary::insert($items);

                if ($save) {
                    return response()->json('You have successfully added ' .$count. ' items.');
                } else {
                    return response()->json('Something went wrong, your request could not be processed.');
                }
            }
        }

        function searchDictionary(Request $request) {
            $term = $request->input('term');

            $result = IDictionary::where('TERM', 'LIKE', '%' .$term. '%')->get();

            return response()->json($result);
        }

        function deleteTerm(Request $request) {
            $tid = $request->input('term_id');
            $validator = Validator::make($request->all(), [
                'term_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors()->all());
            } else {
                $delete = IDictionary::destroy($tid);

                if ($delete) {
                    return response()->json('You have successfully deleted Term #' .$tid);
                } else {
                    return response()->json('Could not deleted Term #' .$tid);
                }
            }
        }

        //FAQs
        function showFAQs() {
            $faqs = FAQ::all();

            return response()->json($faqs);
        }

        function manageFAQs(Request $request) {
            $question = $request->input('question');
            $answer = $request->input('answer');
            $product = $request->input('product_id');

            $count = count($question);

            $validator = Validator::make($request->all(), [
                'question' => 'required',
                'answer' => 'required'
            ]);

            if ($validator->fails()){
                return response()->json($validator->errors()->all());
            } else {
                $items = array();
                for ($i = 0; $i < $count; $i++) {
                    $item = [
                        'QUESTION' => $question[$i],
                        'ANSWER' => $answer[$i],
                        'PRODUCT_ID' => $product[$i]
                    ];
                }

                $items[] = $item;
            }

            $insert = FAQ::insert($items);

            if ($insert) {
                return response()->json('You have successfully added ' .$count. ' items.');
            } else {
                return response()->json('Your request could not be completed');
            }
        }

        function searchFAQ(Request $request) {
            $product = $request->input('product_id');

            $result = FAQ::where('PRODUCT_ID', 'LIKE', '%' .$product. '%')->get();

            return response()->json($result);
        }

        function deleteFAQ(Request $request) {
            $fid = $request->input('faq_id');
            $validator = Validator::make($request->all(), [
                'faq_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors()->all());
            } else {
                $delete = FAQ::destroy($fid);

                if ($delete) {
                    return response()->json('Successfully deleted #' .$fid);
                } else {
                    return response()->json('Your request could not be completed.');
                }
            }
        }

        //Alerts
        function pushAlert(Request $request) {
            $title = $request->input('title');
            $body = $request->input('body');
            $type = $request->input('type');
            $module = $request->input('module');

            $validator = Validator::make($request->all(), [
                'body' => 'required',
                'title' => 'required',
                'type' => 'required',
                'module' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors()->all());
            } else {
                $new = new Alert();
                $new->title = $title;
                $new->body = $body;
                $new->type = $type;
                $new->module = $module;

                $save = $new->save();

                if ($save) {
                    return response()->json('Successfully posted an alert');
                } else {
                    return response()->json('Your request could not be completed.');
                }
            }
        }

        function showAlerts($module) {
            $alerts = Alert::where('module', $module)->get();

            return response()->json($alerts);
        }

        //Valuation centers
        function getValuationCentres() {
            $centers = ValCenter::select('id', 'name', 'longitude', 'latitude', 'phone', 'alt_phone')->get();
            return response()->json($centers);
        }

        function manageValuationCenters(Request $request) {
            $name = $request->input('name');
            $new_name = $request->input('new_name');
            $location = $request->input('location');
            $longitude = $request->input('longitude');
            $latitude = $request->input('latitude');
            $county = $request->input('county');
            $phone = $request->input('phone');
            $alt = $request->input('alt_phone');

            $count = count($name);

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'location' => 'required',
                'phone' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors()->all());
            }

            $query = ValCenter::where('NAME', $name)->first();

            if ($query != null) {
                $update = ValCenter::where('NAME', $name)->update([
                    'NAME' => $new_name,
                    'LOCATION' => $location,
                    'LONGITUDE' => $longitude,
                    'LATITUDE' => $latitude,
                    'COUNTY' => $county,
                    'PHONE' => $phone,
                    'ALT_PHONE' => $alt
                ]);

                if ($update) {
                    return response()->json('Successfully updated');
                } else {
                    return response()->json('Could not complete your request');
                }
            } else {
                $new = new ValCenter();
                $new->NAME = $name;
                $new->LOCATION = $location;
                $new->LONGITUDE = $longitude;
                $new->LATITUDE = $latitude;
                $new->PHONE = $phone;
                $new->COUNTY = $county;
                $new->ALT_PHONE = $alt;

                $insert = $new->save();
                if ($insert) {
                    return response()->json('You have successfully created ' .$name);

                } else {
                    return response()->json('Could not complete your request.');

                }
            }

        }

        function searchValuationCenter(Request $request) {
            $county = $request->input('county');

            $result = ValCenter::where('COUNTY', 'LIKE', '%' .$county. '%')->get();

            return response()->json($result);
        }

        function getValuationCenter(Request $request) {
            $vid = $request->input('valc_id');

            $result = ValCenter::where('ID', $vid)->get();

            return response()->json($result);
        }

        function deleteValuationCenter(Request $request) {
            $vid = $request->input('vc_id');

            $validator = Validator::make($request->all(), [
                'vc_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors()->all());
            } else {
                $delete = ValCenter::destroy($vid);

                if ($delete) {
                    return response()->json('Successfully deleted');
                } else {
                    return response()->json('Something went wrong, your request could not be completed');
                }
            }
        }


        //Garages
        function showGarage() {
            $garages = Garage::all();

            return response()->json($garages);
        }

        function manageGarage(Request $request) {
            $name = $request->input('name');
            $new_name = $request->input('new_name');
            $location = $request->input('location');
            $longitude = $request->input('longitude');
            $latitude = $request->input('latitude');
            // $county = $request->input('county');
            $phone = $request->input('phone');
            $alt = $request->input('alt_phone');

            $count = count($name);

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'location' => 'required',
                'phone' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors()->all());
            }

            $query = Garage::where('name', $name)->first();

            if ($query != null) {
                $update = Garage::where('name', $name)->update([
                    'name' => $new_name,
                    'location' => $location,
                    'longitude' => $longitude,
                    'latitude' => $latitude,
                    'phone' => $phone,
                    'alt_phone' => $alt
                ]);

                if ($update) {
                    return response()->json('Successfully updated');
                } else {
                    return response()->json('Could not complete your request');
                }
            } else {
                $new = new Garage();
                $new->name = $name;
                $new->location = $location;
                $new->longitude = $longitude;
                $new->latitude = $latitude;
                $new->phone = $phone;
                $new->alt_phone = $alt;

                $insert = $new->save();
                if ($insert) {
                    return response()->json('You have successfully created ' .$name);

                } else {
                    return response()->json('Could not complete your request.');

                }
            }
        }

        function deleteGarage(Request $request) {
            $gid = $request->input('g_id');

            $validator = Validator::make($request->all(), [
                'g_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors()->all());
            } else {
                $delete = Garage::destroy($gid);

                if ($delete) {
                    return response()->json('Successfully deleted');
                } else {
                    return response()->json('Something went wrong, your request could not be completed');
                }
            }
        }

        //Branches
        function showBranch() {
            $branches = Branch::all();

            return response()->json($branches);
        }

        function manageBranch(Request $request) {
            $name = $request->input('name');
            $new_name = $request->input('new_name');
            $location = $request->input('location');
            $longitude = $request->input('longitude');
            $latitude = $request->input('latitude');
            // $county = $request->input('county');
            $phone = $request->input('phone');
            $alt = $request->input('alt_phone');

            $count = count($name);

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'location' => 'required',
                'phone' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors()->all());
            }

            $query = Branch::where('name', $name)->first();

            if ($query != null) {
                $update = Branch::where('name', $name)->update([
                    'name' => $new_name,
                    'location' => $location,
                    'longitude' => $longitude,
                    'latitude' => $latitude,
                    'phone' => $phone,
                    'alt_phone' => $alt
                ]);

                if ($update) {
                    return response()->json('Successfully updated');
                } else {
                    return response()->json('Could not complete your request');
                }
            } else {
                $new = new Branch();
                $new->name = $name;
                $new->location = $location;
                $new->longitude = $longitude;
                $new->latitude = $latitude;
                $new->phone = $phone;
                $new->alt_phone = $alt;

                $insert = $new->save();
                if ($insert) {
                    return response()->json('You have successfully created ' .$name);

                } else {
                    return response()->json('Could not complete your request.');

                }
            }
        }

        function deleteBranch(Request $request) {
            $gid = $request->input('b_id');

            $validator = Validator::make($request->all(), [
                'b_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors()->all());
            } else {
                $delete = Branch::destroy($gid);

                if ($delete) {
                    return response()->json('Successfully deleted');
                } else {
                    return response()->json('Something went wrong, your request could not be completed');
                }
            }
        }

        //Service Providers
        function getServiceProviders() {
            $providers = Location::select('id', 'name')->with(['providers' => function($q) {
                $q->where('latitude', '!=', "");
            }])
            //->whereNotNull('providers.longitude')
            ->get();

            return response()->json($providers);
        }

        function manageServiceProviders(Request $request) {

        }

        //Car make
        function showCarMake() {
//            $makes = CarMake::select('id', 'name')->with('cmodels')->get();
            $makes = CarMake::select('id', 'name')->orderBy('name','asc')->get();
            return response()->json($makes);
        }

        function getCModels($id) {
            $models = CarModel::where('make_id', $id)->select('id', 'name')->get();
            return response()->json($models);
        }

        function manageCarMake(Request $request) {
            $name = $request->input('name');
            $new_name = $request->input('new_name');

            $count = count($name);

            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ]);

            // $query = CarMake::where('NAME', $name)->first();

            if ($validator->fails()){
                return response()->json($validator->errors()->all());

            // } elseif ($query != null) {
            //     CarMake::where('NAME', $name)->update([
            //         'NAME' => $new_name
            //     ]);
            //
            //     return response()->json('You have changed: ' . $name . ' to: ' .$new_name);

            } else {


                $items = array();

                if(is_array($name)) {
                    for ($i = 0; $i < $count; $i++) {
                        $item = [
                            'NAME' => $name[$i]
                        ];

                        $items[] = $item;
                    }
                }else{
                     $items =   [
                            'NAME' => $name
                        ];
                }

                $insert = CarMake::insert($items);
                if ($insert) {
                    return response()->json('You have successfully added ' . $count . ' Car makes');

                } else {
                    return response()->json('Could not complete your request');

                }
            }
        }

        function editCarMake($id, Request $request) {
            $model = CarMake::find($id);
            return response()->json($model);

            $v = Validator::make($request->all(), [
                'name' => 'required'
            ]);

            if($v->fails()) {
                return response()->json($validator->errors()->all());
            }

            $model->name = $request->name;

            if($model->save()) {
                return response()->json('Make editted successfully');
            }
        }

        function deleteCarMake(Request $request) {
            $id = $request->input('car_make_id');
            $validator = Validator::make($request->all(), [
                'car_make_id' => 'required'
            ]);

            $query = CarModel::where('CARMAKE_ID', $id)->first();

            if ($validator->fails()){
                return response()->json($validator->errors()->all());

            } elseif ($query != null) {

                return response()->json('You cannot delete a parent record');
            } else {
                CarMake::destroy($id);

                return response()->json('You have successfully deleted record #' .$id);
            }

        }

        //Car model
        function showCarModel($value='') {
            $models = CarModel::all();

            return response()->json($models);
        }

        function manageCarModel(Request $request) {
            $name = $request->input('name');
            $make = $request->input('make_id');
            $new_name = $request->input('new_name');

            $count = count($name);

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'make_id' => 'required'
            ]);

            // $query = CarModel::where('NAME', $name)->first();

            if ($validator->fails()){
                return response()->json($validator->errors()->all());
            // } elseif ($query != null) {
            //     CarModel::where('NAME', $name)->update([
            //         'NAME' => $new_name,
            //         'MAKE_ID' => $make
            //     ]);
            //
            //     return response()->json('You have changed: ' . $name . ' to: ' . $new_name . ' and make to #' .$make);

            } else {
                $items = array();
                for ($i = 0; $i < $count; $i++) {
                    $item = [
                        'NAME' => $name[$i],
                        'MAKE_ID' => $make[$i]
                    ];

                    $items[] = $item;
                }

                $insert = CarModel::insert($items);

                if ($insert) {
                    return response()->json('You have successfully added ' .$count. ' car models');
                } else {
                    return response()->json('Could not complete your request');
                }

            }
        }

        function editCarModel($id, Request $request) {
            $model = CarModel::find($id);
            return response()->json($model);

            $v = Validator::make($request->all(), [
                'name' => 'required'
            ]);

            if($v->fails()) {
                return response()->json($validator->errors()->all());
            }

            $model->name = $request->name;

            if($model->save()) {
                return response()->json('Model editted successfully');
            }
        }

        function deleteCarModel(Request $request) {
            $id = $request->input('model_id');
            $validator = Validator::make($request->all(), [
                'model_id' => 'required'
            ]);

            if ($validator->fails()){
                return response()->json($validator->errors()->all());
            } else {
                $delete = CarModel::destroy($id);
                if ($delete) {
                    return response()->json('You have successfully deleted record #' .$id);
                } else {
                    return response()->json('Could not delete record ' .$id);
                }
            }
        }

        //Car parts
        function showCarParts() {
            // $parts = CarPart::with('parttypes');
            $parts = CarPart::all();

            return response()->json($parts);
        }

        function manageCarParts(Request $request) {
            $name = $request->input('name');
            $new_name = $request->input('new_name');

            $count = count($name);

            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ]);

            // $query = CarPart::where('NAME', $name)->first();

            if ($validator->fails()){
                return response()->json($validator->errors()->all());

            // } elseif ($query != null) {
            //     CarPart::where('NAME', $name)->update(['NAME' => $new_name, 'UPDATED_AT' => date('Y-m-d H:i:s')]);

            } else {
                $items = array();
                for ($i = 0; $i < $count; $i++) {
                    $item = [
                        'NAME' => $name[$i]
                    ];

                    $items[] = $item;
                }

                $insert = CarPart::insert($items);

                if ($insert) {
                    return response()->json('You have created ' . $count . ' parts.');

                } else {
                    return response()->json('Your request could not be completed');

                }

            }
        }

        function deleteCarPart(Request $request) {
            $id = $request->input('car_part_id');

            $validator = Validator::make($request->all(), [
                'car_part_id' => 'required'
            ]);

            $query = CarPartType::where('PART_ID', $id)->first();

            if ($validator->fails()) {
                return response()->json($validator->errors()->all());

            } elseif ($query != null) {
                return response()->json('You cannot delete a parent part');

            } else {
                $delete = CarPart::destroy($id);
                if ($delete) {
                    return response()->json('You have successfully deleted a part #' .$id);
                } else {
                    return response()->json('Your request could not be completed.');
                }
            }
        }

        //Car Part Types
        function showPartTypes($value='')  {
            $types = CarPartType::all();

            return response()->json($types);
        }

        function manageCarPartTypes(Request $request) {
            $name = $request->input('name');
            $part = $request->input('part_id');
            $new_name = $request->input('new_name');

            $count = count($name);

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'part_id' => 'required'
            ]);

            // $query = CarPartType::where('NAME', $name)->first();

            if ($validator->fails()){
                return response()->json($validator->errors()->all());

            // } elseif ($query != null) {
            //     CarPartType::where('NAME', $name)->update(['NAME' => $new_name, 'PART_ID' => $part, 'UPDATED_AT' => date('Y-m-d H:i:s')]);

            } else {
                $items = array();
                for ($i = 0; $i < $count; $i++) {
                    $item = [
                        'NAME' => $name[$i],
                        'PART_ID' => $part[$i]
                    ];

                    $items[] = $item;
                }

                $insert = CarPartType::insert($items);

                if ($insert) {
                    return response()->json('You have added ' . $count . ' part type(s)');

                } else {

                    return response()->json('Could not complete your request');
                }
            }
        }

        function deleteCarPartType(Request $request) {
            $id = $request->input('part_type_id');
            $validator = Validator::make($request->all(), [
                'part_type_id' => 'required'
            ]);

            if ($validator->fails()){
                return response()->json($validator->errors()->all());

            } else {
                $delete = CarPartType::destroy($id);
                if ($delete) {
                    return response()->json('You have successfully deleted a part #' .$id);

                } else {
                    return response()->json('Could not delete record #' .$id);

                }
            }
        }

        //Upload Documents
        function uploadDocuments(Request $request) {
            $file = $request->input('file');

            $validator = Validator::make($request->all(), [
                'file' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors()->all());
            }
            else {
                if ($file->isValid()) {
                    $destinationPath = 'claims';
                    $extension = Input::file('file')->getClientOriginalExtension();
                    $fileName = rand(11111,99999).'.'.$extension;
                    Input::file('file')->move($destinationPath, $fileName);
                    return response()->json('Uploaded successfully');
                }
                else {
                    return response()->json('Uploaded file is not valid');
                }
            }
        }

        //Get User Policies
        function getPolicies(Request $request) {
            $user = JWTAuth::parseToken()->authenticate();
            $nid = User::where('ID', $user)->first()->id_pp_no;

            $conn = DB::connect('oracle2');
            $policies = $conn->table('policies')->where('NID', $nid)->get();

            return response()->json($policies);
        }

        function sendContact(Request $request) {
            $user = JWTAuth::parseToken()->authenticate();
            $uid = $user['id'];

            $f = User::where('id', $uid)->first()->firstname;
            $l = User::where('id', $uid)->first()->lastname;
            $e = User::where('id', $uid)->first()->email;

            $validator = Validator::make($request->all(), [
                'subject' => 'required',
                'body' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors()->all());
            }

            $data = [
                'name' => $f .' '. $l,
                'body' => $request->body
            ];

            $body = [
                'to' => 'dennis.munene@outlook.com',
                'subject' => $request->subject,
                'from' => $e,
                'name' => $f .' '. $l
            ];

            $send = Mail::send('emails.contact', $data, function ($m) use ($body) {
                $m->from($body['from'], $body['name']);
                $m->to($body['to'])->subject($body['subject']);
            });

            if ($send) {
                return response()->json(['message' => 'Your email was sent, we will get back to you shortly'], 200);
            } else {
                return response()->json(['message' => 'Something went wrong, your email was not sent'], 422);
            }
        }

    }
