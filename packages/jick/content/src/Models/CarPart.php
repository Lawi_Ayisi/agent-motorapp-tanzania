<?php
    namespace jick\Content\Models;

    use Illuminate\Database\Eloquent\Model;

    class CarPart extends Model
    {
        protected $table = 'PARTS';

        protected $fillable = ['NAME'];
    }