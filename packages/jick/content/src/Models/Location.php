<?php
namespace jick\Content\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'location';

    protected $fillable = ['name'];

    public function providers()
    {
        $location = $this->hasMany(ServiceProvider::class, 'location_id', 'id');

        return $location;
    }
}
