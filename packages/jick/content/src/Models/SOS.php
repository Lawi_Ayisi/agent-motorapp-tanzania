<?php
    namespace jick\Content\Models;

    use Illuminate\Database\Eloquent\Model;

    class SOS extends Model
    {
        protected $table = 'SOS';

        protected $fillable = ['NAME', 'TYPE', 'LOCATION', 'PHONE', 'ALT_PHONE'];
    }