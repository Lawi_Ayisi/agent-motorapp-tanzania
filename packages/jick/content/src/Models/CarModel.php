<?php
    namespace jick\Content\Models;

    use Illuminate\Database\Eloquent\Model;

    class CarModel extends Model
    {
        protected $table = 'models';

        protected $fillable = ['name', 'make_id'];
    }
