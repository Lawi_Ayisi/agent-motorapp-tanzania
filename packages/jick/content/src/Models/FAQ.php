<?php
    namespace jick\Content\Models;

    use Illuminate\Database\Eloquent\Model;

    class FAQ extends Model
    {
        protected $table = 'FAQS';

        protected $fillable = ['QUESTION', 'ANSWER', 'PRODUCT_ID'];
    }