<?php
    namespace jick\Content\Models;

    use Illuminate\Database\Eloquent\Model;

    class IDictionary extends Model
    {
        protected $table = 'IDICTIONARY';

        protected $fillable = ['term', 'definition', 'synonym'];
    }