<?php
    namespace jick\Content\Models;

    use Illuminate\Database\Eloquent\Model;

    class CarMake extends Model
    {
        protected $table = 'makes';

        protected $fillable = ['name'];

        public function cmodels()
        {
            $cmodel = $this->hasMany(CarModel::class, 'make_id', 'id');

            return $cmodel;
        }
    }
