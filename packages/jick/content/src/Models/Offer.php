<?php
    namespace jick\Content\Models;

    use Illuminate\Database\Eloquent\Model;

    class Offer extends Model
    {
        protected $table = 'OFFERS';

        protected $fillable = ['TITLE', 'BODY', 'PRODUCT_ID', 'EXPIRES_AT'];
    }