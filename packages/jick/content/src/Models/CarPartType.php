<?php
    namespace jick\Content\Models;

    use Illuminate\Database\Eloquent\Model;

    class CarPartType extends Model
    {
        protected $table = 'PARTTYPES';

        protected $fillable = ['NAME', 'PART_ID'];
    }
