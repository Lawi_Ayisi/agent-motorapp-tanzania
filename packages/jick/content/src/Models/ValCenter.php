<?php
    namespace jick\Content\Models;

    use Illuminate\Database\Eloquent\Model;

    class ValCenter extends Model
    {
        protected $table = 'valcentres';

        protected $fillable = ['name', 'longitude', 'latitude', 'location', 'phone', 'alt_phone'];
    }