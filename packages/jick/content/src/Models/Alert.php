<?php
    namespace jick\Content\Models;

    use Illuminate\Database\Eloquent\Model;

    class Alert extends Model
    {
        protected $table = 'alerts';

        protected $fillable = ['title', 'body', 'type', 'module'];
    }