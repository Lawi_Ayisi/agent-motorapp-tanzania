<?php
namespace jick\Content\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceProvider extends Model
{
    protected $table = 'service_providers';

    protected $fillable = ['name', 'address', 'operating_time', 'address', 'contacts', 'location_id'];
}
