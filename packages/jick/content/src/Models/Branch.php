<?php
    namespace jick\Content\Models;

    use Illuminate\Database\Eloquent\Model;

    class Branch extends Model
    {
        protected $table = 'branches';

        protected $fillable = ['name', 'longitude', 'latitude', 'location', 'phone', 'alt_phone'];
    }