<?php
    namespace jick\Content\Models;

    use Illuminate\Database\Eloquent\Model;

    class Garage extends Model
    {
        protected $table = 'garages';

        protected $fillable = ['name', 'longitude', 'latitude', 'location', 'phone', 'alt_phone'];
    }