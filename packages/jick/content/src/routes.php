<?php
    Route::group(['middleware' => 'jwt.auth', 'prefix' => 'api/'], function (){
        Route::post('contact-us', 'jick\content\Controllers\ContentController@sendContact');

        //SOS
        Route::post('manage/sos', 'jick\content\Controllers\ContentController@manageSOS');
        Route::get('sos', 'jick\content\Controllers\ContentController@showSOS');
        Route::post('delete/sos', 'jick\content\Controllers\ContentController@deleteSOS');
        Route::post('search/sos', 'jick\content\Controllers\ContentController@searchSOS');

        //Alerts
        Route::post('add/alert', 'jick\content\Controllers\ContentController@pushAlert');
        Route::get('/alerts/{module}', 'jick\content\Controllers\ContentController@showAlerts');

        //FAQs
        Route::post('manage/faqs', 'jick\content\Controllers\ContentController@manageFAQs');
        Route::get('faqs', 'jick\content\Controllers\ContentController@showFAQs');
        Route::post('delete/faq', 'jick\content\Controllers\ContentController@deleteFAQ');
        Route::post('search/faq', 'jick\content\Controllers\ContentController@searchFAQ');

        //Offers
        Route::post('manage/offers', 'jick\content\Controllers\ContentController@manageOffer');
        Route::get('offers', 'jick\content\Controllers\ContentController@showOffers');
        Route::post('delete/offer', 'jick\content\Controllers\ContentController@deleteOffer');

        //Insurance Dictionary
        Route::post('manage/dictionary', 'jick\content\Controllers\ContentController@manageDictionary');
        Route::get('dictionary', 'jick\content\Controllers\ContentController@showDictionary');
        Route::post('delete/term', 'jick\content\Controllers\ContentController@deleteTerm');
        Route::post('search/dictionary', 'jick\content\Controllers\ContentController@searchDictionary');

        //Branches
        Route::post('manage/branches', 'jick\content\Controllers\ContentController@manageBranch');
        Route::get('branches', 'jick\content\Controllers\ContentController@showBranch');
        Route::post('delete/branch', 'jick\content\Controllers\ContentController@deleteBranch');

        //Garages
        Route::post('manage/garages', 'jick\content\Controllers\ContentController@manageGarage');
        Route::get('garages', 'jick\content\Controllers\ContentController@showGarage');
        Route::post('delete/garage', 'jick\content\Controllers\ContentController@deleteGarage');

        //Car Make
        Route::post('manage/car-makes', 'jick\content\Controllers\ContentController@manageCarMake');
        Route::get('car-makes', 'jick\content\Controllers\ContentController@showCarMake');
        Route::post('delete/car-make', 'jick\content\Controllers\ContentController@deleteCarMake');
        Route::put('car-make/{id}', 'jick\content\Controllers\ContentController@editCarMake');

        //Car Model
        Route::get('car-models/{id}', 'jick\content\Controllers\ContentController@getCModels');
        Route::post('manage/car-models', 'jick\content\Controllers\ContentController@manageCarModel');
        Route::post('delete/car-model', 'jick\content\Controllers\ContentController@deleteCarModel');
        Route::put('car-model/{id}', 'jick\content\Controllers\ContentController@editCarModel');

        //Car Parts
        Route::post('manage/car-parts', 'jick\content\Controllers\ContentController@manageCarParts');
        Route::get('car-parts', 'jick\content\Controllers\ContentController@showCarParts');
        Route::post('delete/car-part', 'jick\content\Controllers\ContentController@deleteCarPart');

        //Car Part Types
        Route::get('part-types', 'jick\content\Controllers\ContentController@showPartTypes');
        Route::post('manage/car-part-types', 'jick\content\Controllers\ContentController@manageCarPartTypes');
        Route::post('delete/car-part-type', 'jick\content\Controllers\ContentController@deleteCarPartType');

        //Valuation centers
        Route::post('manage/valuation-centres', 'jick\content\Controllers\ContentController@manageValuationCenters');       
        Route::post('delete/valuation-centre', 'jick\content\Controllers\ContentController@deleteValuationCenter');
        Route::post('search/valuation-centres', 'jick\content\Controllers\ContentController@searchValuationCenter');
        Route::get('valuation-centres', 'jick\content\Controllers\ContentController@getValuationCentres');

        //Service Providers
        Route::get('service-providers', 'jick\content\Controllers\ContentController@getServiceProviders');
        Route::get('manage/service-providers', 'jick\content\Controllers\ContentController@manageServiceProviders');
    });
