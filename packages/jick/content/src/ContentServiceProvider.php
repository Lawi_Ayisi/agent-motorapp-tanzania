<?php
    namespace jick\content;

    use Illuminate\Support\ServiceProvider;

    class ContentServiceProvider extends ServiceProvider
    {
        public function boot() {
            
        }

        public function register() {
            //Load controllers
            $this->app->make('Jick\content\Controllers\ContentController');

            //Load models
            $this->app->make('Jick\content\Models\Alert');
            $this->app->make('Jick\content\Models\CarMake');
            $this->app->make('Jick\content\Models\CarModel');
            $this->app->make('Jick\content\Models\CarPart');
            $this->app->make('Jick\content\Models\CarPartType');
            $this->app->make('Jick\content\Models\FAQ');
            $this->app->make('Jick\content\Models\IDictionary');
            $this->app->make('Jick\content\Models\Offer');
            $this->app->make('Jick\content\Models\SOS');
            $this->app->make('Jick\content\Models\ValCenter');
            $this->app->make('Jick\content\Models\Garage');
            $this->app->make('Jick\content\Models\Branch');
            $this->app->make('Jick\content\Models\Location');
            $this->app->make('Jick\content\Models\ServiceProvider');

            include __DIR__.'/routes.php';
        }
    }