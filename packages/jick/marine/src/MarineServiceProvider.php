<?php

namespace jick\marine;

use Illuminate\Support\ServiceProvider;

class MarineServiceProvider extends ServiceProvider 
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Jick\marine\controllers\DeclarationController');

        include __DIR__.'/routes.php';
    }
}