<?php

namespace jick\col\facades;

use Illuminate\Support\Facades\Facade;

class SoapManager extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'soapmanager';
    }
}