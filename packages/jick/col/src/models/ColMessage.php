<?php

namespace Jick\col\models;

use Illuminate\Database\Eloquent\Model;

class ColMessage extends Model
{
    protected $table = 'col_messages';

    protected $fillable = ['user_id', 'title', 'body', 'read'];
}
