<?php

namespace Jick\col\models;

use Illuminate\Database\Eloquent\Model;

class ColEntity extends Model
{
    //protected $connection = 'medical_online';

    protected $table = 'col_entities';

    public $timestamps = false;

    protected $fillable = ['entityid', 'phone_number'];
}