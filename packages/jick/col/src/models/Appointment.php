<?php

namespace Jick\col\models;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $table = 'preauth_appointments';

    protected $fillable = ['user_id', 'provider_id', 'apt_date'];
}
