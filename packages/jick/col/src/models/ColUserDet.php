<?php

namespace Jick\col\models;

use Illuminate\Database\Eloquent\Model;

class ColUserDet extends Model
{
  protected $fillable = ['full_names',
                        'home_address', 
                        'email_address',
                        'gender',
                        'main_telephone',
                        'postal_address',
                        'bank_name',
                        'bank_branch',
                        'bank_ac_no'];

}