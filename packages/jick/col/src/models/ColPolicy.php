<?php

namespace Jick\col\models;

use Illuminate\Database\Eloquent\Model;

class ColPolicy extends Model
{
    protected $connection = 'medical_online';

    protected $table = 'MIDBPOLICYDETAIL';

    public $timestamps = false;
}