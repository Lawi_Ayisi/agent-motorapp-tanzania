<?php

namespace Jick\col\models;

use Illuminate\Database\Eloquent\Model;

class PreAuthorization extends Model
{
    protected $table = 'preauthorizations';

    protected $fillable = ['user_id', 'provider_id', 'doctor'];
}
