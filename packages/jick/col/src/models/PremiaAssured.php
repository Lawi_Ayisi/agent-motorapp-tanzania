<?php

namespace Jick\col\models;

use Illuminate\Database\Eloquent\Model;

class PremiaAssured extends Model
{
    protected $connection = 'premier_connect';
    protected $table = 'PM_ASSURED_APP';
    protected $sequence = 'ASSR_CODE';
    public $timestamps = false;
}
