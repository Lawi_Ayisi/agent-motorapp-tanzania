<?php

namespace Jick\col\models;

use Illuminate\Database\Eloquent\Model;

class ServiceProvider extends Model
{
    protected $table = 'service_providers';

    protected $fillable = ['name', 'address', 'operating_time', 'contacts', 'services', 'location_id', 'longitude', 'latitude', 'email'];
}
