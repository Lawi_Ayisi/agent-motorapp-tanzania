<?php

namespace jick\col\controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Mail;
use Jick\col\models\ColMessage;
use Jick\users\models\User;
use JWTAuth;
use Validator;

class ContentController extends Controller
{
    function createMessage(Request $request) {
        $validator = Validator::make($request->all(), [
            'body' => 'required',
            'user_id' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed, see errors below',
                'errors' => $validator->errors()->all()
            ], 422);
        }

        $message = new ColMessage();
        $message->title = $request->title;
        $message->body = $request->body;
        $message->user_id = $request->user_id;

        $save = $message->save();

        if ($save) {
            return response()->json([
                'message' => 'The message has been successfully sent to the client.'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Something went wrong, your request could not be completed.'
            ], 422);
        }
    }

    function getMessages() {
        $user = JWTAuth::parseToken()->authenticate();
        $uid = $user['id'];

        $messages = ColMessage::where([
            'user_id' => $uid,
            'read' => 0
        ])->get();

        return response()->json($messages);
    }

}