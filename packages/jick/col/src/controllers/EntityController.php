<?php

namespace jick\col\controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Jick\col\models\ColUserDet;

use DB;

use Validator;
use JWTAuth;

use jick\col\models\ColEntity;
use Jick\users\models\User;

class EntityController extends Controller
{
        public function index()
        {
            return ColUserDet::all();
        }

        //Save the personal details editted
        public function store(Request $request)
        {
            //Validate, the product must be supplied
            $validator = Validator::make($request->all(), [
                'full_names' => 'required',
                'home_address' => 'required',
                'email_address' => 'required',
                'gender' => 'required',
                'main_telephone' => 'required',
                'postal_address' => 'required',
                /*'bank_name' => 'required',
                'bank_branch' => 'required',
                'bank_ac_no' => 'required',
                'user_id' => 'required'*/
            ]);

            if($validator->fails()) {
                return response()->json([
                    'message' => 'Validation failed',
                    'errors' => $validator->errors()->all()
                ], 422);
            }

            //Get the user id from the token_get_all
            $user = JWTAuth::parseToken()->authenticate();
            $user_id = $user->id;


            $col_user = ColUserDet::firstOrNew(['user_id'=> $user_id]);

            $col_user->full_names = $request->full_names;
            $col_user->home_address = $request->home_address;
            $col_user->email_address = $request->email_address;
            $col_user->gender = $request->gender;
            $col_user->main_telephone = $request->main_telephone;
            $col_user->postal_address = $request->postal_address;
            $col_user->bank_name = $request->bank_name;
            $col_user->bank_branch = $request->bank_branch;
            $col_user->bank_ac_no = $request->bank_ac_no;

            if($col_user->save())
            {
                return $col_user;
            }


        }

        /**
        *Update entity phone from midb to intermediary table
        *This should be run by a cron job
        */
        public function updateEntities()
        {

            $entities = DB::connection('medical_online')->table('MIDBENTITYPHONE')
                    //->select('MIDBENTITYPHONE.entityid', 'countrydialcode', 'phonenumber')
                    ->join('MIDBPOLICYBENEFICIARY', 'MIDBPOLICYBENEFICIARY.ENTITYID=MIDBENTITYPHONE.ENTITYID')
                    ->where('LOWER(phonetypedesc)',  'mobile')
                    ->whereNotNull('countrydialcode')
                    ->whereNotNull('phonenumber')
                    //->where('RELATIONSHIP', 'Company Employee')
                    ->orWhere(function($q) {
                        $q->where('RELATIONSHIP', 'Policy Holder')
                          ->where('RELATIONSHIP', 'Company Employee');
                    })
                    ->get();

            if(!empty($entities))
            {
                foreach($entities as $key => $entity) {

                    $phone = $entity->countrydialcode[0] !== '+' ? '+'.$entity->countrydialcode : '';

                    if(strlen($entity->phonenumber) == 9 && $entity->phonenumber[0] != '0') {
                        //$phone .= substr($entity->phonenumber, 1);

                        $phone .= $entity->phonenumber;

                        $entity = [
                            'entityid' => $entity->entityid,
                            'phone_number' => $phone
                        ];

                        $colEntity = ColEntity::firstOrNew($entity);
                        $colEntity->save();

                        //$entities[$key] = $entity;
                    }else {
                         //unset($entities[$key]);
                    }

                }
            }
        }
}
