<?php

    namespace jick\col\controllers;

    use Illuminate\Http\Request;

    use App\Http\Requests;
    use App\Http\Controllers\Controller;

    use Illuminate\Support\Facades\Mail;
    use Jick\col\models\Appointment;
    use Jick\files\models\File;
    use Jick\col\models\PreAuthorization;
    use jick\Content\Models\ServiceProvider;
    use Jick\users\models\User;
    use JWTAuth;
    use Validator;

    class PreAuthController extends Controller
    {
        function makeAppointment(Request $request) {
            $validator = Validator::make($request->all(), [
                'provider_id' => 'required',
                'apt_date' => 'required'
            ]);

            if($validator->fails()) {
                return response()->json([
                    'message' => 'Validation failed, see errors below',
                    'errors' => $validator->errors()->all()
                ], 422);
            }

            $user = JWTAuth::parseToken()->authenticate();
            $uid = $user['id'];

            $appointment = new Appointment();
            $appointment->user_id = $uid;
            $appointment->provider_id = $request->provider_id;
            $appointment->apt_date = $request->apt_date;

            $fname = User::where('id', $uid)->first()->firstname;
            $lname = User::where('id', $uid)->first()->lastname;
            $phone = User::where('id', $uid)->first()->phone;

            $datetime = $request->apt_date;
            $time = date('h:i A', strtotime($datetime));
            $date = date("D, d M Y ");

            $body = [
                'email' => 'chief.munene@gmail.com', // $email = ServiceProvider::where('id', 'provider_id')->first()->email;
                'name' => ServiceProvider::where('id', $request->provider_id)->first()->name
            ];
            $data = [
                'name' => $fname .' '. $lname,
                'phone' => $phone,
                'provider' => ServiceProvider::where('id', $request->provider_id)->first()->name,
                'date' => $date,
                'time' => $time
            ];

            Mail::send('emails.appointment', $data, function ($m) use ($body) {
                $m->to($body['email'], $body['name'])->subject('Appointment Booking');
            });

            $makeApt = $appointment->save();

            if ($makeApt) {
                return response()->json(['message' => 'The appointment was successfully made.'], 200);
            } else {
                return response()->json(['message' => 'There was a problem completing your appointment request.'], 422);
            }
        }

        function getAppointments() {

        }

        function getAllAppointments() {

        }

        function makePreAuthorization(Request $request) {
            $validator = Validator::make($request->all(), [
                'prov_id' => 'required',
                'doctor' => 'required'
            ]);

            if($validator->fails()) {
                return response()->json([
                    'message' => 'Validation failed, see errors below',
                    'errors' => $validator->errors()->all()
                ], 422);
            }

            $user = JWTAuth::parseToken()->authenticate();
            $uid = $user['id'];

            $preAuth = new PreAuthorization();
            $preAuth->provider_id = $request->prov_id;
            $preAuth->doctor = $request->doctor;

            if($preAuth->save()) {

                //update files foreign key
                if($request->file) {
                    $file = File::first('id');

                    $file->foreign_id = $preAuth->id;

                    $file->save();
                }

                return response()->json(['message' => 'Pre authorization was successfully made.'], 200);
            } else {
                return response()->json(['message' => 'There was a problem completing your pre authorization request.'], 422);
            }

        }
    }