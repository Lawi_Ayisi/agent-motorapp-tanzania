<?php

namespace jick\col;

use Illuminate\Support\ServiceProvider;

class ColServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {}

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //Load controllers
       $this->app->make('Jick\col\controllers\PolicyController');
       $this->app->make('Jick\col\controllers\EntityController');
       $this->app->make('Jick\col\controllers\PreAuthController');
       $this->app->make('Jick\col\controllers\ContentController');

        //Load models
        $this->app->make('Jick\col\models\ColEntity');
        $this->app->make('Jick\col\models\ColPolicy');
        $this->app->make('Jick\col\models\ColUserDet');
        $this->app->make('Jick\col\models\Appointment');
        $this->app->make('Jick\col\models\ServiceProvider');
        $this->app->make('Jick\col\models\ColMessage');

        $this->app->bindIf('soapmanager', function() {
            return new \Jick\col\SoapManager;
        });

        include __DIR__.'/routes.php';
    }
}
