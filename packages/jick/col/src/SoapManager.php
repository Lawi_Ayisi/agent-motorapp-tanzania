<?php

namespace jick\col;

use SoapClient;

class SoapManager {

    protected $wsdl;

    protected $action;

    protected $client;

    function __construct($params = array())
    {
        $this->initialize($params);
    }

    private function initialize($params)
    {
        $this->setWsdl($params['wsdl']);
        $this->setAction($params['action']);

        $options = isset($params['options']) ? $params['options'] : array();

        if(!isset($options['connection_timeout'])) {
            $options['connection_timeout'] = 25;
        }

        if(!isset($options['location'])) {
            $options['location'] = $this->wsdl;
        }

        if(!isset($options['uri'])) {
            $options['uri'] = 'http://tempuri.org/';
        }

        if(!isset($options['trace'])) {
            $options['trace'] = true;
        }

        if(!isset($options['cache_wsdl'])) {
            $options['cache_wsdl'] = WSDL_CACHE_NONE;
        }

        if(!isset($options['soap_version'])) {
            $options['soap_version'] = SOAP_1_2;
        }

        $this->client = new SoapClient($this->wsdl, $options);
    }

    public function request($xml)
    {
        try {

            $response = $this->client->__doRequest($xml, $this->wsdl, $this->action, SOAP_1_2);

            $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $response);
            //dd($response);
            $xml = new \SimpleXMLElement($response);

            //$xml = simplexml_load_string($response);
            //dd($xml);

            $body = $xml->xpath('//sBody')[0];

            $array = json_decode(json_encode((array)$body), TRUE);

            return $array;
        }catch(SoapFault $fault) {
            var_dump($fault);
        }catch(Exception $e) {
            dd($e->getMessage());
        }
    }

    public function setAction($action)
    {
        $this->action = $action;
    }

    public function setWsdl($wsdl)
    {
        $this->wsdl = $wsdl;
    }
}