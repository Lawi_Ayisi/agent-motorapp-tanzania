<?php

namespace jick\roles;

use Illuminate\Support\ServiceProvider;

class RolesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'roles');

       /* $this->publishes([
            __DIR__.'/views' => base_path('resources/views/jick/users'),
        ]);*/
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //Load controllers
        $this->app->make('Jick\roles\controllers\RolesController');

        //Load models
        $this->app->make('Jick\roles\models\Role');

        include __DIR__.'/routes.php';
    }
}
