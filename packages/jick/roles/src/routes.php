<?php

Route::group(['prefix' => 'api/', 'middleware' => 'jwt.auth'], function() {
	Route::resource('roles', 'jick\roles\controllers\RolesController');
});