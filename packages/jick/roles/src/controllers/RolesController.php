<?php

namespace Jick\roles\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Jick\roles\models\Role;
use Validator;

class RolesController extends Controller
{
    function index()
    {
        return Role::all();
    }

    function show($id)
    {
        return Role::find($id);
    }

    function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:permissions'
        ]);

        if($validator->fails()) {
            return response()->json([
                'message' => 'Sorry, an error occured preventing saving',
                'status' => 0,
                'errors' => $validator->errors()->all()
            ], 422);
        }

        $role = Role::firstOrNew(['name'=> $request->name]);

        if($role->save())
        {
            return response()->json([
                'message' => 'Role saved successfully'
            ]);
        }
        //return response()->json($setting->save());
    }

    function update($id, Request $request)
    {
        $role = Role::find($id);

        if($role->exists())
        {
            $status = Role::where('id', $id)->update(['name' => $request->name]);
            return response()->json(['message' => 'Role updated successfully']);
        }

        return response()->json(['message' => 'Role doesnt exist'], 400);
    }

    // function update(Request $request)
    // {

    //     $validator = Validator::make($request->all(), [
    //         'id' => 'required',
    //         'name' => 'required',
    //         'value' => 'required',
    //         'module' => 'required'
    //     ]);

    //     if($validator->fails()) {
    //         return response()->json([
    //             'message' => 'Sorry, an error occured preventing updating',
    //             'status' => 0,
    //             'errors' => $validator->errors()->all()
    //         ], 422);
    //     }

    //      $result = Setting::where('id', $request->id)
    //                 ->update(['name' => $request->name,
    //                     'value' => $request->value,
    //                     'module' => $request->module
    //                     ]);
    //     if($result)
    //     {
    //         return response()->json([
    //             'message' => 'Settings successfully updated'
    //         ]);
    //     }
    //     else
    //     {
    //          return response()->json([
    //             'message' => 'Settings not updated'
    //         ]);
    //     }
    // }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        $role->delete();
        return response()->json(['message' => 'Role successfully deleted']);
    }

}